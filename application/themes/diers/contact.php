<?php include 'includes/top.php'?>
<header id="header-sub">
	<div class="container"> 
		<div class="row headerText">
			<div class="col-md-12 ">
				<h1>Kontakt os</h1>
			</div>
			
			<div class="col-md-12"><?php
			$a = new Area('Header tekst');
			$a->display($c);
			?></div>												
		</div>
	</div>
	<div class="clear"></div>
</header>

<div class="img-cut">
	
</div>
<div class="clear"></div>
</div>	

<div class="container whiteBG mtNeg140">
	<div class="row">
		<div class="col-sm-8 ">
			<div class="row">
			<div class="col-sm-6 contactBox1">
			<?php 
				$a = new Area('Box 1 inhold');
				$a->display($c);
			?>
			</div>
			<div class="col-sm-6 contactBox2">
			<?php 
				$a = new Area('Box 2 inhold');
				$a->display($c);
			?>
			</div>
			</div>
			<div class="contactText">
			<?php 
				$a = new Area('Kontakt tekst');
				$a->display($c);
			?>
			</div>
		</div>
		<div class="col-sm-4 contactForm-container">
		<h2>Kontakt os</h2>
		<?php 
		$a = new Area('Kontakt formular');
		$a->display($c);
	?>
		</div>
	</div>
</div>
<div class="gmapConainer">
	<?php 
		$a = new Area('Google maps');
		$a->display($c);
	?>
</div>
<?php include 'includes/bottom.php'?>