<?php include 'includes/top.php'?>
<header id="header-sub">
	<div class="container"> 
		<div class="row headerText">
			<div class="col-md-12 "><?php 
				$a = new GlobalArea('Header title');
				$a->display();
			?></div>
			
			<div class="col-md-12"><?php
				$a = new Area('Header tekst');
				$a->display($c);
			?></div>												
		</div>
	</div>
	<div class="clear"></div>
</header>

	<div class="img-cut">
		
	</div>
	<div class="clear"></div>
</div>	

<div class="container img-cutbg">
	&nbsp;
</div>

<div class="container pb70 whiteBG">
	<div class="row mt70">
		<div class="col-md-3 sidebar">
			<?php 
				// Set your level here
				// 0 = the first level after home page level (home => CHILD)
				// 1 = the second level (home => child => CHILD)
				// etc.
				$level = 0;
				
				$current = $p = Page::getCurrentPage();
				$tree = array();
				while ($p->getCollectionParentID() >= HOME_CID) {
					array_push($tree, $p);
					$p  = Page::getByID($p->getCollectionParentID());
				}
				$tree = array_reverse($tree);
				if (isset($tree[$level])) {
					$parent = $tree[$level];
					echo '<h2>' .  $parent->getCollectionName() .  '</h2>';
				}
				?>
				<div class="line">&nbsp;</div>
				<?php
				$a = new GlobalArea('Sidebar productgroup');
				$a->display();
			?>
			
		</div>
		<div class="col-md-9">
			<?php $a = new Area('Content');
			$a->display($c); ?>
		</div>
	</div>
</div>
<?php include 'includes/bottom.php';?>	