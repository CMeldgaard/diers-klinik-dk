	<div class="footer">
	<div class="container">
			<div class="col-md-4">
			<h4>Diers klinik</h4>
			<div class="line">&nbsp;</div>
			<?php
			$a = new GlobalArea('Footer kolonne et');
			$a->display();
			?>
			</div>
			<div class="col-md-4">
			<h4>Sikkerhed</h4>
			<div class="line">&nbsp;</div>
			<?php
			$a = new GlobalArea('Footer kolonne to');
			$a->display();
			?>
			</div>
			<div class="col-md-4">
			<h4>Følg os her</h4>
			<div class="line">&nbsp;</div>
			<a href="https://www.facebook.com/pages/Diers-Klinik/446931555357904?ref=ts&fref=ts" target="_blank" rel="nofollow"><img src="<?php echo $this->getThemePath(); ?>/images/facebook.png" style="max-height: 65px;" /></a><br>
			<a href="https://plus.google.com/+DiersKlinikAarhus/posts" target="_blank" rel="nofollow"><img src="<?php echo $this->getThemePath(); ?>/images/googleplus.png" style="max-height: 65px; margin-top: 15px;" /></a><br>
			</div>
			<div class="col-md-12 hruler">
			</div>
			<div class="col-md-12">
			<p>© Copyright - <span class="cWhite">Diers Klinik</span>. Design & Udvikling af <a href="http://www.geekmedia.dk" rel="nofollow">Geek Media</a></p>
			</div>
	</div>
	</div>
</div>
</div>
</div>
<?php
	$cp = new Permissions($c);
	$headerBar = is_object($cp) && ($cp->canWrite() || $cp->canAddSubContent() || $cp->canAdminPage() || $cp->canApproveCollection());
	if (!$headerBar){?>
<script src="concrete/js/jquery.js"></script>
<?php
}
?>
<?php Loader::element('footer_required'); ?>

<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="<?php echo $this->getThemePath(); ?>/js/mfp.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/owl.carousel.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/nicescroll.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/jquery.flexnav.min.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/MetroJs.min.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/functions.js"></script>
<!--Start Cookie Script--> 
<script type="text/javascript" charset="UTF-8" src="http://chs02.cookie-script.com/s/45675132846f1cea32c7aeaf012e3a84.js"></script> 
<!--End Cookie Script-->
</body>	
</html>