<?php
$dir = "application/themes/diers/images/banner";
$images = scandir($dir);
$i = rand(2, sizeof($images)-1);
$style = "background:url(/".$dir."/".$images[$i]."); background-size: cover; background-position: 50%;";
?>

<div id="header-title" style="<?php echo $style;?>; overflow: hidden;">
	<div class="mt-menu-frame">
			<div class="container">
				<div class="row">
					<div class="whiteBG topRow" style="float: right;">
					<div class="langSelector hidden-xs">
						<a href="http://www.diersklinik.de" target="_blank"><img src="<?php echo $this->getThemePath(); ?>/images/de.jpg" /></a>
						<img src="<?php echo $this->getThemePath(); ?>/images/dk.jpg" />
					</div>
						<i class="fa fa-phone"></i>  Ring til os +45 2022 8587 &nbsp;&nbsp;&nbsp; 
						<i class="fa fa-envelope-o"></i>  <a href="/diers/kontakt">Kontakt os</a>
						<ul class="social-icons"> 
							<li><a href="https://www.facebook.com/pages/Diers-Klinik/446931555357904?ref=ts&fref=ts" target="_blank" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://plus.google.com/+DiersKlinikAarhus/posts" target="_blank" rel="nofollow"><i class="fa fa-google-plus"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="row">
				<div class="whiteBG subShadow">
					<div class="mt-menu">
						<div class="col-xs-12 col-sm-12 col-md-2 nopadding">
							<a class="logo" style="margin-top:0px; margin-bottom:0px" href="/">
								<img src="<?php echo $this->getThemePath(); ?>/images/logo.png">
							</a>
							<div class="langSelector hidden-sm hidden-md hidden-lg">
							<a href=""><img src="<?php echo $this->getThemePath(); ?>/images/en.jpg" /></a>
						<a href=""><img src="<?php echo $this->getThemePath(); ?>/images/fr.png" /></a>
						<a href=""><img src="<?php echo $this->getThemePath(); ?>/images/de.jpg" /></a>
						<a href=""><img src="<?php echo $this->getThemePath(); ?>/images/no.png" /></a>
						<a href=""><img src="<?php echo $this->getThemePath(); ?>/images/se.png" /></a>
						<img src="<?php echo $this->getThemePath(); ?>/images/dk.jpg" />
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-10 nopadding">
						<div class="flexnav-menu-button" id="flexnav-menu-button">Menu<span class="touch-button"></span><span class="touch-button"></span></div>
						<div id="nav">
						
							<?php
							$a = new GlobalArea('navigation');
							$a->display();
							?>
						</div>
						</div>
					</div>
					<div class="clear"></div>	
					</div>
				</div>
			</div>
		</div>