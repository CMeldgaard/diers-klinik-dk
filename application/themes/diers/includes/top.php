<!DOCTYPE html>
<html lang="da">
	<head>
		<?php Loader::element('header_required'); ?>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Stylesheets -->
		<link href="<?php echo $this->getThemePath(); ?>/css/reset.css" rel="stylesheet" />
		<link href="<?php echo $this->getThemePath(); ?>/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?php echo $this->getThemePath(); ?>/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo $this->getThemePath(); ?>/css/magnific-popup.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo $this->getThemePath(); ?>/css/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo $this->getThemePath(); ?>/css/owl.theme.css">
		<link href="<?php echo $this->getThemePath(); ?>/css/flexnav.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $this->getThemePath(); ?>/css/style.css" rel="stylesheet" />
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<!--Fonts-->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	</head>
		<body>
		<div class="<?php echo $c->getPageWrapperClass()?>">
		<?php include 'nav.php' ?>
		