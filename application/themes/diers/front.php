<?php include 'includes/top.php'?>
<header id="header">
	<div class="container">
		<div class="row">
			<div class="col-md-4 box-1">
				<div class="box-color"></div>
				<div class="box-info">
					<?php 
						$a = new Area('Box 1 inhold');
						$a->display($c);
					?>
					
				</div>
			</div>
			<div class="col-md-4 box-2">
				<div class="box-color"></div>
				<div class="box-info">
					<?php 
						$a = new Area('Box 2 inhold');
						$a->display($c);
					?>
				</div>
			</div>
			<div class="col-md-4 box-3">
				<div class="box-color"></div>
				<div class="box-info">
					<?php 
						$a = new Area('Box 3 inhold');
						$a->display($c);
					?>
				</div>
			</div>
			
			<div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
</header>

<div class="img-cut">
	
</div>
<div class="clear"></div>
</div>	

<div class="container img-cutbg">
	&nbsp;
</div>

<div class="container pb70 whiteBG">
	<div class="row">
		<div class="col-sm-12 mt70">
					<?php
						$a = new Area('Front titel');
						$a->display($c);
					?>
		</div>
		<div class="col-sm-2 col-sm-offset-1 frontImg">
			<?php
				$a = new Area('Liza diers billede');
				$a->display($c);
			?>
		</div>
		<div class="col-sm-8">
			<?php 
				$a = new Area('Front welcome message');
				$a->display($c);
			?>
			
		</div>
		<br style="clear: both">
		<div class="col-md-12 mt70 pathBox">
			<div class="path-thumb one">
				<figure>
					<span class="overlay"><a href="insemination/forberedelsen"><i class="fa fa-eye"></i></a></span>
					<img src="<?php echo $this->getThemePath(); ?>/images/1.jpg" width="154" class="img-circle img-thumbnail img-responsive" alt="" title="">
					<figcaption>FORBEREDELSEN</figcaption>
				</figure>
			</div>
			<div class="path-thumb two">
				<figure>
					<span class="overlay"><a href="insemination/journalsamtalen"><i class="fa fa-eye"></i></a></span>
					<img src="<?php echo $this->getThemePath(); ?>/images/2.jpg" width="195" class="img-circle img-thumbnail img-responsive" alt="" title="">
					<figcaption>JOURNALSAMTALEN</figcaption>
				</figure>
			</div>
			<div class="path-thumb three">
				<figure>
					<span class="overlay"><a href="insemination"><i class="fa fa-eye"></i></a></span>
					<img src="<?php echo $this->getThemePath(); ?>/images/3.jpg" width="125" class="img-circle img-thumbnail img-responsive" alt="" title="">
					<figcaption>INSEMINATION</figcaption>
				</figure>
			</div>	
			<div class="path-thumb four">
				<figure>
					<span class="overlay"><a href="ultralydsscaning"><i class="fa fa-eye"></i></a></span>
					<img src="<?php echo $this->getThemePath(); ?>/images/4.jpg" width="220" class="img-circle img-thumbnail img-responsive" alt="" title="">
					<figcaption>TIDEN EFTER</figcaption>
				</figure>
			</div>
		</div>
		<div class="col-md-8 col-md-offset-2">
			<?php
				$a = new Area('Overskrift anmeldelser');
				$a->display($c);
			?>
			<div class="metroholder" data-mode="carousel" data-direction="horizontal" data-delay="10000" data-pause-onhover="true" >
			<div class="testimonialContainer">
				<div class="testimonial">
					<div>
						<p>
							Hej med jer ;-) <br />
							Får en lille glad tåre i øjet, når jeg tænker på hvad I har hjulpet mig med. Har en meget glad dreng på snart 16 måneder. Tusind tak.
						Knus til jer ;-)</p>
					</div>
					<div class="author">
					Anne Marie	</div>
				</div>
			</div>
			<div class="testimonialContainer">
				<div class="testimonial">
					<div>
						<p>
							 Billederne afspejler den dejlige stemning der altid er hos jer! :-)<br>
Tak for de to dejligste og smukkeste små typer, som I hjalp til verden! :-)<br>
Kæmpe knus til jer</p>
					</div>
					<div class="author">
					Mette	</div>
				</div>
			</div>
			<div class="testimonialContainer">
				<div class="testimonial">
					<div>
						<p>
							Jeg kan ikke andet end være fuldt ud tilfreds og glad for den supergode behandling, jeg har fået på Diers, da jeg som enlig skulle insemineres i april. Det lykkedes i første forsøg, og nu glæder jeg mig til at få en lillebror til min dreng på 5 år til januar.
<br><br>
Jeg vil varmt anbefale klinikken til andre, der har brug for fertilitetsbehandling.</p>
					</div>
					<div class="author">
					Linda</div>
				</div>
			</div>
			<div class="testimonialContainer">
				<div class="testimonial">
					<div>
						<p>
							Tak for utrolig god behandling og rådgivning, da beslutningen om at blive mor til et donorbarn skulle træffes. For fem dage siden blev jeg mor for første gang. Min vidunderlige søn blev undfanget i første forsøg på Diers Klinik. Verdens dejligste dreng :-)</p>
					</div>
					<div class="author">
					&nbsp;</div>
				</div>
			</div>
			<br style="clear: both">
			</div>
		</div>
		<div class="col-md-10 col-md-offset-1 call-to-action mt70">
			<div class="row" style="display: table;">
				<div class="c2a-text">
					<?php
						$a = new Area('Call to action content');
						$a->display($c);
					?>
					
				</div>
				<div class="c2a-btn">
					<a href="kontakt" class="btn btn-success" style="font-size: 20px;">KONTAKT OS I DAG!</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/bottom.php'?>