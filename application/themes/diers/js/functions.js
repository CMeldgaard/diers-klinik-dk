"use strict";

//Document ready functions
$(document).ready(function() {

$(".flexnav").flexNav();

//Nicescroll
 $('html').niceScroll({
        cursorcolor: "#000",
        cursorborder: "0px solid #fff",
        railpadding: {
            top: 0,
            right: 0,
            left: 0,
            bottom: 0
        },
        cursorwidth: "5px",
        cursorborderradius: "0px",
        cursoropacitymin: 0.3,
        cursoropacitymax: 0.7,
        boxzoom: true,
        horizrailenabled: false,
        zindex: 9999
    });	

//Sætter højden på testimonials containeren på forsiden
var maxHeight = 0;
$(".testimonialContainer").each(function(){
   if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
});

$(".metroholder").height(maxHeight);
	
//Livetile
// apply regular slide universally unless .exclude class is applied 
// NOTE: The default options for each liveTile are being pulled from the 'data-' attributes

    $(".metroholder").not(".exclude").liveTile();
	
});