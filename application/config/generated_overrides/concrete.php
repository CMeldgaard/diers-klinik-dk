<?php

/**
 * -----------------------------------------------------------------------------
 * Generated 2015-08-30T17:31:04+02:00
 *
 * @item      misc.do_page_reindex_check
 * @group     concrete
 * @namespace null
 * -----------------------------------------------------------------------------
 */
return array(
    'locale' => 'da_DK',
    'site' => 'Diers Klinik',
    'version_installed' => '5.7.4.1',
    'misc' => array(
        'access_entity_updated' => 1431041689,
        'latest_version' => '5.7.5.1',
        'seen_introduction' => true,
        'do_page_reindex_check' => false
    ),
    'seo' => array(
        'url_rewriting' => 1,
        'tracking' => array(
            'code' => '<script>
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-63181932-1\', \'auto\');
  ga(\'send\', \'pageview\');

</script>',
            'code_position' => 'bottom'
        )
    ),
    'cache' => array(
        'blocks' => false,
        'assets' => false,
        'theme_css' => false,
        'overrides' => false,
        'pages' => '0',
        'full_page_lifetime' => 'default',
        'full_page_lifetime_value' => null
    ),
    'theme' => array(
        'compress_preprocessor_output' => false
    )
);
