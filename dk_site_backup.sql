-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Host: 10.246.16.234:3306
-- Generation Time: May 27, 2015 at 12:32 PM
-- Server version: 5.5.43-MariaDB-1~wheezy
-- PHP Version: 5.3.3-7+squeeze15

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `diersklinik_dk_db`
--
USE `diersklinik_dk_db`;

-- --------------------------------------------------------

--
-- Table structure for table `AreaLayoutColumns`
--

CREATE TABLE IF NOT EXISTS `AreaLayoutColumns` (
  `arLayoutColumnID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `arLayoutID` int(10) unsigned NOT NULL DEFAULT '0',
  `arLayoutColumnIndex` int(10) unsigned NOT NULL DEFAULT '0',
  `arID` int(10) unsigned NOT NULL DEFAULT '0',
  `arLayoutColumnDisplayID` int(11) DEFAULT '0',
  PRIMARY KEY (`arLayoutColumnID`),
  KEY `arLayoutID` (`arLayoutID`,`arLayoutColumnIndex`),
  KEY `arID` (`arID`),
  KEY `arLayoutColumnDisplayID` (`arLayoutColumnDisplayID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `AreaLayoutCustomColumns`
--

CREATE TABLE IF NOT EXISTS `AreaLayoutCustomColumns` (
  `arLayoutColumnID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `arLayoutColumnWidth` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`arLayoutColumnID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `AreaLayoutPresets`
--

CREATE TABLE IF NOT EXISTS `AreaLayoutPresets` (
  `arLayoutPresetID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `arLayoutID` int(10) unsigned NOT NULL DEFAULT '0',
  `arLayoutPresetName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`arLayoutPresetID`),
  KEY `arLayoutID` (`arLayoutID`),
  KEY `arLayoutPresetName` (`arLayoutPresetName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `AreaLayoutThemeGridColumns`
--

CREATE TABLE IF NOT EXISTS `AreaLayoutThemeGridColumns` (
  `arLayoutColumnID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `arLayoutColumnSpan` int(10) unsigned DEFAULT '0',
  `arLayoutColumnOffset` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`arLayoutColumnID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `AreaLayouts`
--

CREATE TABLE IF NOT EXISTS `AreaLayouts` (
  `arLayoutID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `arLayoutSpacing` int(10) unsigned NOT NULL DEFAULT '0',
  `arLayoutIsCustom` tinyint(1) NOT NULL DEFAULT '0',
  `arLayoutMaxColumns` int(10) unsigned NOT NULL DEFAULT '0',
  `arLayoutUsesThemeGridFramework` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`arLayoutID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `AreaPermissionAssignments`
--

CREATE TABLE IF NOT EXISTS `AreaPermissionAssignments` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkID` int(10) unsigned NOT NULL DEFAULT '0',
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`arHandle`,`pkID`,`paID`),
  KEY `paID` (`paID`),
  KEY `pkID` (`pkID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `AreaPermissionBlockTypeAccessList`
--

CREATE TABLE IF NOT EXISTS `AreaPermissionBlockTypeAccessList` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`paID`,`peID`),
  KEY `peID` (`peID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `AreaPermissionBlockTypeAccessListCustom`
--

CREATE TABLE IF NOT EXISTS `AreaPermissionBlockTypeAccessListCustom` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `btID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`paID`,`peID`,`btID`),
  KEY `peID` (`peID`),
  KEY `btID` (`btID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Areas`
--

CREATE TABLE IF NOT EXISTS `Areas` (
  `arID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arOverrideCollectionPermissions` tinyint(1) NOT NULL DEFAULT '0',
  `arInheritPermissionsFromAreaOnCID` int(10) unsigned NOT NULL DEFAULT '0',
  `arIsGlobal` tinyint(1) NOT NULL DEFAULT '0',
  `arParentID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`arID`),
  KEY `arIsGlobal` (`arIsGlobal`),
  KEY `cID` (`cID`),
  KEY `arHandle` (`arHandle`),
  KEY `arParentID` (`arParentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=418 ;

--
-- Dumping data for table `Areas`
--

INSERT INTO `Areas` (`arID`, `cID`, `arHandle`, `arOverrideCollectionPermissions`, `arInheritPermissionsFromAreaOnCID`, `arIsGlobal`, `arParentID`) VALUES
(1, 124, 'Main', 0, 0, 0, 0),
(2, 125, 'Primary', 0, 0, 0, 0),
(3, 125, 'Secondary 1', 0, 0, 0, 0),
(4, 125, 'Secondary 2', 0, 0, 0, 0),
(5, 125, 'Secondary 3', 0, 0, 0, 0),
(6, 125, 'Secondary 4', 0, 0, 0, 0),
(7, 125, 'Secondary 5', 0, 0, 0, 0),
(8, 142, 'Main', 0, 0, 0, 0),
(9, 143, 'Main', 0, 0, 0, 0),
(10, 1, 'Header Site Title', 0, 0, 1, 0),
(11, 144, 'Main', 0, 0, 0, 0),
(12, 1, 'Header Navigation', 0, 0, 1, 0),
(13, 1, 'Main', 0, 0, 0, 0),
(14, 1, 'Page Footer', 0, 0, 0, 0),
(15, 145, 'Main', 0, 0, 0, 0),
(16, 1, 'Footer Legal', 0, 0, 1, 0),
(17, 146, 'Main', 0, 0, 0, 0),
(18, 1, 'Footer Navigation', 0, 0, 1, 0),
(19, 147, 'Main', 0, 0, 0, 0),
(20, 1, 'Footer Contact', 0, 0, 1, 0),
(21, 148, 'Main', 0, 0, 0, 0),
(22, 148, 'Header Site Title', 0, 0, 1, 0),
(23, 148, 'Header Navigation', 0, 0, 1, 0),
(24, 149, 'Main', 0, 0, 0, 0),
(25, 148, 'Header Search', 0, 0, 1, 0),
(26, 148, 'Page Footer', 0, 0, 0, 0),
(27, 150, 'Main', 0, 0, 0, 0),
(28, 148, 'Footer Site Title', 0, 0, 1, 0),
(29, 151, 'Main', 0, 0, 0, 0),
(30, 148, 'Footer Social', 0, 0, 1, 0),
(31, 148, 'Footer Legal', 0, 0, 1, 0),
(32, 148, 'Footer Navigation', 0, 0, 1, 0),
(33, 148, 'Footer Contact', 0, 0, 1, 0),
(34, 152, 'Main', 0, 0, 0, 0),
(35, 152, 'Header Site Title', 0, 0, 1, 0),
(36, 152, 'Header Navigation', 0, 0, 1, 0),
(37, 152, 'Header Search', 0, 0, 1, 0),
(38, 152, 'Page Footer', 0, 0, 0, 0),
(39, 152, 'Footer Site Title', 0, 0, 1, 0),
(40, 152, 'Footer Social', 0, 0, 1, 0),
(41, 152, 'Footer Legal', 0, 0, 1, 0),
(42, 152, 'Footer Navigation', 0, 0, 1, 0),
(43, 152, 'Footer Contact', 0, 0, 1, 0),
(44, 153, 'Main', 0, 0, 0, 0),
(45, 153, 'Header Site Title', 0, 0, 1, 0),
(46, 153, 'Header Navigation', 0, 0, 1, 0),
(47, 153, 'Header Search', 0, 0, 1, 0),
(48, 153, 'Page Footer', 0, 0, 0, 0),
(49, 153, 'Footer Site Title', 0, 0, 1, 0),
(50, 153, 'Footer Social', 0, 0, 1, 0),
(51, 153, 'Footer Legal', 0, 0, 1, 0),
(52, 153, 'Footer Navigation', 0, 0, 1, 0),
(53, 153, 'Footer Contact', 0, 0, 1, 0),
(54, 139, 'Header Site Title', 0, 0, 1, 0),
(55, 139, 'Header Navigation', 0, 0, 1, 0),
(56, 139, 'Footer Legal', 0, 0, 1, 0),
(57, 139, 'Footer Navigation', 0, 0, 1, 0),
(58, 139, 'Footer Contact', 0, 0, 1, 0),
(59, 1, 'Headline', 0, 0, 0, 0),
(60, 1, 'Content', 0, 0, 0, 0),
(61, 154, 'Main', 0, 0, 0, 0),
(62, 1, 'FooterInfoText', 0, 0, 1, 0),
(63, 155, 'Main', 0, 0, 0, 0),
(64, 1, 'Twitter Feed', 0, 0, 1, 0),
(65, 139, 'Main', 0, 0, 0, 0),
(66, 1, 'Box 1 inhold', 0, 0, 0, 0),
(67, 1, 'Box 2 inhold', 0, 0, 0, 0),
(68, 1, 'Box 3 inhold', 0, 0, 0, 0),
(69, 1, 'Front welcome message', 0, 0, 0, 0),
(70, 1, 'Call to action content', 0, 0, 0, 0),
(71, 1, 'Footer kolonne 1', 0, 0, 0, 0),
(72, 1, 'Footer kolonne 2', 0, 0, 0, 0),
(73, 156, 'Main', 0, 0, 0, 0),
(74, 1, 'Footer kolonne to', 0, 0, 1, 0),
(75, 157, 'Main', 0, 0, 0, 0),
(76, 1, 'Footer kolonne et', 0, 0, 1, 0),
(77, 158, 'Main', 0, 0, 0, 0),
(78, 1, 'navigation', 0, 0, 1, 0),
(79, 159, 'Main', 0, 0, 0, 0),
(80, 159, 'navigation', 0, 0, 1, 0),
(81, 159, 'Box 1 inhold', 0, 0, 0, 0),
(82, 159, 'Box 2 inhold', 0, 0, 0, 0),
(83, 159, 'Box 3 inhold', 0, 0, 0, 0),
(84, 159, 'Front welcome message', 0, 0, 0, 0),
(85, 159, 'Call to action content', 0, 0, 0, 0),
(86, 159, 'Footer kolonne et', 0, 0, 1, 0),
(87, 159, 'Footer kolonne to', 0, 0, 1, 0),
(88, 161, 'Main', 0, 0, 0, 0),
(89, 161, 'navigation', 0, 0, 1, 0),
(90, 161, 'Box 1 inhold', 0, 0, 0, 0),
(91, 161, 'Box 2 inhold', 0, 0, 0, 0),
(92, 161, 'Box 3 inhold', 0, 0, 0, 0),
(93, 161, 'Front welcome message', 0, 0, 0, 0),
(94, 161, 'Call to action content', 0, 0, 0, 0),
(95, 161, 'Footer kolonne et', 0, 0, 1, 0),
(96, 161, 'Footer kolonne to', 0, 0, 1, 0),
(97, 162, 'Main', 0, 0, 0, 0),
(98, 162, 'navigation', 0, 0, 1, 0),
(99, 162, 'Box 1 inhold', 0, 0, 0, 0),
(100, 162, 'Box 2 inhold', 0, 0, 0, 0),
(101, 162, 'Box 3 inhold', 0, 0, 0, 0),
(102, 162, 'Front welcome message', 0, 0, 0, 0),
(103, 162, 'Call to action content', 0, 0, 0, 0),
(104, 162, 'Footer kolonne et', 0, 0, 1, 0),
(105, 162, 'Footer kolonne to', 0, 0, 1, 0),
(106, 163, 'Main', 0, 0, 0, 0),
(107, 163, 'navigation', 0, 0, 1, 0),
(108, 163, 'Box 1 inhold', 0, 0, 0, 0),
(109, 163, 'Box 2 inhold', 0, 0, 0, 0),
(110, 163, 'Box 3 inhold', 0, 0, 0, 0),
(111, 163, 'Front welcome message', 0, 0, 0, 0),
(112, 163, 'Call to action content', 0, 0, 0, 0),
(113, 163, 'Footer kolonne et', 0, 0, 1, 0),
(114, 163, 'Footer kolonne to', 0, 0, 1, 0),
(115, 164, 'Main', 0, 0, 0, 0),
(116, 164, 'navigation', 0, 0, 1, 0),
(117, 164, 'Box 1 inhold', 0, 0, 0, 0),
(118, 164, 'Box 2 inhold', 0, 0, 0, 0),
(119, 164, 'Box 3 inhold', 0, 0, 0, 0),
(120, 164, 'Front welcome message', 0, 0, 0, 0),
(121, 164, 'Call to action content', 0, 0, 0, 0),
(122, 164, 'Footer kolonne et', 0, 0, 1, 0),
(123, 164, 'Footer kolonne to', 0, 0, 1, 0),
(124, 165, 'Main', 0, 0, 0, 0),
(125, 165, 'navigation', 0, 0, 1, 0),
(126, 165, 'Box 1 inhold', 0, 0, 0, 0),
(127, 165, 'Box 2 inhold', 0, 0, 0, 0),
(128, 165, 'Box 3 inhold', 0, 0, 0, 0),
(129, 165, 'Front welcome message', 0, 0, 0, 0),
(130, 165, 'Call to action content', 0, 0, 0, 0),
(131, 165, 'Footer kolonne et', 0, 0, 1, 0),
(132, 165, 'Footer kolonne to', 0, 0, 1, 0),
(133, 166, 'Main', 0, 0, 0, 0),
(134, 166, 'navigation', 0, 0, 1, 0),
(135, 166, 'Box 1 inhold', 0, 0, 0, 0),
(136, 166, 'Box 2 inhold', 0, 0, 0, 0),
(137, 166, 'Box 3 inhold', 0, 0, 0, 0),
(138, 166, 'Front welcome message', 0, 0, 0, 0),
(139, 166, 'Call to action content', 0, 0, 0, 0),
(140, 166, 'Footer kolonne et', 0, 0, 1, 0),
(141, 166, 'Footer kolonne to', 0, 0, 1, 0),
(142, 167, 'Main', 0, 0, 0, 0),
(143, 167, 'navigation', 0, 0, 1, 0),
(144, 167, 'Box 1 inhold', 0, 0, 0, 0),
(145, 167, 'Box 2 inhold', 0, 0, 0, 0),
(146, 167, 'Box 3 inhold', 0, 0, 0, 0),
(147, 167, 'Front welcome message', 0, 0, 0, 0),
(148, 167, 'Call to action content', 0, 0, 0, 0),
(149, 167, 'Footer kolonne et', 0, 0, 1, 0),
(150, 167, 'Footer kolonne to', 0, 0, 1, 0),
(151, 148, 'navigation', 0, 0, 1, 0),
(152, 148, 'Box 1 inhold', 0, 0, 0, 0),
(153, 148, 'Box 2 inhold', 0, 0, 0, 0),
(154, 148, 'Box 3 inhold', 0, 0, 0, 0),
(155, 148, 'Front welcome message', 0, 0, 0, 0),
(156, 148, 'Call to action content', 0, 0, 0, 0),
(157, 148, 'Footer kolonne et', 0, 0, 1, 0),
(158, 148, 'Footer kolonne to', 0, 0, 1, 0),
(159, 167, 'Breadcrumbs', 0, 0, 0, 0),
(160, 167, 'Header tekst', 0, 0, 0, 0),
(161, 167, 'Google maps', 0, 0, 0, 0),
(162, 167, 'Kontakt formular', 0, 0, 0, 0),
(163, 167, 'Kontakt tekst', 0, 0, 0, 0),
(164, 169, 'Main', 0, 0, 0, 0),
(165, 169, 'navigation', 0, 0, 1, 0),
(166, 169, 'Box 1 inhold', 0, 0, 0, 0),
(167, 169, 'Box 2 inhold', 0, 0, 0, 0),
(168, 169, 'Box 3 inhold', 0, 0, 0, 0),
(169, 169, 'Front welcome message', 0, 0, 0, 0),
(170, 169, 'Call to action content', 0, 0, 0, 0),
(171, 169, 'Footer kolonne et', 0, 0, 1, 0),
(172, 169, 'Footer kolonne to', 0, 0, 1, 0),
(173, 170, 'Main', 0, 0, 0, 0),
(174, 170, 'navigation', 0, 0, 1, 0),
(175, 170, 'Box 1 inhold', 0, 0, 0, 0),
(176, 170, 'Box 2 inhold', 0, 0, 0, 0),
(177, 170, 'Box 3 inhold', 0, 0, 0, 0),
(178, 170, 'Front welcome message', 0, 0, 0, 0),
(179, 170, 'Call to action content', 0, 0, 0, 0),
(180, 170, 'Footer kolonne et', 0, 0, 1, 0),
(181, 170, 'Footer kolonne to', 0, 0, 1, 0),
(182, 171, 'Main', 0, 0, 0, 0),
(183, 171, 'navigation', 0, 0, 1, 0),
(184, 171, 'Box 1 inhold', 0, 0, 0, 0),
(185, 171, 'Box 2 inhold', 0, 0, 0, 0),
(186, 171, 'Box 3 inhold', 0, 0, 0, 0),
(187, 171, 'Front welcome message', 0, 0, 0, 0),
(188, 171, 'Call to action content', 0, 0, 0, 0),
(189, 171, 'Footer kolonne et', 0, 0, 1, 0),
(190, 171, 'Footer kolonne to', 0, 0, 1, 0),
(191, 172, 'Main', 0, 0, 0, 0),
(192, 171, 'Sidebar productgroup', 0, 0, 1, 0),
(193, 171, 'Headline', 0, 0, 0, 0),
(194, 171, 'Content', 0, 0, 0, 0),
(195, 171, 'Header tekst', 0, 0, 0, 0),
(196, 171, 'Breadcrumbs', 0, 0, 0, 0),
(197, 173, 'Main', 0, 0, 0, 0),
(198, 171, 'Breadcrumbs container', 0, 0, 1, 0),
(199, 167, 'Breadcrumbs container', 0, 0, 1, 0),
(200, 174, 'Main', 0, 0, 0, 0),
(201, 171, 'Header title', 0, 0, 1, 0),
(202, 169, 'Header title', 0, 0, 1, 0),
(203, 169, 'Header tekst', 0, 0, 0, 0),
(204, 169, 'Breadcrumbs container', 0, 0, 1, 0),
(205, 169, 'Sidebar productgroup', 0, 0, 1, 0),
(206, 169, 'Content', 0, 0, 0, 0),
(207, 175, 'Main', 0, 0, 0, 0),
(208, 175, 'navigation', 0, 0, 1, 0),
(209, 175, 'Header title', 0, 0, 1, 0),
(210, 175, 'Header tekst', 0, 0, 0, 0),
(211, 175, 'Breadcrumbs container', 0, 0, 1, 0),
(212, 175, 'Sidebar productgroup', 0, 0, 1, 0),
(213, 175, 'Content', 0, 0, 0, 0),
(214, 175, 'Footer kolonne et', 0, 0, 1, 0),
(215, 175, 'Footer kolonne to', 0, 0, 1, 0),
(216, 176, 'Main', 0, 0, 0, 0),
(217, 176, 'navigation', 0, 0, 1, 0),
(218, 176, 'Header title', 0, 0, 1, 0),
(219, 176, 'Header tekst', 0, 0, 0, 0),
(220, 176, 'Breadcrumbs container', 0, 0, 1, 0),
(221, 176, 'Sidebar productgroup', 0, 0, 1, 0),
(222, 176, 'Content', 0, 0, 0, 0),
(223, 176, 'Footer kolonne et', 0, 0, 1, 0),
(224, 176, 'Footer kolonne to', 0, 0, 1, 0),
(225, 159, 'Header title', 0, 0, 1, 0),
(226, 159, 'Header tekst', 0, 0, 0, 0),
(227, 159, 'Breadcrumbs container', 0, 0, 1, 0),
(228, 159, 'Sidebar productgroup', 0, 0, 1, 0),
(229, 159, 'Content', 0, 0, 0, 0),
(230, 148, 'Header title', 0, 0, 1, 0),
(231, 148, 'Header tekst', 0, 0, 0, 0),
(232, 148, 'Breadcrumbs container', 0, 0, 1, 0),
(233, 148, 'Sidebar productgroup', 0, 0, 1, 0),
(234, 148, 'Content', 0, 0, 0, 0),
(235, 170, 'Header title', 0, 0, 1, 0),
(236, 170, 'Header tekst', 0, 0, 0, 0),
(237, 170, 'Breadcrumbs container', 0, 0, 1, 0),
(238, 170, 'Sidebar productgroup', 0, 0, 1, 0),
(239, 170, 'Content', 0, 0, 0, 0),
(240, 161, 'Header title', 0, 0, 1, 0),
(241, 161, 'Header tekst', 0, 0, 0, 0),
(242, 161, 'Breadcrumbs container', 0, 0, 1, 0),
(243, 161, 'Sidebar productgroup', 0, 0, 1, 0),
(244, 161, 'Content', 0, 0, 0, 0),
(245, 166, 'Header title', 0, 0, 1, 0),
(246, 166, 'Header tekst', 0, 0, 0, 0),
(247, 166, 'Breadcrumbs container', 0, 0, 1, 0),
(248, 166, 'Sidebar productgroup', 0, 0, 1, 0),
(249, 166, 'Content', 0, 0, 0, 0),
(250, 165, 'Header title', 0, 0, 1, 0),
(251, 165, 'Header tekst', 0, 0, 0, 0),
(252, 165, 'Breadcrumbs container', 0, 0, 1, 0),
(253, 165, 'Sidebar productgroup', 0, 0, 1, 0),
(254, 165, 'Content', 0, 0, 0, 0),
(255, 177, 'Main', 0, 0, 0, 0),
(256, 177, 'navigation', 0, 0, 1, 0),
(257, 177, 'Header title', 0, 0, 1, 0),
(258, 177, 'Header tekst', 0, 0, 0, 0),
(259, 177, 'Breadcrumbs container', 0, 0, 1, 0),
(260, 177, 'Sidebar productgroup', 0, 0, 1, 0),
(261, 177, 'Content', 0, 0, 0, 0),
(262, 177, 'Footer kolonne et', 0, 0, 1, 0),
(263, 177, 'Footer kolonne to', 0, 0, 1, 0),
(264, 178, 'Main', 0, 0, 0, 0),
(265, 178, 'navigation', 0, 0, 1, 0),
(266, 178, 'Header title', 0, 0, 1, 0),
(267, 178, 'Header tekst', 0, 0, 0, 0),
(268, 178, 'Breadcrumbs container', 0, 0, 1, 0),
(269, 178, 'Sidebar productgroup', 0, 0, 1, 0),
(270, 178, 'Content', 0, 0, 0, 0),
(271, 178, 'Footer kolonne et', 0, 0, 1, 0),
(272, 178, 'Footer kolonne to', 0, 0, 1, 0),
(273, 163, 'Header title', 0, 0, 1, 0),
(274, 163, 'Header tekst', 0, 0, 0, 0),
(275, 163, 'Breadcrumbs container', 0, 0, 1, 0),
(276, 163, 'Sidebar productgroup', 0, 0, 1, 0),
(277, 163, 'Content', 0, 0, 0, 0),
(278, 162, 'Header title', 0, 0, 1, 0),
(279, 162, 'Header tekst', 0, 0, 0, 0),
(280, 162, 'Breadcrumbs container', 0, 0, 1, 0),
(281, 162, 'Sidebar productgroup', 0, 0, 1, 0),
(282, 162, 'Content', 0, 0, 0, 0),
(283, 164, 'Header title', 0, 0, 1, 0),
(284, 164, 'Header tekst', 0, 0, 0, 0),
(285, 164, 'Breadcrumbs container', 0, 0, 1, 0),
(286, 164, 'Sidebar productgroup', 0, 0, 1, 0),
(287, 164, 'Content', 0, 0, 0, 0),
(288, 179, 'Main', 0, 0, 0, 0),
(289, 179, 'navigation', 0, 0, 1, 0),
(290, 179, 'Header title', 0, 0, 1, 0),
(291, 179, 'Header tekst', 0, 0, 0, 0),
(292, 179, 'Breadcrumbs container', 0, 0, 1, 0),
(293, 179, 'Sidebar productgroup', 0, 0, 1, 0),
(294, 179, 'Content', 0, 0, 0, 0),
(295, 179, 'Footer kolonne et', 0, 0, 1, 0),
(296, 179, 'Footer kolonne to', 0, 0, 1, 0),
(297, 180, 'Main', 0, 0, 0, 0),
(298, 180, 'navigation', 0, 0, 1, 0),
(299, 180, 'Header title', 0, 0, 1, 0),
(300, 180, 'Header tekst', 0, 0, 0, 0),
(301, 180, 'Breadcrumbs container', 0, 0, 1, 0),
(302, 180, 'Sidebar productgroup', 0, 0, 1, 0),
(303, 180, 'Content', 0, 0, 0, 0),
(304, 180, 'Footer kolonne et', 0, 0, 1, 0),
(305, 180, 'Footer kolonne to', 0, 0, 1, 0),
(306, 181, 'Main', 0, 0, 0, 0),
(307, 181, 'navigation', 0, 0, 1, 0),
(308, 181, 'Header title', 0, 0, 1, 0),
(309, 181, 'Header tekst', 0, 0, 0, 0),
(310, 181, 'Breadcrumbs container', 0, 0, 1, 0),
(311, 181, 'Sidebar productgroup', 0, 0, 1, 0),
(312, 181, 'Content', 0, 0, 0, 0),
(313, 181, 'Footer kolonne et', 0, 0, 1, 0),
(314, 181, 'Footer kolonne to', 0, 0, 1, 0),
(315, 182, 'Main', 0, 0, 0, 0),
(316, 182, 'navigation', 0, 0, 1, 0),
(317, 182, 'Header title', 0, 0, 1, 0),
(318, 182, 'Header tekst', 0, 0, 0, 0),
(319, 182, 'Breadcrumbs container', 0, 0, 1, 0),
(320, 182, 'Sidebar productgroup', 0, 0, 1, 0),
(321, 182, 'Content', 0, 0, 0, 0),
(322, 182, 'Footer kolonne et', 0, 0, 1, 0),
(323, 182, 'Footer kolonne to', 0, 0, 1, 0),
(333, 184, 'Main', 0, 0, 0, 0),
(334, 184, 'navigation', 0, 0, 1, 0),
(335, 184, 'Header title', 0, 0, 1, 0),
(336, 184, 'Header tekst', 0, 0, 0, 0),
(337, 184, 'Breadcrumbs container', 0, 0, 1, 0),
(338, 184, 'Sidebar productgroup', 0, 0, 1, 0),
(339, 184, 'Content', 0, 0, 0, 0),
(340, 184, 'Footer kolonne et', 0, 0, 1, 0),
(341, 184, 'Footer kolonne to', 0, 0, 1, 0),
(342, 185, 'Main', 0, 0, 0, 0),
(343, 185, 'navigation', 0, 0, 1, 0),
(344, 185, 'Header title', 0, 0, 1, 0),
(345, 185, 'Header tekst', 0, 0, 0, 0),
(346, 185, 'Breadcrumbs container', 0, 0, 1, 0),
(347, 185, 'Sidebar productgroup', 0, 0, 1, 0),
(348, 185, 'Content', 0, 0, 0, 0),
(349, 185, 'Footer kolonne et', 0, 0, 1, 0),
(350, 185, 'Footer kolonne to', 0, 0, 1, 0),
(351, 186, 'Main', 0, 0, 0, 0),
(352, 186, 'navigation', 0, 0, 1, 0),
(353, 186, 'Header title', 0, 0, 1, 0),
(354, 186, 'Header tekst', 0, 0, 0, 0),
(355, 186, 'Breadcrumbs container', 0, 0, 1, 0),
(356, 186, 'Sidebar productgroup', 0, 0, 1, 0),
(357, 186, 'Content', 0, 0, 0, 0),
(358, 186, 'Footer kolonne et', 0, 0, 1, 0),
(359, 186, 'Footer kolonne to', 0, 0, 1, 0),
(360, 187, 'Main', 0, 0, 0, 0),
(361, 187, 'navigation', 0, 0, 1, 0),
(362, 187, 'Header title', 0, 0, 1, 0),
(363, 187, 'Header tekst', 0, 0, 0, 0),
(364, 187, 'Breadcrumbs container', 0, 0, 1, 0),
(365, 187, 'Sidebar productgroup', 0, 0, 1, 0),
(366, 187, 'Content', 0, 0, 0, 0),
(367, 187, 'Footer kolonne et', 0, 0, 1, 0),
(368, 187, 'Footer kolonne to', 0, 0, 1, 0),
(369, 188, 'Main', 0, 0, 0, 0),
(370, 188, 'navigation', 0, 0, 1, 0),
(371, 188, 'Header title', 0, 0, 1, 0),
(372, 188, 'Header tekst', 0, 0, 0, 0),
(373, 188, 'Breadcrumbs container', 0, 0, 1, 0),
(374, 188, 'Sidebar productgroup', 0, 0, 1, 0),
(375, 188, 'Content', 0, 0, 0, 0),
(376, 188, 'Footer kolonne et', 0, 0, 1, 0),
(377, 188, 'Footer kolonne to', 0, 0, 1, 0),
(378, 189, 'Main', 0, 0, 0, 0),
(379, 189, 'navigation', 0, 0, 1, 0),
(380, 189, 'Header title', 0, 0, 1, 0),
(381, 189, 'Header tekst', 0, 0, 0, 0),
(382, 189, 'Breadcrumbs container', 0, 0, 1, 0),
(383, 189, 'Sidebar productgroup', 0, 0, 1, 0),
(384, 189, 'Content', 0, 0, 0, 0),
(385, 189, 'Footer kolonne et', 0, 0, 1, 0),
(386, 189, 'Footer kolonne to', 0, 0, 1, 0),
(387, 1, 'Liza diers billede', 0, 0, 0, 0),
(388, 1, 'Liza diers tekst', 0, 0, 0, 0),
(389, 1, 'Overskrift anmeldelser', 0, 0, 0, 0),
(390, 167, 'Header title', 0, 0, 1, 0),
(391, 167, 'Sidebar productgroup', 0, 0, 1, 0),
(392, 167, 'Content', 0, 0, 0, 0),
(393, 1, 'Front titel', 0, 0, 0, 0),
(394, 191, 'Main', 0, 0, 0, 0),
(395, 191, 'navigation', 0, 0, 1, 0),
(396, 191, 'Header title', 0, 0, 1, 0),
(397, 191, 'Header tekst', 0, 0, 0, 0),
(398, 191, 'Sidebar productgroup', 0, 0, 1, 0),
(399, 191, 'Content', 0, 0, 0, 0),
(400, 191, 'Footer kolonne et', 0, 0, 1, 0),
(401, 191, 'Footer kolonne to', 0, 0, 1, 0),
(402, 192, 'Main', 0, 0, 0, 0),
(403, 192, 'navigation', 0, 0, 1, 0),
(404, 192, 'Header title', 0, 0, 1, 0),
(405, 192, 'Header tekst', 0, 0, 0, 0),
(406, 192, 'Sidebar productgroup', 0, 0, 1, 0),
(407, 192, 'Content', 0, 0, 0, 0),
(408, 192, 'Footer kolonne et', 0, 0, 1, 0),
(409, 192, 'Footer kolonne to', 0, 0, 1, 0),
(410, 193, 'Main', 0, 0, 0, 0),
(411, 193, 'navigation', 0, 0, 1, 0),
(412, 193, 'Header title', 0, 0, 1, 0),
(413, 193, 'Header tekst', 0, 0, 0, 0),
(414, 193, 'Sidebar productgroup', 0, 0, 1, 0),
(415, 193, 'Content', 0, 0, 0, 0),
(416, 193, 'Footer kolonne et', 0, 0, 1, 0),
(417, 193, 'Footer kolonne to', 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `AttributeKeyCategories`
--

CREATE TABLE IF NOT EXISTS `AttributeKeyCategories` (
  `akCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `akCategoryHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `akCategoryAllowSets` smallint(6) NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`akCategoryID`),
  KEY `akCategoryHandle` (`akCategoryHandle`),
  KEY `pkgID` (`pkgID`,`akCategoryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `AttributeKeyCategories`
--

INSERT INTO `AttributeKeyCategories` (`akCategoryID`, `akCategoryHandle`, `akCategoryAllowSets`, `pkgID`) VALUES
(1, 'collection', 1, NULL),
(2, 'user', 1, NULL),
(3, 'file', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `AttributeKeys`
--

CREATE TABLE IF NOT EXISTS `AttributeKeys` (
  `akID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `akHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `akName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `akIsSearchable` tinyint(1) NOT NULL DEFAULT '0',
  `akIsSearchableIndexed` tinyint(1) NOT NULL DEFAULT '0',
  `akIsAutoCreated` tinyint(1) NOT NULL DEFAULT '0',
  `akIsInternal` tinyint(1) NOT NULL DEFAULT '0',
  `akIsColumnHeader` tinyint(1) NOT NULL DEFAULT '0',
  `akIsEditable` tinyint(1) NOT NULL DEFAULT '0',
  `atID` int(10) unsigned DEFAULT NULL,
  `akCategoryID` int(10) unsigned DEFAULT NULL,
  `pkgID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`akID`),
  UNIQUE KEY `akHandle` (`akHandle`,`akCategoryID`),
  KEY `akCategoryID` (`akCategoryID`),
  KEY `atID` (`atID`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `AttributeKeys`
--

INSERT INTO `AttributeKeys` (`akID`, `akHandle`, `akName`, `akIsSearchable`, `akIsSearchableIndexed`, `akIsAutoCreated`, `akIsInternal`, `akIsColumnHeader`, `akIsEditable`, `atID`, `akCategoryID`, `pkgID`) VALUES
(1, 'meta_title', 'Meta Title', 1, 1, 1, 0, 0, 1, 1, 1, 0),
(2, 'meta_description', 'Meta Description', 1, 1, 1, 0, 0, 1, 2, 1, 0),
(3, 'meta_keywords', 'Meta Keywords', 1, 1, 1, 0, 0, 1, 2, 1, 0),
(4, 'icon_dashboard', 'Dashboard Icon', 0, 0, 1, 1, 0, 1, 2, 1, 0),
(5, 'exclude_nav', 'Exclude From Nav', 1, 1, 1, 0, 0, 1, 3, 1, 0),
(6, 'exclude_page_list', 'Exclude From Page List', 1, 1, 1, 0, 0, 1, 3, 1, 0),
(7, 'header_extra_content', 'Header Extra Content', 1, 1, 1, 0, 0, 1, 2, 1, 0),
(8, 'tags', 'Tags', 1, 1, 1, 0, 0, 1, 8, 1, 0),
(9, 'is_featured', 'Is Featured', 1, 0, 1, 0, 0, 1, 3, 1, 0),
(10, 'exclude_search_index', 'Exclude From Search Index', 1, 1, 1, 0, 0, 1, 3, 1, 0),
(11, 'exclude_sitemapxml', 'Exclude From sitemap.xml', 1, 1, 1, 0, 0, 1, 3, 1, 0),
(12, 'profile_private_messages_enabled', 'I would like to receive private messages.', 1, 0, 0, 0, 0, 1, 3, 2, 0),
(13, 'profile_private_messages_notification_enabled', 'Send me email notifications when I receive a private message.', 1, 0, 0, 0, 0, 1, 3, 2, 0),
(14, 'width', 'Width', 1, 1, 1, 0, 0, 1, 6, 3, 0),
(15, 'height', 'Height', 1, 1, 1, 0, 0, 1, 6, 3, 0),
(16, 'account_profile_links', 'Personal Links', 0, 0, 0, 0, 0, 1, 11, 2, 0),
(17, 'duration', 'Duration', 1, 1, 1, 0, 0, 1, 6, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `AttributeSetKeys`
--

CREATE TABLE IF NOT EXISTS `AttributeSetKeys` (
  `akID` int(10) unsigned NOT NULL DEFAULT '0',
  `asID` int(10) unsigned NOT NULL DEFAULT '0',
  `displayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`akID`,`asID`),
  KEY `asID` (`asID`,`displayOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `AttributeSetKeys`
--

INSERT INTO `AttributeSetKeys` (`akID`, `asID`, `displayOrder`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(7, 1, 4),
(11, 1, 5),
(9, 2, 1),
(5, 2, 2),
(6, 2, 3),
(10, 2, 4),
(8, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `AttributeSets`
--

CREATE TABLE IF NOT EXISTS `AttributeSets` (
  `asID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `akCategoryID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  `asIsLocked` tinyint(1) NOT NULL DEFAULT '1',
  `asDisplayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`asID`),
  UNIQUE KEY `asHandle` (`asHandle`),
  KEY `akCategoryID` (`akCategoryID`,`asDisplayOrder`),
  KEY `pkgID` (`pkgID`,`asID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `AttributeSets`
--

INSERT INTO `AttributeSets` (`asID`, `asName`, `asHandle`, `akCategoryID`, `pkgID`, `asIsLocked`, `asDisplayOrder`) VALUES
(1, 'SEO', 'seo', 1, 0, 0, 0),
(2, 'Navigation and Indexing', 'navigation', 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `AttributeTypeCategories`
--

CREATE TABLE IF NOT EXISTS `AttributeTypeCategories` (
  `atID` int(10) unsigned NOT NULL DEFAULT '0',
  `akCategoryID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`atID`,`akCategoryID`),
  KEY `akCategoryID` (`akCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `AttributeTypeCategories`
--

INSERT INTO `AttributeTypeCategories` (`atID`, `akCategoryID`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 2),
(10, 1),
(10, 2),
(10, 3),
(11, 2);

-- --------------------------------------------------------

--
-- Table structure for table `AttributeTypes`
--

CREATE TABLE IF NOT EXISTS `AttributeTypes` (
  `atID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `atHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `atName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`atID`),
  UNIQUE KEY `atHandle` (`atHandle`),
  KEY `pkgID` (`pkgID`,`atID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `AttributeTypes`
--

INSERT INTO `AttributeTypes` (`atID`, `atHandle`, `atName`, `pkgID`) VALUES
(1, 'text', 'Text', 0),
(2, 'textarea', 'Text Area', 0),
(3, 'boolean', 'Checkbox', 0),
(4, 'date_time', 'Date/Time', 0),
(5, 'image_file', 'Image/File', 0),
(6, 'number', 'Number', 0),
(7, 'rating', 'Rating', 0),
(8, 'select', 'Select', 0),
(9, 'address', 'Address', 0),
(10, 'topics', 'Topics', 0),
(11, 'social_links', 'Social Links', 0);

-- --------------------------------------------------------

--
-- Table structure for table `AttributeValues`
--

CREATE TABLE IF NOT EXISTS `AttributeValues` (
  `avID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `akID` int(10) unsigned DEFAULT NULL,
  `avDateAdded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uID` int(10) unsigned DEFAULT NULL,
  `atID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`avID`),
  KEY `akID` (`akID`),
  KEY `uID` (`uID`),
  KEY `atID` (`atID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=292 ;

--
-- Dumping data for table `AttributeValues`
--

INSERT INTO `AttributeValues` (`avID`, `akID`, `avDateAdded`, `uID`, `atID`) VALUES
(1, 4, '2015-04-30 16:39:31', 1, 2),
(2, 3, '2015-04-30 16:39:32', 1, 2),
(3, 3, '2015-04-30 16:39:32', 1, 2),
(4, 3, '2015-04-30 16:39:32', 1, 2),
(5, 3, '2015-04-30 16:39:32', 1, 2),
(6, 3, '2015-04-30 16:39:32', 1, 2),
(7, 3, '2015-04-30 16:39:32', 1, 2),
(8, 3, '2015-04-30 16:39:32', 1, 2),
(9, 5, '2015-04-30 16:39:32', 1, 3),
(10, 3, '2015-04-30 16:39:32', 1, 2),
(11, 3, '2015-04-30 16:39:32', 1, 2),
(12, 3, '2015-04-30 16:39:32', 1, 2),
(13, 3, '2015-04-30 16:39:32', 1, 2),
(14, 3, '2015-04-30 16:39:32', 1, 2),
(15, 3, '2015-04-30 16:39:32', 1, 2),
(16, 5, '2015-04-30 16:39:32', 1, 3),
(17, 3, '2015-04-30 16:39:32', 1, 2),
(18, 5, '2015-04-30 16:39:32', 1, 3),
(19, 3, '2015-04-30 16:39:32', 1, 2),
(20, 3, '2015-04-30 16:39:32', 1, 2),
(21, 3, '2015-04-30 16:39:32', 1, 2),
(22, 3, '2015-04-30 16:39:32', 1, 2),
(23, 3, '2015-04-30 16:39:32', 1, 2),
(24, 3, '2015-04-30 16:39:32', 1, 2),
(25, 3, '2015-04-30 16:39:32', 1, 2),
(26, 3, '2015-04-30 16:39:33', 1, 2),
(27, 3, '2015-04-30 16:39:33', 1, 2),
(28, 5, '2015-04-30 16:39:33', 1, 3),
(29, 5, '2015-04-30 16:39:33', 1, 3),
(30, 5, '2015-04-30 16:39:33', 1, 3),
(31, 5, '2015-04-30 16:39:33', 1, 3),
(32, 5, '2015-04-30 16:39:33', 1, 3),
(33, 5, '2015-04-30 16:39:33', 1, 3),
(34, 5, '2015-04-30 16:39:33', 1, 3),
(35, 5, '2015-04-30 16:39:33', 1, 3),
(36, 3, '2015-04-30 16:39:33', 1, 2),
(37, 3, '2015-04-30 16:39:33', 1, 2),
(38, 3, '2015-04-30 16:39:33', 1, 2),
(39, 4, '2015-04-30 16:39:33', 1, 2),
(40, 3, '2015-04-30 16:39:33', 1, 2),
(41, 3, '2015-04-30 16:39:33', 1, 2),
(42, 5, '2015-04-30 16:39:33', 1, 3),
(43, 10, '2015-04-30 16:39:33', 1, 3),
(44, 3, '2015-04-30 16:39:33', 1, 2),
(45, 3, '2015-04-30 16:39:33', 1, 2),
(46, 3, '2015-04-30 16:39:33', 1, 2),
(47, 5, '2015-04-30 16:39:33', 1, 3),
(48, 3, '2015-04-30 16:39:33', 1, 2),
(49, 3, '2015-04-30 16:39:33', 1, 2),
(50, 3, '2015-04-30 16:39:33', 1, 2),
(51, 5, '2015-04-30 16:39:33', 1, 3),
(52, 3, '2015-04-30 16:39:33', 1, 2),
(53, 3, '2015-04-30 16:39:34', 1, 2),
(54, 3, '2015-04-30 16:39:34', 1, 2),
(55, 3, '2015-04-30 16:39:34', 1, 2),
(56, 3, '2015-04-30 16:39:34', 1, 2),
(57, 3, '2015-04-30 16:39:34', 1, 2),
(58, 3, '2015-04-30 16:39:34', 1, 2),
(59, 3, '2015-04-30 16:39:34', 1, 2),
(60, 3, '2015-04-30 16:39:34', 1, 2),
(61, 3, '2015-04-30 16:39:34', 1, 2),
(62, 3, '2015-04-30 16:39:34', 1, 2),
(63, 3, '2015-04-30 16:39:34', 1, 2),
(64, 3, '2015-04-30 16:39:34', 1, 2),
(65, 3, '2015-04-30 16:39:34', 1, 2),
(66, 3, '2015-04-30 16:39:34', 1, 2),
(67, 3, '2015-04-30 16:39:34', 1, 2),
(68, 3, '2015-04-30 16:39:34', 1, 2),
(69, 3, '2015-04-30 16:39:34', 1, 2),
(70, 3, '2015-04-30 16:39:34', 1, 2),
(71, 3, '2015-04-30 16:39:34', 1, 2),
(72, 3, '2015-04-30 16:39:34', 1, 2),
(73, 3, '2015-04-30 16:39:34', 1, 2),
(74, 3, '2015-04-30 16:39:34', 1, 2),
(75, 3, '2015-04-30 16:39:34', 1, 2),
(76, 3, '2015-04-30 16:39:34', 1, 2),
(77, 3, '2015-04-30 16:39:34', 1, 2),
(78, 3, '2015-04-30 16:39:34', 1, 2),
(79, 3, '2015-04-30 16:39:34', 1, 2),
(80, 3, '2015-04-30 16:39:35', 1, 2),
(81, 3, '2015-04-30 16:39:35', 1, 2),
(82, 3, '2015-04-30 16:39:35', 1, 2),
(83, 3, '2015-04-30 16:39:35', 1, 2),
(84, 3, '2015-04-30 16:39:35', 1, 2),
(85, 3, '2015-04-30 16:39:35', 1, 2),
(86, 3, '2015-04-30 16:39:35', 1, 2),
(87, 3, '2015-04-30 16:39:35', 1, 2),
(88, 3, '2015-04-30 16:39:35', 1, 2),
(89, 3, '2015-04-30 16:39:35', 1, 2),
(90, 3, '2015-04-30 16:39:35', 1, 2),
(91, 3, '2015-04-30 16:39:35', 1, 2),
(92, 3, '2015-04-30 16:39:36', 1, 2),
(93, 3, '2015-04-30 16:39:36', 1, 2),
(94, 3, '2015-04-30 16:39:37', 1, 2),
(95, 3, '2015-04-30 16:39:37', 1, 2),
(96, 3, '2015-04-30 16:39:37', 1, 2),
(97, 3, '2015-04-30 16:39:37', 1, 2),
(98, 10, '2015-04-30 16:39:37', 1, 3),
(99, 3, '2015-04-30 16:39:37', 1, 2),
(100, 3, '2015-04-30 16:39:37', 1, 2),
(101, 3, '2015-04-30 16:39:37', 1, 2),
(102, 3, '2015-04-30 16:39:37', 1, 2),
(103, 3, '2015-04-30 16:39:37', 1, 2),
(104, 3, '2015-04-30 16:39:37', 1, 2),
(105, 5, '2015-04-30 16:39:37', 1, 3),
(106, 5, '2015-04-30 16:39:37', 1, 3),
(107, 10, '2015-04-30 16:39:37', 1, 3),
(108, 4, '2015-04-30 16:39:40', 1, 2),
(109, 4, '2015-04-30 16:39:40', 1, 2),
(110, 4, '2015-04-30 16:39:40', 1, 2),
(111, 4, '2015-04-30 16:39:40', 1, 2),
(112, 14, '2015-05-07 23:34:50', 1, 6),
(113, 15, '2015-05-07 23:34:50', 1, 6),
(114, 1, '2015-05-08 00:36:29', 1, 1),
(115, 2, '2015-05-08 00:36:29', 1, 2),
(116, 3, '2015-05-08 00:36:29', 1, 2),
(117, 7, '2015-05-08 00:36:29', 1, 2),
(118, 11, '2015-05-08 00:36:29', 1, 3),
(119, 1, '2015-05-08 00:53:55', 1, 1),
(120, 2, '2015-05-08 00:53:55', 1, 2),
(121, 3, '2015-05-08 00:53:55', 1, 2),
(122, 7, '2015-05-08 00:53:55', 1, 2),
(123, 11, '2015-05-08 00:53:55', 1, 3),
(124, 14, '2015-05-13 07:32:48', 1, 6),
(125, 15, '2015-05-13 07:32:48', 1, 6),
(126, 5, '2015-05-18 12:32:28', 1, 3),
(127, 6, '2015-05-18 12:32:28', 1, 3),
(128, 10, '2015-05-18 12:32:28', 1, 3),
(129, 1, '2015-05-18 12:35:42', 1, 1),
(130, 2, '2015-05-18 12:35:42', 1, 2),
(131, 3, '2015-05-18 12:35:42', 1, 2),
(132, 7, '2015-05-18 12:35:42', 1, 2),
(133, 11, '2015-05-18 12:35:42', 1, 3),
(134, 6, '2015-05-18 12:35:42', 1, 3),
(135, 5, '2015-05-18 12:35:42', 1, 3),
(136, 5, '2015-05-18 12:35:59', 1, 3),
(137, 5, '2015-05-18 12:37:01', 1, 3),
(138, 6, '2015-05-18 12:37:01', 1, 3),
(139, 5, '2015-05-19 08:37:30', 1, 3),
(140, 5, '2015-05-19 08:53:09', 1, 3),
(141, 6, '2015-05-19 08:53:09', 1, 3),
(142, 1, '2015-05-19 09:15:41', 1, 1),
(143, 2, '2015-05-19 09:15:41', 1, 2),
(144, 3, '2015-05-19 09:15:41', 1, 2),
(145, 7, '2015-05-19 09:15:41', 1, 2),
(146, 11, '2015-05-19 09:15:41', 1, 3),
(147, 1, '2015-05-19 09:18:11', 1, 1),
(148, 2, '2015-05-19 09:18:11', 1, 2),
(149, 3, '2015-05-19 09:18:11', 1, 2),
(150, 7, '2015-05-19 09:18:11', 1, 2),
(151, 11, '2015-05-19 09:18:12', 1, 3),
(152, 1, '2015-05-19 09:19:37', 1, 1),
(153, 2, '2015-05-19 09:19:38', 1, 2),
(154, 3, '2015-05-19 09:19:38', 1, 2),
(155, 7, '2015-05-19 09:19:38', 1, 2),
(156, 11, '2015-05-19 09:19:38', 1, 3),
(157, 1, '2015-05-19 09:20:47', 1, 1),
(158, 2, '2015-05-19 09:20:47', 1, 2),
(159, 3, '2015-05-19 09:20:47', 1, 2),
(160, 7, '2015-05-19 09:20:47', 1, 2),
(161, 11, '2015-05-19 09:20:47', 1, 3),
(162, 1, '2015-05-19 09:23:35', 1, 1),
(163, 2, '2015-05-19 09:23:35', 1, 2),
(164, 3, '2015-05-19 09:23:36', 1, 2),
(165, 7, '2015-05-19 09:23:36', 1, 2),
(166, 11, '2015-05-19 09:23:36', 1, 3),
(167, 1, '2015-05-19 09:30:27', 1, 1),
(168, 2, '2015-05-19 09:30:27', 1, 2),
(169, 3, '2015-05-19 09:30:27', 1, 2),
(170, 7, '2015-05-19 09:30:27', 1, 2),
(171, 11, '2015-05-19 09:30:27', 1, 3),
(172, 1, '2015-05-19 09:32:08', 1, 1),
(173, 2, '2015-05-19 09:32:08', 1, 2),
(174, 3, '2015-05-19 09:32:08', 1, 2),
(175, 7, '2015-05-19 09:32:08', 1, 2),
(176, 11, '2015-05-19 09:32:08', 1, 3),
(177, 1, '2015-05-19 10:05:48', 1, 1),
(178, 2, '2015-05-19 10:05:48', 1, 2),
(179, 3, '2015-05-19 10:05:48', 1, 2),
(180, 7, '2015-05-19 10:05:48', 1, 2),
(181, 11, '2015-05-19 10:05:48', 1, 3),
(182, 1, '2015-05-19 10:07:44', 1, 1),
(183, 2, '2015-05-19 10:07:44', 1, 2),
(184, 3, '2015-05-19 10:07:44', 1, 2),
(185, 7, '2015-05-19 10:07:44', 1, 2),
(186, 11, '2015-05-19 10:07:44', 1, 3),
(187, 1, '2015-05-19 11:17:30', 1, 1),
(188, 2, '2015-05-19 11:17:30', 1, 2),
(189, 3, '2015-05-19 11:17:30', 1, 2),
(190, 7, '2015-05-19 11:17:30', 1, 2),
(191, 11, '2015-05-19 11:17:30', 1, 3),
(192, 1, '2015-05-19 11:18:17', 1, 1),
(193, 2, '2015-05-19 11:18:17', 1, 2),
(194, 3, '2015-05-19 11:18:18', 1, 2),
(195, 7, '2015-05-19 11:18:18', 1, 2),
(196, 11, '2015-05-19 11:18:18', 1, 3),
(197, 1, '2015-05-19 11:21:03', 1, 1),
(198, 2, '2015-05-19 11:21:03', 1, 2),
(199, 3, '2015-05-19 11:21:03', 1, 2),
(200, 7, '2015-05-19 11:21:03', 1, 2),
(201, 11, '2015-05-19 11:21:04', 1, 3),
(202, 1, '2015-05-19 12:53:07', 1, 1),
(203, 2, '2015-05-19 12:53:07', 1, 2),
(204, 3, '2015-05-19 12:53:07', 1, 2),
(205, 7, '2015-05-19 12:53:07', 1, 2),
(206, 11, '2015-05-19 12:53:07', 1, 3),
(207, 14, '2015-05-19 13:33:44', 1, 6),
(208, 15, '2015-05-19 13:33:45', 1, 6),
(209, 14, '2015-05-19 13:33:46', 1, 6),
(210, 15, '2015-05-19 13:33:46', 1, 6),
(211, 14, '2015-05-19 13:33:58', 1, 6),
(212, 15, '2015-05-19 13:33:58', 1, 6),
(213, 14, '2015-05-19 13:34:00', 1, 6),
(214, 15, '2015-05-19 13:34:00', 1, 6),
(215, 14, '2015-05-19 13:34:01', 1, 6),
(216, 15, '2015-05-19 13:34:01', 1, 6),
(217, 3, '2015-05-19 15:46:56', 1, 2),
(218, 1, '2015-05-20 11:05:02', 1, 1),
(219, 2, '2015-05-20 11:05:02', 1, 2),
(220, 3, '2015-05-20 11:05:02', 1, 2),
(221, 7, '2015-05-20 11:05:02', 1, 2),
(222, 11, '2015-05-20 11:05:02', 1, 3),
(223, 14, '2015-05-20 11:08:23', 1, 6),
(224, 15, '2015-05-20 11:08:23', 1, 6),
(225, 1, '2015-05-20 11:11:30', 1, 1),
(226, 2, '2015-05-20 11:11:30', 1, 2),
(227, 3, '2015-05-20 11:11:30', 1, 2),
(228, 7, '2015-05-20 11:11:30', 1, 2),
(229, 11, '2015-05-20 11:11:30', 1, 3),
(230, 1, '2015-05-20 11:13:03', 1, 1),
(231, 2, '2015-05-20 11:13:03', 1, 2),
(232, 3, '2015-05-20 11:13:03', 1, 2),
(233, 7, '2015-05-20 11:13:03', 1, 2),
(234, 11, '2015-05-20 11:13:03', 1, 3),
(235, 14, '2015-05-22 11:47:36', 1, 6),
(236, 15, '2015-05-22 11:47:36', 1, 6),
(237, 1, '2015-05-22 13:54:32', 1, 1),
(238, 2, '2015-05-22 13:54:32', 1, 2),
(239, 3, '2015-05-22 13:54:32', 1, 2),
(240, 7, '2015-05-22 13:54:32', 1, 2),
(241, 11, '2015-05-22 13:54:32', 1, 3),
(242, 14, '2015-05-23 14:09:24', 1, 6),
(243, 15, '2015-05-23 14:09:25', 1, 6),
(244, 14, '2015-05-23 14:10:05', 1, 6),
(245, 15, '2015-05-23 14:10:05', 1, 6),
(246, 14, '2015-05-23 15:41:52', 1, 6),
(247, 15, '2015-05-23 15:41:52', 1, 6),
(248, 14, '2015-05-23 15:43:16', 1, 6),
(249, 15, '2015-05-23 15:43:17', 1, 6),
(250, 14, '2015-05-26 08:24:18', 1, 6),
(251, 15, '2015-05-26 08:24:18', 1, 6),
(252, 14, '2015-05-26 08:27:16', 1, 6),
(253, 15, '2015-05-26 08:27:16', 1, 6),
(254, 14, '2015-05-26 08:54:38', 1, 6),
(255, 15, '2015-05-26 08:54:38', 1, 6),
(256, 14, '2015-05-26 09:00:21', 1, 6),
(257, 15, '2015-05-26 09:00:21', 1, 6),
(258, 14, '2015-05-26 09:00:22', 1, 6),
(259, 15, '2015-05-26 09:00:23', 1, 6),
(260, 14, '2015-05-26 09:00:24', 1, 6),
(261, 15, '2015-05-26 09:00:24', 1, 6),
(262, 14, '2015-05-26 09:00:27', 1, 6),
(263, 15, '2015-05-26 09:00:27', 1, 6),
(264, 14, '2015-05-26 09:00:29', 1, 6),
(265, 15, '2015-05-26 09:00:29', 1, 6),
(266, 14, '2015-05-26 09:00:31', 1, 6),
(267, 15, '2015-05-26 09:00:31', 1, 6),
(268, 14, '2015-05-26 09:00:32', 1, 6),
(269, 15, '2015-05-26 09:00:32', 1, 6),
(270, 14, '2015-05-26 09:00:34', 1, 6),
(271, 15, '2015-05-26 09:00:34', 1, 6),
(272, 14, '2015-05-26 09:00:36', 1, 6),
(273, 15, '2015-05-26 09:00:36', 1, 6),
(274, 14, '2015-05-26 09:00:38', 1, 6),
(275, 15, '2015-05-26 09:00:38', 1, 6),
(276, 14, '2015-05-26 09:00:40', 1, 6),
(277, 15, '2015-05-26 09:00:40', 1, 6),
(278, 14, '2015-05-26 09:00:41', 1, 6),
(279, 15, '2015-05-26 09:00:41', 1, 6),
(280, 14, '2015-05-26 09:00:43', 1, 6),
(281, 15, '2015-05-26 09:00:43', 1, 6),
(282, 14, '2015-05-26 09:01:41', 1, 6),
(283, 15, '2015-05-26 09:01:41', 1, 6),
(284, 14, '2015-05-26 09:05:54', 1, 6),
(285, 15, '2015-05-26 09:05:54', 1, 6),
(286, 14, '2015-05-26 09:05:56', 1, 6),
(287, 15, '2015-05-26 09:05:56', 1, 6),
(288, 14, '2015-05-26 10:52:48', 1, 6),
(289, 15, '2015-05-26 10:52:48', 1, 6),
(290, 14, '2015-05-26 10:54:04', 1, 6),
(291, 15, '2015-05-26 10:54:04', 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `AuthenticationTypes`
--

CREATE TABLE IF NOT EXISTS `AuthenticationTypes` (
  `authTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `authTypeHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authTypeName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authTypeIsEnabled` tinyint(1) NOT NULL,
  `authTypeDisplayOrder` int(10) unsigned DEFAULT NULL,
  `pkgID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`authTypeID`),
  UNIQUE KEY `authTypeHandle` (`authTypeHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `AuthenticationTypes`
--

INSERT INTO `AuthenticationTypes` (`authTypeID`, `authTypeHandle`, `authTypeName`, `authTypeIsEnabled`, `authTypeDisplayOrder`, `pkgID`) VALUES
(1, 'concrete', 'Standard', 1, 0, 0),
(2, 'community', 'concrete5.org', 0, 0, 0),
(3, 'facebook', 'Facebook', 0, 0, 0),
(4, 'twitter', 'Twitter', 0, 0, 0),
(5, 'google', 'Google', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `BannedWords`
--

CREATE TABLE IF NOT EXISTS `BannedWords` (
  `bwID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bannedWord` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`bwID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `BannedWords`
--

INSERT INTO `BannedWords` (`bwID`, `bannedWord`) VALUES
(1, 'fuck'),
(2, 'shit'),
(3, 'bitch'),
(4, 'ass');

-- --------------------------------------------------------

--
-- Table structure for table `BasicWorkflowPermissionAssignments`
--

CREATE TABLE IF NOT EXISTS `BasicWorkflowPermissionAssignments` (
  `wfID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkID` int(10) unsigned NOT NULL DEFAULT '0',
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`wfID`,`pkID`,`paID`),
  KEY `pkID` (`pkID`),
  KEY `paID` (`paID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `BasicWorkflowProgressData`
--

CREATE TABLE IF NOT EXISTS `BasicWorkflowProgressData` (
  `wpID` int(10) unsigned NOT NULL DEFAULT '0',
  `uIDStarted` int(10) unsigned NOT NULL DEFAULT '0',
  `uIDCompleted` int(10) unsigned NOT NULL DEFAULT '0',
  `wpDateCompleted` datetime DEFAULT NULL,
  PRIMARY KEY (`wpID`),
  KEY `uIDStarted` (`uIDStarted`),
  KEY `uIDCompleted` (`uIDCompleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `BlockFeatureAssignments`
--

CREATE TABLE IF NOT EXISTS `BlockFeatureAssignments` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvID` int(10) unsigned NOT NULL DEFAULT '0',
  `bID` int(10) unsigned NOT NULL DEFAULT '0',
  `faID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`cvID`,`bID`,`faID`),
  KEY `faID` (`faID`,`cID`,`cvID`),
  KEY `bID` (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `BlockFeatureAssignments`
--

INSERT INTO `BlockFeatureAssignments` (`cID`, `cvID`, `bID`, `faID`) VALUES
(1, 18, 370, 2),
(1, 19, 370, 2),
(1, 21, 411, 6),
(1, 22, 412, 7),
(1, 23, 412, 7),
(1, 24, 412, 7),
(1, 25, 412, 7),
(1, 26, 412, 7),
(1, 27, 412, 7),
(1, 28, 468, 10),
(156, 3, 34, 1),
(166, 2, 382, 3),
(166, 3, 383, 4),
(166, 4, 384, 5),
(166, 5, 384, 5),
(166, 6, 384, 5),
(170, 5, 455, 9),
(175, 7, 454, 8);

-- --------------------------------------------------------

--
-- Table structure for table `BlockPermissionAssignments`
--

CREATE TABLE IF NOT EXISTS `BlockPermissionAssignments` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvID` int(10) unsigned NOT NULL DEFAULT '0',
  `bID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkID` int(10) unsigned NOT NULL DEFAULT '0',
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`cvID`,`bID`,`pkID`,`paID`),
  KEY `bID` (`bID`),
  KEY `pkID` (`pkID`),
  KEY `paID` (`paID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `BlockRelations`
--

CREATE TABLE IF NOT EXISTS `BlockRelations` (
  `brID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bID` int(10) unsigned NOT NULL DEFAULT '0',
  `originalBID` int(10) unsigned NOT NULL DEFAULT '0',
  `relationType` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`brID`),
  KEY `bID` (`bID`),
  KEY `originalBID` (`originalBID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

--
-- Dumping data for table `BlockRelations`
--

INSERT INTO `BlockRelations` (`brID`, `bID`, `originalBID`, `relationType`) VALUES
(3, 110, 35, 'DUPLICATE'),
(4, 141, 139, 'DUPLICATE'),
(5, 143, 123, 'DUPLICATE'),
(7, 186, 144, 'DUPLICATE'),
(9, 192, 187, 'DUPLICATE'),
(10, 256, 255, 'DUPLICATE'),
(11, 257, 247, 'DUPLICATE'),
(12, 258, 214, 'DUPLICATE'),
(13, 269, 268, 'DUPLICATE'),
(15, 272, 271, 'DUPLICATE'),
(17, 275, 274, 'DUPLICATE'),
(18, 302, 99, 'DUPLICATE'),
(19, 332, 27, 'DUPLICATE'),
(20, 333, 29, 'DUPLICATE'),
(21, 352, 351, 'DUPLICATE'),
(22, 353, 332, 'DUPLICATE'),
(23, 354, 104, 'DUPLICATE'),
(24, 355, 353, 'DUPLICATE'),
(25, 356, 25, 'DUPLICATE'),
(26, 357, 192, 'DUPLICATE'),
(27, 358, 188, 'DUPLICATE'),
(28, 359, 358, 'DUPLICATE'),
(29, 360, 267, 'DUPLICATE'),
(30, 361, 141, 'DUPLICATE'),
(31, 367, 258, 'DUPLICATE'),
(32, 369, 361, 'DUPLICATE'),
(33, 372, 362, 'DUPLICATE'),
(34, 373, 363, 'DUPLICATE'),
(35, 374, 364, 'DUPLICATE'),
(36, 375, 366, 'DUPLICATE'),
(37, 383, 382, 'DUPLICATE'),
(39, 387, 28, 'DUPLICATE'),
(40, 390, 388, 'DUPLICATE'),
(41, 408, 387, 'DUPLICATE'),
(42, 412, 411, 'DUPLICATE'),
(43, 414, 410, 'DUPLICATE'),
(44, 417, 360, 'DUPLICATE'),
(45, 418, 142, 'DUPLICATE'),
(46, 419, 357, 'DUPLICATE'),
(47, 420, 185, 'DUPLICATE'),
(48, 421, 367, 'DUPLICATE'),
(49, 422, 275, 'DUPLICATE'),
(50, 423, 365, 'DUPLICATE'),
(51, 424, 33, 'DUPLICATE'),
(52, 425, 414, 'DUPLICATE'),
(53, 426, 26, 'DUPLICATE'),
(54, 427, 423, 'DUPLICATE'),
(55, 440, 428, 'DUPLICATE'),
(56, 441, 425, 'DUPLICATE'),
(57, 442, 355, 'DUPLICATE'),
(58, 443, 356, 'DUPLICATE'),
(59, 451, 450, 'DUPLICATE'),
(60, 452, 451, 'DUPLICATE'),
(61, 453, 442, 'DUPLICATE'),
(62, 468, 412, 'DUPLICATE'),
(63, 472, 440, 'DUPLICATE'),
(64, 474, 421, 'DUPLICATE');

-- --------------------------------------------------------

--
-- Table structure for table `BlockTypePermissionBlockTypeAccessList`
--

CREATE TABLE IF NOT EXISTS `BlockTypePermissionBlockTypeAccessList` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`paID`,`peID`),
  KEY `peID` (`peID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `BlockTypePermissionBlockTypeAccessListCustom`
--

CREATE TABLE IF NOT EXISTS `BlockTypePermissionBlockTypeAccessListCustom` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `btID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`paID`,`peID`,`btID`),
  KEY `peID` (`peID`),
  KEY `btID` (`btID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `BlockTypeSetBlockTypes`
--

CREATE TABLE IF NOT EXISTS `BlockTypeSetBlockTypes` (
  `btID` int(10) unsigned NOT NULL DEFAULT '0',
  `btsID` int(10) unsigned NOT NULL DEFAULT '0',
  `displayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`btID`,`btsID`),
  KEY `btsID` (`btsID`,`displayOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `BlockTypeSetBlockTypes`
--

INSERT INTO `BlockTypeSetBlockTypes` (`btID`, `btsID`, `displayOrder`) VALUES
(12, 1, 0),
(25, 1, 1),
(27, 1, 2),
(15, 1, 3),
(26, 1, 4),
(19, 1, 5),
(11, 2, 0),
(18, 2, 1),
(28, 2, 2),
(30, 2, 3),
(29, 2, 4),
(13, 2, 5),
(36, 2, 6),
(20, 2, 7),
(31, 2, 8),
(35, 2, 9),
(17, 3, 0),
(32, 3, 1),
(14, 3, 2),
(34, 4, 0),
(5, 4, 1),
(21, 4, 2),
(22, 4, 3),
(23, 4, 4),
(16, 5, 0),
(33, 5, 1),
(37, 5, 2),
(38, 5, 3),
(24, 5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `BlockTypeSets`
--

CREATE TABLE IF NOT EXISTS `BlockTypeSets` (
  `btsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `btsName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `btsHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  `btsDisplayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`btsID`),
  UNIQUE KEY `btsHandle` (`btsHandle`),
  KEY `btsDisplayOrder` (`btsDisplayOrder`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `BlockTypeSets`
--

INSERT INTO `BlockTypeSets` (`btsID`, `btsName`, `btsHandle`, `pkgID`, `btsDisplayOrder`) VALUES
(1, 'Basic', 'basic', 0, 0),
(2, 'Navigation', 'navigation', 0, 0),
(3, 'Forms', 'form', 0, 0),
(4, 'Social Networking', 'social', 0, 0),
(5, 'Multimedia', 'multimedia', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `BlockTypes`
--

CREATE TABLE IF NOT EXISTS `BlockTypes` (
  `btID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `btHandle` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `btName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `btDescription` text COLLATE utf8_unicode_ci,
  `btCopyWhenPropagate` tinyint(1) NOT NULL DEFAULT '0',
  `btIncludeAll` tinyint(1) NOT NULL DEFAULT '0',
  `btIsInternal` tinyint(1) NOT NULL DEFAULT '0',
  `btSupportsInlineAdd` tinyint(1) NOT NULL DEFAULT '0',
  `btSupportsInlineEdit` tinyint(1) NOT NULL DEFAULT '0',
  `btIgnorePageThemeGridFrameworkContainer` tinyint(1) NOT NULL DEFAULT '0',
  `btDisplayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  `btInterfaceWidth` int(10) unsigned NOT NULL DEFAULT '400',
  `btInterfaceHeight` int(10) unsigned NOT NULL DEFAULT '400',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`btID`),
  UNIQUE KEY `btHandle` (`btHandle`),
  KEY `btDisplayOrder` (`btDisplayOrder`,`btName`,`btID`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

--
-- Dumping data for table `BlockTypes`
--

INSERT INTO `BlockTypes` (`btID`, `btHandle`, `btName`, `btDescription`, `btCopyWhenPropagate`, `btIncludeAll`, `btIsInternal`, `btSupportsInlineAdd`, `btSupportsInlineEdit`, `btIgnorePageThemeGridFrameworkContainer`, `btDisplayOrder`, `btInterfaceWidth`, `btInterfaceHeight`, `pkgID`) VALUES
(1, 'core_area_layout', 'Area Layout', 'Proxy block for area layouts.', 0, 0, 1, 1, 1, 0, 0, 400, 400, 0),
(2, 'core_page_type_composer_control_output', 'Composer Control', 'Proxy block for blocks that need to be output through composer.', 0, 0, 1, 0, 0, 0, 0, 400, 400, 0),
(3, 'core_scrapbook_display', 'Scrapbook Display', 'Proxy block for blocks pasted through the scrapbook.', 0, 0, 1, 0, 0, 0, 0, 400, 400, 0),
(4, 'core_stack_display', 'Stack Display', 'Proxy block for stacks added through the UI.', 0, 0, 1, 0, 0, 0, 0, 400, 400, 0),
(5, 'core_conversation', 'Conversation', 'Displays conversations on a page.', 1, 0, 0, 0, 0, 0, 0, 400, 400, 0),
(6, 'dashboard_featured_addon', 'Dashboard Featured Add-On', 'Features an add-on from concrete5.org.', 0, 0, 1, 0, 0, 0, 0, 300, 100, 0),
(7, 'dashboard_featured_theme', 'Dashboard Featured Theme', 'Features a theme from concrete5.org.', 0, 0, 1, 0, 0, 0, 0, 300, 100, 0),
(8, 'dashboard_newsflow_latest', 'Dashboard Newsflow Latest', 'Grabs the latest newsflow data from concrete5.org.', 0, 0, 1, 0, 0, 0, 0, 400, 400, 0),
(9, 'dashboard_app_status', 'Dashboard App Status', 'Displays update and welcome back information on your dashboard.', 0, 0, 1, 0, 0, 0, 0, 400, 400, 0),
(10, 'dashboard_site_activity', 'Dashboard Site Activity', 'Displays a summary of website activity.', 0, 0, 1, 0, 0, 0, 0, 400, 400, 0),
(11, 'autonav', 'Auto-Nav', 'Creates navigation trees and sitemaps.', 0, 0, 0, 0, 0, 0, 0, 800, 350, 0),
(12, 'content', 'Content', 'HTML/WYSIWYG Editor Content.', 0, 0, 0, 1, 1, 0, 0, 600, 465, 0),
(13, 'date_navigation', 'Date Navigation', 'Displays a list of months to filter a page list by.', 0, 0, 0, 0, 0, 0, 0, 400, 450, 0),
(14, 'external_form', 'External Form', 'Include external forms in the filesystem and place them on pages.', 0, 0, 0, 0, 0, 0, 0, 370, 175, 0),
(15, 'file', 'File', 'Link to files stored in the asset library.', 0, 0, 0, 0, 0, 0, 0, 300, 250, 0),
(16, 'page_attribute_display', 'Page Attribute Display', 'Displays the value of a page attribute for the current page.', 0, 0, 0, 0, 0, 0, 0, 500, 365, 0),
(17, 'form', 'Formular', 'Byg formularer og spørgeskemaer.', 0, 0, 0, 0, 0, 0, 0, 420, 430, 0),
(18, 'page_title', 'Side Titel', 'Viser en Sides Titel', 0, 0, 0, 0, 0, 0, 0, 400, 400, 0),
(19, 'feature', 'Feature', 'Displays an icon, a title, and a short paragraph description.', 0, 0, 0, 0, 0, 0, 0, 400, 520, 0),
(20, 'topic_list', 'Topic List', 'Displays a list of your site''s topics, allowing you to click on them to filter a page list.', 0, 0, 0, 0, 0, 0, 0, 400, 400, 0),
(21, 'social_links', 'Social Links', 'Allows users to add social icons to their website', 0, 0, 0, 0, 0, 0, 0, 400, 400, 0),
(22, 'testimonial', 'Testimonial', 'Displays a quote or paragraph next to biographical information and a person''s picture.', 0, 0, 0, 0, 0, 0, 0, 450, 560, 0),
(23, 'share_this_page', 'Share This Page', 'Allows users to share this page with social networks.', 0, 0, 0, 0, 0, 0, 0, 400, 400, 0),
(24, 'google_map', 'Google Maps kort', 'Indtast en adresse, og et Google kort over lokaliteten vil blive placeret på din side.', 0, 0, 0, 0, 0, 0, 0, 400, 320, 0),
(25, 'html', 'HTML', 'For adding HTML by hand.', 0, 0, 0, 0, 0, 1, 0, 600, 500, 0),
(26, 'horizontal_rule', 'Horizontal Rule', 'Adds a thin hairline horizontal divider to the page.', 0, 0, 0, 0, 0, 1, 0, 400, 400, 0),
(27, 'image', 'Image', 'Adds images and onstates from the library to pages.', 0, 0, 0, 0, 0, 0, 0, 400, 550, 0),
(28, 'faq', 'FAQ', 'Frequently Asked Questions Block', 0, 0, 0, 0, 0, 0, 0, 600, 465, 0),
(29, 'next_previous', 'Next & Previous Nav', 'Navigate through sibling pages.', 0, 0, 0, 0, 0, 0, 0, 430, 400, 0),
(30, 'page_list', 'Side Liste', 'Vis sider efter type og område.', 0, 0, 0, 0, 0, 0, 0, 800, 350, 0),
(31, 'rss_displayer', 'RSS Displayer', 'Fetch, parse and display the contents of an RSS or Atom feed.', 0, 0, 0, 0, 0, 0, 0, 400, 550, 0),
(32, 'search', 'Search', 'Add a search box to your site.', 0, 0, 0, 0, 0, 0, 0, 400, 420, 0),
(33, 'image_slider', 'Image Slider', 'Display your images and captions in an attractive slideshow format.', 0, 0, 0, 0, 0, 1, 0, 600, 465, 0),
(34, 'survey', 'Survey', 'Provide a simple survey, along with results in a pie chart format.', 0, 0, 0, 0, 0, 0, 0, 420, 400, 0),
(35, 'switch_language', 'Switch Language', 'Adds a front-end language switcher to your website.', 0, 0, 0, 0, 0, 0, 0, 500, 150, 0),
(36, 'tags', 'Tags', 'List pages based on type, area.', 0, 0, 0, 0, 0, 0, 0, 450, 439, 0),
(37, 'video', 'Video Player', 'Embeds uploaded video into a web page. Supports WebM, Ogg, and Quicktime/MPEG4 formats.', 0, 0, 0, 0, 0, 0, 0, 320, 270, 0),
(38, 'youtube', 'YouTube Video', 'Embeds a YouTube Video in your web page.', 0, 0, 0, 0, 0, 0, 0, 400, 430, 0),
(39, 'vivid_thumb_gallery', 'Thumb Gallery', 'Add a Gallery of Images', 0, 0, 0, 0, 0, 0, 0, 800, 465, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Blocks`
--

CREATE TABLE IF NOT EXISTS `Blocks` (
  `bID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bDateAdded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bDateModified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bFilename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bIsActive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `btID` int(10) unsigned NOT NULL DEFAULT '0',
  `uID` int(10) unsigned DEFAULT NULL,
  `btCachedBlockRecord` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`bID`),
  KEY `btID` (`btID`),
  KEY `uID` (`uID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=476 ;

--
-- Dumping data for table `Blocks`
--

INSERT INTO `Blocks` (`bID`, `bName`, `bDateAdded`, `bDateModified`, `bFilename`, `bIsActive`, `btID`, `uID`, `btCachedBlockRecord`) VALUES
(1, '', '2015-04-30 16:39:37', '2015-04-30 16:39:37', NULL, '0', 12, 1, NULL),
(2, '', '2015-04-30 16:39:37', '2015-04-30 16:39:37', NULL, '0', 9, 1, NULL),
(3, '', '2015-04-30 16:39:37', '2015-04-30 16:39:37', NULL, '0', 10, 1, NULL),
(4, '', '2015-04-30 16:39:37', '2015-04-30 16:39:37', NULL, '0', 8, 1, NULL),
(5, '', '2015-04-30 16:39:37', '2015-04-30 16:39:37', NULL, '0', 8, 1, NULL),
(6, '', '2015-04-30 16:39:37', '2015-04-30 16:39:37', NULL, '0', 7, 1, NULL),
(7, '', '2015-04-30 16:39:37', '2015-04-30 16:39:37', NULL, '0', 6, 1, NULL),
(8, '', '2015-04-30 16:39:37', '2015-04-30 16:39:37', NULL, '0', 8, 1, NULL),
(9, '', '2015-04-30 16:39:44', '2015-04-30 16:39:44', NULL, '0', 2, 1, NULL),
(15, '', '2015-04-30 20:45:51', '2015-04-30 20:45:51', NULL, '0', 12, 1, NULL),
(19, '', '2015-04-30 20:47:57', '2015-04-30 20:47:57', NULL, '0', 12, 1, NULL),
(24, '', '2015-04-30 20:48:32', '2015-04-30 20:48:32', NULL, '0', 12, 1, NULL),
(25, '', '2015-04-30 21:34:48', '2015-04-30 21:34:48', NULL, '0', 12, 1, NULL),
(26, '', '2015-04-30 21:35:19', '2015-04-30 21:35:19', NULL, '0', 12, 1, NULL),
(27, '', '2015-04-30 21:35:56', '2015-04-30 21:35:56', NULL, '0', 12, 1, NULL),
(28, '', '2015-04-30 21:37:11', '2015-04-30 21:37:11', NULL, '0', 12, 1, NULL),
(29, '', '2015-04-30 22:22:00', '2015-04-30 22:22:00', NULL, '0', 12, 1, NULL),
(30, '', '2015-05-07 23:26:53', '2015-05-07 23:26:53', NULL, '0', 12, 1, NULL),
(31, '', '2015-05-07 23:27:19', '2015-05-07 23:27:19', NULL, '0', 12, 1, NULL),
(32, '', '2015-05-07 23:27:57', '2015-05-07 23:27:57', NULL, '0', 12, 1, NULL),
(33, '', '2015-05-07 23:31:06', '2015-05-07 23:31:06', NULL, '0', 12, 1, NULL),
(34, '', '2015-05-07 23:35:03', '2015-05-07 23:35:03', NULL, '0', 27, 1, NULL),
(35, '', '2015-05-08 00:20:29', '2015-05-08 00:27:31', 'main-nav.php', '0', 11, 1, NULL),
(55, '', '2015-05-08 00:58:29', '2015-05-08 00:58:29', NULL, '0', 12, 1, NULL),
(60, '', '2015-05-08 00:59:46', '2015-05-08 00:59:46', NULL, '0', 12, 1, NULL),
(67, '', '2015-05-08 01:01:03', '2015-05-08 01:01:03', NULL, '0', 12, 1, NULL),
(72, '', '2015-05-08 01:01:53', '2015-05-08 01:01:53', NULL, '0', 12, 1, NULL),
(76, '', '2015-05-08 01:02:53', '2015-05-08 01:02:53', NULL, '0', 12, 1, NULL),
(77, '', '2015-05-08 01:02:54', '2015-05-08 01:02:54', NULL, '0', 12, 1, NULL),
(83, '', '2015-05-08 01:03:48', '2015-05-08 01:03:48', NULL, '0', 12, 1, NULL),
(88, '', '2015-05-08 01:04:31', '2015-05-08 01:04:31', NULL, '0', 12, 1, NULL),
(94, '', '2015-05-12 07:34:06', '2015-05-12 07:34:06', NULL, '0', 12, 1, NULL),
(96, '', '2015-05-12 07:46:29', '2015-05-12 07:49:54', 'breadcrumb.php', '0', 11, 1, NULL),
(98, '', '2015-05-12 08:03:32', '2015-05-12 08:03:32', NULL, '0', 24, 1, NULL),
(99, '', '2015-05-12 08:11:37', '2015-05-12 08:11:37', NULL, '0', 17, 1, NULL),
(104, '', '2015-05-12 08:57:03', '2015-05-12 08:57:03', NULL, '1', 12, 1, NULL),
(109, '', '2015-05-12 09:22:39', '2015-05-12 09:22:39', NULL, '0', 12, 1, NULL),
(110, '', '2015-05-12 09:23:28', '2015-05-13 06:57:54', 'main-nav.php', '1', 11, 1, NULL),
(115, '', '2015-05-12 09:54:09', '2015-05-12 09:54:09', NULL, '0', 12, 1, NULL),
(119, '', '2015-05-12 10:12:10', '2015-05-12 10:12:10', NULL, '0', 12, 1, NULL),
(120, '', '2015-05-12 10:14:51', '2015-05-12 10:14:51', NULL, '0', 12, 1, NULL),
(121, '', '2015-05-12 10:18:07', '2015-05-12 10:18:25', 'breadcrumb.php', '0', 11, 1, NULL),
(122, '', '2015-05-12 10:21:20', '2015-05-12 10:21:20', NULL, '0', 18, 1, NULL),
(123, '', '2015-05-12 10:32:09', '2015-05-12 10:32:09', NULL, '0', 12, 1, NULL),
(124, '', '2015-05-12 10:36:20', '2015-05-12 10:36:20', NULL, '0', 11, 1, NULL),
(133, '', '2015-05-12 13:25:54', '2015-05-12 13:25:54', NULL, '0', 12, 1, NULL),
(138, '', '2015-05-12 13:26:42', '2015-05-12 13:26:42', NULL, '0', 12, 1, NULL),
(139, '', '2015-05-13 07:22:26', '2015-05-13 07:33:35', NULL, '0', 12, 1, NULL),
(140, '', '2015-05-13 07:31:32', '2015-05-13 07:31:32', NULL, '0', 12, 1, NULL),
(141, '', '2015-05-13 07:40:30', '2015-05-13 07:42:39', '', '1', 12, 1, NULL),
(142, '', '2015-05-13 07:46:34', '2015-05-13 07:46:34', NULL, '0', 12, 1, NULL),
(143, '', '2015-05-13 08:11:54', '2015-05-13 08:11:55', NULL, '1', 12, 1, NULL),
(144, '', '2015-05-13 08:22:03', '2015-05-13 08:26:14', NULL, '0', 12, 1, NULL),
(145, '', '2015-05-13 08:31:26', '2015-05-13 08:31:26', NULL, '1', 12, 1, NULL),
(146, '', '2015-05-13 08:31:59', '2015-05-13 08:31:59', NULL, '0', 12, 1, NULL),
(184, '', '2015-05-13 08:36:39', '2015-05-13 08:36:39', NULL, '0', 12, 1, NULL),
(185, '', '2015-05-13 08:39:53', '2015-05-13 08:41:13', NULL, '0', 12, 1, NULL),
(186, '', '2015-05-13 08:47:07', '2015-05-13 08:47:07', NULL, '1', 12, 1, NULL),
(187, '', '2015-05-13 08:52:32', '2015-05-13 08:52:32', NULL, '0', 12, 1, NULL),
(188, '', '2015-05-13 08:54:10', '2015-05-13 08:54:10', NULL, '1', 12, 1, NULL),
(189, '', '2015-05-13 08:58:18', '2015-05-13 08:58:18', NULL, '0', 12, 1, NULL),
(190, '', '2015-05-13 09:00:15', '2015-05-13 09:00:15', NULL, '0', 12, 1, NULL),
(191, '', '2015-05-13 09:02:57', '2015-05-13 09:02:57', NULL, '0', 12, 1, NULL),
(192, '', '2015-05-13 09:04:16', '2015-05-13 09:04:16', NULL, '1', 12, 1, NULL),
(197, '', '2015-05-13 09:09:32', '2015-05-13 09:09:32', NULL, '0', 12, 1, NULL),
(202, '', '2015-05-13 09:10:18', '2015-05-13 09:10:18', NULL, '0', 12, 1, NULL),
(203, '', '2015-05-13 09:13:30', '2015-05-13 09:13:30', NULL, '0', 12, 1, NULL),
(204, '', '2015-05-13 09:14:38', '2015-05-13 09:14:38', NULL, '0', 12, 1, NULL),
(206, '', '2015-05-13 09:15:13', '2015-05-13 09:15:13', NULL, '0', 12, 1, NULL),
(209, '', '2015-05-13 09:15:44', '2015-05-13 09:15:44', NULL, '0', 12, 1, NULL),
(213, '', '2015-05-13 09:20:21', '2015-05-13 09:20:21', NULL, '0', 12, 1, NULL),
(214, '', '2015-05-13 09:22:11', '2015-05-13 09:22:11', NULL, '0', 12, 1, NULL),
(225, '', '2015-05-13 09:36:09', '2015-05-13 09:36:09', NULL, '0', 12, 1, NULL),
(241, '', '2015-05-13 09:41:08', '2015-05-13 09:41:08', NULL, '0', 12, 1, NULL),
(242, '', '2015-05-13 09:41:11', '2015-05-13 09:41:11', NULL, '0', 12, 1, NULL),
(243, '', '2015-05-13 09:41:12', '2015-05-13 09:41:12', NULL, '0', 12, 1, NULL),
(245, '', '2015-05-13 09:42:39', '2015-05-13 09:42:39', NULL, '0', 12, 1, NULL),
(247, '', '2015-05-13 10:02:40', '2015-05-13 10:02:40', NULL, '0', 12, 1, NULL),
(254, '', '2015-05-13 10:11:17', '2015-05-13 10:11:17', NULL, '0', 12, 1, NULL),
(255, '', '2015-05-13 10:17:08', '2015-05-13 10:17:08', NULL, '0', 12, 1, NULL),
(256, '', '2015-05-13 10:23:28', '2015-05-13 10:23:28', NULL, '1', 12, 1, NULL),
(257, '', '2015-05-13 10:24:59', '2015-05-13 10:24:59', NULL, '1', 12, 1, NULL),
(258, '', '2015-05-13 10:29:33', '2015-05-13 10:31:21', NULL, '1', 12, 1, NULL),
(265, '', '2015-05-13 10:37:47', '2015-05-13 10:37:47', NULL, '0', 12, 1, NULL),
(266, '', '2015-05-13 10:39:36', '2015-05-13 10:39:55', NULL, '0', 12, 1, NULL),
(267, '', '2015-05-13 10:44:56', '2015-05-13 10:44:56', NULL, '0', 12, 1, NULL),
(268, '', '2015-05-13 11:14:57', '2015-05-13 11:19:38', NULL, '0', 12, 1, NULL),
(269, '', '2015-05-13 11:22:32', '2015-05-13 11:28:21', NULL, '1', 12, 1, NULL),
(271, '', '2015-05-13 11:41:48', '2015-05-13 11:42:52', NULL, '0', 12, 1, NULL),
(272, '', '2015-05-13 11:44:09', '2015-05-13 11:48:54', NULL, '1', 12, 1, NULL),
(274, '', '2015-05-13 11:54:36', '2015-05-13 11:56:43', NULL, '0', 12, 1, NULL),
(275, '', '2015-05-13 11:58:33', '2015-05-13 11:58:53', NULL, '1', 12, 1, NULL),
(276, '', '2015-05-18 12:14:15', '2015-05-18 12:14:15', NULL, '0', 12, 1, NULL),
(279, '', '2015-05-18 12:15:12', '2015-05-18 12:15:12', NULL, '0', 12, 1, NULL),
(300, '', '2015-05-18 12:18:30', '2015-05-18 12:18:30', NULL, '0', 12, 1, NULL),
(301, '', '2015-05-18 12:19:35', '2015-05-18 12:19:35', NULL, '0', 12, 1, NULL),
(302, '', '2015-05-18 12:21:23', '2015-05-18 12:21:23', NULL, '1', 17, 1, NULL),
(311, '', '2015-05-18 12:27:11', '2015-05-18 12:27:11', NULL, '0', 12, 1, NULL),
(317, '', '2015-05-18 12:28:13', '2015-05-18 12:28:13', NULL, '0', 12, 1, NULL),
(323, '', '2015-05-18 12:29:01', '2015-05-18 12:29:01', NULL, '0', 12, 1, NULL),
(329, '', '2015-05-18 12:32:06', '2015-05-18 12:32:06', NULL, '0', 12, 1, NULL),
(331, '', '2015-05-18 12:35:20', '2015-05-18 12:35:20', NULL, '0', 12, 1, NULL),
(332, '', '2015-05-18 12:39:16', '2015-05-18 12:39:16', NULL, '1', 12, 1, NULL),
(333, '', '2015-05-18 12:40:40', '2015-05-18 12:40:40', NULL, '1', 12, 1, NULL),
(334, '', '2015-05-19 08:32:37', '2015-05-19 08:32:37', NULL, '0', 12, 1, NULL),
(341, '', '2015-05-19 08:37:00', '2015-05-19 08:37:00', NULL, '0', 12, 1, NULL),
(342, '', '2015-05-19 08:43:48', '2015-05-19 08:43:48', NULL, '0', 12, 1, NULL),
(343, '', '2015-05-19 08:46:17', '2015-05-19 08:46:17', NULL, '0', 12, 1, NULL),
(350, '', '2015-05-19 08:48:23', '2015-05-19 08:48:23', NULL, '0', 12, 1, NULL),
(351, '', '2015-05-19 08:50:39', '2015-05-19 08:50:39', NULL, '0', 12, 1, NULL),
(352, '', '2015-05-19 08:52:18', '2015-05-19 08:52:18', NULL, '1', 12, 1, NULL),
(353, '', '2015-05-19 09:00:41', '2015-05-19 09:01:24', NULL, '1', 12, 1, NULL),
(354, '', '2015-05-19 09:02:13', '2015-05-19 09:02:13', NULL, '1', 12, 1, NULL),
(355, '', '2015-05-19 09:03:28', '2015-05-19 09:05:16', NULL, '1', 12, 1, NULL),
(356, '', '2015-05-19 09:04:38', '2015-05-19 09:05:00', NULL, '1', 12, 1, NULL),
(357, '', '2015-05-19 09:27:35', '2015-05-19 09:27:48', NULL, '1', 12, 1, NULL),
(358, '', '2015-05-19 10:14:50', '2015-05-19 10:14:51', NULL, '1', 12, 1, NULL),
(359, '', '2015-05-19 10:16:15', '2015-05-19 10:16:15', NULL, '1', 12, 1, NULL),
(360, '', '2015-05-19 10:17:06', '2015-05-19 10:17:40', NULL, '1', 12, 1, NULL),
(361, '', '2015-05-19 12:47:35', '2015-05-19 12:47:36', '', '1', 12, 1, NULL),
(362, '', '2015-05-19 12:48:28', '2015-05-19 12:48:28', NULL, '0', 12, 1, NULL),
(363, '', '2015-05-19 12:49:26', '2015-05-19 12:49:26', NULL, '0', 12, 1, NULL),
(364, '', '2015-05-19 12:50:12', '2015-05-19 12:50:12', NULL, '0', 12, 1, NULL),
(365, '', '2015-05-19 12:50:40', '2015-05-19 12:50:40', NULL, '0', 12, 1, NULL),
(366, '', '2015-05-19 12:50:57', '2015-05-19 12:50:57', NULL, '0', 12, 1, NULL),
(367, '', '2015-05-19 12:51:24', '2015-05-19 12:51:24', NULL, '1', 12, 1, NULL),
(368, '', '2015-05-19 12:54:30', '2015-05-19 12:54:30', NULL, '0', 12, 1, NULL),
(369, '', '2015-05-19 12:59:10', '2015-05-19 13:34:55', '', '1', 12, 1, NULL),
(370, '', '2015-05-19 13:02:31', '2015-05-19 13:02:31', NULL, '0', 27, 1, NULL),
(371, '', '2015-05-19 13:03:55', '2015-05-19 13:03:55', NULL, '0', 12, 1, NULL),
(372, '', '2015-05-19 13:35:52', '2015-05-19 13:35:53', NULL, '1', 12, 1, NULL),
(373, '', '2015-05-19 13:36:28', '2015-05-19 13:36:28', NULL, '1', 12, 1, NULL),
(374, '', '2015-05-19 13:37:09', '2015-05-19 13:37:09', NULL, '1', 12, 1, NULL),
(375, '', '2015-05-19 13:38:10', '2015-05-19 13:38:10', NULL, '1', 12, 1, NULL),
(376, '', '2015-05-19 13:42:47', '2015-05-19 13:42:47', NULL, '0', 26, 1, NULL),
(377, '', '2015-05-19 13:43:02', '2015-05-19 13:43:02', NULL, '0', 26, 1, NULL),
(378, '', '2015-05-19 13:43:18', '2015-05-19 13:43:18', NULL, '0', 26, 1, NULL),
(379, '', '2015-05-19 13:43:29', '2015-05-19 13:43:29', NULL, '0', 26, 1, NULL),
(380, '', '2015-05-19 13:43:38', '2015-05-19 13:43:38', NULL, '0', 26, 1, NULL),
(382, '', '2015-05-19 13:58:32', '2015-05-19 13:58:32', NULL, '0', 5, 1, NULL),
(383, '', '2015-05-19 14:19:26', '2015-05-19 14:19:37', NULL, '1', 5, 1, NULL),
(384, '', '2015-05-19 15:52:38', '2015-05-19 16:25:03', 'review.php', '0', 5, 1, NULL),
(385, '', '2015-05-19 16:46:49', '2015-05-19 16:47:08', NULL, '0', 12, 1, NULL),
(387, '', '2015-05-20 06:10:25', '2015-05-20 06:12:19', NULL, '1', 12, 1, NULL),
(388, '', '2015-05-20 06:20:50', '2015-05-20 06:20:50', NULL, '0', 12, 1, NULL),
(389, '', '2015-05-20 06:23:58', '2015-05-20 06:23:58', NULL, '0', 24, 1, NULL),
(390, '', '2015-05-20 06:24:16', '2015-05-20 06:24:16', NULL, '1', 12, 1, NULL),
(391, '', '2015-05-20 06:32:00', '2015-05-20 06:32:00', NULL, '0', 12, 1, NULL),
(398, '', '2015-05-20 07:04:07', '2015-05-20 07:04:07', NULL, '0', 12, 1, NULL),
(401, '', '2015-05-20 07:04:20', '2015-05-20 07:04:20', NULL, '0', 12, 1, NULL),
(404, '', '2015-05-20 07:09:41', '2015-05-20 07:09:41', NULL, '0', 17, 1, NULL),
(406, '', '2015-05-20 07:10:23', '2015-05-20 07:10:23', NULL, '0', 12, 1, NULL),
(407, '', '2015-05-20 07:10:24', '2015-05-20 07:10:24', NULL, '0', 12, 1, NULL),
(408, '', '2015-05-20 07:13:44', '2015-05-20 07:13:45', NULL, '1', 12, 1, NULL),
(409, '', '2015-05-20 07:14:19', '2015-05-20 07:14:19', NULL, '0', 12, 1, NULL),
(410, '', '2015-05-20 07:14:30', '2015-05-20 07:25:03', '', '0', 17, 1, NULL),
(411, '', '2015-05-20 07:15:32', '2015-05-20 07:15:50', NULL, '0', 27, 1, NULL),
(412, '', '2015-05-20 07:17:23', '2015-05-20 07:17:23', NULL, '1', 27, 1, NULL),
(413, '', '2015-05-20 09:25:38', '2015-05-20 09:25:59', NULL, '0', 12, 1, NULL),
(414, '', '2015-05-20 10:26:29', '2015-05-20 10:26:29', '', '1', 17, 1, NULL),
(415, '', '2015-05-20 10:28:33', '2015-05-20 10:29:24', NULL, '0', 12, 1, NULL),
(416, '', '2015-05-20 10:31:56', '2015-05-20 10:31:56', NULL, '0', 12, 1, NULL),
(417, '', '2015-05-20 10:35:51', '2015-05-20 10:35:51', NULL, '1', 12, 1, NULL),
(418, '', '2015-05-20 10:36:34', '2015-05-20 10:36:34', NULL, '1', 12, 1, NULL),
(419, '', '2015-05-20 10:39:14', '2015-05-20 10:39:14', NULL, '1', 12, 1, NULL),
(420, '', '2015-05-20 10:41:30', '2015-05-20 10:41:30', NULL, '1', 12, 1, NULL),
(421, '', '2015-05-20 10:44:04', '2015-05-20 10:44:04', NULL, '1', 12, 1, NULL),
(422, '', '2015-05-20 11:01:41', '2015-05-20 11:02:01', NULL, '1', 12, 1, NULL),
(423, '', '2015-05-20 11:08:53', '2015-05-20 11:08:53', NULL, '1', 12, 1, NULL),
(424, '', '2015-05-20 11:17:42', '2015-05-20 11:17:42', NULL, '1', 12, 1, NULL),
(425, '', '2015-05-20 11:19:11', '2015-05-20 11:23:09', '', '1', 17, 1, NULL),
(426, '', '2015-05-20 11:19:52', '2015-05-20 11:19:52', NULL, '1', 12, 1, NULL),
(427, '', '2015-05-22 10:36:10', '2015-05-22 10:36:10', NULL, '1', 12, 1, NULL),
(428, '', '2015-05-22 11:41:51', '2015-05-22 11:41:51', NULL, '0', 12, 1, NULL),
(430, '', '2015-05-22 11:42:10', '2015-05-22 11:42:10', NULL, '0', 12, 1, NULL),
(431, '', '2015-05-22 11:45:54', '2015-05-22 11:49:05', NULL, '0', 12, 1, NULL),
(438, '', '2015-05-22 12:02:33', '2015-05-22 12:02:33', NULL, '0', 12, 1, NULL),
(439, '', '2015-05-22 12:05:15', '2015-05-22 12:05:15', NULL, '0', 12, 1, NULL),
(440, '', '2015-05-22 12:08:33', '2015-05-22 12:41:53', NULL, '1', 12, 1, NULL),
(441, '', '2015-05-22 13:13:04', '2015-05-22 13:17:14', '', '1', 17, 1, NULL),
(442, '', '2015-05-22 13:43:12', '2015-05-22 13:43:13', NULL, '1', 12, 1, NULL),
(443, '', '2015-05-22 13:46:08', '2015-05-22 13:46:08', NULL, '1', 12, 1, NULL),
(449, '', '2015-05-22 13:48:56', '2015-05-22 13:48:56', NULL, '0', 12, 1, NULL),
(450, '', '2015-05-22 13:49:51', '2015-05-22 13:49:51', NULL, '0', 12, 1, NULL),
(451, '', '2015-05-22 13:51:25', '2015-05-22 13:51:26', NULL, '1', 12, 1, NULL),
(452, '', '2015-05-22 13:52:53', '2015-05-22 13:52:53', NULL, '1', 12, 1, NULL),
(453, '', '2015-05-22 13:55:21', '2015-05-22 13:55:21', NULL, '1', 12, 1, NULL),
(454, '', '2015-05-23 14:10:36', '2015-05-23 14:10:36', NULL, '0', 27, 1, NULL),
(455, '', '2015-05-23 15:42:01', '2015-05-23 15:43:21', NULL, '0', 27, 1, NULL),
(467, '', '2015-05-25 08:17:14', '2015-05-25 08:17:14', NULL, '0', 12, 1, NULL),
(468, '', '2015-05-26 08:24:23', '2015-05-26 08:24:24', NULL, '1', 27, 1, NULL),
(469, '', '2015-05-26 08:29:35', '2015-05-26 08:29:35', 'lightbox', '0', 39, 1, NULL),
(470, '', '2015-05-26 09:11:57', '2015-05-26 09:11:57', NULL, '0', 12, 1, NULL),
(471, '', '2015-05-26 09:13:55', '2015-05-26 09:13:55', NULL, '0', 12, 1, NULL),
(472, '', '2015-05-26 09:53:21', '2015-05-26 09:53:21', NULL, '1', 12, 1, NULL),
(473, '', '2015-05-26 10:16:42', '2015-05-26 10:16:42', NULL, '0', 12, 1, NULL),
(474, '', '2015-05-26 10:20:07', '2015-05-26 10:20:08', NULL, '1', 12, 1, NULL),
(475, '', '2015-05-26 10:20:38', '2015-05-26 10:20:38', NULL, '0', 12, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `CollectionAttributeValues`
--

CREATE TABLE IF NOT EXISTS `CollectionAttributeValues` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvID` int(10) unsigned NOT NULL DEFAULT '0',
  `akID` int(10) unsigned NOT NULL DEFAULT '0',
  `avID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`cID`,`cvID`,`akID`),
  KEY `akID` (`akID`),
  KEY `avID` (`avID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `CollectionAttributeValues`
--

INSERT INTO `CollectionAttributeValues` (`cID`, `cvID`, `akID`, `avID`) VALUES
(2, 1, 4, 1),
(3, 1, 3, 2),
(4, 1, 3, 3),
(5, 1, 3, 4),
(6, 1, 3, 5),
(8, 1, 3, 6),
(9, 1, 3, 7),
(10, 1, 3, 8),
(11, 1, 5, 9),
(11, 1, 3, 10),
(12, 1, 3, 11),
(13, 1, 3, 12),
(14, 1, 3, 13),
(15, 1, 3, 14),
(16, 1, 3, 15),
(16, 1, 5, 16),
(17, 1, 3, 17),
(17, 1, 5, 18),
(19, 1, 3, 19),
(20, 1, 3, 20),
(22, 1, 3, 21),
(23, 1, 3, 22),
(24, 1, 3, 23),
(25, 1, 3, 24),
(26, 1, 3, 25),
(28, 1, 3, 26),
(29, 1, 3, 27),
(29, 1, 5, 28),
(31, 1, 5, 29),
(32, 1, 5, 30),
(33, 1, 5, 31),
(34, 1, 5, 32),
(35, 1, 5, 33),
(36, 1, 5, 34),
(38, 1, 5, 35),
(39, 1, 3, 36),
(40, 1, 3, 37),
(41, 1, 3, 38),
(43, 1, 4, 39),
(44, 1, 3, 40),
(48, 1, 3, 41),
(50, 1, 5, 42),
(50, 1, 10, 43),
(50, 1, 3, 44),
(51, 1, 3, 45),
(52, 1, 3, 46),
(53, 1, 5, 47),
(54, 1, 3, 48),
(55, 1, 3, 49),
(56, 1, 3, 50),
(56, 1, 5, 51),
(57, 1, 3, 52),
(58, 1, 3, 53),
(59, 1, 3, 54),
(61, 1, 3, 55),
(62, 1, 3, 56),
(63, 1, 3, 57),
(64, 1, 3, 58),
(65, 1, 3, 59),
(66, 1, 3, 60),
(67, 1, 3, 61),
(68, 1, 3, 62),
(73, 1, 3, 63),
(74, 1, 3, 64),
(75, 1, 3, 65),
(76, 1, 3, 66),
(77, 1, 3, 67),
(79, 1, 3, 68),
(80, 1, 3, 69),
(81, 1, 3, 70),
(82, 1, 3, 71),
(84, 1, 3, 72),
(85, 1, 3, 73),
(86, 1, 3, 74),
(87, 1, 3, 75),
(89, 1, 3, 76),
(90, 1, 3, 77),
(93, 1, 3, 78),
(94, 1, 3, 79),
(95, 1, 3, 80),
(96, 1, 3, 81),
(98, 1, 3, 82),
(99, 1, 3, 83),
(100, 1, 3, 84),
(101, 1, 3, 85),
(102, 1, 3, 86),
(103, 1, 3, 87),
(104, 1, 3, 88),
(105, 1, 3, 89),
(106, 1, 3, 90),
(107, 1, 3, 91),
(108, 1, 3, 92),
(109, 1, 3, 93),
(111, 1, 3, 94),
(112, 1, 3, 95),
(113, 1, 3, 96),
(114, 1, 3, 97),
(116, 1, 10, 98),
(117, 1, 3, 99),
(118, 1, 3, 100),
(119, 1, 3, 101),
(120, 1, 3, 102),
(121, 1, 3, 103),
(123, 1, 3, 104),
(124, 1, 5, 105),
(125, 1, 5, 106),
(125, 1, 10, 107),
(126, 1, 4, 108),
(127, 1, 4, 109),
(128, 1, 4, 110),
(131, 1, 4, 111),
(1, 8, 1, 114),
(1, 9, 1, 114),
(1, 8, 2, 115),
(1, 9, 2, 115),
(1, 8, 3, 116),
(1, 9, 3, 116),
(1, 8, 7, 117),
(1, 9, 7, 117),
(1, 8, 11, 118),
(1, 9, 11, 118),
(152, 2, 1, 119),
(152, 2, 2, 120),
(152, 2, 3, 121),
(152, 2, 7, 122),
(152, 2, 11, 123),
(184, 3, 5, 126),
(184, 3, 6, 127),
(184, 3, 10, 128),
(1, 10, 1, 129),
(1, 11, 1, 129),
(1, 12, 1, 129),
(1, 13, 1, 129),
(1, 14, 1, 129),
(1, 10, 2, 130),
(1, 11, 2, 130),
(1, 12, 2, 130),
(1, 13, 2, 130),
(1, 14, 2, 130),
(1, 10, 3, 131),
(1, 11, 3, 131),
(1, 12, 3, 131),
(1, 13, 3, 131),
(1, 14, 3, 131),
(1, 10, 7, 132),
(1, 11, 7, 132),
(1, 12, 7, 132),
(1, 13, 7, 132),
(1, 14, 7, 132),
(1, 10, 11, 133),
(1, 11, 11, 133),
(1, 12, 11, 133),
(1, 13, 11, 133),
(1, 14, 11, 133),
(1, 10, 6, 134),
(1, 11, 6, 134),
(1, 12, 6, 134),
(1, 13, 6, 134),
(1, 14, 6, 134),
(1, 15, 6, 134),
(1, 16, 6, 134),
(1, 17, 6, 134),
(1, 18, 6, 134),
(1, 19, 6, 134),
(1, 20, 6, 134),
(1, 21, 6, 134),
(1, 22, 6, 134),
(1, 23, 6, 134),
(1, 24, 6, 134),
(1, 25, 6, 134),
(1, 26, 6, 134),
(1, 27, 6, 134),
(1, 28, 6, 134),
(1, 10, 5, 135),
(1, 11, 5, 135),
(1, 12, 5, 135),
(1, 13, 5, 135),
(1, 14, 5, 135),
(1, 15, 5, 135),
(1, 16, 5, 135),
(1, 17, 5, 135),
(1, 18, 5, 135),
(1, 19, 5, 135),
(1, 20, 5, 135),
(1, 21, 5, 135),
(1, 22, 5, 135),
(1, 23, 5, 135),
(1, 24, 5, 135),
(1, 25, 5, 135),
(1, 26, 5, 135),
(1, 27, 5, 135),
(1, 28, 5, 135),
(162, 3, 5, 136),
(162, 4, 5, 137),
(162, 4, 6, 138),
(186, 2, 5, 139),
(162, 5, 5, 140),
(162, 5, 6, 141),
(148, 3, 1, 142),
(148, 3, 2, 143),
(148, 3, 3, 144),
(148, 3, 7, 145),
(148, 3, 11, 146),
(169, 4, 1, 147),
(169, 5, 1, 147),
(169, 6, 1, 147),
(169, 7, 1, 147),
(169, 8, 1, 147),
(169, 9, 1, 147),
(169, 4, 2, 148),
(169, 5, 2, 148),
(169, 6, 2, 148),
(169, 7, 2, 148),
(169, 8, 2, 148),
(169, 9, 2, 148),
(169, 4, 3, 149),
(169, 5, 3, 149),
(169, 6, 3, 149),
(169, 7, 3, 149),
(169, 8, 3, 149),
(169, 9, 3, 149),
(169, 4, 7, 150),
(169, 5, 7, 150),
(169, 6, 7, 150),
(169, 7, 7, 150),
(169, 8, 7, 150),
(169, 9, 7, 150),
(169, 4, 11, 151),
(169, 5, 11, 151),
(169, 6, 11, 151),
(169, 7, 11, 151),
(169, 8, 11, 151),
(169, 9, 11, 151),
(170, 2, 1, 152),
(170, 3, 1, 152),
(170, 4, 1, 152),
(170, 5, 1, 152),
(170, 6, 1, 152),
(170, 7, 1, 152),
(170, 2, 2, 153),
(170, 3, 2, 153),
(170, 4, 2, 153),
(170, 5, 2, 153),
(170, 6, 2, 153),
(170, 7, 2, 153),
(170, 2, 3, 154),
(170, 3, 3, 154),
(170, 4, 3, 154),
(170, 5, 3, 154),
(170, 6, 3, 154),
(170, 7, 3, 154),
(170, 2, 7, 155),
(170, 3, 7, 155),
(170, 4, 7, 155),
(170, 5, 7, 155),
(170, 6, 7, 155),
(170, 7, 7, 155),
(170, 2, 11, 156),
(170, 3, 11, 156),
(170, 4, 11, 156),
(170, 5, 11, 156),
(170, 6, 11, 156),
(170, 7, 11, 156),
(159, 4, 1, 157),
(159, 5, 1, 157),
(159, 4, 2, 158),
(159, 5, 2, 158),
(159, 4, 3, 159),
(159, 5, 3, 159),
(159, 4, 7, 160),
(159, 5, 7, 160),
(159, 4, 11, 161),
(159, 5, 11, 161),
(171, 9, 1, 162),
(171, 9, 2, 163),
(171, 9, 3, 164),
(171, 9, 7, 165),
(171, 9, 11, 166),
(175, 5, 1, 167),
(175, 6, 1, 167),
(175, 7, 1, 167),
(175, 5, 2, 168),
(175, 6, 2, 168),
(175, 7, 2, 168),
(175, 5, 3, 169),
(175, 6, 3, 169),
(175, 7, 3, 169),
(175, 5, 7, 170),
(175, 6, 7, 170),
(175, 7, 7, 170),
(175, 5, 11, 171),
(175, 6, 11, 171),
(175, 7, 11, 171),
(178, 3, 1, 172),
(178, 4, 1, 172),
(178, 3, 2, 173),
(178, 4, 2, 173),
(178, 3, 3, 174),
(178, 4, 3, 174),
(178, 3, 7, 175),
(178, 4, 7, 175),
(178, 3, 11, 176),
(178, 4, 11, 176),
(148, 4, 1, 177),
(148, 4, 2, 178),
(148, 4, 3, 179),
(148, 4, 7, 180),
(148, 4, 11, 181),
(148, 5, 1, 182),
(148, 6, 1, 182),
(148, 7, 1, 182),
(148, 5, 2, 183),
(148, 6, 2, 183),
(148, 7, 2, 183),
(148, 5, 3, 184),
(148, 6, 3, 184),
(148, 7, 3, 184),
(148, 5, 7, 185),
(148, 6, 7, 185),
(148, 7, 7, 185),
(148, 5, 11, 186),
(148, 6, 11, 186),
(148, 7, 11, 186),
(1, 15, 1, 187),
(1, 15, 2, 188),
(1, 15, 3, 189),
(1, 15, 7, 190),
(1, 15, 11, 191),
(1, 16, 1, 192),
(1, 16, 2, 193),
(1, 16, 3, 194),
(1, 16, 7, 195),
(1, 16, 11, 196),
(1, 17, 1, 197),
(1, 18, 1, 197),
(1, 19, 1, 197),
(1, 20, 1, 197),
(1, 21, 1, 197),
(1, 22, 1, 197),
(1, 17, 2, 198),
(1, 18, 2, 198),
(1, 19, 2, 198),
(1, 20, 2, 198),
(1, 21, 2, 198),
(1, 22, 2, 198),
(1, 17, 3, 199),
(1, 18, 3, 199),
(1, 19, 3, 199),
(1, 20, 3, 199),
(1, 21, 3, 199),
(1, 22, 3, 199),
(1, 17, 7, 200),
(1, 18, 7, 200),
(1, 19, 7, 200),
(1, 20, 7, 200),
(1, 21, 7, 200),
(1, 22, 7, 200),
(1, 17, 11, 201),
(1, 18, 11, 201),
(1, 19, 11, 201),
(1, 20, 11, 201),
(1, 21, 11, 201),
(1, 22, 11, 201),
(161, 8, 1, 202),
(161, 9, 1, 202),
(161, 10, 1, 202),
(161, 8, 2, 203),
(161, 9, 2, 203),
(161, 10, 2, 203),
(161, 8, 3, 204),
(161, 9, 3, 204),
(161, 10, 3, 204),
(161, 8, 7, 205),
(161, 9, 7, 205),
(161, 10, 7, 205),
(161, 8, 11, 206),
(161, 9, 11, 206),
(161, 10, 11, 206),
(190, 1, 3, 217),
(164, 10, 1, 218),
(164, 10, 2, 219),
(164, 10, 3, 220),
(164, 10, 7, 221),
(164, 10, 11, 222),
(148, 8, 1, 225),
(148, 8, 2, 226),
(148, 8, 3, 227),
(148, 8, 7, 228),
(148, 8, 11, 229),
(1, 23, 1, 230),
(1, 24, 1, 230),
(1, 25, 1, 230),
(1, 26, 1, 230),
(1, 27, 1, 230),
(1, 28, 1, 230),
(1, 23, 2, 231),
(1, 24, 2, 231),
(1, 25, 2, 231),
(1, 26, 2, 231),
(1, 27, 2, 231),
(1, 28, 2, 231),
(1, 23, 3, 232),
(1, 24, 3, 232),
(1, 25, 3, 232),
(1, 26, 3, 232),
(1, 27, 3, 232),
(1, 28, 3, 232),
(1, 23, 7, 233),
(1, 24, 7, 233),
(1, 25, 7, 233),
(1, 26, 7, 233),
(1, 27, 7, 233),
(1, 28, 7, 233),
(1, 23, 11, 234),
(1, 24, 11, 234),
(1, 25, 11, 234),
(1, 26, 11, 234),
(1, 27, 11, 234),
(1, 28, 11, 234),
(192, 5, 1, 237),
(192, 5, 2, 238),
(192, 5, 3, 239),
(192, 5, 7, 240),
(192, 5, 11, 241);

-- --------------------------------------------------------

--
-- Table structure for table `CollectionSearchIndexAttributes`
--

CREATE TABLE IF NOT EXISTS `CollectionSearchIndexAttributes` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `ak_meta_title` longtext COLLATE utf8_unicode_ci,
  `ak_meta_description` longtext COLLATE utf8_unicode_ci,
  `ak_meta_keywords` longtext COLLATE utf8_unicode_ci,
  `ak_icon_dashboard` longtext COLLATE utf8_unicode_ci,
  `ak_exclude_nav` tinyint(1) DEFAULT '0',
  `ak_exclude_page_list` tinyint(1) DEFAULT '0',
  `ak_header_extra_content` longtext COLLATE utf8_unicode_ci,
  `ak_tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ak_is_featured` tinyint(1) DEFAULT '0',
  `ak_exclude_search_index` tinyint(1) DEFAULT '0',
  `ak_exclude_sitemapxml` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`cID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `CollectionSearchIndexAttributes`
--

INSERT INTO `CollectionSearchIndexAttributes` (`cID`, `ak_meta_title`, `ak_meta_description`, `ak_meta_keywords`, `ak_icon_dashboard`, `ak_exclude_nav`, `ak_exclude_page_list`, `ak_header_extra_content`, `ak_tags`, `ak_is_featured`, `ak_exclude_search_index`, `ak_exclude_sitemapxml`) VALUES
(1, 'Diers Klinik - fertilitetsklinik i Aarhus', 'Vi tilbyder insemination og ultralydsscanninger til par, singler og lesbiske ', 'Meta nøgleord', NULL, 1, 1, '', NULL, 0, 0, 0),
(2, NULL, NULL, NULL, 'fa fa-th-large', 0, 0, NULL, NULL, 0, 0, 0),
(3, NULL, NULL, 'pages, add page, delete page, copy, move, alias', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(4, NULL, NULL, 'pages, add page, delete page, copy, move, alias', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(5, NULL, NULL, 'pages, add page, delete page, copy, move, alias, bulk', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(6, NULL, NULL, 'find page, search page, search, find, pages, sitemap', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(8, NULL, NULL, 'add file, delete file, copy, move, alias, resize, crop, rename, images, title, attribute', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(9, NULL, NULL, 'file, file attributes, title, attribute, description, rename', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(10, NULL, NULL, 'files, category, categories', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(11, NULL, NULL, 'new file set', NULL, 1, 0, NULL, NULL, 0, 0, 0),
(12, NULL, NULL, 'users, groups, people, find, delete user, remove user, change password, password', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(13, NULL, NULL, 'find, search, people, delete user, remove user, change password, password', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(14, NULL, NULL, 'user, group, people, permissions, expire, badges', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(15, NULL, NULL, 'user attributes, user data, gather data, registration data', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(16, NULL, NULL, 'new user, create', NULL, 1, 0, NULL, NULL, 0, 0, 0),
(17, NULL, NULL, 'new user group, new group, group, create', NULL, 1, 0, NULL, NULL, 0, 0, 0),
(19, NULL, NULL, 'group set', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(20, NULL, NULL, 'community, points, karma', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(22, NULL, NULL, 'action, community actions', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(23, NULL, NULL, 'forms, log, error, email, mysql, exception, survey', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(24, NULL, NULL, 'forms, questions, response, data', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(25, NULL, NULL, 'questions, quiz, response', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(26, NULL, NULL, 'forms, log, error, email, mysql, exception, survey, history', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(28, NULL, NULL, 'new theme, theme, active theme, change theme, template, css', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(29, NULL, NULL, 'page types', NULL, 1, 0, NULL, NULL, 0, 0, 0),
(31, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(32, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(33, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(34, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(35, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(36, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(38, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(39, NULL, NULL, 'page attributes, custom', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(40, NULL, NULL, 'single, page, custom, application', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(41, NULL, NULL, 'atom, rss, feed, syndication', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(43, NULL, NULL, NULL, 'icon-bullhorn', 0, 0, NULL, NULL, 0, 0, 0),
(44, NULL, NULL, 'add workflow, remove workflow', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(48, NULL, NULL, 'stacks, reusable content, scrapbook, copy, paste, paste block, copy block, site name, logo', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(50, NULL, NULL, 'edit stacks, view stacks, all stacks', NULL, 1, 0, NULL, NULL, 0, 1, 0),
(51, NULL, NULL, 'block, refresh, custom', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(52, NULL, NULL, 'add-on, addon, add on, package, app, ecommerce, discussions, forums, themes, templates, blocks', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(53, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(54, NULL, NULL, 'add-on, addon, ecommerce, install, discussions, forums, themes, templates, blocks', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(55, NULL, NULL, 'update, upgrade', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(56, NULL, NULL, 'concrete5.org, my account, marketplace', NULL, 1, 0, NULL, NULL, 0, 0, 0),
(57, NULL, NULL, 'buy theme, new theme, marketplace, template', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(58, NULL, NULL, 'buy addon, buy add on, buy add-on, purchase addon, purchase add on, purchase add-on, find addon, new addon, marketplace', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(59, NULL, NULL, 'dashboard, configuration', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(61, NULL, NULL, 'website name, title', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(62, NULL, NULL, 'accessibility, easy mode', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(63, NULL, NULL, 'sharing, facebook, twitter', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(64, NULL, NULL, 'logo, favicon, iphone, icon, bookmark', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(65, NULL, NULL, 'tinymce, content block, fonts, editor, content, overlay', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(66, NULL, NULL, 'translate, translation, internationalization, multilingual', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(67, NULL, NULL, 'timezone, profile, locale', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(68, NULL, NULL, 'multilingual, localization, internationalization, i18n', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(73, NULL, NULL, 'vanity, pretty url, seo, pageview, view', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(74, NULL, NULL, 'bulk, seo, change keywords, engine, optimization, search', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(75, NULL, NULL, 'traffic, statistics, google analytics, quant, pageviews, hits', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(76, NULL, NULL, 'pretty, slug', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(77, NULL, NULL, 'configure search, site search, search option', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(79, NULL, NULL, 'file options, file manager, upload, modify', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(80, NULL, NULL, 'security, files, media, extension, manager, upload', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(81, NULL, NULL, 'images, picture, responsive, retina', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(82, NULL, NULL, 'security, alternate storage, hide files', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(84, NULL, NULL, 'cache option, change cache, override, turn on cache, turn off cache, no cache, page cache, caching', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(85, NULL, NULL, 'cache option, turn off cache, no cache, page cache, caching', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(86, NULL, NULL, 'index search, reindex search, build sitemap, sitemap.xml, clear old versions, page versions, remove old', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(87, NULL, NULL, 'queries, database, mysql', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(89, NULL, NULL, 'editors, hide site, offline, private, public, access', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(90, NULL, NULL, 'security, actions, administrator, admin, package, marketplace, search', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(93, NULL, NULL, 'security, lock ip, lock out, block ip, address, restrict, access', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(94, NULL, NULL, 'security, registration', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(95, NULL, NULL, 'antispam, block spam, security', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(96, NULL, NULL, 'lock site, under construction, hide, hidden', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(98, NULL, NULL, 'profile, login, redirect, specific, dashboard, administrators', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(99, NULL, NULL, 'member profile, member page, community, forums, social, avatar', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(100, NULL, NULL, 'signup, new user, community, public registration, public, registration', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(101, NULL, NULL, 'auth, authentication, types, oauth, facebook, login, registration', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(102, NULL, NULL, 'smtp, mail settings', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(103, NULL, NULL, 'email server, mail settings, mail configuration, external, internal', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(104, NULL, NULL, 'test smtp, test mail', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(105, NULL, NULL, 'email server, mail settings, mail configuration, private message, message system, import, email, message', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(106, NULL, NULL, 'conversations', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(107, NULL, NULL, 'conversations', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(108, NULL, NULL, 'conversations ratings, ratings, community, community points', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(109, NULL, NULL, 'conversations bad words, banned words, banned, bad words, bad, words, list', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(111, NULL, NULL, 'attribute configuration', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(112, NULL, NULL, 'attributes, sets', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(113, NULL, NULL, 'attributes, types', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(114, NULL, NULL, 'topics, tags, taxonomy', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(116, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 1, 0),
(117, NULL, NULL, 'overrides, system info, debug, support, help', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(118, NULL, NULL, 'errors, exceptions, develop, support, help', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(119, NULL, NULL, 'email, logging, logs, smtp, pop, errors, mysql, log', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(120, NULL, NULL, 'network, proxy server', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(121, NULL, NULL, 'export, backup, database, sql, mysql, encryption, restore', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(123, NULL, NULL, 'upgrade, new version, update', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(124, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(125, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 1, 0),
(126, NULL, NULL, NULL, 'fa fa-edit', 0, 0, NULL, NULL, 0, 0, 0),
(127, NULL, NULL, NULL, 'fa fa-trash-o', 0, 0, NULL, NULL, 0, 0, 0),
(128, NULL, NULL, NULL, 'fa fa-th', 0, 0, NULL, NULL, 0, 0, 0),
(131, NULL, NULL, NULL, 'fa fa-briefcase', 0, 0, NULL, NULL, 0, 0, 0),
(148, 'Diers Klinik - fertilitetsklinik i Aarhus', 'Diers Klinik tilbyder insemination og ultralydsscaning til par, singler og lesbiske på fertilitetsklinikken i Aarhus', '', NULL, 0, 0, '', NULL, 0, 0, 0),
(152, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(153, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(156, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(157, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(158, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(159, 'Diers Klinik er specialister i insemination', 'Vi kan lave insemination med både donorsæd eller med din partners sæd', '', NULL, 0, 0, '', NULL, 0, 0, 0),
(161, 'Insemination med både anonyme og kendte sæddonorer', 'I Diers Klinik har du flere muligheder, hvad angår valg af sæddonor.', '', NULL, 0, 0, '', NULL, 0, 0, 0),
(162, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0),
(163, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(164, 'Priser for insemination, donor og ultralydsscanning', 'Diers Klinik har konkurrencedygtige priser for insemination. Her kommer ingen ekstra gebyrer', '', NULL, 0, 0, '', NULL, 0, 0, 0),
(165, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(166, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(167, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(169, 'Diers klinik har erfarne jordemødre og sygeplejersker', 'Vores personale har stor erfaring med insemination og ultarlydsscanninger', '', NULL, 0, 0, '', NULL, 0, 0, 0),
(170, 'Diers Klinik ligger i toppen med insemination', 'Hos Diers Klinik har opnået nogle af Danmarks bedste succesrater for graviditeter', '', NULL, 0, 0, '', NULL, 0, 0, 0),
(171, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(172, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(173, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(174, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(175, 'Trygge rammer for din insemination', 'Inden du kan få foretaget kunstig befrugtning på Diers Klinik, skal du have været til en journalsamtale hos os. ', '', NULL, 0, 0, '', NULL, 0, 0, 0),
(176, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(178, 'Sådan bliver du lettest gravid', 'Du kan selv øge chancerne for at blive gravid og vi hjælper dig på vej', '', NULL, 0, 0, '', NULL, 0, 0, 0),
(179, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(180, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(181, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(184, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, 0, 1, 0),
(185, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(186, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(187, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(188, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(189, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(190, NULL, NULL, 'database, entities, doctrine, orm', NULL, 0, 0, NULL, NULL, 0, 0, 0),
(191, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(192, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0),
(193, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `CollectionVersionAreaStyles`
--

CREATE TABLE IF NOT EXISTS `CollectionVersionAreaStyles` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvID` int(10) unsigned NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `issID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`cvID`,`arHandle`),
  KEY `issID` (`issID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `CollectionVersionAreaStyles`
--

INSERT INTO `CollectionVersionAreaStyles` (`cID`, `cvID`, `arHandle`, `issID`) VALUES
(171, 6, 'Sidebar productgroup', 7),
(171, 7, 'Sidebar productgroup', 7),
(171, 8, 'Sidebar productgroup', 7),
(171, 9, 'Sidebar productgroup', 7);

-- --------------------------------------------------------

--
-- Table structure for table `CollectionVersionBlockStyles`
--

CREATE TABLE IF NOT EXISTS `CollectionVersionBlockStyles` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvID` int(10) unsigned NOT NULL DEFAULT '0',
  `bID` int(10) unsigned NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `issID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`cvID`,`bID`,`arHandle`),
  KEY `bID` (`bID`,`issID`),
  KEY `issID` (`issID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `CollectionVersionBlockStyles`
--

INSERT INTO `CollectionVersionBlockStyles` (`cID`, `cvID`, `bID`, `arHandle`, `issID`) VALUES
(158, 3, 35, 'Main', 1),
(158, 4, 110, 'Main', 1),
(167, 10, 96, 'Breadcrumbs', 2),
(167, 11, 96, 'Breadcrumbs', 2),
(167, 12, 96, 'Breadcrumbs', 2),
(167, 13, 96, 'Breadcrumbs', 2),
(167, 14, 96, 'Breadcrumbs', 2),
(167, 15, 96, 'Breadcrumbs', 2),
(167, 16, 96, 'Breadcrumbs', 2),
(167, 17, 96, 'Breadcrumbs', 2),
(167, 18, 96, 'Breadcrumbs', 2),
(167, 19, 96, 'Breadcrumbs', 2),
(167, 20, 96, 'Breadcrumbs', 2),
(167, 21, 96, 'Breadcrumbs', 2),
(167, 22, 96, 'Breadcrumbs', 2),
(167, 23, 96, 'Breadcrumbs', 2),
(167, 24, 96, 'Breadcrumbs', 2),
(167, 25, 96, 'Breadcrumbs', 2),
(173, 3, 121, 'Main', 3),
(158, 5, 110, 'Main', 4),
(169, 3, 141, 'Content', 5),
(169, 4, 361, 'Content', 5),
(169, 5, 369, 'Content', 5),
(169, 6, 369, 'Content', 5),
(169, 7, 369, 'Content', 5),
(169, 8, 369, 'Content', 5),
(169, 9, 369, 'Content', 5),
(167, 21, 410, 'Content', 9),
(167, 22, 414, 'Content', 9),
(167, 23, 414, 'Content', 9),
(167, 24, 425, 'Content', 9),
(167, 25, 441, 'Content', 9);

-- --------------------------------------------------------

--
-- Table structure for table `CollectionVersionBlocks`
--

CREATE TABLE IF NOT EXISTS `CollectionVersionBlocks` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvID` int(10) unsigned NOT NULL DEFAULT '1',
  `bID` int(10) unsigned NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cbDisplayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  `isOriginal` tinyint(1) NOT NULL DEFAULT '0',
  `cbOverrideAreaPermissions` tinyint(1) NOT NULL DEFAULT '0',
  `cbIncludeAll` tinyint(1) NOT NULL DEFAULT '0',
  `cbOverrideBlockTypeCacheSettings` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`cvID`,`bID`,`arHandle`),
  KEY `bID` (`bID`,`cID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `CollectionVersionBlocks`
--

INSERT INTO `CollectionVersionBlocks` (`cID`, `cvID`, `bID`, `arHandle`, `cbDisplayOrder`, `isOriginal`, `cbOverrideAreaPermissions`, `cbIncludeAll`, `cbOverrideBlockTypeCacheSettings`) VALUES
(1, 3, 25, 'Box 1 inhold', 0, 1, 0, 0, 0),
(1, 3, 26, 'Box 2 inhold', 0, 1, 0, 0, 0),
(1, 3, 27, 'Box 3 inhold', 0, 1, 0, 0, 0),
(1, 3, 28, 'Front welcome message', 0, 1, 0, 0, 0),
(1, 4, 25, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 4, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 4, 27, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 4, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 4, 29, 'Call to action content', 0, 1, 0, 0, 0),
(1, 5, 25, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 5, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 5, 27, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 5, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 5, 29, 'Call to action content', 0, 0, 0, 0, 0),
(1, 5, 30, 'Footer kolonne 2', 1, 1, 0, 0, 0),
(1, 5, 31, 'Footer kolonne 2', 0, 1, 0, 0, 0),
(1, 6, 25, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 6, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 6, 27, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 6, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 6, 29, 'Call to action content', 0, 0, 0, 0, 0),
(1, 6, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 6, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 7, 25, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 7, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 7, 27, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 7, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 7, 29, 'Call to action content', 0, 0, 0, 0, 0),
(1, 7, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 7, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 8, 25, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 8, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 8, 27, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 8, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 8, 29, 'Call to action content', 0, 0, 0, 0, 0),
(1, 8, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 8, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 9, 25, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 9, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 9, 27, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 9, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 9, 29, 'Call to action content', 0, 0, 0, 0, 0),
(1, 9, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 9, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 10, 25, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 10, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 10, 27, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 10, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 10, 29, 'Call to action content', 0, 0, 0, 0, 0),
(1, 10, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 10, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 11, 25, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 11, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 11, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 11, 29, 'Call to action content', 0, 0, 0, 0, 0),
(1, 11, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 11, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 11, 332, 'Box 3 inhold', 0, 1, 0, 0, 0),
(1, 12, 25, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 12, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 12, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 12, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 12, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 12, 332, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 12, 333, 'Call to action content', 0, 1, 0, 0, 0),
(1, 13, 25, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 13, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 13, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 13, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 13, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 13, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 13, 353, 'Box 3 inhold', 0, 1, 0, 0, 0),
(1, 14, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 14, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 14, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 14, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 14, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 14, 355, 'Box 3 inhold', 0, 1, 0, 0, 0),
(1, 14, 356, 'Box 1 inhold', 0, 1, 0, 0, 0),
(1, 15, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 15, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 15, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 15, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 15, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 15, 355, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 15, 356, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 16, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 16, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 16, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 16, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 16, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 16, 355, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 16, 356, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 17, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 17, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 17, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 17, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 17, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 17, 355, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 17, 356, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 18, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 18, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 18, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 18, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 18, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 18, 355, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 18, 356, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 18, 370, 'Liza diers billede', 0, 1, 0, 0, 0),
(1, 18, 371, 'Liza diers tekst', 0, 1, 0, 0, 0),
(1, 19, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 19, 28, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 19, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 19, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 19, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 19, 355, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 19, 356, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 19, 370, 'Liza diers billede', 0, 0, 0, 0, 0),
(1, 19, 371, 'Liza diers tekst', 0, 0, 0, 0, 0),
(1, 19, 385, 'Overskrift anmeldelser', 0, 1, 0, 0, 0),
(1, 20, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 20, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 20, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 20, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 20, 355, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 20, 356, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 20, 385, 'Overskrift anmeldelser', 0, 0, 0, 0, 0),
(1, 20, 387, 'Front welcome message', 0, 1, 0, 0, 0),
(1, 21, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 21, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 21, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 21, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 21, 355, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 21, 356, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 21, 385, 'Overskrift anmeldelser', 0, 0, 0, 0, 0),
(1, 21, 408, 'Front welcome message', 0, 1, 0, 0, 0),
(1, 21, 409, 'Front titel', 0, 1, 0, 0, 0),
(1, 21, 411, 'Liza diers billede', 0, 1, 0, 0, 0),
(1, 22, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 22, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 22, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 22, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 22, 355, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 22, 356, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 22, 385, 'Overskrift anmeldelser', 0, 0, 0, 0, 0),
(1, 22, 408, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 22, 409, 'Front titel', 0, 0, 0, 0, 0),
(1, 22, 412, 'Liza diers billede', 0, 1, 0, 0, 0),
(1, 23, 26, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 23, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 23, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 23, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 23, 355, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 23, 356, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 23, 385, 'Overskrift anmeldelser', 0, 0, 0, 0, 0),
(1, 23, 408, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 23, 409, 'Front titel', 0, 0, 0, 0, 0),
(1, 23, 412, 'Liza diers billede', 0, 0, 0, 0, 0),
(1, 24, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 24, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 24, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 24, 355, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 24, 356, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 24, 385, 'Overskrift anmeldelser', 0, 0, 0, 0, 0),
(1, 24, 408, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 24, 409, 'Front titel', 0, 0, 0, 0, 0),
(1, 24, 412, 'Liza diers billede', 0, 0, 0, 0, 0),
(1, 24, 426, 'Box 2 inhold', 0, 1, 0, 0, 0),
(1, 25, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 25, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 25, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 25, 356, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 25, 385, 'Overskrift anmeldelser', 0, 0, 0, 0, 0),
(1, 25, 408, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 25, 409, 'Front titel', 0, 0, 0, 0, 0),
(1, 25, 412, 'Liza diers billede', 0, 0, 0, 0, 0),
(1, 25, 426, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 25, 442, 'Box 3 inhold', 0, 1, 0, 0, 0),
(1, 26, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 26, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 26, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 26, 385, 'Overskrift anmeldelser', 0, 0, 0, 0, 0),
(1, 26, 408, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 26, 409, 'Front titel', 0, 0, 0, 0, 0),
(1, 26, 412, 'Liza diers billede', 0, 0, 0, 0, 0),
(1, 26, 426, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 26, 442, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 26, 443, 'Box 1 inhold', 0, 1, 0, 0, 0),
(1, 27, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 27, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 27, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 27, 385, 'Overskrift anmeldelser', 0, 0, 0, 0, 0),
(1, 27, 408, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 27, 409, 'Front titel', 0, 0, 0, 0, 0),
(1, 27, 412, 'Liza diers billede', 0, 0, 0, 0, 0),
(1, 27, 426, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 27, 443, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 27, 453, 'Box 3 inhold', 0, 1, 0, 0, 0),
(1, 28, 30, 'Footer kolonne 2', 1, 0, 0, 0, 0),
(1, 28, 31, 'Footer kolonne 2', 0, 0, 0, 0, 0),
(1, 28, 333, 'Call to action content', 0, 0, 0, 0, 0),
(1, 28, 385, 'Overskrift anmeldelser', 0, 0, 0, 0, 0),
(1, 28, 408, 'Front welcome message', 0, 0, 0, 0, 0),
(1, 28, 409, 'Front titel', 0, 0, 0, 0, 0),
(1, 28, 426, 'Box 2 inhold', 0, 0, 0, 0, 0),
(1, 28, 443, 'Box 1 inhold', 0, 0, 0, 0, 0),
(1, 28, 453, 'Box 3 inhold', 0, 0, 0, 0, 0),
(1, 28, 468, 'Liza diers billede', 0, 1, 0, 0, 0),
(124, 1, 1, 'Main', 0, 1, 0, 0, 0),
(125, 1, 2, 'Primary', 0, 1, 0, 0, 0),
(125, 1, 3, 'Primary', 1, 1, 0, 0, 0),
(125, 1, 4, 'Secondary 1', 0, 1, 0, 0, 0),
(125, 1, 5, 'Secondary 2', 0, 1, 0, 0, 0),
(125, 1, 6, 'Secondary 3', 0, 1, 0, 0, 0),
(125, 1, 7, 'Secondary 4', 0, 1, 0, 0, 0),
(125, 1, 8, 'Secondary 5', 0, 1, 0, 0, 0),
(142, 1, 9, 'Main', 0, 1, 0, 0, 0),
(148, 1, 15, 'Main', 0, 1, 0, 0, 0),
(148, 2, 15, 'Main', 0, 0, 0, 0, 0),
(148, 2, 267, 'Content', 0, 1, 0, 0, 0),
(148, 3, 15, 'Main', 0, 0, 0, 0, 0),
(148, 3, 267, 'Content', 0, 0, 0, 0, 0),
(148, 4, 15, 'Main', 0, 0, 0, 0, 0),
(148, 4, 267, 'Content', 0, 0, 0, 0, 0),
(148, 5, 15, 'Main', 0, 0, 0, 0, 0),
(148, 5, 267, 'Content', 0, 0, 0, 0, 0),
(148, 6, 15, 'Main', 0, 0, 0, 0, 0),
(148, 6, 360, 'Content', 0, 1, 0, 0, 0),
(148, 7, 15, 'Main', 0, 0, 0, 0, 0),
(148, 7, 417, 'Content', 0, 1, 0, 0, 0),
(148, 8, 15, 'Main', 0, 0, 0, 0, 0),
(148, 8, 417, 'Content', 0, 0, 0, 0, 0),
(152, 1, 19, 'Main', 0, 1, 0, 0, 0),
(152, 2, 19, 'Main', 0, 0, 0, 0, 0),
(153, 1, 24, 'Main', 0, 1, 0, 0, 0),
(156, 2, 32, 'Main', 0, 1, 0, 0, 0),
(156, 3, 32, 'Main', 0, 0, 0, 0, 0),
(156, 3, 34, 'Main', 1, 1, 0, 0, 0),
(157, 2, 33, 'Main', 0, 1, 0, 0, 0),
(157, 3, 424, 'Main', 0, 1, 0, 0, 0),
(158, 2, 35, 'Main', 0, 1, 0, 0, 0),
(158, 3, 35, 'Main', 0, 0, 0, 0, 0),
(158, 4, 110, 'Main', 0, 1, 0, 0, 0),
(158, 5, 110, 'Main', 0, 0, 0, 0, 0),
(159, 1, 55, 'Main', 0, 1, 0, 0, 0),
(159, 2, 55, 'Main', 0, 0, 0, 0, 0),
(159, 2, 142, 'Content', 0, 1, 0, 0, 0),
(159, 3, 55, 'Main', 0, 0, 0, 0, 0),
(159, 3, 142, 'Content', 0, 0, 0, 0, 0),
(159, 3, 190, 'Header tekst', 0, 1, 0, 0, 0),
(159, 4, 55, 'Main', 0, 0, 0, 0, 0),
(159, 4, 142, 'Content', 0, 0, 0, 0, 0),
(159, 4, 190, 'Header tekst', 0, 0, 0, 0, 0),
(159, 5, 55, 'Main', 0, 0, 0, 0, 0),
(159, 5, 190, 'Header tekst', 0, 0, 0, 0, 0),
(159, 5, 418, 'Content', 0, 1, 0, 0, 0),
(161, 1, 60, 'Main', 0, 1, 0, 0, 0),
(161, 2, 213, 'Main', 0, 1, 0, 0, 0),
(161, 3, 213, 'Main', 0, 0, 0, 0, 0),
(161, 3, 214, 'Content', 0, 1, 0, 0, 0),
(161, 4, 214, 'Content', 0, 0, 0, 0, 0),
(161, 4, 241, 'Main', 0, 1, 0, 0, 0),
(161, 5, 214, 'Content', 0, 0, 0, 0, 0),
(161, 5, 242, 'Main', 0, 1, 0, 0, 0),
(161, 6, 214, 'Content', 0, 0, 0, 0, 0),
(161, 6, 243, 'Main', 0, 1, 0, 0, 0),
(161, 7, 243, 'Main', 0, 0, 0, 0, 0),
(161, 7, 258, 'Content', 0, 1, 0, 0, 0),
(161, 8, 243, 'Main', 0, 0, 0, 0, 0),
(161, 8, 367, 'Content', 0, 1, 0, 0, 0),
(161, 9, 243, 'Main', 0, 0, 0, 0, 0),
(161, 9, 421, 'Content', 0, 1, 0, 0, 0),
(161, 10, 243, 'Main', 0, 0, 0, 0, 0),
(161, 10, 474, 'Content', 0, 1, 0, 0, 0),
(161, 10, 475, 'Header tekst', 0, 1, 0, 0, 0),
(162, 1, 67, 'Main', 0, 1, 0, 0, 0),
(162, 2, 209, 'Main', 0, 1, 0, 0, 0),
(162, 3, 331, 'Main', 0, 1, 0, 0, 0),
(162, 4, 331, 'Main', 0, 0, 0, 0, 0),
(162, 5, 331, 'Main', 0, 0, 0, 0, 0),
(163, 1, 72, 'Main', 0, 1, 0, 0, 0),
(163, 2, 202, 'Main', 0, 1, 0, 0, 0),
(163, 3, 202, 'Main', 0, 0, 0, 0, 0),
(163, 3, 203, 'Content', 0, 1, 0, 0, 0),
(163, 4, 202, 'Main', 0, 0, 0, 0, 0),
(163, 4, 203, 'Content', 0, 0, 0, 0, 0),
(163, 4, 204, 'Header tekst', 0, 1, 0, 0, 0),
(163, 5, 203, 'Content', 0, 0, 0, 0, 0),
(163, 5, 204, 'Header tekst', 0, 0, 0, 0, 0),
(163, 5, 206, 'Main', 0, 1, 0, 0, 0),
(163, 6, 203, 'Content', 0, 0, 0, 0, 0),
(163, 6, 204, 'Header tekst', 0, 0, 0, 0, 0),
(163, 6, 245, 'Main', 0, 1, 0, 0, 0),
(164, 1, 76, 'Main', 0, 1, 0, 0, 0),
(164, 2, 77, 'Main', 0, 1, 0, 0, 0),
(164, 3, 77, 'Main', 0, 0, 0, 0, 0),
(164, 3, 268, 'Content', 0, 1, 0, 0, 0),
(164, 4, 77, 'Main', 0, 0, 0, 0, 0),
(164, 4, 269, 'Content', 0, 1, 0, 0, 0),
(164, 5, 77, 'Main', 0, 0, 0, 0, 0),
(164, 5, 271, 'Content', 0, 1, 0, 0, 0),
(164, 6, 77, 'Main', 0, 0, 0, 0, 0),
(164, 6, 272, 'Content', 0, 1, 0, 0, 0),
(164, 7, 77, 'Main', 0, 0, 0, 0, 0),
(164, 7, 272, 'Content', 0, 0, 0, 0, 0),
(164, 7, 274, 'Content', 1, 1, 0, 0, 0),
(164, 8, 77, 'Main', 0, 0, 0, 0, 0),
(164, 8, 272, 'Content', 0, 0, 0, 0, 0),
(164, 8, 275, 'Content', 1, 1, 0, 0, 0),
(164, 9, 77, 'Main', 0, 0, 0, 0, 0),
(164, 9, 272, 'Content', 0, 0, 0, 0, 0),
(164, 9, 422, 'Content', 1, 1, 0, 0, 0),
(164, 10, 77, 'Main', 0, 0, 0, 0, 0),
(164, 10, 272, 'Content', 0, 0, 0, 0, 0),
(164, 10, 422, 'Content', 1, 0, 0, 0, 0),
(165, 1, 83, 'Main', 0, 1, 0, 0, 0),
(165, 2, 83, 'Main', 0, 0, 0, 0, 0),
(165, 2, 388, 'Content', 0, 1, 0, 0, 0),
(165, 3, 83, 'Main', 0, 0, 0, 0, 0),
(165, 3, 389, 'Content', 1, 1, 0, 0, 0),
(165, 3, 390, 'Content', 0, 1, 0, 0, 0),
(165, 4, 83, 'Main', 0, 0, 0, 0, 0),
(165, 4, 389, 'Content', 2, 0, 0, 0, 0),
(165, 4, 390, 'Content', 1, 0, 0, 0, 0),
(165, 4, 416, 'Content', 0, 1, 0, 0, 0),
(166, 1, 88, 'Main', 0, 1, 0, 0, 0),
(166, 2, 88, 'Main', 0, 0, 0, 0, 0),
(166, 2, 382, 'Content', 0, 1, 0, 0, 0),
(166, 3, 88, 'Main', 0, 0, 0, 0, 0),
(166, 3, 383, 'Content', 0, 1, 0, 0, 0),
(166, 4, 88, 'Main', 0, 0, 0, 0, 0),
(166, 4, 384, 'Content', 0, 1, 0, 0, 0),
(166, 5, 384, 'Content', 0, 0, 0, 0, 0),
(166, 5, 401, 'Main', 0, 1, 0, 0, 0),
(166, 6, 384, 'Content', 1, 0, 0, 0, 0),
(166, 6, 401, 'Main', 0, 0, 0, 0, 0),
(166, 6, 413, 'Content', 0, 1, 0, 0, 0),
(167, 10, 94, 'Main', 0, 0, 0, 0, 0),
(167, 10, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 10, 98, 'Google maps', 0, 0, 0, 0, 0),
(167, 10, 99, 'Kontakt formular', 0, 0, 0, 0, 0),
(167, 10, 104, 'Box 1 inhold', 0, 0, 0, 0, 0),
(167, 10, 145, 'Box 2 inhold', 0, 0, 0, 0, 0),
(167, 10, 188, 'Header tekst', 0, 0, 0, 0, 0),
(167, 11, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 11, 98, 'Google maps', 0, 0, 0, 0, 0),
(167, 11, 99, 'Kontakt formular', 0, 0, 0, 0, 0),
(167, 11, 104, 'Box 1 inhold', 0, 0, 0, 0, 0),
(167, 11, 145, 'Box 2 inhold', 0, 0, 0, 0, 0),
(167, 11, 188, 'Header tekst', 0, 0, 0, 0, 0),
(167, 11, 279, 'Main', 0, 1, 0, 0, 0),
(167, 12, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 12, 98, 'Google maps', 0, 0, 0, 0, 0),
(167, 12, 104, 'Box 1 inhold', 0, 0, 0, 0, 0),
(167, 12, 145, 'Box 2 inhold', 0, 0, 0, 0, 0),
(167, 12, 188, 'Header tekst', 0, 0, 0, 0, 0),
(167, 12, 279, 'Main', 0, 0, 0, 0, 0),
(167, 12, 302, 'Kontakt formular', 0, 1, 0, 0, 0),
(167, 13, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 13, 98, 'Google maps', 0, 0, 0, 0, 0),
(167, 13, 145, 'Box 2 inhold', 0, 0, 0, 0, 0),
(167, 13, 188, 'Header tekst', 0, 0, 0, 0, 0),
(167, 13, 279, 'Main', 0, 0, 0, 0, 0),
(167, 13, 302, 'Kontakt formular', 0, 0, 0, 0, 0),
(167, 13, 354, 'Box 1 inhold', 0, 1, 0, 0, 0),
(167, 14, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 14, 98, 'Google maps', 0, 0, 0, 0, 0),
(167, 14, 145, 'Box 2 inhold', 0, 0, 0, 0, 0),
(167, 14, 279, 'Main', 0, 0, 0, 0, 0),
(167, 14, 302, 'Kontakt formular', 0, 0, 0, 0, 0),
(167, 14, 354, 'Kontakt tekst', 0, 0, 0, 0, 0),
(167, 14, 358, 'Header tekst', 0, 1, 0, 0, 0),
(167, 15, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 15, 98, 'Google maps', 0, 0, 0, 0, 0),
(167, 15, 145, 'Box 2 inhold', 0, 0, 0, 0, 0),
(167, 15, 279, 'Main', 0, 0, 0, 0, 0),
(167, 15, 302, 'Kontakt formular', 0, 0, 0, 0, 0),
(167, 15, 359, 'Header tekst', 0, 1, 0, 0, 0),
(167, 16, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 16, 98, 'Google maps', 0, 0, 0, 0, 0),
(167, 16, 302, 'Kontakt formular', 0, 0, 0, 0, 0),
(167, 16, 359, 'Header tekst', 0, 0, 0, 0, 0),
(167, 16, 391, 'Main', 0, 1, 0, 0, 0),
(167, 17, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 17, 359, 'Header tekst', 0, 0, 0, 0, 0),
(167, 17, 398, 'Main', 0, 1, 0, 0, 0),
(167, 18, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 18, 359, 'Header tekst', 0, 0, 0, 0, 0),
(167, 18, 404, 'Google maps', 0, 1, 0, 0, 0),
(167, 18, 406, 'Main', 0, 1, 0, 0, 0),
(167, 19, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 19, 359, 'Header tekst', 0, 0, 0, 0, 0),
(167, 19, 404, 'Google maps', 0, 0, 0, 0, 0),
(167, 19, 407, 'Main', 0, 1, 0, 0, 0),
(167, 20, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 20, 359, 'Header tekst', 0, 0, 0, 0, 0),
(167, 20, 404, 'Google maps', 0, 0, 0, 0, 0),
(167, 20, 407, 'Main', 0, 0, 0, 0, 0),
(167, 20, 410, 'Content', 0, 1, 0, 0, 0),
(167, 21, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 21, 359, 'Header tekst', 0, 0, 0, 0, 0),
(167, 21, 404, 'Google maps', 0, 0, 0, 0, 0),
(167, 21, 407, 'Main', 0, 0, 0, 0, 0),
(167, 21, 410, 'Content', 0, 0, 0, 0, 0),
(167, 22, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 22, 359, 'Header tekst', 0, 0, 0, 0, 0),
(167, 22, 404, 'Google maps', 0, 0, 0, 0, 0),
(167, 22, 407, 'Main', 0, 0, 0, 0, 0),
(167, 22, 414, 'Content', 0, 1, 0, 0, 0),
(167, 23, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 23, 359, 'Header tekst', 0, 0, 0, 0, 0),
(167, 23, 404, 'Google maps', 0, 0, 0, 0, 0),
(167, 23, 407, 'Main', 0, 0, 0, 0, 0),
(167, 23, 414, 'Content', 1, 0, 0, 0, 0),
(167, 23, 415, 'Content', 0, 1, 0, 0, 0),
(167, 24, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 24, 359, 'Header tekst', 0, 0, 0, 0, 0),
(167, 24, 404, 'Google maps', 0, 0, 0, 0, 0),
(167, 24, 407, 'Main', 0, 0, 0, 0, 0),
(167, 24, 415, 'Content', 0, 0, 0, 0, 0),
(167, 24, 425, 'Content', 1, 1, 0, 0, 0),
(167, 25, 96, 'Breadcrumbs', 0, 0, 0, 0, 0),
(167, 25, 359, 'Header tekst', 0, 0, 0, 0, 0),
(167, 25, 404, 'Google maps', 0, 0, 0, 0, 0),
(167, 25, 407, 'Main', 0, 0, 0, 0, 0),
(167, 25, 415, 'Content', 0, 0, 0, 0, 0),
(167, 25, 441, 'Content', 1, 1, 0, 0, 0),
(169, 1, 109, 'Main', 0, 1, 0, 0, 0),
(169, 2, 109, 'Main', 0, 0, 0, 0, 0),
(169, 2, 139, 'Content', 0, 1, 0, 0, 0),
(169, 2, 140, 'Header tekst', 0, 1, 0, 0, 0),
(169, 3, 109, 'Main', 0, 0, 0, 0, 0),
(169, 3, 140, 'Header tekst', 0, 0, 0, 0, 0),
(169, 3, 141, 'Content', 0, 1, 0, 0, 0),
(169, 4, 140, 'Header tekst', 0, 0, 0, 0, 0),
(169, 4, 361, 'Content', 0, 1, 0, 0, 0),
(169, 4, 362, 'Content', 1, 1, 0, 0, 0),
(169, 4, 363, 'Content', 2, 1, 0, 0, 0),
(169, 4, 364, 'Content', 3, 1, 0, 0, 0),
(169, 4, 365, 'Content', 4, 1, 0, 0, 0),
(169, 4, 366, 'Content', 5, 1, 0, 0, 0),
(169, 4, 368, 'Main', 0, 1, 0, 0, 0),
(169, 5, 140, 'Header tekst', 0, 0, 0, 0, 0),
(169, 5, 365, 'Content', 4, 0, 0, 0, 0),
(169, 5, 368, 'Main', 0, 0, 0, 0, 0),
(169, 5, 369, 'Content', 0, 1, 0, 0, 0),
(169, 5, 372, 'Content', 1, 1, 0, 0, 0),
(169, 5, 373, 'Content', 2, 1, 0, 0, 0),
(169, 5, 374, 'Content', 3, 1, 0, 0, 0),
(169, 5, 375, 'Content', 5, 1, 0, 0, 0),
(169, 6, 140, 'Header tekst', 0, 0, 0, 0, 0),
(169, 6, 365, 'Content', 8, 0, 0, 0, 0),
(169, 6, 368, 'Main', 0, 0, 0, 0, 0),
(169, 6, 369, 'Content', 0, 0, 0, 0, 0),
(169, 6, 372, 'Content', 2, 0, 0, 0, 0),
(169, 6, 373, 'Content', 4, 0, 0, 0, 0),
(169, 6, 374, 'Content', 6, 0, 0, 0, 0),
(169, 6, 375, 'Content', 10, 0, 0, 0, 0),
(169, 6, 376, 'Content', 1, 1, 0, 0, 0),
(169, 6, 377, 'Content', 3, 1, 0, 0, 0),
(169, 6, 378, 'Content', 5, 1, 0, 0, 0),
(169, 6, 379, 'Content', 7, 1, 0, 0, 0),
(169, 6, 380, 'Content', 9, 1, 0, 0, 0),
(169, 7, 140, 'Header tekst', 0, 0, 0, 0, 0),
(169, 7, 368, 'Main', 0, 0, 0, 0, 0),
(169, 7, 369, 'Content', 0, 0, 0, 0, 0),
(169, 7, 372, 'Content', 2, 0, 0, 0, 0),
(169, 7, 373, 'Content', 4, 0, 0, 0, 0),
(169, 7, 374, 'Content', 6, 0, 0, 0, 0),
(169, 7, 375, 'Content', 10, 0, 0, 0, 0),
(169, 7, 376, 'Content', 1, 0, 0, 0, 0),
(169, 7, 377, 'Content', 3, 0, 0, 0, 0),
(169, 7, 378, 'Content', 5, 0, 0, 0, 0),
(169, 7, 379, 'Content', 7, 0, 0, 0, 0),
(169, 7, 380, 'Content', 9, 0, 0, 0, 0),
(169, 7, 423, 'Content', 8, 1, 0, 0, 0),
(169, 8, 140, 'Header tekst', 0, 0, 0, 0, 0),
(169, 8, 368, 'Main', 0, 0, 0, 0, 0),
(169, 8, 369, 'Content', 0, 0, 0, 0, 0),
(169, 8, 372, 'Content', 2, 0, 0, 0, 0),
(169, 8, 373, 'Content', 4, 0, 0, 0, 0),
(169, 8, 374, 'Content', 6, 0, 0, 0, 0),
(169, 8, 375, 'Content', 10, 0, 0, 0, 0),
(169, 8, 376, 'Content', 1, 0, 0, 0, 0),
(169, 8, 377, 'Content', 3, 0, 0, 0, 0),
(169, 8, 378, 'Content', 5, 0, 0, 0, 0),
(169, 8, 379, 'Content', 7, 0, 0, 0, 0),
(169, 8, 380, 'Content', 9, 0, 0, 0, 0),
(169, 8, 427, 'Content', 8, 1, 0, 0, 0),
(169, 9, 140, 'Header tekst', 0, 0, 0, 0, 0),
(169, 9, 368, 'Main', 0, 0, 0, 0, 0),
(169, 9, 369, 'Content', 0, 0, 0, 0, 0),
(169, 9, 372, 'Content', 3, 0, 0, 0, 0),
(169, 9, 373, 'Content', 5, 0, 0, 0, 0),
(169, 9, 374, 'Content', 7, 0, 0, 0, 0),
(169, 9, 375, 'Content', 11, 0, 0, 0, 0),
(169, 9, 376, 'Content', 2, 0, 0, 0, 0),
(169, 9, 377, 'Content', 4, 0, 0, 0, 0),
(169, 9, 378, 'Content', 6, 0, 0, 0, 0),
(169, 9, 379, 'Content', 8, 0, 0, 0, 0),
(169, 9, 380, 'Content', 10, 0, 0, 0, 0),
(169, 9, 427, 'Content', 9, 0, 0, 0, 0),
(169, 9, 431, 'Content', 1, 1, 0, 0, 0),
(170, 1, 115, 'Main', 0, 1, 0, 0, 0),
(170, 2, 115, 'Main', 0, 0, 0, 0, 0),
(170, 3, 428, 'Content', 0, 1, 0, 0, 0),
(170, 3, 430, 'Main', 0, 1, 0, 0, 0),
(170, 4, 430, 'Main', 0, 0, 0, 0, 0),
(170, 4, 440, 'Content', 0, 1, 0, 0, 0),
(170, 5, 430, 'Main', 0, 0, 0, 0, 0),
(170, 5, 440, 'Content', 0, 0, 0, 0, 0),
(170, 5, 455, 'Content', 1, 1, 0, 0, 0),
(170, 6, 430, 'Main', 0, 0, 0, 0, 0),
(170, 6, 472, 'Content', 0, 1, 0, 0, 0),
(170, 7, 430, 'Main', 0, 0, 0, 0, 0),
(170, 7, 472, 'Content', 0, 0, 0, 0, 0),
(170, 7, 473, 'Header tekst', 0, 1, 0, 0, 0),
(171, 1, 119, 'Main', 0, 1, 0, 0, 0),
(171, 2, 120, 'Main', 0, 1, 0, 0, 0),
(171, 3, 120, 'Main', 0, 0, 0, 0, 0),
(171, 4, 120, 'Main', 0, 0, 0, 0, 0),
(171, 4, 123, 'Content', 0, 1, 0, 0, 0),
(171, 5, 120, 'Main', 0, 0, 0, 0, 0),
(171, 5, 143, 'Content', 0, 1, 0, 0, 0),
(171, 6, 120, 'Main', 0, 0, 0, 0, 0),
(171, 6, 143, 'Content', 0, 0, 0, 0, 0),
(171, 6, 144, 'Content', 1, 1, 0, 0, 0),
(171, 7, 120, 'Main', 0, 0, 0, 0, 0),
(171, 7, 143, 'Content', 0, 0, 0, 0, 0),
(171, 7, 186, 'Content', 1, 1, 0, 0, 0),
(171, 8, 120, 'Main', 0, 0, 0, 0, 0),
(171, 8, 143, 'Content', 0, 0, 0, 0, 0),
(171, 8, 186, 'Content', 1, 0, 0, 0, 0),
(171, 8, 189, 'Header tekst', 0, 1, 0, 0, 0),
(171, 9, 120, 'Main', 0, 0, 0, 0, 0),
(171, 9, 143, 'Content', 0, 0, 0, 0, 0),
(171, 9, 186, 'Content', 1, 0, 0, 0, 0),
(171, 9, 189, 'Header tekst', 0, 0, 0, 0, 0),
(172, 2, 124, 'Main', 0, 1, 0, 0, 0),
(173, 2, 121, 'Main', 0, 1, 0, 0, 0),
(173, 3, 121, 'Main', 0, 0, 0, 0, 0),
(174, 2, 122, 'Main', 0, 1, 0, 0, 0),
(175, 1, 133, 'Main', 0, 1, 0, 0, 0),
(175, 2, 133, 'Main', 0, 0, 0, 0, 0),
(175, 2, 187, 'Content', 0, 1, 0, 0, 0),
(175, 3, 133, 'Main', 0, 0, 0, 0, 0),
(175, 3, 187, 'Content', 0, 0, 0, 0, 0),
(175, 3, 191, 'Header tekst', 0, 1, 0, 0, 0),
(175, 4, 133, 'Main', 0, 0, 0, 0, 0),
(175, 4, 191, 'Header tekst', 0, 0, 0, 0, 0),
(175, 4, 192, 'Content', 0, 1, 0, 0, 0),
(175, 5, 133, 'Main', 0, 0, 0, 0, 0),
(175, 5, 191, 'Header tekst', 0, 0, 0, 0, 0),
(175, 5, 357, 'Content', 0, 1, 0, 0, 0),
(175, 6, 133, 'Main', 0, 0, 0, 0, 0),
(175, 6, 191, 'Header tekst', 0, 0, 0, 0, 0),
(175, 6, 419, 'Content', 0, 1, 0, 0, 0),
(175, 7, 133, 'Main', 0, 0, 0, 0, 0),
(175, 7, 191, 'Header tekst', 0, 0, 0, 0, 0),
(175, 7, 419, 'Content', 0, 0, 0, 0, 0),
(175, 7, 454, 'Content', 1, 1, 0, 0, 0),
(176, 1, 138, 'Main', 0, 1, 0, 0, 0),
(176, 2, 197, 'Main', 0, 1, 0, 0, 0),
(177, 1, 146, 'Main', 0, 1, 0, 0, 0),
(178, 1, 184, 'Main', 0, 1, 0, 0, 0),
(178, 2, 184, 'Main', 0, 0, 0, 0, 0),
(178, 2, 185, 'Content', 0, 1, 0, 0, 0),
(178, 3, 184, 'Main', 0, 0, 0, 0, 0),
(178, 3, 185, 'Content', 0, 0, 0, 0, 0),
(178, 4, 184, 'Main', 0, 0, 0, 0, 0),
(178, 4, 420, 'Content', 0, 1, 0, 0, 0),
(179, 1, 225, 'Main', 0, 1, 0, 0, 0),
(179, 2, 225, 'Main', 0, 0, 0, 0, 0),
(179, 3, 225, 'Main', 0, 0, 0, 0, 0),
(179, 3, 247, 'Content', 0, 1, 0, 0, 0),
(179, 4, 225, 'Main', 0, 0, 0, 0, 0),
(179, 4, 247, 'Content', 0, 0, 0, 0, 0),
(179, 5, 225, 'Main', 0, 0, 0, 0, 0),
(179, 5, 247, 'Content', 0, 0, 0, 0, 0),
(179, 6, 225, 'Main', 0, 0, 0, 0, 0),
(179, 6, 257, 'Content', 0, 1, 0, 0, 0),
(180, 1, 254, 'Main', 0, 1, 0, 0, 0),
(180, 2, 254, 'Main', 0, 0, 0, 0, 0),
(180, 2, 255, 'Content', 0, 1, 0, 0, 0),
(180, 3, 254, 'Main', 0, 0, 0, 0, 0),
(180, 3, 256, 'Content', 0, 1, 0, 0, 0),
(181, 1, 265, 'Main', 0, 1, 0, 0, 0),
(181, 2, 265, 'Main', 0, 0, 0, 0, 0),
(181, 2, 266, 'Content', 0, 1, 0, 0, 0),
(182, 1, 276, 'Main', 0, 1, 0, 0, 0),
(184, 1, 300, 'Main', 0, 1, 0, 0, 0),
(184, 2, 300, 'Main', 0, 0, 0, 0, 0),
(184, 2, 301, 'Content', 0, 1, 0, 0, 0),
(184, 3, 300, 'Main', 0, 0, 0, 0, 0),
(184, 3, 301, 'Content', 0, 0, 0, 0, 0),
(185, 1, 311, 'Main', 0, 1, 0, 0, 0),
(185, 2, 311, 'Main', 0, 0, 0, 0, 0),
(185, 2, 334, 'Content', 0, 1, 0, 0, 0),
(186, 1, 317, 'Main', 0, 1, 0, 0, 0),
(186, 2, 341, 'Main', 0, 1, 0, 0, 0),
(187, 1, 323, 'Main', 0, 1, 0, 0, 0),
(187, 2, 323, 'Main', 0, 0, 0, 0, 0),
(187, 2, 342, 'Content', 0, 1, 0, 0, 0),
(188, 1, 329, 'Main', 0, 1, 0, 0, 0),
(188, 2, 329, 'Main', 0, 0, 0, 0, 0),
(188, 2, 343, 'Content', 0, 1, 0, 0, 0),
(189, 1, 350, 'Main', 0, 1, 0, 0, 0),
(189, 2, 350, 'Main', 0, 0, 0, 0, 0),
(189, 2, 351, 'Content', 0, 1, 0, 0, 0),
(189, 3, 350, 'Main', 0, 0, 0, 0, 0),
(189, 3, 352, 'Content', 0, 1, 0, 0, 0),
(191, 1, 438, 'Main', 0, 1, 0, 0, 0),
(191, 2, 438, 'Main', 0, 0, 0, 0, 0),
(191, 2, 439, 'Content', 0, 1, 0, 0, 0),
(192, 1, 449, 'Main', 0, 1, 0, 0, 0),
(192, 2, 449, 'Main', 0, 0, 0, 0, 0),
(192, 2, 450, 'Content', 0, 1, 0, 0, 0),
(192, 3, 449, 'Main', 0, 0, 0, 0, 0),
(192, 3, 451, 'Content', 0, 1, 0, 0, 0),
(192, 4, 449, 'Main', 0, 0, 0, 0, 0),
(192, 4, 452, 'Content', 0, 1, 0, 0, 0),
(192, 5, 449, 'Main', 0, 0, 0, 0, 0),
(192, 5, 452, 'Content', 0, 0, 0, 0, 0),
(193, 1, 467, 'Main', 0, 1, 0, 0, 0),
(193, 2, 467, 'Main', 0, 0, 0, 0, 0),
(193, 2, 469, 'Content', 0, 1, 0, 0, 0),
(193, 3, 467, 'Main', 0, 0, 0, 0, 0),
(193, 3, 469, 'Content', 1, 0, 0, 0, 0),
(193, 3, 470, 'Header tekst', 0, 1, 0, 0, 0),
(193, 3, 471, 'Content', 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `CollectionVersionBlocksCacheSettings`
--

CREATE TABLE IF NOT EXISTS `CollectionVersionBlocksCacheSettings` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvID` int(10) unsigned NOT NULL DEFAULT '1',
  `bID` int(10) unsigned NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btCacheBlockOutput` tinyint(1) NOT NULL DEFAULT '0',
  `btCacheBlockOutputOnPost` tinyint(1) NOT NULL DEFAULT '0',
  `btCacheBlockOutputForRegisteredUsers` tinyint(1) NOT NULL DEFAULT '0',
  `btCacheBlockOutputLifetime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`cvID`,`bID`,`arHandle`),
  KEY `bID` (`bID`,`cID`,`cvID`,`arHandle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `CollectionVersionBlocksOutputCache`
--

CREATE TABLE IF NOT EXISTS `CollectionVersionBlocksOutputCache` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvID` int(10) unsigned NOT NULL DEFAULT '1',
  `bID` int(10) unsigned NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btCachedBlockOutput` longtext COLLATE utf8_unicode_ci,
  `btCachedBlockOutputExpires` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`cvID`,`bID`,`arHandle`),
  KEY `bID` (`bID`,`cID`,`cvID`,`arHandle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `CollectionVersionFeatureAssignments`
--

CREATE TABLE IF NOT EXISTS `CollectionVersionFeatureAssignments` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvID` int(10) unsigned NOT NULL DEFAULT '1',
  `faID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`cvID`,`faID`),
  KEY `faID` (`faID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `CollectionVersionFeatureAssignments`
--

INSERT INTO `CollectionVersionFeatureAssignments` (`cID`, `cvID`, `faID`) VALUES
(1, 18, 2),
(1, 19, 2),
(1, 21, 6),
(1, 22, 7),
(1, 23, 7),
(1, 24, 7),
(1, 25, 7),
(1, 26, 7),
(1, 27, 7),
(1, 28, 10),
(156, 3, 1),
(166, 2, 3),
(166, 3, 4),
(166, 4, 5),
(166, 5, 5),
(166, 6, 5),
(170, 5, 9),
(175, 7, 8);

-- --------------------------------------------------------

--
-- Table structure for table `CollectionVersionRelatedEdits`
--

CREATE TABLE IF NOT EXISTS `CollectionVersionRelatedEdits` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvID` int(10) unsigned NOT NULL DEFAULT '0',
  `cRelationID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvRelationID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`cvID`,`cRelationID`,`cvRelationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `CollectionVersionRelatedEdits`
--

INSERT INTO `CollectionVersionRelatedEdits` (`cID`, `cvID`, `cRelationID`, `cvRelationID`) VALUES
(171, 5, 158, 5);

-- --------------------------------------------------------

--
-- Table structure for table `CollectionVersionThemeCustomStyles`
--

CREATE TABLE IF NOT EXISTS `CollectionVersionThemeCustomStyles` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvID` int(10) unsigned NOT NULL DEFAULT '1',
  `pThemeID` int(10) unsigned NOT NULL DEFAULT '0',
  `scvlID` int(10) unsigned DEFAULT '0',
  `preset` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sccRecordID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`cID`,`cvID`,`pThemeID`),
  KEY `pThemeID` (`pThemeID`),
  KEY `scvlID` (`scvlID`),
  KEY `sccRecordID` (`sccRecordID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `CollectionVersions`
--

CREATE TABLE IF NOT EXISTS `CollectionVersions` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvID` int(10) unsigned NOT NULL DEFAULT '1',
  `cvName` text COLLATE utf8_unicode_ci,
  `cvHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cvDescription` text COLLATE utf8_unicode_ci,
  `cvDatePublic` datetime DEFAULT NULL,
  `cvDateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cvComments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cvIsApproved` tinyint(1) NOT NULL DEFAULT '0',
  `cvIsNew` tinyint(1) NOT NULL DEFAULT '0',
  `cvAuthorUID` int(10) unsigned DEFAULT NULL,
  `cvApproverUID` int(10) unsigned DEFAULT NULL,
  `pThemeID` int(10) unsigned NOT NULL DEFAULT '0',
  `pTemplateID` int(10) unsigned NOT NULL DEFAULT '0',
  `cvActivateDatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`cID`,`cvID`),
  KEY `cvIsApproved` (`cvIsApproved`),
  KEY `cvAuthorUID` (`cvAuthorUID`),
  KEY `cvApproverUID` (`cvApproverUID`),
  KEY `pThemeID` (`pThemeID`),
  KEY `pTemplateID` (`pTemplateID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `CollectionVersions`
--

INSERT INTO `CollectionVersions` (`cID`, `cvID`, `cvName`, `cvHandle`, `cvDescription`, `cvDatePublic`, `cvDateCreated`, `cvComments`, `cvIsApproved`, `cvIsNew`, `cvAuthorUID`, `cvApproverUID`, `pThemeID`, `pTemplateID`, `cvActivateDatetime`) VALUES
(1, 1, 'Home', 'home', '', '2015-04-30 16:39:10', '2015-04-30 16:39:10', 'Initial Version', 0, 0, 1, NULL, 2, 5, NULL),
(1, 2, 'Home', 'home', '', '2015-04-30 16:39:10', '2015-04-30 21:23:07', 'Ny version 2', 0, 0, 1, 1, 2, 8, NULL),
(1, 3, 'Home', 'home', '', '2015-04-30 16:39:10', '2015-04-30 21:34:48', 'Version 3', 0, 0, 1, 1, 2, 8, NULL),
(1, 4, 'Home', 'home', '', '2015-04-30 16:39:10', '2015-04-30 22:22:00', 'Version 4', 0, 0, 1, 1, 2, 8, NULL),
(1, 5, 'Home', 'home', '', '2015-04-30 16:39:10', '2015-05-07 23:26:53', 'Version 5', 0, 0, 1, 1, 2, 8, NULL),
(1, 6, 'Home', 'home', '', '2015-04-30 16:39:10', '2015-05-07 23:31:07', 'Version 6', 0, 0, 1, 1, 2, 8, NULL),
(1, 7, 'Home', 'home', '', '2015-04-30 16:39:10', '2015-05-07 23:35:04', 'Version 7', 0, 0, 1, 1, 2, 8, NULL),
(1, 8, 'Forside', 'forside', '', '2015-04-30 16:39:10', '2015-05-08 00:20:29', 'Version 8', 0, 0, 1, 1, 2, 8, NULL),
(1, 9, 'Forside', 'forside', '', '2015-04-30 16:39:10', '2015-05-12 09:23:28', 'Version 9', 0, 0, 1, 1, 2, 8, NULL),
(1, 10, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-18 12:35:42', 'Version 10', 0, 0, 1, 1, 2, 8, NULL),
(1, 11, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-18 12:39:16', 'Version 11', 0, 0, 1, 1, 2, 8, NULL),
(1, 12, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-18 12:40:40', 'Ny version 12', 0, 0, 1, 1, 2, 8, NULL),
(1, 13, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-19 09:00:41', 'Version 13', 0, 0, 1, 1, 2, 8, NULL),
(1, 14, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-19 09:03:28', 'Version 14', 0, 0, 1, 1, 2, 8, NULL),
(1, 15, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-19 11:17:30', 'Version 15', 0, 0, 1, 1, 2, 8, NULL),
(1, 16, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-19 11:18:17', 'Ny version 16', 0, 0, 1, 1, 2, 8, NULL),
(1, 17, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-19 11:21:03', 'Version 17', 0, 0, 1, 1, 2, 8, NULL),
(1, 18, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-19 13:02:31', 'Version 18', 0, 0, 1, 1, 2, 8, NULL),
(1, 19, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-19 16:46:49', 'Version 19', 0, 0, 1, 1, 2, 8, NULL),
(1, 20, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-20 06:10:10', 'Version 20', 0, 0, 1, 1, 2, 8, NULL),
(1, 21, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-20 07:13:44', 'Version 21', 0, 0, 1, 1, 2, 8, NULL),
(1, 22, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-20 07:17:23', 'Version 22', 0, 0, 1, 1, 2, 8, NULL),
(1, 23, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-20 11:13:02', 'Version 23', 0, 0, 1, 1, 2, 8, NULL),
(1, 24, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-20 11:19:52', 'Version 24', 0, 0, 1, 1, 2, 8, NULL),
(1, 25, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-22 13:43:12', 'Version 25', 0, 0, 1, 1, 2, 8, NULL),
(1, 26, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-22 13:46:07', 'Version 26', 0, 0, 1, 1, 2, 8, NULL),
(1, 27, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-22 13:55:20', 'Version 27', 0, 0, 1, 1, 2, 8, NULL),
(1, 28, 'Forside', 'forside', '', '2015-04-30 16:39:00', '2015-05-26 08:24:23', 'Version 28', 1, 0, 1, 1, 2, 8, NULL),
(2, 1, 'Dashboard', 'dashboard', '', '2015-04-30 16:39:21', '2015-04-30 16:39:21', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(3, 1, 'Sitemap', 'sitemap', 'Whole world at a glance.', '2015-04-30 16:39:21', '2015-04-30 16:39:21', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(4, 1, 'Full Sitemap', 'full', '', '2015-04-30 16:39:21', '2015-04-30 16:39:21', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(5, 1, 'Flat View', 'explore', '', '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(6, 1, 'Page Search', 'search', '', '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(7, 1, 'Files', 'files', 'All documents and images.', '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(8, 1, 'File Manager', 'search', '', '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(9, 1, 'Attributes', 'attributes', '', '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(10, 1, 'File Sets', 'sets', '', '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(11, 1, 'Add File Set', 'add_set', '', '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(12, 1, 'Members', 'users', 'Add and manage the user accounts and groups on your website.', '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(13, 1, 'Search Users', 'search', '', '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(14, 1, 'User Groups', 'groups', '', '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(15, 1, 'Attributes', 'attributes', '', '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(16, 1, 'Add User', 'add', '', '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(17, 1, 'Add Group', 'add_group', '', '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(18, 1, 'Move Multiple Groups', 'bulkupdate', '', '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(19, 1, 'Group Sets', 'group_sets', '', '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(20, 1, 'Community Points', 'points', NULL, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(21, 1, 'Assign Points', 'assign', NULL, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(22, 1, 'Actions', 'actions', NULL, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(23, 1, 'Reports', 'reports', 'Get data from forms and logs.', '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(24, 1, 'Form Results', 'forms', 'Get submission data.', '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(25, 1, 'Surveys', 'surveys', '', '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(26, 1, 'Logs', 'logs', '', '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(27, 1, 'Pages & Themes', 'pages', 'Reskin your site.', '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(28, 1, 'Themes', 'themes', 'Reskin your site.', '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(29, 1, 'Inspect', 'inspect', '', '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(30, 1, 'Page Types', 'types', '', '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(31, 1, 'Organize Page Type Order', 'organize', '', '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(32, 1, 'Add Page Type', 'add', '', '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(33, 1, 'Compose Form', 'form', '', '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(34, 1, 'Defaults and Output', 'output', '', '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(35, 1, 'Page Type Attributes', 'attributes', '', '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(36, 1, 'Page Type Permissions', 'permissions', '', '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(37, 1, 'Page Templates', 'templates', 'Form factors for pages in your site.', '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(38, 1, 'Add Page Template', 'add', 'Add page templates to your site.', '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(39, 1, 'Attributes', 'attributes', '', '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(40, 1, 'Single Pages', 'single', '', '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(41, 1, 'RSS Feeds', 'feeds', '', '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(42, 1, 'Conversations', 'conversations', '', '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(43, 1, 'Messages', 'messages', '', '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(44, 1, 'Workflow', 'workflow', '', '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(45, 1, 'Workflow List', 'workflows', '', '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(46, 1, 'Waiting for Me', 'me', '', '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(47, 1, 'Stacks & Blocks', 'blocks', 'Manage sitewide content and administer block types.', '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(48, 1, 'Stacks', 'stacks', 'Share content across your site.', '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(49, 1, 'Block & Stack Permissions', 'permissions', 'Control who can add blocks and stacks on your site.', '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(50, 1, 'Stack List', 'list', '', '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(51, 1, 'Block Types', 'types', 'Manage the installed block types in your site.', '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(52, 1, 'Extend concrete5', 'extend', '', '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(53, 1, 'Dashboard', 'news', '', '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(54, 1, 'Add Functionality', 'install', 'Install add-ons & themes.', '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(55, 1, 'Update Add-Ons', 'update', 'Update your installed packages.', '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(56, 1, 'Connect to the Community', 'connect', 'Connect to the concrete5 community.', '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(57, 1, 'Get More Themes', 'themes', 'Download themes from concrete5.org.', '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(58, 1, 'Get More Add-Ons', 'addons', 'Download add-ons from concrete5.org.', '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(59, 1, 'System & Settings', 'system', 'Secure and setup your site.', '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(60, 1, 'Basics', 'basics', 'Basic information about your website.', '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(61, 1, 'Site Name', 'name', '', '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(62, 1, 'Accessibility', 'accessibility', '', '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(63, 1, 'Social Links', 'social', '', '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(64, 1, 'Bookmark Icons', 'icons', 'Bookmark icon and mobile home screen icon setup.', '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(65, 1, 'Rich Text Editor', 'editor', '', '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(66, 1, 'Languages', 'multilingual', '', '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(67, 1, 'Time Zone', 'timezone', '', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(68, 1, 'Multilingual', 'multilingual', 'Run your site in multiple languages.', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(69, 1, 'Multilingual Setup', 'setup', '', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(70, 1, 'Page Report', 'page_report', '', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(71, 1, 'Translate Site Interface', 'translate_interface', '', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(72, 1, 'SEO & Statistics', 'seo', 'Enable pretty URLs and tracking codes.', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(73, 1, 'URLs and Redirection', 'urls', '', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(74, 1, 'Bulk SEO Updater', 'bulk', '', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(75, 1, 'Tracking Codes', 'codes', '', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(76, 1, 'Excluded URL Word List', 'excluded', '', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(77, 1, 'Search Index', 'searchindex', '', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(78, 1, 'Files', 'files', '', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(79, 1, 'File Manager Permissions', 'permissions', '', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(80, 1, 'Allowed File Types', 'filetypes', '', '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(81, 1, 'Thumbnails', 'thumbnails', '', '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(82, 1, 'File Storage Locations', 'storage', '', '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(83, 1, 'Optimization', 'optimization', 'Keep your site running well.', '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(84, 1, 'Cache & Speed Settings', 'cache', '', '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(85, 1, 'Clear Cache', 'clearcache', '', '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(86, 1, 'Automated Jobs', 'jobs', '', '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(87, 1, 'Database Query Log', 'query_log', '', '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(88, 1, 'Permissions & Access', 'permissions', 'Control who sees and edits your site.', '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(89, 1, 'Site Access', 'site', '', '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(90, 1, 'Task Permissions', 'tasks', '', '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(91, 1, 'User Permissions', 'users', '', '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(92, 1, 'Advanced Permissions', 'advanced', '', '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(93, 1, 'IP Blacklist', 'blacklist', '', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(94, 1, 'Captcha Setup', 'captcha', '', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(95, 1, 'Spam Control', 'antispam', '', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(96, 1, 'Maintenance Mode', 'maintenance', '', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(97, 1, 'Login & Registration', 'registration', 'Change login behaviors and setup public profiles.', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(98, 1, 'Login Destination', 'postlogin', '', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(99, 1, 'Public Profiles', 'profiles', '', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(100, 1, 'Public Registration', 'open', '', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(101, 1, 'Authentication Types', 'authentication', '', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(102, 1, 'Email', 'mail', 'Control how your site send and processes mail.', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(103, 1, 'SMTP Method', 'method', '', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(104, 1, 'Test Mail Settings', 'test', '', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(105, 1, 'Email Importers', 'importers', '', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(106, 1, 'Conversations', 'conversations', 'Manage your conversations settings', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(107, 1, 'Settings', 'settings', '', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(108, 1, 'Community Points', 'points', '', '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(109, 1, 'Banned Words', 'bannedwords', '', '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(110, 1, 'Conversation Permissions', 'permissions', '', '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(111, 1, 'Attributes', 'attributes', 'Setup attributes for pages, users, files and more.', '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(112, 1, 'Sets', 'sets', 'Group attributes into sets for easier organization', '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(113, 1, 'Types', 'types', 'Choose which attribute types are available for different items.', '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(114, 1, 'Topics', 'topics', '', '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(115, 1, 'Add Topic Tree', 'add', '', '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(116, 1, 'Environment', 'environment', 'Advanced settings for web developers.', '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(117, 1, 'Environment Information', 'info', '', '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(118, 1, 'Debug Settings', 'debug', '', '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(119, 1, 'Logging Settings', 'logging', '', '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(120, 1, 'Proxy Server', 'proxy', '', '2015-04-30 16:39:31', '2015-04-30 16:39:31', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(121, 1, 'Backup & Restore', 'backup', 'Backup or restore your website.', '2015-04-30 16:39:31', '2015-04-30 16:39:31', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(122, 1, 'Backup Database', 'backup', '', '2015-04-30 16:39:31', '2015-04-30 16:39:31', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(123, 1, 'Update concrete5', 'update', '', '2015-04-30 16:39:31', '2015-04-30 16:39:31', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(124, 1, 'Welcome to concrete5', 'welcome', 'Learn about how to use concrete5, how to develop for concrete5, and get general help.', '2015-04-30 16:39:31', '2015-04-30 16:39:31', 'Initial Version', 1, 0, 1, NULL, 2, 4, NULL),
(125, 1, 'Customize Dashboard Home', 'home', '', '2015-04-30 16:39:31', '2015-04-30 16:39:31', 'Initial Version', 1, 0, 1, NULL, 2, 2, NULL),
(126, 1, 'Drafts', '!drafts', '', '2015-04-30 16:39:38', '2015-04-30 16:39:38', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(127, 1, 'Trash', '!trash', '', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(128, 1, 'Stacks', '!stacks', '', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(129, 1, 'Login', 'login', '', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(130, 1, 'Register', 'register', '', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(131, 1, 'My Account', 'account', '', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(132, 1, 'Edit Profile', 'edit_profile', 'Edit your user profile and change password.', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(133, 1, 'Profile Picture', 'avatar', 'Specify a new image attached to posts or edits.', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(134, 1, 'Messages', 'messages', 'Inbox for site-specific messages.', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(135, 1, 'Inbox', 'inbox', '', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(136, 1, 'Members', 'members', '', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(137, 1, 'View Profile', 'profile', '', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(138, 1, 'Directory', 'directory', '', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(139, 1, 'Page Not Found', 'page_not_found', '', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(140, 1, 'Page Forbidden', 'page_forbidden', '', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(141, 1, 'Download File', 'download_file', '', '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(142, 1, '', NULL, NULL, '2015-04-30 16:39:43', '2015-04-30 16:39:43', 'Initial Version', 1, 0, NULL, NULL, 0, 5, NULL),
(143, 1, 'Header Site Title', 'header-site-title', NULL, '2015-04-30 20:35:38', '2015-04-30 20:35:38', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(144, 1, 'Header Navigation', 'header-navigation', NULL, '2015-04-30 20:35:38', '2015-04-30 20:35:38', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(145, 1, 'Footer Legal', 'footer-legal', NULL, '2015-04-30 20:35:38', '2015-04-30 20:35:38', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(146, 1, 'Footer Navigation', 'footer-navigation', NULL, '2015-04-30 20:35:38', '2015-04-30 20:35:38', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(147, 1, 'Footer Contact', 'footer-contact', NULL, '2015-04-30 20:35:38', '2015-04-30 20:35:38', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(148, 1, 'Om Diers klinik', 'om-diers-klinik', '', '2015-04-30 20:45:12', '2015-04-30 20:45:12', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(148, 2, 'Om Diers klinik', 'om-diers-klinik', '', '2015-04-30 20:45:12', '2015-05-13 10:44:56', 'Version 2', 0, 0, 1, 1, 2, 5, NULL),
(148, 3, 'Om Diers klinik', 'om-diers-klinik', '', '2015-04-30 20:45:12', '2015-05-19 09:15:40', 'Ny version 3', 0, 0, 1, 1, 2, 5, NULL),
(148, 4, 'Om Diers klinik', 'om-diers-klinik', '', '2015-04-30 20:45:12', '2015-05-19 10:05:47', 'Ny version 4', 0, 0, 1, 1, 2, 5, NULL),
(148, 5, 'Om Diers klinik', 'om-diers-klinik', '', '2015-04-30 20:45:12', '2015-05-19 10:07:43', 'Version 5', 0, 0, 1, 1, 2, 5, NULL),
(148, 6, 'Om Diers klinik', 'om-diers-klinik', '', '2015-04-30 20:45:12', '2015-05-19 10:17:06', 'Version 6', 0, 0, 1, 1, 2, 5, NULL),
(148, 7, 'Om Diers klinik', 'om-diers-klinik', '', '2015-04-30 20:45:12', '2015-05-20 10:35:51', 'Version 7', 0, 0, 1, 1, 2, 5, NULL),
(148, 8, 'Om Diers klinik', 'om-diers-klinik', '', '2015-04-30 20:45:12', '2015-05-20 11:11:29', 'Ny version 8', 1, 0, 1, 1, 2, 5, NULL),
(149, 1, 'Header Search', 'header-search', NULL, '2015-04-30 20:45:15', '2015-04-30 20:45:15', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(150, 1, 'Footer Site Title', 'footer-site-title', NULL, '2015-04-30 20:45:15', '2015-04-30 20:45:15', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(151, 1, 'Footer Social', 'footer-social', NULL, '2015-04-30 20:45:15', '2015-04-30 20:45:15', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(152, 1, 'EN', 'en', '', '2015-04-30 20:47:37', '2015-04-30 20:47:37', 'Initial Version', 1, 0, 1, 1, 2, 5, NULL),
(152, 2, 'EN', 'en', '', '2015-04-30 20:47:37', '2015-05-08 00:53:54', 'Ny version 2', 0, 1, 1, NULL, 2, 5, NULL),
(153, 1, 'om diers 2', 'om-diers-2', '', '2015-04-30 20:48:06', '2015-04-30 20:48:06', 'Initial Version', 1, 0, 1, 1, 2, 5, NULL),
(154, 1, 'FooterInfoText', 'footerinfotext', NULL, '2015-04-30 21:21:46', '2015-04-30 21:21:46', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(155, 1, 'Twitter Feed', 'twitter-feed', NULL, '2015-04-30 21:21:46', '2015-04-30 21:21:46', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(156, 1, 'Footer kolonne to', 'footer-kolonne', NULL, '2015-05-07 23:27:40', '2015-05-07 23:27:40', 'Initial Version', 0, 0, 1, NULL, 2, 0, NULL),
(156, 2, 'Footer kolonne to', 'footer-kolonne', NULL, '2015-05-07 23:27:40', '2015-05-07 23:27:57', 'Ny version 2', 0, 0, 1, 1, 2, 0, NULL),
(156, 3, 'Footer kolonne to', 'footer-kolonne', NULL, '2015-05-07 23:27:40', '2015-05-07 23:35:03', 'Ny version 3', 1, 0, 1, 1, 2, 0, NULL),
(157, 1, 'Footer kolonne et', 'footer-kolonne-et', NULL, '2015-05-07 23:30:27', '2015-05-07 23:30:27', 'Initial Version', 0, 0, 1, NULL, 2, 0, NULL),
(157, 2, 'Footer kolonne et', 'footer-kolonne-et', NULL, '2015-05-07 23:30:27', '2015-05-07 23:31:06', 'Ny version 2', 0, 0, 1, 1, 2, 0, NULL),
(157, 3, 'Footer kolonne et', 'footer-kolonne-et', NULL, '2015-05-07 23:30:27', '2015-05-20 11:17:42', 'Ny version 3', 1, 0, 1, 1, 2, 0, NULL),
(158, 1, 'navigation', 'navigation', NULL, '2015-05-08 00:19:38', '2015-05-08 00:19:38', 'Initial Version', 0, 0, 1, NULL, 2, 0, NULL),
(158, 2, 'navigation', 'navigation', NULL, '2015-05-08 00:19:38', '2015-05-08 00:20:29', 'Ny version 2', 0, 0, 1, 1, 2, 0, NULL),
(158, 3, 'navigation', 'navigation', NULL, '2015-05-08 00:19:38', '2015-05-08 00:27:30', 'Ny version 3', 0, 0, 1, 1, 2, 0, NULL),
(158, 4, 'navigation', 'navigation', NULL, '2015-05-08 00:19:38', '2015-05-12 09:23:28', 'Ny version 4', 1, 0, 1, 1, 2, 0, NULL),
(158, 5, 'navigation', 'navigation', NULL, '2015-05-08 00:19:38', '2015-05-13 06:57:54', 'Ny version 5', 0, 0, 1, NULL, 2, 0, NULL),
(159, 1, 'Insemination', 'insemination', '', '2015-05-08 00:54:10', '2015-05-08 00:54:10', 'Initial Version', 0, 0, 1, 1, 2, 9, NULL),
(159, 2, 'Insemination', 'insemination', '', '2015-05-08 00:54:10', '2015-05-13 07:46:34', 'Version 2', 0, 0, 1, 1, 2, 9, NULL),
(159, 3, 'Insemination', 'insemination', '', '2015-05-08 00:54:10', '2015-05-13 09:00:15', 'Version 3', 0, 0, 1, 1, 2, 9, NULL),
(159, 4, 'Insemination', 'insemination', '', '2015-05-08 00:54:10', '2015-05-19 09:20:47', 'Ny version 4', 0, 0, 1, 1, 2, 9, NULL),
(159, 5, 'Insemination', 'insemination', '', '2015-05-08 00:54:10', '2015-05-20 10:36:34', 'Version 5', 1, 0, 1, 1, 2, 9, NULL),
(160, 1, '', NULL, NULL, '2015-05-08 00:58:23', '2015-05-08 00:58:23', 'Initial Version', 1, 0, NULL, NULL, 0, 9, NULL),
(161, 1, 'Sæddonore', 'saeddonore', '', '2015-05-08 00:59:16', '2015-05-08 00:59:16', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(161, 2, 'Sæddonorerne', 'saeddonorerne', '', '2015-05-08 00:59:16', '2015-05-13 09:20:10', 'Ny version 2', 0, 0, 1, 1, 2, 5, NULL),
(161, 3, 'Sæddonorerne', 'saeddonorerne', '', '2015-05-08 00:59:16', '2015-05-13 09:22:11', 'Version 3', 0, 0, 1, 1, 2, 5, NULL),
(161, 4, 'Sæddonorerne', 'saeddonorerne', '', '2015-05-08 00:59:16', '2015-05-13 09:38:30', 'Ny version 4', 0, 0, 1, 1, 2, 5, NULL),
(161, 5, 'Sæddonorerne', 'saeddonorerne', '', '2015-05-08 00:59:16', '2015-05-13 09:41:11', 'Ny version 5', 0, 0, 1, 1, 2, 5, NULL),
(161, 6, 'Sæddonorerne', 'saeddonorerne', '', '2015-05-08 00:59:16', '2015-05-13 09:41:12', 'Ny version 6', 0, 0, 1, 1, 2, 5, NULL),
(161, 7, 'Sæddonorerne', 'saeddonorerne', '', '2015-05-08 00:59:16', '2015-05-13 10:29:33', 'Version 7', 0, 0, 1, 1, 2, 5, NULL),
(161, 8, 'Sæddonorerne', 'saeddonorerne', '', '2015-05-08 00:59:16', '2015-05-19 12:51:24', 'Version 8', 0, 0, 1, 1, 2, 5, NULL),
(161, 9, 'Sæddonorerne', 'saeddonorerne', '', '2015-05-08 00:59:16', '2015-05-20 10:44:04', 'Version 9', 0, 0, 1, 1, 2, 5, NULL),
(161, 10, 'Sæddonorerne', 'saeddonorerne', '', '2015-05-08 00:59:16', '2015-05-26 10:20:07', 'Version 10', 1, 0, 1, 1, 2, 5, NULL),
(162, 1, 'Hjemmeinsimination', 'hjemmeinsimina', '', '2015-05-08 01:00:22', '2015-05-08 01:00:22', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(162, 2, 'Hjemmeinsemination', 'hjemmeinsemination', '', '2015-05-08 01:00:22', '2015-05-13 09:15:40', 'Ny version 2', 0, 0, 1, 1, 2, 5, NULL),
(162, 3, 'Hjemmeinsemination', 'hjemmeinsemination', '', '2015-05-08 01:00:00', '2015-05-18 12:35:15', 'Version 3', 0, 0, 1, 1, 2, 5, NULL),
(162, 4, 'Hjemmeinsemination', 'hjemmeinsemination', '', '2015-05-08 01:00:00', '2015-05-18 12:37:01', 'Version 4', 0, 0, 1, 1, 2, 5, NULL),
(162, 5, 'Hjemmeinsemination', 'hjemmeinsemination', '', '2015-05-08 01:00:00', '2015-05-19 08:53:08', 'Ny version 5', 1, 0, 1, 1, 2, 5, NULL),
(163, 1, 'Ultralydscaning', 'ultralydscaning', '', '2015-05-08 01:01:21', '2015-05-08 01:01:21', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(163, 2, 'Ultralydscanning', 'ultralydscanning', '', '2015-05-08 01:01:21', '2015-05-13 09:10:00', 'Ny version 2', 0, 0, 1, 1, 2, 5, NULL),
(163, 3, 'Ultralydscanning', 'ultralydscanning', '', '2015-05-08 01:01:21', '2015-05-13 09:13:30', 'Version 3', 0, 0, 1, 1, 2, 5, NULL),
(163, 4, 'Ultralydscanning', 'ultralydscanning', '', '2015-05-08 01:01:21', '2015-05-13 09:14:38', 'Version 4', 0, 0, 1, 1, 2, 5, NULL),
(163, 5, 'Ultralydscaning', 'ultralydscaning', '', '2015-05-08 01:01:21', '2015-05-13 09:15:11', 'Ny version 5', 0, 0, 1, 1, 2, 5, NULL),
(163, 6, 'Ultralydsscaning', 'ultralydsscaning', '', '2015-05-08 01:01:21', '2015-05-13 09:42:38', 'Ny version 6', 1, 0, 1, 1, 2, 5, NULL),
(164, 1, 'Priser', 'priser', '', '2015-05-08 01:02:26', '2015-05-08 01:02:26', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(164, 2, 'Priser', 'priser', '', '2015-05-08 01:02:26', '2015-05-08 01:02:54', 'Ny version 2', 0, 0, 1, 1, 2, 5, NULL),
(164, 3, 'Priser', 'priser', '', '2015-05-08 01:02:26', '2015-05-13 11:14:57', 'Version 3', 0, 0, 1, 1, 2, 5, NULL),
(164, 4, 'Priser', 'priser', '', '2015-05-08 01:02:26', '2015-05-13 11:22:32', 'Version 4', 0, 0, 1, 1, 2, 5, NULL),
(164, 5, 'Priser', 'priser', '', '2015-05-08 01:02:26', '2015-05-13 11:33:37', 'Version 5', 0, 0, 1, 1, 2, 5, NULL),
(164, 6, 'Priser', 'priser', '', '2015-05-08 01:02:26', '2015-05-13 11:44:09', 'Ny version 6', 0, 0, 1, 1, 2, 5, NULL),
(164, 7, 'Priser', 'priser', '', '2015-05-08 01:02:26', '2015-05-13 11:54:36', 'Version 7', 0, 0, 1, 1, 2, 5, NULL),
(164, 8, 'Priser', 'priser', '', '2015-05-08 01:02:26', '2015-05-13 11:58:33', 'Version 8', 0, 0, 1, 1, 2, 5, NULL),
(164, 9, 'Priser', 'priser', '', '2015-05-08 01:02:26', '2015-05-20 11:01:41', 'Version 9', 0, 0, 1, 1, 2, 5, NULL),
(164, 10, 'Priser', 'priser', '', '2015-05-08 01:02:26', '2015-05-20 11:05:00', 'Version 10', 1, 0, 1, 1, 2, 5, NULL),
(165, 1, 'Praktisk info', 'praktisk-info', '', '2015-05-08 01:03:13', '2015-05-08 01:03:13', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(165, 2, 'Praktisk info', 'praktisk-info', '', '2015-05-08 01:03:13', '2015-05-20 06:20:50', 'Version 2', 0, 0, 1, 1, 2, 5, NULL),
(165, 3, 'Praktisk info', 'praktisk-info', '', '2015-05-08 01:03:13', '2015-05-20 06:23:58', 'Version 3', 0, 0, 1, 1, 2, 5, NULL),
(165, 4, 'Praktisk info', 'praktisk-info', '', '2015-05-08 01:03:13', '2015-05-20 10:31:56', 'Version 4', 1, 0, 1, 1, 2, 5, NULL),
(166, 1, 'Anmeldelser', 'anmeldelser', '', '2015-05-08 01:04:00', '2015-05-08 01:04:00', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(166, 2, 'Anmeldelser', 'anmeldelser', '', '2015-05-08 01:04:00', '2015-05-19 13:57:23', 'Version 2', 0, 0, 1, 1, 2, 5, NULL),
(166, 3, 'Anmeldelser', 'anmeldelser', '', '2015-05-08 01:04:00', '2015-05-19 14:19:26', 'Version 3', 0, 0, 1, 1, 2, 5, NULL),
(166, 4, 'Anmeldelser', 'anmeldelser', '', '2015-05-08 01:04:00', '2015-05-19 15:51:41', 'Version 4', 0, 0, 1, 1, 2, 5, NULL),
(166, 5, 'Anmeldelser', 'anmeldelser', '', '2015-05-08 01:04:00', '2015-05-20 07:03:55', 'Ny version 5', 0, 0, 1, 1, 2, 5, NULL),
(166, 6, 'Anmeldelser', 'anmeldelser', '', '2015-05-08 01:04:00', '2015-05-20 09:25:38', 'Version 6', 1, 0, 1, 1, 2, 5, NULL),
(167, 10, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-18 12:11:35', 'Ny version 10', 0, 0, 1, 1, 2, 7, NULL),
(167, 11, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-18 12:14:59', 'Ny version 11', 0, 0, 1, 1, 2, 7, NULL),
(167, 12, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-18 12:21:23', 'Version 12', 0, 0, 1, 1, 2, 7, NULL),
(167, 13, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-19 09:02:12', 'Version 13', 0, 0, 1, 1, 2, 7, NULL),
(167, 14, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-19 10:10:25', 'Version 14', 0, 0, 1, 1, 2, 7, NULL),
(167, 15, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-19 10:16:14', 'Version 15', 0, 0, 1, 1, 2, 7, NULL),
(167, 16, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-20 06:30:29', 'Version 16', 0, 0, 1, 1, 2, 7, NULL),
(167, 17, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-20 07:03:29', 'Version 17', 0, 0, 1, 1, 2, 5, NULL),
(167, 18, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-20 07:07:56', 'Ny version 18', 0, 0, 1, 1, 2, 5, NULL),
(167, 19, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-20 07:10:24', 'Ny version 19', 0, 0, 1, 1, 2, 5, NULL),
(167, 20, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-20 07:14:30', 'Version 20', 0, 0, 1, 1, 2, 5, NULL),
(167, 21, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-20 07:25:03', 'Version 21', 0, 0, 1, 1, 2, 5, NULL),
(167, 22, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-20 10:26:29', 'Version 22', 0, 0, 1, 1, 2, 5, NULL),
(167, 23, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-20 10:28:33', 'Version 23', 0, 0, 1, 1, 2, 5, NULL),
(167, 24, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-20 11:19:11', 'Version 24', 0, 0, 1, 1, 2, 5, NULL),
(167, 25, 'Kontakt', 'kontakt', '', '2015-05-08 01:04:48', '2015-05-22 13:13:04', 'Version 25', 1, 0, 1, 1, 2, 5, NULL),
(168, 1, '', NULL, NULL, '2015-05-08 01:05:09', '2015-05-08 01:05:09', 'Initial Version', 1, 0, NULL, NULL, 0, 7, NULL),
(169, 1, 'Personale', 'personale', '', '2015-05-12 09:22:07', '2015-05-12 09:22:07', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(169, 2, 'Personale', 'personale', '', '2015-05-12 09:22:07', '2015-05-13 07:22:25', 'Version 2', 0, 0, 1, NULL, 2, 5, NULL),
(169, 3, 'Personale', 'personale', '', '2015-05-12 09:22:07', '2015-05-13 07:40:30', 'Version 3', 0, 0, 1, 1, 2, 5, NULL),
(169, 4, 'Personale', 'personale', '', '2015-05-12 09:22:07', '2015-05-19 09:18:11', 'Ny version 4', 0, 0, 1, 1, 2, 5, NULL),
(169, 5, 'Personale', 'personale', '', '2015-05-12 09:22:07', '2015-05-19 12:59:09', 'Version 5', 0, 0, 1, 1, 2, 5, NULL),
(169, 6, 'Personale', 'personale', '', '2015-05-12 09:22:07', '2015-05-19 13:42:47', 'Version 6', 0, 0, 1, 1, 2, 5, NULL),
(169, 7, 'Personale', 'personale', '', '2015-05-12 09:22:07', '2015-05-20 11:08:53', 'Version 7', 0, 0, 1, 1, 2, 5, NULL),
(169, 8, 'Personale', 'personale', '', '2015-05-12 09:22:07', '2015-05-22 10:36:09', 'Version 8', 0, 0, 1, 1, 2, 5, NULL),
(169, 9, 'Personale', 'personale', '', '2015-05-12 09:22:07', '2015-05-22 11:45:54', 'Version 9', 1, 0, 1, 1, 2, 5, NULL),
(170, 1, 'Statistiker', 'statistiker', '', '2015-05-12 09:53:37', '2015-05-12 09:53:37', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(170, 2, 'Statistiker', 'statistiker', '', '2015-05-12 09:53:37', '2015-05-19 09:19:37', 'Ny version 2', 0, 0, 1, 1, 2, 5, NULL),
(170, 3, 'Statistik', 'statistiker', '', '2015-05-12 09:53:37', '2015-05-22 11:41:51', 'Version 3', 0, 0, 1, 1, 2, 5, NULL),
(170, 4, 'Statistik', 'statistiker', '', '2015-05-12 09:53:37', '2015-05-22 12:08:33', 'Version 4', 0, 0, 1, 1, 2, 5, NULL),
(170, 5, 'Statistik', 'statistiker', '', '2015-05-12 09:53:37', '2015-05-23 15:42:01', 'Version 5', 0, 0, 1, 1, 2, 5, NULL),
(170, 6, 'Statistik', 'statistiker', '', '2015-05-12 09:53:37', '2015-05-26 09:53:21', 'Version 6', 0, 0, 1, 1, 2, 5, NULL),
(170, 7, 'Statistik', 'statistiker', '', '2015-05-12 09:53:37', '2015-05-26 10:16:42', 'Version 7', 1, 0, 1, 1, 2, 5, NULL),
(171, 1, 'Forberedelsen', 'forberedelsen', '', '2015-05-12 10:02:13', '2015-05-12 10:02:13', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(171, 2, 'Forberedelsen', 'forberedelsen', '', '2015-05-12 10:02:13', '2015-05-12 10:14:51', 'Ny version 2', 0, 0, 1, 1, 2, 9, NULL),
(171, 3, 'Forberedelsen', 'forberedelsen', '', '2015-05-12 10:02:13', '2015-05-12 10:18:07', 'Version 3', 0, 0, 1, 1, 2, 9, NULL),
(171, 4, 'Forberedelsen', 'forberedelsen', '', '2015-05-12 10:02:13', '2015-05-12 10:21:20', 'Version 4', 0, 0, 1, 1, 2, 9, NULL),
(171, 5, 'Forberedelsen', 'forberedelsen', '', '2015-05-12 10:02:13', '2015-05-13 06:57:54', 'Version 5', 0, 0, 1, NULL, 2, 9, NULL),
(171, 6, 'Forberedelsen', 'forberedelsen', '', '2015-05-12 10:02:13', '2015-05-13 08:22:03', 'Ny version 6', 0, 0, 1, 1, 2, 9, NULL),
(171, 7, 'Forberedelsen', 'forberedelsen', '', '2015-05-12 10:02:13', '2015-05-13 08:47:07', 'Version 7', 0, 0, 1, 1, 2, 9, NULL),
(171, 8, 'Forberedelsen', 'forberedelsen', '', '2015-05-12 10:02:13', '2015-05-13 08:58:18', 'Version 8', 1, 0, 1, 1, 2, 9, NULL),
(171, 9, 'Forberedelsen', 'forberedelsen', '', '2015-05-12 10:02:13', '2015-05-19 09:23:35', 'Ny version 9', 0, 1, 1, NULL, 2, 9, NULL),
(172, 1, 'Sidebar productgroup', 'sidebar-productgroup', NULL, '2015-05-12 10:15:06', '2015-05-12 10:15:06', 'Initial Version', 0, 0, 1, NULL, 2, 0, NULL),
(172, 2, 'Sidebar productgroup', 'sidebar-productgroup', NULL, '2015-05-12 10:15:06', '2015-05-12 10:36:20', 'Ny version 2', 1, 0, 1, 1, 2, 0, NULL),
(173, 1, 'Breadcrumbs container', 'breadcrumbs-container', NULL, '2015-05-12 10:16:51', '2015-05-12 10:16:51', 'Initial Version', 0, 0, 1, NULL, 2, 0, NULL),
(173, 2, 'Breadcrumbs container', 'breadcrumbs-container', NULL, '2015-05-12 10:16:51', '2015-05-12 10:18:07', 'Ny version 2', 0, 0, 1, 1, 2, 0, NULL),
(173, 3, 'Breadcrumbs container', 'breadcrumbs-container', NULL, '2015-05-12 10:16:51', '2015-05-12 10:18:25', 'Ny version 3', 1, 0, 1, 1, 2, 0, NULL),
(174, 1, 'Header title', 'header-title', NULL, '2015-05-12 10:20:46', '2015-05-12 10:20:46', 'Initial Version', 0, 0, 1, NULL, 2, 0, NULL),
(174, 2, 'Header title', 'header-title', NULL, '2015-05-12 10:20:46', '2015-05-12 10:21:20', 'Ny version 2', 1, 0, 1, 1, 2, 0, NULL),
(175, 1, 'Journalsamtalen', 'journalsamtalen', '', '2015-05-12 13:24:51', '2015-05-12 13:24:51', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(175, 2, 'Journalsamtalen', 'journalsamtalen', '', '2015-05-12 13:24:51', '2015-05-13 08:52:32', 'Version 2', 0, 0, 1, 1, 2, 5, NULL),
(175, 3, 'Journalsamtalen', 'journalsamtalen', '', '2015-05-12 13:24:51', '2015-05-13 09:02:57', 'Version 3', 0, 0, 1, 1, 2, 5, NULL),
(175, 4, 'Journalsamtalen', 'journalsamtalen', '', '2015-05-12 13:24:51', '2015-05-13 09:04:16', 'Version 4', 0, 0, 1, 1, 2, 5, NULL),
(175, 5, 'Journalsamtalen', 'journalsamtalen', '', '2015-05-12 13:24:51', '2015-05-19 09:27:35', 'Version 5', 0, 0, 1, 1, 2, 5, NULL),
(175, 6, 'Journalsamtalen', 'journalsamtalen', '', '2015-05-12 13:24:51', '2015-05-20 10:39:14', 'Version 6', 0, 0, 1, 1, 2, 5, NULL),
(175, 7, 'Journalsamtalen', 'journalsamtalen', '', '2015-05-12 13:24:51', '2015-05-23 14:10:36', 'Version 7', 1, 0, 1, 1, 2, 5, NULL),
(176, 1, 'Scanning', 'scanning', '', '2015-05-12 13:26:11', '2015-05-12 13:26:11', 'Initial Version', 1, 0, 1, 1, 2, 5, NULL),
(176, 2, 'Scanning', 'scanning', '', '2015-05-12 13:26:11', '2015-05-13 09:09:12', 'Ny version 2', 0, 1, 1, NULL, 2, 5, NULL),
(177, 1, '', '', NULL, '2015-05-13 08:31:59', '2015-05-13 08:31:59', 'Initial Version', 0, 1, 1, NULL, 2, 5, NULL),
(178, 1, 'Gode råd', 'gode-rad', '', '2015-05-13 08:33:15', '2015-05-13 08:33:15', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(178, 2, 'Gode råd', 'gode-rad', '', '2015-05-13 08:33:15', '2015-05-13 08:39:53', 'Ny version 2', 0, 0, 1, 1, 2, 5, NULL),
(178, 3, 'Gode råd', 'gode-rad', '', '2015-05-13 08:33:15', '2015-05-19 09:32:07', 'Ny version 3', 0, 0, 1, 1, 2, 5, NULL),
(178, 4, 'Gode råd', 'gode-rad', '', '2015-05-13 08:33:15', '2015-05-20 10:41:30', 'Version 4', 1, 0, 1, 1, 2, 5, NULL),
(179, 1, 'Donor med profil', 'kendt_donor', '', '2015-05-13 09:33:21', '2015-05-13 09:33:21', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(179, 2, 'Ikke Anonym donor', 'ikke-anonym-donor', '', '2015-05-13 09:33:21', '2015-05-13 10:00:46', 'Ny version 2', 0, 0, 1, 1, 2, 5, NULL),
(179, 3, 'Ikke Anonym donor', 'ikke-anonym-donor', '', '2015-05-13 09:33:21', '2015-05-13 10:02:40', 'Version 3', 0, 0, 1, 1, 2, 5, NULL),
(179, 4, '', '', '', '2015-05-13 09:33:21', '2015-05-13 10:04:02', 'Ny version 4', 0, 0, 1, 1, 2, 5, NULL),
(179, 5, 'Ikke-anonym donor', 'ikke-anonym-do', '', '2015-05-13 09:33:21', '2015-05-13 10:04:55', 'Ny version 5', 0, 0, 1, 1, 2, 5, NULL),
(179, 6, 'Ikke-anonym donor', 'ikke-anonym-do', '', '2015-05-13 09:33:21', '2015-05-13 10:24:59', 'Version 6', 1, 0, 1, 1, 2, 5, NULL),
(180, 1, 'Anonym donor', 'anonym-donor', '', '2015-05-13 10:10:35', '2015-05-13 10:10:35', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(180, 2, 'Anonym donor', 'anonym-donor', '', '2015-05-13 10:10:35', '2015-05-13 10:17:08', 'Version 2', 0, 0, 1, 1, 2, 5, NULL),
(180, 3, 'Anonym donor', 'anonym-donor', '', '2015-05-13 10:10:35', '2015-05-13 10:23:28', 'Version 3', 1, 0, 1, 1, 2, 5, NULL),
(181, 1, 'Partnersæd', 'partnersaed', '', '2015-05-13 10:37:07', '2015-05-13 10:37:07', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(181, 2, 'Partnersæd', 'partnersaed', '', '2015-05-13 10:37:07', '2015-05-13 10:39:36', 'Ny version 2', 1, 0, 1, 1, 2, 5, NULL),
(182, 1, '', '', NULL, '2015-05-18 12:14:13', '2015-05-18 12:14:13', 'Initial Version', 0, 1, 1, NULL, 2, 5, NULL),
(184, 1, 'Tak for din henvendelse', 'tak-din-henvendelse', '', '2015-05-18 12:17:48', '2015-05-18 12:17:48', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(184, 2, 'Tak for din henvendelse', 'tak-din-henvendelse', '', '2015-05-18 12:17:48', '2015-05-18 12:19:35', 'Version 2', 0, 0, 1, 1, 2, 5, NULL),
(184, 3, 'Tak for din henvendelse', 'tak-din-henvendelse', '', '2015-05-18 12:17:00', '2015-05-18 12:32:27', 'Ny version 3', 1, 0, 1, 1, 2, 5, NULL),
(185, 1, 'Åbningstider', 'abningstider', '', '2015-05-18 12:26:21', '2015-05-18 12:26:21', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(185, 2, 'Åbningstider', 'abningstider', '', '2015-05-18 12:26:21', '2015-05-19 08:32:36', 'Version 2', 1, 0, 1, 1, 2, 5, NULL),
(186, 1, 'Parkering', 'par', '', '2015-05-18 12:27:25', '2015-05-18 12:27:25', 'Initial Version', 1, 0, 1, 1, 2, 5, NULL),
(186, 2, 'Parkering', 'par', '', '2015-05-18 12:27:00', '2015-05-19 08:36:30', 'Ny version 2', 0, 1, 1, NULL, 2, 5, NULL),
(187, 1, 'Overnatning', 'overnatning', '', '2015-05-18 12:28:28', '2015-05-18 12:28:28', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(187, 2, 'Overnatning', 'overnatning', '', '2015-05-18 12:28:28', '2015-05-19 08:43:48', 'Ny version 2', 1, 0, 1, 1, 2, 5, NULL),
(188, 1, 'Nyttige links', 'nyttige-links', '', '2015-05-18 12:31:27', '2015-05-18 12:31:27', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(188, 2, 'Nyttige links', 'nyttige-links', '', '2015-05-18 12:31:27', '2015-05-19 08:46:17', 'Version 2', 1, 0, 1, 1, 2, 5, NULL),
(189, 1, 'Bøger og artikler', 'boger-og-artikler', '', '2015-05-19 08:47:39', '2015-05-19 08:47:39', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(189, 2, 'Bøger og artikler', 'boger-og-artikler', '', '2015-05-19 08:47:39', '2015-05-19 08:50:39', 'Version 2', 0, 0, 1, 1, 2, 5, NULL),
(189, 3, 'Bøger og artikler', 'boger-og-artikler', '', '2015-05-19 08:47:39', '2015-05-19 08:52:18', 'Version 3', 1, 0, 1, 1, 2, 5, NULL),
(190, 1, 'Database Entities', 'entities', NULL, '2015-05-19 15:46:55', '2015-05-19 15:46:55', 'Initial Version', 1, 0, 1, NULL, 2, 0, NULL),
(191, 1, 'Kendt donor', 'kendt-do', '', '2015-05-22 12:01:50', '2015-05-22 12:01:50', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(191, 2, 'Kendt donor', 'kendt-do', '', '2015-05-22 12:01:50', '2015-05-22 12:05:15', 'Version 2', 1, 0, 1, 1, 2, 5, NULL),
(192, 1, 'Medmoderskab/faderskab', 'medmoderskabfaderskab', '', '2015-05-22 13:48:16', '2015-05-22 13:48:16', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(192, 2, 'Medmoderskab/faderskab', 'medmoderskabfaderskab', '', '2015-05-22 13:48:16', '2015-05-22 13:49:51', 'Version 2', 0, 0, 1, 1, 2, 5, NULL),
(192, 3, 'Medmoderskab/faderskab', 'medmoderskabfaderskab', '', '2015-05-22 13:48:16', '2015-05-22 13:51:25', 'Version 3', 0, 0, 1, 1, 2, 5, NULL),
(192, 4, 'Medmoderskab/faderskab', 'medmoderskabfaderskab', '', '2015-05-22 13:48:16', '2015-05-22 13:52:53', 'Version 4', 1, 0, 1, 1, 2, 5, NULL),
(192, 5, 'Medmoderskab/faderskab', 'medmoderskabfaderskab', '', '2015-05-22 13:48:16', '2015-05-22 13:54:31', 'Ny version 5', 0, 1, 1, NULL, 2, 5, NULL),
(193, 1, 'Diers børn', 'diers-born', '', '2015-05-25 08:16:00', '2015-05-25 08:16:00', 'Initial Version', 0, 0, 1, 1, 2, 5, NULL),
(193, 2, 'Diers børn', 'diers-born', '', '2015-05-25 08:16:00', '2015-05-26 08:29:35', 'Version 2', 0, 0, 1, 1, 2, 5, NULL),
(193, 3, 'Diers børn', 'diers-born', '', '2015-05-25 08:16:00', '2015-05-26 09:11:57', 'Version 3', 1, 0, 1, 1, 2, 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Collections`
--

CREATE TABLE IF NOT EXISTS `Collections` (
  `cID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cDateAdded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cDateModified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cID`),
  KEY `cIDDateModified` (`cID`,`cDateModified`),
  KEY `cDateModified` (`cDateModified`),
  KEY `cDateAdded` (`cDateAdded`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=194 ;

--
-- Dumping data for table `Collections`
--

INSERT INTO `Collections` (`cID`, `cDateAdded`, `cDateModified`, `cHandle`) VALUES
(1, '2015-04-30 16:39:10', '2015-05-26 08:28:46', 'home'),
(2, '2015-04-30 16:39:21', '2015-04-30 16:39:21', 'dashboard'),
(3, '2015-04-30 16:39:21', '2015-04-30 16:39:21', 'sitemap'),
(4, '2015-04-30 16:39:21', '2015-04-30 16:39:21', 'full'),
(5, '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'explore'),
(6, '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'search'),
(7, '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'files'),
(8, '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'search'),
(9, '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'attributes'),
(10, '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'sets'),
(11, '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'add_set'),
(12, '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'users'),
(13, '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'search'),
(14, '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'groups'),
(15, '2015-04-30 16:39:22', '2015-04-30 16:39:22', 'attributes'),
(16, '2015-04-30 16:39:22', '2015-04-30 16:39:23', 'add'),
(17, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'add_group'),
(18, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'bulkupdate'),
(19, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'group_sets'),
(20, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'points'),
(21, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'assign'),
(22, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'actions'),
(23, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'reports'),
(24, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'forms'),
(25, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'surveys'),
(26, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'logs'),
(27, '2015-04-30 16:39:23', '2015-04-30 16:39:23', 'pages'),
(28, '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'themes'),
(29, '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'inspect'),
(30, '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'types'),
(31, '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'organize'),
(32, '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'add'),
(33, '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'form'),
(34, '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'output'),
(35, '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'attributes'),
(36, '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'permissions'),
(37, '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'templates'),
(38, '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'add'),
(39, '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'attributes'),
(40, '2015-04-30 16:39:24', '2015-04-30 16:39:24', 'single'),
(41, '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'feeds'),
(42, '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'conversations'),
(43, '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'messages'),
(44, '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'workflow'),
(45, '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'workflows'),
(46, '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'me'),
(47, '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'blocks'),
(48, '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'stacks'),
(49, '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'permissions'),
(50, '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'list'),
(51, '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'types'),
(52, '2015-04-30 16:39:25', '2015-04-30 16:39:25', 'extend'),
(53, '2015-04-30 16:39:25', '2015-04-30 16:39:26', 'news'),
(54, '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'install'),
(55, '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'update'),
(56, '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'connect'),
(57, '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'themes'),
(58, '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'addons'),
(59, '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'system'),
(60, '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'basics'),
(61, '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'name'),
(62, '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'accessibility'),
(63, '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'social'),
(64, '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'icons'),
(65, '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'editor'),
(66, '2015-04-30 16:39:26', '2015-04-30 16:39:26', 'multilingual'),
(67, '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'timezone'),
(68, '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'multilingual'),
(69, '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'setup'),
(70, '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'page_report'),
(71, '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'translate_interface'),
(72, '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'seo'),
(73, '2015-04-30 16:39:27', '2015-05-19 15:46:55', 'urls'),
(74, '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'bulk'),
(75, '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'codes'),
(76, '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'excluded'),
(77, '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'searchindex'),
(78, '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'files'),
(79, '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'permissions'),
(80, '2015-04-30 16:39:27', '2015-04-30 16:39:27', 'filetypes'),
(81, '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'thumbnails'),
(82, '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'storage'),
(83, '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'optimization'),
(84, '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'cache'),
(85, '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'clearcache'),
(86, '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'jobs'),
(87, '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'query_log'),
(88, '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'permissions'),
(89, '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'site'),
(90, '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'tasks'),
(91, '2015-04-30 16:39:28', '2015-04-30 16:39:28', 'users'),
(92, '2015-04-30 16:39:28', '2015-04-30 16:39:29', 'advanced'),
(93, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'blacklist'),
(94, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'captcha'),
(95, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'antispam'),
(96, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'maintenance'),
(97, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'registration'),
(98, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'postlogin'),
(99, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'profiles'),
(100, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'open'),
(101, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'authentication'),
(102, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'mail'),
(103, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'method'),
(104, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'test'),
(105, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'importers'),
(106, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'conversations'),
(107, '2015-04-30 16:39:29', '2015-04-30 16:39:29', 'settings'),
(108, '2015-04-30 16:39:29', '2015-04-30 16:39:30', 'points'),
(109, '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'bannedwords'),
(110, '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'permissions'),
(111, '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'attributes'),
(112, '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'sets'),
(113, '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'types'),
(114, '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'topics'),
(115, '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'add'),
(116, '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'environment'),
(117, '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'info'),
(118, '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'debug'),
(119, '2015-04-30 16:39:30', '2015-04-30 16:39:30', 'logging'),
(120, '2015-04-30 16:39:31', '2015-04-30 16:39:31', 'proxy'),
(121, '2015-04-30 16:39:31', '2015-04-30 16:39:31', 'backup'),
(122, '2015-04-30 16:39:31', '2015-04-30 16:39:31', 'backup'),
(123, '2015-04-30 16:39:31', '2015-04-30 16:39:31', 'update'),
(124, '2015-04-30 16:39:31', '2015-04-30 16:39:31', 'welcome'),
(125, '2015-04-30 16:39:31', '2015-04-30 16:39:31', 'home'),
(126, '2015-04-30 16:39:38', '2015-04-30 16:39:39', '!drafts'),
(127, '2015-04-30 16:39:39', '2015-04-30 16:39:39', '!trash'),
(128, '2015-04-30 16:39:39', '2015-04-30 16:39:39', '!stacks'),
(129, '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'login'),
(130, '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'register'),
(131, '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'account'),
(132, '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'edit_profile'),
(133, '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'avatar'),
(134, '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'messages'),
(135, '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'inbox'),
(136, '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'members'),
(137, '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'profile'),
(138, '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'directory'),
(139, '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'page_not_found'),
(140, '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'page_forbidden'),
(141, '2015-04-30 16:39:39', '2015-04-30 16:39:39', 'download_file'),
(142, '2015-04-30 16:39:43', '2015-04-30 16:39:43', NULL),
(143, '2015-04-30 20:35:38', '2015-04-30 20:35:38', 'header-site-title'),
(144, '2015-04-30 20:35:38', '2015-04-30 20:35:38', 'header-navigation'),
(145, '2015-04-30 20:35:38', '2015-04-30 20:35:38', 'footer-legal'),
(146, '2015-04-30 20:35:38', '2015-04-30 20:35:38', 'footer-navigation'),
(147, '2015-04-30 20:35:38', '2015-04-30 20:35:38', 'footer-contact'),
(148, '2015-04-30 20:45:12', '2015-05-27 07:19:11', ''),
(149, '2015-04-30 20:45:15', '2015-04-30 20:45:15', 'header-search'),
(150, '2015-04-30 20:45:15', '2015-04-30 20:45:15', 'footer-site-title'),
(151, '2015-04-30 20:45:15', '2015-04-30 20:45:15', 'footer-social'),
(152, '2015-04-30 20:47:37', '2015-05-08 00:53:55', ''),
(153, '2015-04-30 20:48:06', '2015-04-30 20:48:32', ''),
(154, '2015-04-30 21:21:46', '2015-04-30 21:21:46', 'footerinfotext'),
(155, '2015-04-30 21:21:46', '2015-04-30 21:21:46', 'twitter-feed'),
(156, '2015-05-07 23:27:40', '2015-05-07 23:35:17', 'footer-kolonne'),
(157, '2015-05-07 23:30:27', '2015-05-20 11:18:02', 'footer-kolonne-et'),
(158, '2015-05-08 00:19:38', '2015-05-12 09:23:37', 'navigation'),
(159, '2015-05-08 00:54:10', '2015-05-20 10:36:40', ''),
(160, '2015-05-08 00:58:23', '2015-05-08 00:58:23', NULL),
(161, '2015-05-08 00:59:16', '2015-05-26 10:20:47', ''),
(162, '2015-05-08 01:00:22', '2015-05-19 08:53:38', ''),
(163, '2015-05-08 01:01:21', '2015-05-13 09:42:39', ''),
(164, '2015-05-08 01:02:26', '2015-05-20 11:05:31', ''),
(165, '2015-05-08 01:03:13', '2015-05-20 10:32:07', ''),
(166, '2015-05-08 01:04:00', '2015-05-20 09:26:11', ''),
(167, '2015-05-08 01:04:48', '2015-05-22 13:17:30', ''),
(168, '2015-05-08 01:05:09', '2015-05-08 01:05:09', NULL),
(169, '2015-05-12 09:22:07', '2015-05-22 11:49:15', ''),
(170, '2015-05-12 09:53:37', '2015-05-26 10:16:54', ''),
(171, '2015-05-12 10:02:13', '2015-05-19 09:23:35', ''),
(172, '2015-05-12 10:15:06', '2015-05-12 10:36:28', 'sidebar-productgroup'),
(173, '2015-05-12 10:16:51', '2015-05-12 10:19:21', 'breadcrumbs-container'),
(174, '2015-05-12 10:20:46', '2015-05-12 10:36:28', 'header-title'),
(175, '2015-05-12 13:24:51', '2015-05-23 14:10:59', ''),
(176, '2015-05-12 13:26:11', '2015-05-13 09:09:34', ''),
(177, '2015-05-13 08:31:59', '2015-05-13 08:31:59', ''),
(178, '2015-05-13 08:33:15', '2015-05-20 10:41:39', ''),
(179, '2015-05-13 09:33:21', '2015-05-13 10:25:11', ''),
(180, '2015-05-13 10:10:35', '2015-05-13 10:23:46', ''),
(181, '2015-05-13 10:37:07', '2015-05-13 10:40:09', ''),
(182, '2015-05-18 12:14:13', '2015-05-18 12:14:15', ''),
(184, '2015-05-18 12:17:48', '2015-05-18 12:32:44', ''),
(185, '2015-05-18 12:26:21', '2015-05-19 08:32:46', ''),
(186, '2015-05-18 12:27:25', '2015-05-19 08:38:12', ''),
(187, '2015-05-18 12:28:28', '2015-05-19 08:44:02', ''),
(188, '2015-05-18 12:31:27', '2015-05-19 08:46:25', ''),
(189, '2015-05-19 08:47:39', '2015-05-19 08:52:26', ''),
(190, '2015-05-19 15:46:55', '2015-05-19 15:46:56', 'entities'),
(191, '2015-05-22 12:01:50', '2015-05-22 12:05:24', ''),
(192, '2015-05-22 13:48:16', '2015-05-22 13:54:32', ''),
(193, '2015-05-25 08:16:00', '2015-05-26 09:14:20', '');

-- --------------------------------------------------------

--
-- Table structure for table `Config`
--

CREATE TABLE IF NOT EXISTS `Config` (
  `configNamespace` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `configGroup` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `configItem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `configValue` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`configNamespace`,`configGroup`,`configItem`),
  KEY `configGroup` (`configGroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Config`
--

INSERT INTO `Config` (`configNamespace`, `configGroup`, `configItem`, `configValue`) VALUES
('', 'concrete', 'marketplace.token', 'VXZOn8l2SOfSDpAF2WWArpRavU7IsK4pgZORhfwbXTzrC6rxxKtMPi0QAguaq5wk'),
('', 'concrete', 'marketplace.url_token', 'gcp6zropnrzphqftck4xnmyb'),
('', 'concrete', 'security.token.encryption', 'ujbwsMhvMtXnsa3e6VDL2EF6hmLgxikno8pTFFqObkc1kQRw5u4CMPKvZXI79pyo'),
('', 'concrete', 'security.token.jobs', 'cmfua28izFV23Wpn42fMv7wvtl6IkNsh6SazrsjiPtOvHnhyJYz5Tx52PN8H5nKF'),
('', 'concrete', 'security.token.validation', 'miC2ghvQ8ujsVjwq4A0sh9stfcA6RGTBQ9kfU5jGBZ1YUkufiXo7JRggpJu8Ti4s');

-- --------------------------------------------------------

--
-- Table structure for table `ConfigStore`
--

CREATE TABLE IF NOT EXISTS `ConfigStore` (
  `cfKey` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cfValue` longtext COLLATE utf8_unicode_ci,
  `uID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cfKey`,`uID`),
  KEY `uID` (`uID`,`cfKey`),
  KEY `pkgID` (`pkgID`,`cfKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ConfigStore`
--

INSERT INTO `ConfigStore` (`cfKey`, `timestamp`, `cfValue`, `uID`, `pkgID`) VALUES
('DISABLED_HELP_NOTIFICATIONS', '2015-04-30 20:45:42', 'a:2:{s:5:"panel";a:2:{s:14:"/page/composer";b:1;s:16:"/page/attributes";b:1;}s:4:"page";a:6:{s:26:"/dashboard/system/seo/urls";b:1;s:23:"/dashboard/pages/themes";b:1;s:41:"/dashboard/system/optimization/clearcache";b:1;s:23:"/dashboard/sitemap/full";b:1;s:31:"/dashboard/system/backup/backup";b:1;s:31:"/dashboard/system/backup/update";b:1;}}', 1, 0),
('MAIN_HELP_LAST_VIEWED', '2015-05-19 15:40:31', '1432050031', 1, 0),
('NEWSFLOW_LAST_VIEWED', '2015-04-30 16:39:45', '1432723928', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ConversationDiscussions`
--

CREATE TABLE IF NOT EXISTS `ConversationDiscussions` (
  `cnvDiscussionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cnvDiscussionDateCreated` datetime NOT NULL,
  `cID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`cnvDiscussionID`),
  KEY `cID` (`cID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ConversationEditors`
--

CREATE TABLE IF NOT EXISTS `ConversationEditors` (
  `cnvEditorID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cnvEditorHandle` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnvEditorName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnvEditorIsActive` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cnvEditorID`),
  KEY `pkgID` (`pkgID`,`cnvEditorHandle`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ConversationEditors`
--

INSERT INTO `ConversationEditors` (`cnvEditorID`, `cnvEditorHandle`, `cnvEditorName`, `cnvEditorIsActive`, `pkgID`) VALUES
(1, 'plain_text', 'Plain Text', 0, 0),
(2, 'markdown', 'Markdown', 0, 0),
(3, 'redactor', 'Redactor', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ConversationFeatureDetailAssignments`
--

CREATE TABLE IF NOT EXISTS `ConversationFeatureDetailAssignments` (
  `faID` int(10) unsigned NOT NULL DEFAULT '0',
  `cnvID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`faID`),
  KEY `cnvID` (`cnvID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ConversationFeatureDetailAssignments`
--

INSERT INTO `ConversationFeatureDetailAssignments` (`faID`, `cnvID`) VALUES
(3, 1),
(4, 1),
(5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ConversationFlaggedMessageTypes`
--

CREATE TABLE IF NOT EXISTS `ConversationFlaggedMessageTypes` (
  `cnvMessageFlagTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cnvMessageFlagTypeHandle` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cnvMessageFlagTypeID`),
  UNIQUE KEY `cnvMessageFlagTypeHandle` (`cnvMessageFlagTypeHandle`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ConversationFlaggedMessageTypes`
--

INSERT INTO `ConversationFlaggedMessageTypes` (`cnvMessageFlagTypeID`, `cnvMessageFlagTypeHandle`) VALUES
(1, 'spam');

-- --------------------------------------------------------

--
-- Table structure for table `ConversationFlaggedMessages`
--

CREATE TABLE IF NOT EXISTS `ConversationFlaggedMessages` (
  `cnvMessageID` int(10) unsigned NOT NULL,
  `cnvMessageFlagTypeID` int(11) DEFAULT NULL,
  PRIMARY KEY (`cnvMessageID`),
  KEY `cnvMessageFlagTypeID` (`cnvMessageFlagTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ConversationMessageAttachments`
--

CREATE TABLE IF NOT EXISTS `ConversationMessageAttachments` (
  `cnvMessageAttachmentID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cnvMessageID` int(11) DEFAULT NULL,
  `fID` int(11) DEFAULT NULL,
  PRIMARY KEY (`cnvMessageAttachmentID`),
  KEY `cnvMessageID` (`cnvMessageID`),
  KEY `fID` (`fID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ConversationMessageRatings`
--

CREATE TABLE IF NOT EXISTS `ConversationMessageRatings` (
  `cnvMessageRatingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cnvMessageID` int(10) unsigned DEFAULT NULL,
  `cnvRatingTypeID` int(10) unsigned NOT NULL DEFAULT '0',
  `cnvMessageRatingIP` tinyblob,
  `timestamp` datetime DEFAULT NULL,
  `uID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cnvMessageRatingID`),
  KEY `cnvMessageID` (`cnvMessageID`,`cnvRatingTypeID`),
  KEY `cnvRatingTypeID` (`cnvRatingTypeID`),
  KEY `uID` (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ConversationMessages`
--

CREATE TABLE IF NOT EXISTS `ConversationMessages` (
  `cnvMessageID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cnvID` int(10) unsigned NOT NULL DEFAULT '0',
  `uID` int(10) unsigned NOT NULL DEFAULT '0',
  `cnvEditorID` int(10) unsigned NOT NULL DEFAULT '0',
  `cnvMessageSubmitIP` tinyblob,
  `cnvMessageSubmitUserAgent` longtext COLLATE utf8_unicode_ci,
  `cnvMessageLevel` int(10) unsigned NOT NULL DEFAULT '0',
  `cnvMessageParentID` int(10) unsigned NOT NULL DEFAULT '0',
  `cnvMessageDateCreated` datetime DEFAULT NULL,
  `cnvMessageSubject` text COLLATE utf8_unicode_ci,
  `cnvMessageBody` text COLLATE utf8_unicode_ci,
  `cnvIsMessageDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `cnvIsMessageApproved` tinyint(1) NOT NULL DEFAULT '0',
  `cnvMessageTotalRatingScore` bigint(20) DEFAULT '0',
  `cnvMessageAuthorName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnvMessageAuthorEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnvMessageAuthorWebsite` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cnvMessageID`),
  KEY `cnvID` (`cnvID`),
  KEY `cnvMessageParentID` (`cnvMessageParentID`),
  KEY `uID` (`uID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `ConversationMessages`
--

INSERT INTO `ConversationMessages` (`cnvMessageID`, `cnvID`, `uID`, `cnvEditorID`, `cnvMessageSubmitIP`, `cnvMessageSubmitUserAgent`, `cnvMessageLevel`, `cnvMessageParentID`, `cnvMessageDateCreated`, `cnvMessageSubject`, `cnvMessageBody`, `cnvIsMessageDeleted`, `cnvIsMessageApproved`, `cnvMessageTotalRatingScore`, `cnvMessageAuthorName`, `cnvMessageAuthorEmail`, `cnvMessageAuthorWebsite`) VALUES
(1, 1, 1, 3, 0x3365633634303231, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36', 0, 0, '2015-05-19 13:59:16', NULL, '<p>Test af besked</p>', 1, 0, 0, NULL, NULL, NULL),
(2, 1, 0, 3, 0x3365633634303231, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36', 0, 0, '2015-05-19 14:12:48', NULL, '<p>Test 2</p>', 1, 0, 0, NULL, NULL, NULL),
(3, 1, 0, 3, 0x3365633634303231, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36', 1, 2, '2015-05-19 14:17:34', NULL, '<p>Besvarelse</p>', 1, 0, 0, NULL, NULL, NULL),
(4, 2, 0, 3, 0x3365633634303231, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36', 0, 0, '2015-05-19 15:54:35', NULL, '<p>Tekst</p>', 1, 0, 0, 'Fuld navn', 'cm@geekmedia.dk', ''),
(5, 2, 1, 3, 0x3365633634303231, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36', 0, 0, '2015-05-19 15:59:06', NULL, '<p>Test</p><p>Redigeret tekst</p>', 1, 0, 0, 'admin', 'cm@geekmedia.dk', NULL),
(6, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2014-08-24 22:35:31', NULL, '<p>Hej med jer<img src="http://diersklinik.dk/files/misc/icons/smileys/smiley-wink.png" alt=";-)"><br>Får en lille glad tåre i øjet, når jeg tænker på hvad I har hjulpet mig med. Har en meget glad dreng på snart 16 måneder. Tusind tak.<br>Knus til jer<img src="http://diersklinik.dk/files/misc/icons/smileys/smiley-wink.png" alt=";-)"></p>', 0, 1, 0, 'Anne Marie', 'cm@geekmedia.dk', NULL),
(7, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2013-08-12 20:36:00', NULL, '<p>Pleasant atmosphere, professional service. A great employee. I remain very satisfied! <img src="http://diersklinik.dk/files/misc/icons/smileys/smiley.png" alt=":)"> Thanks Kristine</p>', 0, 1, 0, 'Juste', 'cm@geekmedia.dk', NULL),
(8, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2013-03-24 22:22:00', NULL, '<p>Ich war nicht in der Klinik, aber interessantes Internetseitenangebot; Wunschbabies aus dem Einkaufskorb. Herzliche Glückwünsche zu euren Wareneinkäufen und eine schöne Lebzeit mit eurem gekauften Glück <img src="http://diersklinik.dk/files/misc/icons/smileys/smiley.png" alt=":-)"></p>', 0, 1, 0, 'Anonym', 'cm@geekmedia.dk', NULL),
(9, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2012-10-11 14:44:00', NULL, '<p>Da har vi vært på ultralyd (uke16-17) og fått beskjed at det mest sansynelig er en liten jentebaby inni magen <img src="http://diersklinik.dk/files/misc/icons/smileys/heart.png" alt="<3"></p>', 0, 1, 0, 'Ida & Tonje', 'cm@geekmedia.dk', NULL),
(10, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2012-08-07 13:36:00', NULL, '<p>Jeg kan ikke andet end være fuldt ud tilfreds og glad for den supergode behandling, jeg har fået på Diers, da jeg som enlig skulle insemineres i april. Det lykkedes i første forsøg, og nu glæder jeg mig til at få en lillebror til min dreng på 5 år til januar.</p><p>Jeg vil varmt anbefale klinikken til andre, der har brug for fertilitetsbehandling.</p>', 0, 1, 0, 'Linda', 'cm@geekmedia.dk', NULL),
(11, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2012-07-09 10:28:00', NULL, '<p>Vi vil takke for hyggelig og imøtekommende ankomst til klinikken 24 Juni 2012 og vi er utrolig glad for den positive testen vi fikk den 7 Juli 2012 etter første forsøk <img src="http://diersklinik.dk/files/misc/icons/smileys/smiley.png" alt=":)"></p>', 0, 1, 0, 'Ida & Tonje', 'cm@geekmedia.dk', NULL),
(12, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2012-02-02 21:04:00', NULL, '<p>Ville lige sige Hej, og fortælle hvor lykkelig jeg er over at havde taget valget om et donorbarn som jeg føde i juli 2011. En søn som kom i første forsøg. Jeg har allerede fortalt andre der går med tanker om at få et donorbarn om jeres dejlige klinik og hvor søde og imødekommende i er.</p>', 0, 1, 0, 'Anonym', 'cm@geekmedia.dk', NULL),
(13, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2011-09-24 19:40:00', NULL, '<p>Tak for utrolig god behandling og rådgivning, da beslutningen om at blive mor til et donorbarn skulle træffes. For fem dage siden blev jeg mor for første gang. Min vidunderlige søn blev undfanget i første forsøg på Diers Klinik. Verdens dejligste dreng <img src="http://diersklinik.dk/files/misc/icons/smileys/smiley.png" alt=":-)"></p>', 0, 1, 0, 'Anonym', 'cm@geekmedia.dk', NULL),
(14, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2011-01-03 14:18:00', NULL, '<p>For snart et år siden blev jeg gravid på Diers Klinik efter et meget behageligt og professionelt forløb. </p><p>Vi er to forældre, der derfor absolut kun kan videregive vores varmeste anbefalinger.</p><p>Diers Klinik kan vi takke for vores vidunderlige tre måneder gamle søn, og vi vil ikke tøve med at bruge klinikken igen, når vi forhåbentlig skal beriges med endnu et barn engang i fremtiden.</p>', 0, 1, 0, 'Anonym', 'cm@geekmedia.dk', NULL),
(15, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2010-01-11 21:37:00', NULL, '<p>Hej Liza</p><p>Har lige stået og betragtet min skønne lille datter, på 2,5 år, som er blevet til på Diers klinik.<br>Når jeg kigger på hende, ved jeg lige præcis, hvorfor jeg her om en uge eller to, igen besøger jeres hyggelige klinik, for at blive gravid igen.<br>Kan overhovedet ikke forestille mig at skulle insemineres andre steder, end hos jer.<br>Stor tak for super behandling !<br>Mange kærlige hilsner Lotte</p>', 0, 1, 0, 'Anonym', 'cm@geekmedia.dk', NULL),
(16, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2009-11-07 19:23:00', NULL, '<p>Kære Liza </p><p>Vi har en lille lækker guldklump, som I har hjulpet til verden og vi vil bare sige tak for et super forløb. Vi glæder os til at skulle komme hos jer igen, når det bliver tid til en lillesøster eller lillebror. </p><p>Det er dejligt at komme på Diers Klinik, hvor der er en uformel og afslappet atmosfære og hvor man altid føler sig velkommen og i trykke hænder.</p><p>Kh. to stolte mødre</p>', 0, 1, 0, 'Anonym', 'cm@geekmedia.dk', NULL),
(17, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2009-08-12 22:14:00', NULL, '<p>Jeg sidder i øjeblikket med en dejlig stor mave og venter bare på at føde mit donorbarn der er undfanget i første forsøg hos Diers Klinik. </p><p>Jeg er så lykkelig for at have fået muligheden for igen at blive mor, denne gang med en donor som far. </p><p>Den behandling jeg som enlig kvinde har fået hos jer, har været fantastisk. I er meget professionelle og søde og jeg glæder mig meget til at kunne sende jer et billede af min søn om små 6 uger.</p><p>Jeg vil helt sikkert bruge Diers Klinik igen hvis jeg skal have een mere ... og det skal jeg nok.</p><p>Stort knus og tak til jer begge. </p><p>Lea</p>', 0, 1, 0, 'Anonym', 'cm@geekmedia.dk', NULL),
(18, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2009-08-05 11:59:00', NULL, '<p>Vi har været rigtig glade for den behandling, vi har fået. Den har fuldt ud levet op til vores forventninger. Vi har følt, at der er blevet taget rigtig godt imod os, at vi er blevet taget seriøst og at behandlingen har været meget personlig og rettet mod netop vores ønsker og behov. Det har været utroligt rart og trygt, at vi altid har kunnet ringe, hvis vi har været i tvivl om noget, og i klinikken har der altid været god tid til at snakke og stille spørgsmål. Det har desuden været rigtigt dejligt, at I inseminerer på stort set alle tidspunkter, så behandling med hormoner har kunnet undgås.<br>Mvh. Nina og Line</p>', 0, 1, 0, 'Anonym', 'cm@geekmedia.dk', NULL),
(19, 2, 1, 3, 0x3265316631303063, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 0, 0, '2009-06-19 10:12:00', NULL, '<p>Jeg er veldig godt fornøyd, og deres klinikk er et veldig bra tilbud.... og dere to Liza og Kristine er noe hyggelige kvinner, som gjør jobben godt man føler seg trygg med det samme man kommer inn døra hos dere <img src="http://diersklinik.dk/files/misc/icons/smileys/smiley.png" alt=":-)">....... )<br>Klem Carina</p>', 0, 1, 0, 'Anonym', 'cm@geekmedia.dk', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ConversationPermissionAddMessageAccessList`
--

CREATE TABLE IF NOT EXISTS `ConversationPermissionAddMessageAccessList` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  PRIMARY KEY (`paID`,`peID`),
  KEY `peID` (`peID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ConversationPermissionAddMessageAccessList`
--

INSERT INTO `ConversationPermissionAddMessageAccessList` (`paID`, `peID`, `permission`) VALUES
(74, 3, 'U');

-- --------------------------------------------------------

--
-- Table structure for table `ConversationPermissionAssignments`
--

CREATE TABLE IF NOT EXISTS `ConversationPermissionAssignments` (
  `cnvID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkID` int(10) unsigned NOT NULL DEFAULT '0',
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cnvID`,`pkID`,`paID`),
  KEY `paID` (`paID`),
  KEY `pkID` (`pkID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ConversationPermissionAssignments`
--

INSERT INTO `ConversationPermissionAssignments` (`cnvID`, `pkID`, `paID`) VALUES
(0, 66, 65),
(0, 67, 66),
(0, 68, 70),
(0, 69, 68),
(0, 70, 67),
(0, 71, 69),
(0, 72, 71),
(0, 73, 72);

-- --------------------------------------------------------

--
-- Table structure for table `ConversationRatingTypes`
--

CREATE TABLE IF NOT EXISTS `ConversationRatingTypes` (
  `cnvRatingTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cnvRatingTypeHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnvRatingTypeName` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnvRatingTypeCommunityPoints` int(11) DEFAULT NULL,
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cnvRatingTypeID`),
  UNIQUE KEY `cnvRatingTypeHandle` (`cnvRatingTypeHandle`),
  KEY `pkgID` (`pkgID`,`cnvRatingTypeHandle`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ConversationRatingTypes`
--

INSERT INTO `ConversationRatingTypes` (`cnvRatingTypeID`, `cnvRatingTypeHandle`, `cnvRatingTypeName`, `cnvRatingTypeCommunityPoints`, `pkgID`) VALUES
(1, 'up_vote', 'Up Vote', 1, 0),
(2, 'down_vote', 'Down Vote', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ConversationSubscriptions`
--

CREATE TABLE IF NOT EXISTS `ConversationSubscriptions` (
  `cnvID` int(10) unsigned NOT NULL DEFAULT '0',
  `uID` int(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(1) COLLATE utf8_unicode_ci DEFAULT 'S',
  PRIMARY KEY (`cnvID`,`uID`),
  KEY `cnvID` (`cnvID`),
  KEY `uID` (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Conversations`
--

CREATE TABLE IF NOT EXISTS `Conversations` (
  `cnvID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cID` int(10) unsigned DEFAULT '0',
  `cnvDateCreated` datetime NOT NULL,
  `cnvDateLastMessage` datetime DEFAULT NULL,
  `cnvParentMessageID` int(10) unsigned DEFAULT '0',
  `cnvAttachmentsEnabled` tinyint(1) NOT NULL DEFAULT '1',
  `cnvMessagesTotal` int(10) unsigned DEFAULT '0',
  `cnvOverrideGlobalPermissions` tinyint(1) DEFAULT '0',
  `cnvAttachmentOverridesEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `cnvMaxFilesGuest` int(11) DEFAULT '0',
  `cnvMaxFilesRegistered` int(11) DEFAULT '0',
  `cnvMaxFileSizeGuest` int(11) DEFAULT '0',
  `cnvMaxFileSizeRegistered` int(11) DEFAULT '0',
  `cnvFileExtensions` text COLLATE utf8_unicode_ci,
  `cnvNotificationOverridesEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `cnvEnableSubscription` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`cnvID`),
  KEY `cID` (`cID`),
  KEY `cnvParentMessageID` (`cnvParentMessageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Conversations`
--

INSERT INTO `Conversations` (`cnvID`, `cID`, `cnvDateCreated`, `cnvDateLastMessage`, `cnvParentMessageID`, `cnvAttachmentsEnabled`, `cnvMessagesTotal`, `cnvOverrideGlobalPermissions`, `cnvAttachmentOverridesEnabled`, `cnvMaxFilesGuest`, `cnvMaxFilesRegistered`, `cnvMaxFileSizeGuest`, `cnvMaxFileSizeRegistered`, `cnvFileExtensions`, `cnvNotificationOverridesEnabled`, `cnvEnableSubscription`) VALUES
(1, 166, '2015-05-19 13:58:32', '2015-05-19 13:58:32', 0, 0, 0, 0, 1, 3, 10, 1, 0, '*.jpg;*.gif;*.jpeg;*.png;*.doc;*.docx;*.zip', 0, 0),
(2, 166, '2015-05-19 15:52:38', '2015-05-20 06:47:09', 0, 1, 14, 0, 0, 0, 0, 0, 0, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `DownloadStatistics`
--

CREATE TABLE IF NOT EXISTS `DownloadStatistics` (
  `dsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fID` int(10) unsigned NOT NULL,
  `fvID` int(10) unsigned NOT NULL,
  `uID` int(10) unsigned NOT NULL,
  `rcID` int(10) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`dsID`),
  KEY `fID` (`fID`,`timestamp`),
  KEY `fvID` (`fID`,`fvID`),
  KEY `uID` (`uID`),
  KEY `rcID` (`rcID`),
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=49 ;

--
-- Dumping data for table `DownloadStatistics`
--

INSERT INTO `DownloadStatistics` (`dsID`, `fID`, `fvID`, `uID`, `rcID`, `timestamp`) VALUES
(1, 2, 1, 1, 0, '2015-05-20 06:11:15'),
(2, 2, 1, 1, 0, '2015-05-20 06:11:36'),
(3, 2, 1, 1, 0, '2015-05-20 06:11:40'),
(4, 8, 1, 1, 0, '2015-05-20 11:08:39'),
(5, 8, 1, 1, 0, '2015-05-20 11:08:47'),
(6, 8, 1, 1, 0, '2015-05-20 11:08:50'),
(7, 8, 1, 1, 169, '2015-05-20 11:08:57'),
(8, 8, 1, 1, 169, '2015-05-20 11:09:19'),
(9, 8, 1, 0, 169, '2015-05-20 14:42:39'),
(10, 8, 1, 0, 169, '2015-05-22 09:10:59'),
(11, 8, 1, 0, 169, '2015-05-22 09:11:03'),
(12, 8, 1, 0, 169, '2015-05-22 09:11:33'),
(13, 8, 1, 0, 169, '2015-05-22 09:14:05'),
(14, 8, 1, 0, 169, '2015-05-22 09:16:48'),
(15, 8, 1, 1, 169, '2015-05-22 10:34:45'),
(16, 8, 1, 1, 169, '2015-05-22 10:35:40'),
(17, 8, 1, 1, 0, '2015-05-22 10:36:00'),
(18, 8, 1, 1, 169, '2015-05-22 10:36:34'),
(19, 8, 1, 1, 169, '2015-05-22 11:44:48'),
(20, 8, 1, 1, 169, '2015-05-22 11:45:00'),
(21, 9, 1, 1, 0, '2015-05-22 11:47:46'),
(22, 8, 1, 1, 169, '2015-05-22 11:48:20'),
(23, 9, 1, 1, 0, '2015-05-22 11:48:50'),
(24, 9, 1, 1, 0, '2015-05-22 11:48:55'),
(25, 9, 1, 1, 0, '2015-05-22 11:49:02'),
(26, 9, 1, 1, 169, '2015-05-22 11:49:08'),
(27, 8, 1, 1, 169, '2015-05-22 11:49:20'),
(28, 9, 1, 1, 169, '2015-05-22 11:49:21'),
(29, 9, 1, 0, 169, '2015-05-22 15:24:49'),
(30, 8, 1, 0, 169, '2015-05-22 15:24:50'),
(31, 9, 1, 0, 169, '2015-05-23 17:24:32'),
(32, 8, 1, 0, 169, '2015-05-23 17:24:34'),
(33, 9, 1, 0, 169, '2015-05-23 21:39:42'),
(34, 8, 1, 0, 169, '2015-05-23 21:39:44'),
(35, 9, 1, 0, 169, '2015-05-25 16:38:44'),
(36, 8, 1, 0, 169, '2015-05-25 16:38:45'),
(37, 12, 2, 1, 0, '2015-05-26 09:53:14'),
(38, 12, 2, 1, 170, '2015-05-26 09:53:24'),
(39, 12, 2, 1, 170, '2015-05-26 09:53:52'),
(40, 12, 2, 1, 170, '2015-05-26 10:14:56'),
(41, 12, 2, 1, 170, '2015-05-26 10:17:00'),
(42, 9, 1, 0, 169, '2015-05-26 12:13:40'),
(43, 8, 1, 0, 169, '2015-05-26 12:13:41'),
(44, 12, 2, 0, 170, '2015-05-26 19:38:50'),
(45, 12, 2, 0, 170, '2015-05-26 19:41:51'),
(46, 12, 2, 0, 170, '2015-05-26 19:42:23'),
(47, 12, 2, 0, 170, '2015-05-26 19:42:24'),
(48, 12, 2, 1, 170, '2015-05-27 07:19:21');

-- --------------------------------------------------------

--
-- Table structure for table `FeatureAssignments`
--

CREATE TABLE IF NOT EXISTS `FeatureAssignments` (
  `faID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feID` int(10) unsigned DEFAULT NULL,
  `fcID` int(10) unsigned DEFAULT NULL,
  `fdObject` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`faID`),
  KEY `feID` (`feID`),
  KEY `fcID` (`fcID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `FeatureAssignments`
--

INSERT INTO `FeatureAssignments` (`faID`, `feID`, `fcID`, `fdObject`) VALUES
(1, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:53:"/diers/application/files/7614/3104/1688/sikkerhed.png";s:8:"\0*\0width";d:225;s:9:"\0*\0height";d:112;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(2, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:48:"/diers/application/files/8414/3150/2367/Liza.jpg";s:8:"\0*\0width";d:250;s:9:"\0*\0height";d:167;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(3, 6, 1, 'O:47:"Concrete\\Core\\Feature\\Detail\\ConversationDetail":3:{s:8:"\0*\0cnvID";s:1:"1";s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(4, 6, 1, 'O:47:"Concrete\\Core\\Feature\\Detail\\ConversationDetail":3:{s:8:"\0*\0cnvID";s:1:"1";s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(5, 6, 1, 'O:47:"Concrete\\Core\\Feature\\Detail\\ConversationDetail":3:{s:8:"\0*\0cnvID";s:1:"2";s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(6, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";N;s:8:"\0*\0width";N;s:9:"\0*\0height";N;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(7, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:48:"/diers/application/files/4514/3204/2441/Liza.jpg";s:8:"\0*\0width";d:170;s:9:"\0*\0height";d:165;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(8, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:58:"/diers/application/files/2714/3239/0204/Journalsamtale.jpg";s:8:"\0*\0width";d:800;s:9:"\0*\0height";d:533;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(9, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:58:"/diers/application/files/5914/3239/5711/Successrate-01.jpg";s:8:"\0*\0width";d:264;s:9:"\0*\0height";d:237;s:7:"\0*\0item";N;s:5:"error";s:0:"";}'),
(10, 5, 1, 'O:40:"Concrete\\Core\\Feature\\Detail\\ImageDetail":5:{s:6:"\0*\0src";s:53:"/diers/application/files/5214/3262/8656/Diers_017.jpg";s:8:"\0*\0width";d:500;s:9:"\0*\0height";d:500;s:7:"\0*\0item";N;s:5:"error";s:0:"";}');

-- --------------------------------------------------------

--
-- Table structure for table `FeatureCategories`
--

CREATE TABLE IF NOT EXISTS `FeatureCategories` (
  `fcID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fcHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`fcID`),
  UNIQUE KEY `fcHandle` (`fcHandle`),
  KEY `pkgID` (`pkgID`,`fcID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `FeatureCategories`
--

INSERT INTO `FeatureCategories` (`fcID`, `fcHandle`, `pkgID`) VALUES
(1, 'collection_version', 0),
(2, 'gathering_item', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Features`
--

CREATE TABLE IF NOT EXISTS `Features` (
  `feID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feScore` int(11) NOT NULL DEFAULT '1',
  `feHasCustomClass` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`feID`),
  UNIQUE KEY `feHandle` (`feHandle`),
  KEY `pkgID` (`pkgID`,`feID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `Features`
--

INSERT INTO `Features` (`feID`, `feHandle`, `feScore`, `feHasCustomClass`, `pkgID`) VALUES
(1, 'title', 1, 0, 0),
(2, 'link', 1, 0, 0),
(3, 'author', 1, 0, 0),
(4, 'date_time', 1, 0, 0),
(5, 'image', 500, 1, 0),
(6, 'conversation', 10, 1, 0),
(7, 'description', 1, 0, 0),
(8, 'featured', 1000, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `FileAttributeValues`
--

CREATE TABLE IF NOT EXISTS `FileAttributeValues` (
  `fID` int(10) unsigned NOT NULL DEFAULT '0',
  `fvID` int(10) unsigned NOT NULL DEFAULT '0',
  `akID` int(10) unsigned NOT NULL DEFAULT '0',
  `avID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`fID`,`fvID`,`akID`),
  KEY `akID` (`akID`),
  KEY `avID` (`avID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `FileAttributeValues`
--

INSERT INTO `FileAttributeValues` (`fID`, `fvID`, `akID`, `avID`) VALUES
(1, 1, 14, 112),
(1, 1, 15, 113),
(2, 1, 14, 124),
(2, 1, 15, 125),
(3, 1, 14, 207),
(3, 1, 15, 208),
(4, 1, 14, 209),
(4, 1, 15, 210),
(5, 1, 14, 211),
(5, 1, 15, 212),
(6, 1, 14, 213),
(6, 1, 15, 214),
(7, 1, 14, 215),
(7, 1, 15, 216),
(8, 1, 14, 223),
(8, 1, 15, 224),
(9, 1, 14, 235),
(9, 1, 15, 236),
(10, 1, 14, 242),
(10, 1, 15, 243),
(11, 1, 14, 244),
(11, 1, 15, 245),
(12, 1, 14, 246),
(12, 1, 15, 247),
(12, 2, 14, 248),
(12, 2, 15, 249),
(7, 2, 14, 250),
(7, 2, 15, 251),
(15, 1, 14, 256),
(15, 1, 15, 257),
(16, 1, 14, 258),
(16, 1, 15, 259),
(17, 1, 14, 260),
(17, 1, 15, 261),
(18, 1, 14, 262),
(18, 1, 15, 263),
(19, 1, 14, 264),
(19, 1, 15, 265),
(20, 1, 14, 266),
(20, 1, 15, 267),
(21, 1, 14, 268),
(21, 1, 15, 269),
(23, 1, 14, 272),
(23, 1, 15, 273),
(24, 1, 14, 274),
(24, 1, 15, 275),
(25, 1, 14, 276),
(25, 1, 15, 277),
(27, 1, 14, 280),
(27, 1, 15, 281),
(29, 1, 14, 284),
(29, 1, 15, 285),
(30, 1, 14, 286),
(30, 1, 15, 287),
(31, 1, 14, 288),
(31, 1, 15, 289),
(32, 1, 14, 290),
(32, 1, 15, 291);

-- --------------------------------------------------------

--
-- Table structure for table `FileImageThumbnailTypes`
--

CREATE TABLE IF NOT EXISTS `FileImageThumbnailTypes` (
  `ftTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ftTypeHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ftTypeName` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ftTypeWidth` int(11) NOT NULL DEFAULT '0',
  `ftTypeHeight` int(11) DEFAULT NULL,
  `ftTypeIsRequired` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ftTypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `FileImageThumbnailTypes`
--

INSERT INTO `FileImageThumbnailTypes` (`ftTypeID`, `ftTypeHandle`, `ftTypeName`, `ftTypeWidth`, `ftTypeHeight`, `ftTypeIsRequired`) VALUES
(1, 'file_manager_listing', 'File Manager Thumbnails', 60, 60, 1),
(2, 'file_manager_detail', 'File Manager Detail Thumbnails', 400, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `FilePermissionAssignments`
--

CREATE TABLE IF NOT EXISTS `FilePermissionAssignments` (
  `fID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkID` int(10) unsigned NOT NULL DEFAULT '0',
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`fID`,`pkID`,`paID`),
  KEY `pkID` (`pkID`),
  KEY `paID` (`paID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `FilePermissionFileTypes`
--

CREATE TABLE IF NOT EXISTS `FilePermissionFileTypes` (
  `extension` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `fsID` int(10) unsigned NOT NULL DEFAULT '0',
  `gID` int(10) unsigned NOT NULL DEFAULT '0',
  `uID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`fsID`,`gID`,`uID`,`extension`),
  KEY `gID` (`gID`),
  KEY `uID` (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `FileSearchIndexAttributes`
--

CREATE TABLE IF NOT EXISTS `FileSearchIndexAttributes` (
  `fID` int(10) unsigned NOT NULL DEFAULT '0',
  `ak_width` decimal(14,4) DEFAULT '0.0000',
  `ak_height` decimal(14,4) DEFAULT '0.0000',
  `ak_duration` decimal(14,4) DEFAULT '0.0000',
  PRIMARY KEY (`fID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `FileSearchIndexAttributes`
--

INSERT INTO `FileSearchIndexAttributes` (`fID`, `ak_width`, `ak_height`, `ak_duration`) VALUES
(1, '225.0000', '112.0000', '0.0000'),
(2, '250.0000', '167.0000', '0.0000'),
(3, '170.0000', '165.0000', '0.0000'),
(4, '170.0000', '165.0000', '0.0000'),
(5, '170.0000', '165.0000', '0.0000'),
(6, '170.0000', '165.0000', '0.0000'),
(7, '500.0000', '500.0000', '0.0000'),
(8, '170.0000', '165.0000', '0.0000'),
(9, '170.0000', '165.0000', '0.0000'),
(10, '5184.0000', '3456.0000', '0.0000'),
(11, '800.0000', '533.0000', '0.0000'),
(12, '351.0000', '349.0000', '0.0000'),
(13, '1900.0000', '1267.0000', '0.0000'),
(14, '640.0000', '852.0000', '0.0000'),
(15, '960.0000', '638.0000', '0.0000'),
(16, '657.0000', '960.0000', '0.0000'),
(17, '960.0000', '1280.0000', '0.0000'),
(18, '843.0000', '960.0000', '0.0000'),
(19, '640.0000', '852.0000', '0.0000'),
(20, '720.0000', '960.0000', '0.0000'),
(21, '3264.0000', '2448.0000', '0.0000'),
(22, '1280.0000', '960.0000', '0.0000'),
(23, '3264.0000', '2448.0000', '0.0000'),
(24, '1280.0000', '960.0000', '0.0000'),
(25, '3264.0000', '2448.0000', '0.0000'),
(26, '1280.0000', '960.0000', '0.0000'),
(27, '1280.0000', '851.0000', '0.0000'),
(28, '640.0000', '852.0000', '0.0000'),
(29, '960.0000', '1280.0000', '0.0000'),
(30, '960.0000', '1280.0000', '0.0000'),
(31, '2448.0000', '3264.0000', '0.0000'),
(32, '1000.0000', '1333.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `FileSetFiles`
--

CREATE TABLE IF NOT EXISTS `FileSetFiles` (
  `fsfID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fID` int(10) unsigned NOT NULL,
  `fsID` int(10) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsDisplayOrder` int(10) unsigned NOT NULL,
  PRIMARY KEY (`fsfID`),
  KEY `fID` (`fID`),
  KEY `fsID` (`fsID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `FileSetFiles`
--

INSERT INTO `FileSetFiles` (`fsfID`, `fID`, `fsID`, `timestamp`, `fsDisplayOrder`) VALUES
(1, 15, 1, '2015-05-26 09:01:55', 0),
(2, 16, 1, '2015-05-26 09:01:55', 1),
(3, 17, 1, '2015-05-26 09:01:55', 2),
(4, 18, 1, '2015-05-26 09:01:55', 3),
(5, 19, 1, '2015-05-26 09:01:55', 4),
(6, 20, 1, '2015-05-26 09:01:55', 5),
(7, 22, 1, '2015-05-26 09:01:55', 6),
(8, 24, 1, '2015-05-26 09:01:55', 7),
(9, 26, 1, '2015-05-26 09:01:55', 8),
(10, 27, 1, '2015-05-26 09:01:55', 9),
(11, 28, 1, '2015-05-26 09:01:55', 10),
(12, 29, 1, '2015-05-26 09:06:08', 11),
(13, 30, 1, '2015-05-26 09:06:08', 12),
(14, 32, 1, '2015-05-26 10:54:18', 13);

-- --------------------------------------------------------

--
-- Table structure for table `FileSetPermissionAssignments`
--

CREATE TABLE IF NOT EXISTS `FileSetPermissionAssignments` (
  `fsID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkID` int(10) unsigned NOT NULL DEFAULT '0',
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`fsID`,`pkID`,`paID`),
  KEY `paID` (`paID`),
  KEY `pkID` (`pkID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `FileSetPermissionAssignments`
--

INSERT INTO `FileSetPermissionAssignments` (`fsID`, `pkID`, `paID`) VALUES
(0, 39, 31),
(0, 40, 32),
(0, 41, 33),
(0, 42, 34),
(0, 43, 35),
(0, 44, 36),
(0, 45, 38),
(0, 46, 37),
(0, 47, 39);

-- --------------------------------------------------------

--
-- Table structure for table `FileSetPermissionFileTypeAccessList`
--

CREATE TABLE IF NOT EXISTS `FileSetPermissionFileTypeAccessList` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`paID`,`peID`),
  KEY `peID` (`peID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `FileSetPermissionFileTypeAccessListCustom`
--

CREATE TABLE IF NOT EXISTS `FileSetPermissionFileTypeAccessListCustom` (
  `extension` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`paID`,`peID`,`extension`),
  KEY `peID` (`peID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `FileSetSavedSearches`
--

CREATE TABLE IF NOT EXISTS `FileSetSavedSearches` (
  `fsID` int(10) unsigned NOT NULL DEFAULT '0',
  `fsSearchRequest` text COLLATE utf8_unicode_ci,
  `fsResultColumns` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`fsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `FileSets`
--

CREATE TABLE IF NOT EXISTS `FileSets` (
  `fsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fsName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `uID` int(10) unsigned NOT NULL DEFAULT '0',
  `fsType` smallint(6) NOT NULL,
  `fsOverrideGlobalPermissions` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`fsID`),
  KEY `uID` (`uID`,`fsType`,`fsName`),
  KEY `fsName` (`fsName`),
  KEY `fsType` (`fsType`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `FileSets`
--

INSERT INTO `FileSets` (`fsID`, `fsName`, `uID`, `fsType`, `fsOverrideGlobalPermissions`) VALUES
(1, 'Gallery', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `FileStorageLocationTypes`
--

CREATE TABLE IF NOT EXISTS `FileStorageLocationTypes` (
  `fslTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fslTypeHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fslTypeName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`fslTypeID`),
  UNIQUE KEY `fslTypeHandle` (`fslTypeHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `FileStorageLocationTypes`
--

INSERT INTO `FileStorageLocationTypes` (`fslTypeID`, `fslTypeHandle`, `fslTypeName`, `pkgID`) VALUES
(1, 'default', 'Default', 0),
(2, 'local', 'Local', 0);

-- --------------------------------------------------------

--
-- Table structure for table `FileStorageLocations`
--

CREATE TABLE IF NOT EXISTS `FileStorageLocations` (
  `fslID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fslName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fslConfiguration` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fslIsDefault` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fslID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `FileStorageLocations`
--

INSERT INTO `FileStorageLocations` (`fslID`, `fslName`, `fslConfiguration`, `fslIsDefault`) VALUES
(1, 'Default', 'O:69:"Concrete\\Core\\File\\StorageLocation\\Configuration\\DefaultConfiguration":1:{s:10:"\0*\0default";b:1;}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `FileVersionLog`
--

CREATE TABLE IF NOT EXISTS `FileVersionLog` (
  `fvlID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fID` int(10) unsigned NOT NULL DEFAULT '0',
  `fvID` int(10) unsigned NOT NULL DEFAULT '0',
  `fvUpdateTypeID` smallint(5) unsigned NOT NULL DEFAULT '0',
  `fvUpdateTypeAttributeID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`fvlID`),
  KEY `fvID` (`fID`,`fvID`,`fvlID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=71 ;

--
-- Dumping data for table `FileVersionLog`
--

INSERT INTO `FileVersionLog` (`fvlID`, `fID`, `fvID`, `fvUpdateTypeID`, `fvUpdateTypeAttributeID`) VALUES
(1, 1, 1, 5, 14),
(2, 1, 1, 5, 15),
(3, 2, 1, 5, 14),
(4, 2, 1, 5, 15),
(5, 3, 1, 5, 14),
(6, 3, 1, 5, 15),
(7, 4, 1, 5, 14),
(8, 4, 1, 5, 15),
(9, 5, 1, 5, 14),
(10, 5, 1, 5, 15),
(11, 6, 1, 5, 14),
(12, 6, 1, 5, 15),
(13, 7, 1, 5, 14),
(14, 7, 1, 5, 15),
(15, 8, 1, 5, 14),
(16, 8, 1, 5, 15),
(17, 9, 1, 5, 14),
(18, 9, 1, 5, 15),
(19, 10, 1, 5, 14),
(20, 10, 1, 5, 15),
(21, 11, 1, 5, 14),
(22, 11, 1, 5, 15),
(23, 12, 1, 5, 14),
(24, 12, 1, 5, 15),
(25, 12, 2, 1, 0),
(26, 12, 2, 5, 14),
(27, 12, 2, 5, 15),
(28, 7, 2, 1, 0),
(29, 7, 2, 5, 14),
(30, 7, 2, 5, 15),
(35, 15, 1, 5, 14),
(36, 15, 1, 5, 15),
(37, 16, 1, 5, 14),
(38, 16, 1, 5, 15),
(39, 17, 1, 5, 14),
(40, 17, 1, 5, 15),
(41, 18, 1, 5, 14),
(42, 18, 1, 5, 15),
(43, 19, 1, 5, 14),
(44, 19, 1, 5, 15),
(45, 20, 1, 5, 14),
(46, 20, 1, 5, 15),
(47, 21, 1, 5, 14),
(48, 21, 1, 5, 15),
(51, 23, 1, 5, 14),
(52, 23, 1, 5, 15),
(53, 24, 1, 5, 14),
(54, 24, 1, 5, 15),
(55, 25, 1, 5, 14),
(56, 25, 1, 5, 15),
(59, 27, 1, 5, 14),
(60, 27, 1, 5, 15),
(63, 29, 1, 5, 14),
(64, 29, 1, 5, 15),
(65, 30, 1, 5, 14),
(66, 30, 1, 5, 15),
(67, 31, 1, 5, 14),
(68, 31, 1, 5, 15),
(69, 32, 1, 5, 14),
(70, 32, 1, 5, 15);

-- --------------------------------------------------------

--
-- Table structure for table `FileVersions`
--

CREATE TABLE IF NOT EXISTS `FileVersions` (
  `fID` int(10) unsigned NOT NULL DEFAULT '0',
  `fvID` int(10) unsigned NOT NULL DEFAULT '0',
  `fvFilename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fvPrefix` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fvGenericType` smallint(5) unsigned NOT NULL DEFAULT '0',
  `fvSize` int(10) unsigned NOT NULL DEFAULT '0',
  `fvTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fvDescription` text COLLATE utf8_unicode_ci,
  `fvTags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fvIsApproved` int(10) unsigned NOT NULL DEFAULT '1',
  `fvDateAdded` datetime DEFAULT NULL,
  `fvApproverUID` int(10) unsigned NOT NULL DEFAULT '0',
  `fvAuthorUID` int(10) unsigned NOT NULL DEFAULT '0',
  `fvActivateDatetime` datetime DEFAULT NULL,
  `fvHasListingThumbnail` tinyint(1) NOT NULL DEFAULT '0',
  `fvHasDetailThumbnail` tinyint(1) NOT NULL DEFAULT '0',
  `fvExtension` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fvType` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`fID`,`fvID`),
  KEY `fvExtension` (`fvExtension`),
  KEY `fvType` (`fvType`),
  KEY `fvFilename` (`fvFilename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `FileVersions`
--

INSERT INTO `FileVersions` (`fID`, `fvID`, `fvFilename`, `fvPrefix`, `fvGenericType`, `fvSize`, `fvTitle`, `fvDescription`, `fvTags`, `fvIsApproved`, `fvDateAdded`, `fvApproverUID`, `fvAuthorUID`, `fvActivateDatetime`, `fvHasListingThumbnail`, `fvHasDetailThumbnail`, `fvExtension`, `fvType`) VALUES
(1, 1, 'sikkerhed.png', '761431041688', 0, 8700, 'sikkerhed.png', NULL, '', 1, '2015-05-07 23:34:49', 1, 1, '2015-05-07 23:34:49', 1, 0, 'png', 1),
(2, 1, 'Liza.jpg', '841431502367', 0, 12989, 'Liza.jpg', NULL, '', 1, '2015-05-13 07:32:47', 1, 1, '2015-05-13 07:32:47', 1, 0, 'jpg', 1),
(3, 1, 'Anne_Louise.jpg', '391432042423', 0, 9442, 'Anne Louise.jpg', NULL, '', 1, '2015-05-19 13:33:43', 1, 1, '2015-05-19 13:33:43', 1, 0, 'jpg', 1),
(4, 1, 'Annesofie.jpg', '271432042426', 0, 9359, 'Annesofie.jpg', NULL, '', 1, '2015-05-19 13:33:46', 1, 1, '2015-05-19 13:33:46', 1, 0, 'jpg', 1),
(5, 1, 'kristina.jpg', '771432042438', 0, 6242, 'kristina.jpg', NULL, '', 1, '2015-05-19 13:33:58', 1, 1, '2015-05-19 13:33:58', 1, 0, 'jpg', 1),
(6, 1, 'Janni.jpg', '481432042439', 0, 8392, 'Janni.jpg', NULL, '', 1, '2015-05-19 13:33:59', 1, 1, '2015-05-19 13:33:59', 1, 0, 'jpg', 1),
(7, 1, 'Liza.jpg', '451432042441', 0, 10029, 'Liza.jpg', NULL, '', 0, '2015-05-19 13:34:01', 1, 1, '2015-05-19 13:34:01', 1, 0, 'jpg', 1),
(7, 2, 'Diers_017.jpg', '521432628656', 0, 179322, 'Liza.jpg', NULL, '', 1, '2015-05-26 08:24:17', 1, 1, '2015-05-19 13:34:01', 1, 1, 'jpg', 1),
(8, 1, 'Louise.jpg', '841432120102', 0, 5211, 'Louise.jpg', '', '', 1, '2015-05-20 11:08:22', 1, 1, '2015-05-20 11:08:22', 1, 0, 'jpg', 1),
(9, 1, 'Lise.jpg', '251432295255', 0, 5442, 'Lise.jpg', '', '', 1, '2015-05-22 11:47:35', 1, 1, '2015-05-22 11:47:35', 1, 0, 'jpg', 1),
(10, 1, 'Journalsamtale.jpg', '451432390162', 0, 0, 'Journalsamtale.jpg', '', '', 1, '2015-05-23 14:09:23', 1, 1, '2015-05-23 14:09:23', 0, 0, NULL, 0),
(11, 1, 'Journalsamtale.jpg', '271432390204', 0, 69655, 'Journalsamtale.jpg', '', '', 1, '2015-05-23 14:10:04', 1, 1, '2015-05-23 14:10:04', 1, 1, 'jpg', 1),
(12, 1, 'Successrate-01.jpg', '591432395711', 0, 50412, 'Successrate-01.jpg', '', '', 0, '2015-05-23 15:41:51', 1, 1, '2015-05-23 15:41:51', 1, 0, 'jpg', 1),
(12, 2, 'Successrate.jpg', '271432395795', 0, 66382, 'Successrate-01.jpg', '', '', 1, '2015-05-23 15:43:16', 1, 1, '2015-05-23 15:41:51', 1, 0, 'jpg', 1),
(15, 1, '11261098_1439971562986909_977605472_n.jpg', '111432630820', 0, 45753, '11261098_1439971562986909_977605472_n.jpg', '', '', 1, '2015-05-26 09:00:20', 1, 1, '2015-05-26 09:00:20', 1, 1, 'jpg', 1),
(16, 1, '11258933_360901614114137_758810059_n.jpg', '451432630822', 0, 113596, '11258933_360901614114137_758810059_n.jpg', '', '', 1, '2015-05-26 09:00:22', 1, 1, '2015-05-26 09:00:22', 1, 1, 'jpg', 1),
(17, 1, 'billeder.jpg', '641432630824', 0, 392463, 'billeder.jpg', '', '', 1, '2015-05-26 09:00:24', 1, 1, '2015-05-26 09:00:24', 1, 1, 'jpg', 1),
(18, 1, '11350307_10153250972615985_9822235_n.jpg', '701432630826', 0, 60875, '11350307_10153250972615985_9822235_n.jpg', '', '', 1, '2015-05-26 09:00:26', 1, 1, '2015-05-26 09:00:26', 1, 1, 'jpg', 1),
(19, 1, '11117590_10204466762773419_804665824_n.jpg', '181432630829', 0, 141357, '11117590_10204466762773419_804665824_n.jpg', '', '', 1, '2015-05-26 09:00:29', 1, 1, '2015-05-26 09:00:29', 1, 1, 'jpg', 1),
(20, 1, '11350082_1028781587150465_264443990_n.jpg', '321432630830', 0, 160404, '11350082_1028781587150465_264443990_n.jpg', '', '', 1, '2015-05-26 09:00:30', 1, 1, '2015-05-26 09:00:30', 1, 1, 'jpg', 1),
(21, 1, 'IMG_0110.JPG', '381432630832', 0, 0, 'IMG_0110.JPG', '', '', 1, '2015-05-26 09:00:32', 1, 1, '2015-05-26 09:00:32', 0, 0, NULL, 0),
(23, 1, 'IMG_0111.JPG', '101432630836', 0, 0, 'IMG_0111.JPG', '', '', 1, '2015-05-26 09:00:36', 1, 1, '2015-05-26 09:00:36', 0, 0, NULL, 0),
(24, 1, 'F1BD01DE-9488-4F20-A502-7063FDC5DA12.JPG', '321432630837', 0, 387566, 'F1BD01DE-9488-4F20-A502-7063FDC5DA12.JPG', '', '', 1, '2015-05-26 09:00:38', 1, 1, '2015-05-26 09:00:38', 1, 1, 'JPG', 1),
(25, 1, 'IMG_0112.JPG', '891432630839', 0, 0, 'IMG_0112.JPG', '', '', 1, '2015-05-26 09:00:39', 1, 1, '2015-05-26 09:00:39', 0, 0, NULL, 0),
(27, 1, 'IMG_5749.JPG', '971432630843', 0, 254407, 'IMG_5749.JPG', '', '', 1, '2015-05-26 09:00:43', 1, 1, '2015-05-26 09:00:43', 1, 1, 'JPG', 1),
(29, 1, 'IMG_2248.JPG', '641432631154', 0, 378725, 'IMG_2248.JPG', '', '', 1, '2015-05-26 09:05:54', 1, 1, '2015-05-26 09:05:54', 1, 1, 'JPG', 1),
(30, 1, 'IMG_5663.JPG', '351432631156', 0, 688503, 'IMG_5663.JPG', '', '', 1, '2015-05-26 09:05:56', 1, 1, '2015-05-26 09:05:56', 1, 1, 'JPG', 1),
(31, 1, 'IMG_0198.JPG', '251432637567', 0, 0, 'IMG_0198.JPG', '', '', 1, '2015-05-26 10:52:47', 1, 1, '2015-05-26 10:52:47', 0, 0, NULL, 0),
(32, 1, 'IMG_0198.JPG', '671432637644', 0, 178748, 'IMG_0198.JPG', '', '', 1, '2015-05-26 10:54:04', 1, 1, '2015-05-26 10:54:04', 1, 1, 'JPG', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Files`
--

CREATE TABLE IF NOT EXISTS `Files` (
  `fID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fDateAdded` datetime DEFAULT NULL,
  `uID` int(10) unsigned NOT NULL DEFAULT '0',
  `fslID` int(10) unsigned NOT NULL DEFAULT '0',
  `ocID` int(10) unsigned NOT NULL DEFAULT '0',
  `fOverrideSetPermissions` tinyint(1) NOT NULL DEFAULT '0',
  `fPassword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`fID`,`uID`,`fslID`),
  KEY `uID` (`uID`),
  KEY `fslID` (`fslID`),
  KEY `ocID` (`ocID`),
  KEY `fOverrideSetPermissions` (`fOverrideSetPermissions`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `Files`
--

INSERT INTO `Files` (`fID`, `fDateAdded`, `uID`, `fslID`, `ocID`, `fOverrideSetPermissions`, `fPassword`) VALUES
(1, '2015-05-07 23:34:48', 1, 1, 0, 0, NULL),
(2, '2015-05-13 07:32:47', 1, 1, 0, 0, NULL),
(3, '2015-05-19 13:33:43', 1, 1, 0, 0, NULL),
(4, '2015-05-19 13:33:46', 1, 1, 0, 0, NULL),
(5, '2015-05-19 13:33:58', 1, 1, 0, 0, NULL),
(6, '2015-05-19 13:33:59', 1, 1, 0, 0, NULL),
(7, '2015-05-19 13:34:01', 1, 1, 0, 0, NULL),
(8, '2015-05-20 11:08:22', 1, 1, 0, 0, NULL),
(9, '2015-05-22 11:47:35', 1, 1, 0, 0, NULL),
(10, '2015-05-23 14:09:22', 1, 1, 0, 0, NULL),
(11, '2015-05-23 14:10:04', 1, 1, 0, 0, NULL),
(12, '2015-05-23 15:41:51', 1, 1, 0, 0, NULL),
(15, '2015-05-26 09:00:20', 1, 1, 0, 0, NULL),
(16, '2015-05-26 09:00:22', 1, 1, 0, 0, NULL),
(17, '2015-05-26 09:00:24', 1, 1, 0, 0, NULL),
(18, '2015-05-26 09:00:26', 1, 1, 0, 0, NULL),
(19, '2015-05-26 09:00:29', 1, 1, 0, 0, NULL),
(20, '2015-05-26 09:00:30', 1, 1, 0, 0, NULL),
(21, '2015-05-26 09:00:32', 1, 1, 0, 0, NULL),
(23, '2015-05-26 09:00:36', 1, 1, 0, 0, NULL),
(24, '2015-05-26 09:00:37', 1, 1, 0, 0, NULL),
(25, '2015-05-26 09:00:39', 1, 1, 0, 0, NULL),
(27, '2015-05-26 09:00:43', 1, 1, 0, 0, NULL),
(29, '2015-05-26 09:05:54', 1, 1, 0, 0, NULL),
(30, '2015-05-26 09:05:56', 1, 1, 0, 0, NULL),
(31, '2015-05-26 10:52:47', 1, 1, 0, 0, NULL),
(32, '2015-05-26 10:54:04', 1, 1, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `GatheringConfiguredDataSources`
--

CREATE TABLE IF NOT EXISTS `GatheringConfiguredDataSources` (
  `gcsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gaID` int(10) unsigned DEFAULT NULL,
  `gasID` int(10) unsigned DEFAULT NULL,
  `gcdObject` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`gcsID`),
  KEY `gaID` (`gaID`),
  KEY `gasID` (`gasID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `GatheringDataSources`
--

CREATE TABLE IF NOT EXISTS `GatheringDataSources` (
  `gasID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gasName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gasHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  `gasDisplayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`gasID`),
  UNIQUE KEY `gasHandle` (`gasHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `GatheringDataSources`
--

INSERT INTO `GatheringDataSources` (`gasID`, `gasName`, `gasHandle`, `pkgID`, `gasDisplayOrder`) VALUES
(1, 'Site Page', 'page', 0, 0),
(2, 'RSS Feed', 'rss_feed', 0, 1),
(3, 'Flickr Feed', 'flickr_feed', 0, 2),
(4, 'Twitter', 'twitter', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `GatheringItemFeatureAssignments`
--

CREATE TABLE IF NOT EXISTS `GatheringItemFeatureAssignments` (
  `gafaID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gaiID` int(10) unsigned DEFAULT NULL,
  `faID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`gafaID`),
  KEY `gaiID` (`gaiID`,`faID`),
  KEY `faID` (`faID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `GatheringItemSelectedTemplates`
--

CREATE TABLE IF NOT EXISTS `GatheringItemSelectedTemplates` (
  `gaiID` int(10) unsigned NOT NULL DEFAULT '0',
  `gatID` int(10) unsigned NOT NULL DEFAULT '0',
  `gatTypeID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`gaiID`,`gatID`),
  UNIQUE KEY `gatUniqueKey` (`gaiID`,`gatTypeID`),
  KEY `gatTypeID` (`gatTypeID`),
  KEY `gatID` (`gatID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `GatheringItemTemplateFeatures`
--

CREATE TABLE IF NOT EXISTS `GatheringItemTemplateFeatures` (
  `gfeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gatID` int(10) unsigned DEFAULT NULL,
  `feID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`gfeID`),
  KEY `gatID` (`gatID`),
  KEY `feID` (`feID`,`gatID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=67 ;

--
-- Dumping data for table `GatheringItemTemplateFeatures`
--

INSERT INTO `GatheringItemTemplateFeatures` (`gfeID`, `gatID`, `feID`) VALUES
(4, 1, 1),
(10, 2, 1),
(13, 3, 1),
(16, 4, 1),
(19, 5, 1),
(23, 7, 1),
(29, 11, 1),
(33, 12, 1),
(37, 13, 1),
(42, 14, 1),
(47, 15, 1),
(53, 17, 1),
(56, 18, 1),
(63, 21, 1),
(64, 22, 1),
(3, 1, 2),
(9, 2, 2),
(12, 3, 2),
(15, 4, 2),
(18, 5, 2),
(21, 6, 2),
(25, 8, 2),
(27, 9, 2),
(41, 13, 3),
(46, 14, 3),
(51, 16, 3),
(55, 17, 3),
(62, 20, 3),
(66, 22, 3),
(2, 1, 4),
(8, 2, 4),
(14, 4, 4),
(17, 5, 4),
(31, 11, 4),
(35, 12, 4),
(39, 13, 4),
(44, 14, 4),
(61, 20, 4),
(5, 1, 5),
(22, 6, 5),
(24, 8, 5),
(26, 9, 5),
(28, 10, 5),
(32, 11, 5),
(36, 12, 5),
(40, 13, 5),
(45, 14, 5),
(49, 15, 5),
(50, 16, 5),
(59, 19, 5),
(65, 22, 5),
(20, 5, 6),
(1, 1, 7),
(7, 2, 7),
(11, 3, 7),
(30, 11, 7),
(34, 12, 7),
(38, 13, 7),
(43, 14, 7),
(48, 15, 7),
(52, 16, 7),
(54, 17, 7),
(57, 18, 7),
(58, 19, 7),
(60, 20, 7),
(6, 1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `GatheringItemTemplateTypes`
--

CREATE TABLE IF NOT EXISTS `GatheringItemTemplateTypes` (
  `gatTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gatTypeHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`gatTypeID`),
  UNIQUE KEY `gatTypeHandle` (`gatTypeHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `GatheringItemTemplateTypes`
--

INSERT INTO `GatheringItemTemplateTypes` (`gatTypeID`, `gatTypeHandle`, `pkgID`) VALUES
(1, 'tile', 0),
(2, 'detail', 0);

-- --------------------------------------------------------

--
-- Table structure for table `GatheringItemTemplates`
--

CREATE TABLE IF NOT EXISTS `GatheringItemTemplates` (
  `gatID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gatHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gatName` text COLLATE utf8_unicode_ci,
  `gatHasCustomClass` tinyint(1) NOT NULL DEFAULT '0',
  `gatFixedSlotWidth` int(10) unsigned DEFAULT '0',
  `gatFixedSlotHeight` int(10) unsigned DEFAULT '0',
  `gatForceDefault` int(10) unsigned DEFAULT '0',
  `pkgID` int(10) unsigned DEFAULT NULL,
  `gatTypeID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`gatID`),
  UNIQUE KEY `gatHandle` (`gatHandle`,`gatTypeID`),
  KEY `gatTypeID` (`gatTypeID`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `GatheringItemTemplates`
--

INSERT INTO `GatheringItemTemplates` (`gatID`, `gatHandle`, `gatName`, `gatHasCustomClass`, `gatFixedSlotWidth`, `gatFixedSlotHeight`, `gatForceDefault`, `pkgID`, `gatTypeID`) VALUES
(1, 'featured', 'Featured Item', 0, 6, 2, 1, 0, 1),
(2, 'title_date_description', 'Title Date & Description', 0, 0, 0, 0, 0, 1),
(3, 'title_description', 'Title & Description', 0, 0, 0, 0, 0, 1),
(4, 'title_date', 'Title & Date', 0, 0, 0, 0, 0, 1),
(5, 'title_date_comments', 'Title, Date & Comments', 1, 0, 0, 0, 0, 1),
(6, 'thumbnail', 'Thumbnail', 0, 0, 0, 0, 0, 1),
(7, 'basic', 'Basic', 0, 0, 0, 0, 0, 2),
(8, 'image_sharing_link', 'Image Sharing Link', 0, 0, 0, 0, 0, 2),
(9, 'image_conversation', 'Image Conversation', 0, 0, 0, 0, 0, 2),
(10, 'image', 'Large Image', 0, 0, 0, 0, 0, 2),
(11, 'masthead_image_left', 'Masthead Image Left', 0, 0, 0, 0, 0, 1),
(12, 'masthead_image_right', 'Masthead Image Right', 0, 0, 0, 0, 0, 1),
(13, 'masthead_image_byline_right', 'Masthead Image Byline Right', 0, 0, 0, 0, 0, 1),
(14, 'masthead_image_byline_left', 'Masthead Image Byline Left', 0, 0, 0, 0, 0, 1),
(15, 'image_masthead_description_center', 'Image Masthead Description Center', 0, 0, 0, 0, 0, 1),
(16, 'image_byline_description_center', 'Image Byline Description Center', 0, 0, 0, 0, 0, 1),
(17, 'masthead_byline_description', 'Masthead Byline Description', 0, 0, 0, 0, 0, 1),
(18, 'masthead_description', 'Masthead Description', 0, 0, 0, 0, 0, 1),
(19, 'thumbnail_description_center', 'Thumbnail & Description Center', 0, 0, 0, 0, 0, 1),
(20, 'tweet', 'Tweet', 0, 0, 0, 0, 0, 1),
(21, 'vimeo', 'Vimeo', 0, 0, 0, 0, 0, 1),
(22, 'image_overlay_headline', 'Image Overlay Headline', 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `GatheringItems`
--

CREATE TABLE IF NOT EXISTS `GatheringItems` (
  `gaiID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gaID` int(10) unsigned DEFAULT NULL,
  `gasID` int(10) unsigned DEFAULT NULL,
  `gaiDateTimeCreated` datetime NOT NULL,
  `gaiPublicDateTime` datetime NOT NULL,
  `gaiTitle` text COLLATE utf8_unicode_ci,
  `gaiSlotWidth` int(10) unsigned DEFAULT '1',
  `gaiSlotHeight` int(10) unsigned DEFAULT '1',
  `gaiKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gaiBatchDisplayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  `gaiBatchTimestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `gaiIsDeleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`gaiID`),
  UNIQUE KEY `gaiUniqueKey` (`gaiKey`,`gasID`,`gaID`),
  KEY `gaID` (`gaID`,`gaiBatchTimestamp`),
  KEY `gasID` (`gasID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `GatheringPermissionAssignments`
--

CREATE TABLE IF NOT EXISTS `GatheringPermissionAssignments` (
  `gaID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkID` int(10) unsigned NOT NULL DEFAULT '0',
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`gaID`,`pkID`,`paID`),
  KEY `pkID` (`pkID`),
  KEY `paID` (`paID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Gatherings`
--

CREATE TABLE IF NOT EXISTS `Gatherings` (
  `gaID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gaDateCreated` datetime NOT NULL,
  `gaDateLastUpdated` datetime NOT NULL,
  PRIMARY KEY (`gaID`),
  KEY `gaDateLastUpdated` (`gaDateLastUpdated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `GroupSetGroups`
--

CREATE TABLE IF NOT EXISTS `GroupSetGroups` (
  `gID` int(10) unsigned NOT NULL DEFAULT '0',
  `gsID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`gID`,`gsID`),
  KEY `gsID` (`gsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `GroupSets`
--

CREATE TABLE IF NOT EXISTS `GroupSets` (
  `gsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gsName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`gsID`),
  KEY `gsName` (`gsName`),
  KEY `pkgID` (`pkgID`,`gsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Groups`
--

CREATE TABLE IF NOT EXISTS `Groups` (
  `gID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `gDescription` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gUserExpirationIsEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `gUserExpirationMethod` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gUserExpirationSetDateTime` datetime DEFAULT NULL,
  `gUserExpirationInterval` int(10) unsigned NOT NULL DEFAULT '0',
  `gUserExpirationAction` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gIsBadge` tinyint(1) NOT NULL DEFAULT '0',
  `gBadgeFID` int(10) unsigned NOT NULL DEFAULT '0',
  `gBadgeDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gBadgeCommunityPointValue` int(11) NOT NULL DEFAULT '0',
  `gIsAutomated` tinyint(1) NOT NULL DEFAULT '0',
  `gCheckAutomationOnRegister` tinyint(1) NOT NULL DEFAULT '0',
  `gCheckAutomationOnLogin` tinyint(1) NOT NULL DEFAULT '0',
  `gCheckAutomationOnJobRun` tinyint(1) NOT NULL DEFAULT '0',
  `gPath` text COLLATE utf8_unicode_ci,
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`gID`),
  KEY `gName` (`gName`),
  KEY `gBadgeFID` (`gBadgeFID`),
  KEY `pkgID` (`pkgID`),
  KEY `gPath` (`gPath`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `Groups`
--

INSERT INTO `Groups` (`gID`, `gName`, `gDescription`, `gUserExpirationIsEnabled`, `gUserExpirationMethod`, `gUserExpirationSetDateTime`, `gUserExpirationInterval`, `gUserExpirationAction`, `gIsBadge`, `gBadgeFID`, `gBadgeDescription`, `gBadgeCommunityPointValue`, `gIsAutomated`, `gCheckAutomationOnRegister`, `gCheckAutomationOnLogin`, `gCheckAutomationOnJobRun`, `gPath`, `pkgID`) VALUES
(1, 'Guest', 'The guest group represents unregistered visitors to your site.', 0, NULL, NULL, 0, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, '/Guest', 0),
(2, 'Registered Users', 'The registered users group represents all user accounts.', 0, NULL, NULL, 0, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, '/Registered Users', 0),
(3, 'Administrators', '', 0, NULL, NULL, 0, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, '/Administrators', 0);

-- --------------------------------------------------------

--
-- Table structure for table `JobSetJobs`
--

CREATE TABLE IF NOT EXISTS `JobSetJobs` (
  `jsID` int(10) unsigned NOT NULL DEFAULT '0',
  `jID` int(10) unsigned NOT NULL DEFAULT '0',
  `jRunOrder` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`jsID`,`jID`),
  KEY `jID` (`jID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `JobSetJobs`
--

INSERT INTO `JobSetJobs` (`jsID`, `jID`, `jRunOrder`) VALUES
(1, 1, 0),
(1, 4, 0),
(1, 5, 0),
(1, 6, 0),
(1, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `JobSets`
--

CREATE TABLE IF NOT EXISTS `JobSets` (
  `jsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jsName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  `jDateLastRun` datetime DEFAULT NULL,
  `isScheduled` smallint(6) NOT NULL DEFAULT '0',
  `scheduledInterval` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'days',
  `scheduledValue` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`jsID`),
  KEY `pkgID` (`pkgID`),
  KEY `jsName` (`jsName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `JobSets`
--

INSERT INTO `JobSets` (`jsID`, `jsName`, `pkgID`, `jDateLastRun`, `isScheduled`, `scheduledInterval`, `scheduledValue`) VALUES
(1, 'Default', 0, NULL, 0, 'days', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Jobs`
--

CREATE TABLE IF NOT EXISTS `Jobs` (
  `jID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `jDescription` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jDateInstalled` datetime DEFAULT NULL,
  `jDateLastRun` datetime DEFAULT NULL,
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  `jLastStatusText` longtext COLLATE utf8_unicode_ci,
  `jLastStatusCode` smallint(6) NOT NULL DEFAULT '0',
  `jStatus` varchar(14) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ENABLED',
  `jHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jNotUninstallable` smallint(6) NOT NULL DEFAULT '0',
  `isScheduled` smallint(6) NOT NULL DEFAULT '0',
  `scheduledInterval` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'days',
  `scheduledValue` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`jID`),
  UNIQUE KEY `jHandle` (`jHandle`),
  KEY `pkgID` (`pkgID`),
  KEY `isScheduled` (`isScheduled`,`jDateLastRun`,`jID`),
  KEY `jDateLastRun` (`jDateLastRun`,`jID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `Jobs`
--

INSERT INTO `Jobs` (`jID`, `jName`, `jDescription`, `jDateInstalled`, `jDateLastRun`, `pkgID`, `jLastStatusText`, `jLastStatusCode`, `jStatus`, `jHandle`, `jNotUninstallable`, `isScheduled`, `scheduledInterval`, `scheduledValue`) VALUES
(1, 'Index Search Engine - Updates', 'Index the site to allow searching to work quickly and accurately. Only reindexes pages that have changed since last indexing.', '2015-04-30 16:39:19', NULL, 0, NULL, 0, 'ENABLED', 'index_search', 1, 0, 'days', 0),
(2, 'Index Search Engine - All', 'Empties the page search index and reindexes all pages.', '2015-04-30 16:39:19', NULL, 0, NULL, 0, 'ENABLED', 'index_search_all', 1, 0, 'days', 0),
(3, 'Check Automated Groups', 'Automatically add users to groups and assign badges.', '2015-04-30 16:39:19', NULL, 0, NULL, 0, 'ENABLED', 'check_automated_groups', 0, 0, 'days', 0),
(4, 'Generate the sitemap.xml file', 'Generate the sitemap.xml file that search engines use to crawl your site.', '2015-04-30 16:39:19', NULL, 0, NULL, 0, 'ENABLED', 'generate_sitemap', 0, 0, 'days', 0),
(5, 'Process Email Posts', 'Polls an email account and grabs private messages/postings that are sent there..', '2015-04-30 16:39:19', NULL, 0, NULL, 0, 'ENABLED', 'process_email', 0, 0, 'days', 0),
(6, 'Remove Old Page Versions', 'Removes all except the 10 most recent page versions for each page.', '2015-04-30 16:39:19', NULL, 0, NULL, 0, 'ENABLED', 'remove_old_page_versions', 0, 0, 'days', 0),
(7, 'Update Gatherings', 'Loads new items into gatherings.', '2015-04-30 16:39:19', NULL, 0, NULL, 0, 'ENABLED', 'update_gatherings', 0, 0, 'days', 0);

-- --------------------------------------------------------

--
-- Table structure for table `JobsLog`
--

CREATE TABLE IF NOT EXISTS `JobsLog` (
  `jlID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jID` int(10) unsigned NOT NULL,
  `jlMessage` longtext COLLATE utf8_unicode_ci NOT NULL,
  `jlTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `jlError` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`jlID`),
  KEY `jID` (`jID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Logs`
--

CREATE TABLE IF NOT EXISTS `Logs` (
  `logID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` int(10) unsigned NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci,
  `uID` int(10) unsigned DEFAULT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`logID`),
  KEY `channel` (`channel`),
  KEY `uID` (`uID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=78 ;

--
-- Dumping data for table `Logs`
--

INSERT INTO `Logs` (`logID`, `channel`, `time`, `message`, `uID`, `level`) VALUES
(1, 'exceptions', 1430426684, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 0, 600),
(2, 'exceptions', 1430428790, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/default.php:1 include(includes/header.php): failed to open stream: No such file or directory (2)\n', 1, 600),
(3, 'exceptions', 1430428800, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/default.php:1 include(includes/header.php): failed to open stream: No such file or directory (2)\n', 1, 600),
(4, 'exceptions', 1430428909, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(5, 'exceptions', 1430428910, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(6, 'exceptions', 1430428911, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(7, 'exceptions', 1430428911, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(8, 'exceptions', 1430428914, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(9, 'exceptions', 1431041795, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(10, 'exceptions', 1431044368, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:33 syntax error, unexpected ''<'' (4)\n', 1, 600),
(11, 'exceptions', 1431044369, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:33 syntax error, unexpected ''<'' (4)\n', 1, 600),
(12, 'exceptions', 1431045346, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/top.php:29 include(includes/nav.php): failed to open stream: No such file or directory (2)\n', 1, 600),
(13, 'exceptions', 1431045492, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(14, 'application', 1431046435, 'Side "EN" med stien "/en" Flyttet til papirkurven', 1, 100),
(15, 'exceptions', 1431046711, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/text.php:1 include(includes/header.php): failed to open stream: No such file or directory (2)\n', 1, 600),
(16, 'exceptions', 1431047120, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/contact.php:1 include(includes/header.php): failed to open stream: No such file or directory (2)\n', 1, 600),
(17, 'exceptions', 1431063250, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/text.php:1 include(includes/header.php): failed to open stream: No such file or directory (2)\n', 0, 600),
(18, 'exceptions', 1431070235, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/text.php:1 include(includes/header.php): failed to open stream: No such file or directory (2)\n', 0, 600),
(19, 'exceptions', 1431070292, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/text.php:1 include(includes/header.php): failed to open stream: No such file or directory (2)\n', 0, 600),
(20, 'exceptions', 1431195092, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 0, 600),
(21, 'exceptions', 1431195112, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/text.php:1 include(includes/header.php): failed to open stream: No such file or directory (2)\n', 0, 600),
(22, 'exceptions', 1431408475, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/text.php:1 include(includes/header.php): failed to open stream: No such file or directory (2)\n', 0, 600),
(23, 'exceptions', 1431416557, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(24, 'exceptions', 1431416811, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/src/Html/Service/Navigation.php:60 Call to a member function getCollectionParentID() on null (1)\n', 1, 600),
(25, 'exceptions', 1431419956, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(26, 'exceptions', 1431425693, 'Særtilfælde opstået:/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/text.php:1 include(includes/header.php): failed to open stream: No such file or directory (2)\n', 1, 600),
(27, 'exceptions', 1431499160, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 0, 600),
(28, 'exceptions', 1431499898, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(29, 'exceptions', 1431505886, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(30, 'application', 1431508170, 'Side "Scanning" med stien "/insemination/scanning" Flyttet til papirkurven', 1, 100),
(31, 'application', 1431508174, 'Side "Scanning" med stien "/!trash/scanning" Flyttet til papirkurven', 1, 100),
(32, 'exceptions', 1431940642, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 0, 600),
(33, 'exceptions', 1431950394, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(34, 'application', 1431951438, 'Side "" med stien "/!drafts/183" slettet', 1, 100),
(35, 'exceptions', 1431956628, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(36, 'exceptions', 1431978445, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 0, 600),
(37, 'exceptions', 1432020683, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 0, 600),
(38, 'exceptions', 1432020685, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 0, 600),
(39, 'application', 1432024591, 'Side "Parkering" med stien "/praktisk-info/par" Flyttet til papirkurven', 1, 100),
(40, 'application', 1432024594, 'Side "Parkering" med stien "/!trash/par" Flyttet til papirkurven', 1, 100),
(41, 'application', 1432024606, 'Side "Parkering" med stien "/!trash/par" Flyttet til papirkurven', 1, 100),
(42, 'application', 1432024607, 'Side "Parkering" med stien "/!trash/par" Flyttet til papirkurven', 1, 100),
(43, 'exceptions', 1432030007, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(44, 'exceptions', 1432038146, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(45, 'exceptions', 1432039848, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(46, 'exceptions', 1432044688, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 0, 600),
(47, 'exceptions', 1432044690, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 0, 600),
(48, 'exceptions', 1432044692, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 0, 600),
(49, 'exceptions', 1432044693, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 0, 600),
(50, 'exceptions', 1432046373, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(51, 'exceptions', 1432046373, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(52, 'exceptions', 1432046374, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(53, 'exceptions', 1432046379, 'Særtilfælde opstået:Unknown:0 PHP Startup: XCache: Unable to initialize module\nModule compiled with module API=20121212\nPHP    compiled with module API=20131226\nThese options need to match\n (32)\n', 1, 600),
(54, 'exceptions', 1432050031, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor/doctrine/common/lib/Doctrine/Common/Proxy/AbstractProxyFactory.php:207 Doctrine\\Common\\Proxy\\AbstractProxyFactory::getProxyDefinition(): Failed opening required ''/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/config/doctrine/proxies/__CG__ConcreteCoreFileStorageLocationStorageLocation.php'' (include_path=''/customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor:.:/usr/share/php'') (64)\n', 1, 600),
(55, 'exceptions', 1432050033, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor/doctrine/common/lib/Doctrine/Common/Proxy/AbstractProxyFactory.php:207 Doctrine\\Common\\Proxy\\AbstractProxyFactory::getProxyDefinition(): Failed opening required ''/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/config/doctrine/proxies/__CG__ConcreteCoreFileStorageLocationStorageLocation.php'' (include_path=''/customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor:.:/usr/share/php'') (64)\n', 1, 600),
(56, 'exceptions', 1432050060, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor/doctrine/common/lib/Doctrine/Common/Proxy/AbstractProxyFactory.php:207 Doctrine\\Common\\Proxy\\AbstractProxyFactory::getProxyDefinition(): Failed opening required ''/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/config/doctrine/proxies/__CG__ConcreteCoreFileStorageLocationStorageLocation.php'' (include_path=''/customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor:.:/usr/share/php'') (64)\n', 1, 600),
(57, 'exceptions', 1432050101, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor/doctrine/common/lib/Doctrine/Common/Proxy/AbstractProxyFactory.php:207 Doctrine\\Common\\Proxy\\AbstractProxyFactory::getProxyDefinition(): Failed opening required ''/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/config/doctrine/proxies/__CG__ConcreteCoreFileStorageLocationStorageLocation.php'' (include_path=''/customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor:.:/usr/share/php'') (64)\n', 1, 600),
(58, 'exceptions', 1432050139, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor/doctrine/common/lib/Doctrine/Common/Proxy/AbstractProxyFactory.php:207 Doctrine\\Common\\Proxy\\AbstractProxyFactory::getProxyDefinition(): Failed opening required ''/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/config/doctrine/proxies/__CG__ConcreteCoreFileStorageLocationStorageLocation.php'' (include_path=''/customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor:.:/usr/share/php'') (64)\n', 1, 600),
(59, 'exceptions', 1432050160, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor/doctrine/common/lib/Doctrine/Common/Proxy/AbstractProxyFactory.php:207 Doctrine\\Common\\Proxy\\AbstractProxyFactory::getProxyDefinition(): Failed opening required ''/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/config/doctrine/proxies/__CG__ConcreteCoreFileStorageLocationStorageLocation.php'' (include_path=''/customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor:.:/usr/share/php'') (64)\n', 1, 600),
(60, 'exceptions', 1432050255, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor/doctrine/common/lib/Doctrine/Common/Proxy/AbstractProxyFactory.php:207 Doctrine\\Common\\Proxy\\AbstractProxyFactory::getProxyDefinition(): Failed opening required ''/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/config/doctrine/proxies/__CG__ConcreteCoreFileStorageLocationStorageLocation.php'' (include_path=''/customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor:.:/usr/share/php'') (64)\n', 1, 600),
(61, 'exceptions', 1432050286, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor/doctrine/common/lib/Doctrine/Common/Proxy/AbstractProxyFactory.php:207 Doctrine\\Common\\Proxy\\AbstractProxyFactory::getProxyDefinition(): Failed opening required ''/customers/a/e/0/geekmedia.dk/httpd.www/diers/application/config/doctrine/proxies/__CG__ConcreteCoreFileStorageLocationStorageLocation.php'' (include_path=''/customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor:.:/usr/share/php'') (64)\n', 1, 600),
(62, 'exceptions', 1432107553, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:3 scandir(/diers/application/themes/diers/images/banner): failed to open dir: No such file or directory (2)\n', 1, 600),
(63, 'exceptions', 1432107581, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:3 scandir(diers/application/themes/diers/images/banner): failed to open dir: No such file or directory (2)\n', 1, 600),
(64, 'exceptions', 1432107633, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:3 scandir(diers/application/themes/diers/images/banner): failed to open dir: No such file or directory (2)\n', 1, 600),
(65, 'exceptions', 1432107655, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:3 scandir(diers/application/themes/diers/images/banner): failed to open dir: No such file or directory (2)\n', 1, 600),
(66, 'exceptions', 1432107674, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:3 scandir(/diers/application/themes/diers/images/banner): failed to open dir: No such file or directory (2)\n', 1, 600),
(67, 'exceptions', 1432107756, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:3 scandir(/diers/application/themes/diers/images/banner): failed to open dir: No such file or directory (2)\n', 1, 600),
(68, 'exceptions', 1432107778, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:3 scandir(/diers/application/themes/diers/images/banner): failed to open dir: No such file or directory (2)\n', 1, 600),
(69, 'exceptions', 1432107784, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:3 scandir(/diers/application/themes/diers/images/banner): failed to open dir: No such file or directory (2)\n', 1, 600),
(70, 'exceptions', 1432107807, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:3 scandir(diers/application/themes/diers/images/banner): failed to open dir: No such file or directory (2)\n', 1, 600),
(71, 'exceptions', 1432107826, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:3 scandir(diers/application/themes/diers/images/banner): failed to open dir: No such file or directory (2)\n', 1, 600),
(72, 'exceptions', 1432108391, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:6 syntax error, unexpected ''$i'' (T_VARIABLE), expecting '','' or '';'' (4)\n', 1, 600),
(73, 'exceptions', 1432110164, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:3 scandir(/application/themes/diers/images/banner): failed to open dir: No such file or directory (2)\n', 1, 600),
(74, 'exceptions', 1432113679, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/application/themes/diers/includes/nav.php:3 syntax error, unexpected '')'' (4)\n', 0, 600),
(75, 'exceptions', 1432300385, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/controllers/dialog/block/edit.php:46 Call to a member function getInstance() on null (1)\n', 1, 600),
(76, 'exceptions', 1432626069, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/src/Block/BlockType/BlockType.php:151 Class ''\\Concrete\\Package\\VividThumbGallery\\Block\\VividThumbGallery\\Controller'' not found (1)\n', 1, 600),
(77, 'exceptions', 1432626165, 'Exception Occurred: /customers/a/e/0/geekmedia.dk/httpd.www/diers/concrete/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/AbstractMySQLDriver.php:53 An exception occurred while executing ''DROP TABLE btVividThumbGallery, btVividThumbGalleryThumb'':\n\nSQLSTATE[42S02]: Base table or view not found: 1051 Unknown table ''btVividThumbGallery,btVividThumbGalleryThumb'' (0)\n', 1, 600);

-- --------------------------------------------------------

--
-- Table structure for table `MailImporters`
--

CREATE TABLE IF NOT EXISTS `MailImporters` (
  `miID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `miHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `miServer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `miUsername` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `miPassword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `miEncryption` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `miIsEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `miEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `miPort` int(10) unsigned NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned DEFAULT NULL,
  `miConnectionMethod` varchar(8) COLLATE utf8_unicode_ci DEFAULT 'POP',
  PRIMARY KEY (`miID`),
  UNIQUE KEY `miHandle` (`miHandle`),
  KEY `pkgID` (`pkgID`,`miID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `MailImporters`
--

INSERT INTO `MailImporters` (`miID`, `miHandle`, `miServer`, `miUsername`, `miPassword`, `miEncryption`, `miIsEnabled`, `miEmail`, `miPort`, `pkgID`, `miConnectionMethod`) VALUES
(1, 'private_message', '', NULL, NULL, NULL, 0, '', 0, 0, 'POP');

-- --------------------------------------------------------

--
-- Table structure for table `MailValidationHashes`
--

CREATE TABLE IF NOT EXISTS `MailValidationHashes` (
  `mvhID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `miID` int(10) unsigned NOT NULL DEFAULT '0',
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `mHash` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `mDateGenerated` int(10) unsigned NOT NULL DEFAULT '0',
  `mDateRedeemed` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`mvhID`),
  UNIQUE KEY `mHash` (`mHash`),
  KEY `miID` (`miID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MultilingualPageRelations`
--

CREATE TABLE IF NOT EXISTS `MultilingualPageRelations` (
  `mpRelationID` int(10) unsigned NOT NULL DEFAULT '0',
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `mpLanguage` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mpLocale` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`mpRelationID`,`cID`,`mpLocale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `MultilingualSections`
--

CREATE TABLE IF NOT EXISTS `MultilingualSections` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `msLanguage` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `msCountry` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `msNumPlurals` int(10) unsigned NOT NULL DEFAULT '2',
  `msPluralRule` varchar(400) COLLATE utf8_unicode_ci NOT NULL DEFAULT '(n != 1)',
  `msPluralCases` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'one@1\nother@0, 2~16, 100, 1000, 10000, 100000, 1000000, …',
  PRIMARY KEY (`cID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `MultilingualTranslations`
--

CREATE TABLE IF NOT EXISTS `MultilingualTranslations` (
  `mtID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mtSectionID` int(10) unsigned NOT NULL DEFAULT '0',
  `msgid` text COLLATE utf8_unicode_ci NOT NULL,
  `msgstr` text COLLATE utf8_unicode_ci,
  `context` text COLLATE utf8_unicode_ci,
  `comments` text COLLATE utf8_unicode_ci,
  `reference` text COLLATE utf8_unicode_ci,
  `flags` text COLLATE utf8_unicode_ci,
  `updated` datetime DEFAULT NULL,
  `msgidPlural` longtext COLLATE utf8_unicode_ci,
  `msgstrPlurals` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`mtID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `OauthUserMap`
--

CREATE TABLE IF NOT EXISTS `OauthUserMap` (
  `user_id` int(10) unsigned NOT NULL,
  `namespace` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `binding` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`namespace`),
  UNIQUE KEY `oauth_binding` (`binding`,`namespace`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Packages`
--

CREATE TABLE IF NOT EXISTS `Packages` (
  `pkgID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pkgName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `pkgDescription` text COLLATE utf8_unicode_ci,
  `pkgDateInstalled` datetime NOT NULL,
  `pkgIsInstalled` tinyint(1) NOT NULL DEFAULT '1',
  `pkgVersion` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pkgAvailableVersion` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`pkgID`),
  UNIQUE KEY `pkgHandle` (`pkgHandle`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Packages`
--

INSERT INTO `Packages` (`pkgID`, `pkgName`, `pkgHandle`, `pkgDescription`, `pkgDateInstalled`, `pkgIsInstalled`, `pkgVersion`, `pkgAvailableVersion`) VALUES
(2, 'Thumb Gallery', 'vivid_thumb_gallery', 'Add a Gallery of Thumbnails to your Site', '2015-05-26 07:43:25', 1, '1.0.3', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `PageFeeds`
--

CREATE TABLE IF NOT EXISTS `PageFeeds` (
  `pfID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cParentID` int(10) unsigned NOT NULL DEFAULT '1',
  `pfTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pfHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pfDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pfIncludeAllDescendents` tinyint(1) NOT NULL DEFAULT '0',
  `pfContentToDisplay` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `pfAreaHandleToDisplay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pfDisplayAliases` tinyint(1) NOT NULL DEFAULT '0',
  `ptID` smallint(5) unsigned DEFAULT NULL,
  `pfDisplayFeaturedOnly` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`pfID`),
  UNIQUE KEY `pfHandle` (`pfHandle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `PagePaths`
--

CREATE TABLE IF NOT EXISTS `PagePaths` (
  `ppID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cID` int(10) unsigned DEFAULT '0',
  `cPath` text COLLATE utf8_unicode_ci NOT NULL,
  `ppIsCanonical` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `ppGeneratedFromURLSlugs` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ppID`),
  KEY `cID` (`cID`),
  KEY `ppIsCanonical` (`ppIsCanonical`),
  KEY `cPath` (`cPath`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=190 ;

--
-- Dumping data for table `PagePaths`
--

INSERT INTO `PagePaths` (`ppID`, `cID`, `cPath`, `ppIsCanonical`, `ppGeneratedFromURLSlugs`) VALUES
(1, 2, '/dashboard', '1', 0),
(2, 3, '/dashboard/sitemap', '1', 0),
(3, 4, '/dashboard/sitemap/full', '1', 0),
(4, 5, '/dashboard/sitemap/explore', '1', 0),
(5, 6, '/dashboard/sitemap/search', '1', 0),
(6, 7, '/dashboard/files', '1', 0),
(7, 8, '/dashboard/files/search', '1', 0),
(8, 9, '/dashboard/files/attributes', '1', 0),
(9, 10, '/dashboard/files/sets', '1', 0),
(10, 11, '/dashboard/files/add_set', '1', 0),
(11, 12, '/dashboard/users', '1', 0),
(12, 13, '/dashboard/users/search', '1', 0),
(13, 14, '/dashboard/users/groups', '1', 0),
(14, 15, '/dashboard/users/attributes', '1', 0),
(15, 16, '/dashboard/users/add', '1', 0),
(16, 17, '/dashboard/users/add_group', '1', 0),
(17, 18, '/dashboard/users/groups/bulkupdate', '1', 0),
(18, 19, '/dashboard/users/group_sets', '1', 0),
(19, 20, '/dashboard/users/points', '1', 0),
(20, 21, '/dashboard/users/points/assign', '1', 0),
(21, 22, '/dashboard/users/points/actions', '1', 0),
(22, 23, '/dashboard/reports', '1', 0),
(23, 24, '/dashboard/reports/forms', '1', 0),
(24, 25, '/dashboard/reports/surveys', '1', 0),
(25, 26, '/dashboard/reports/logs', '1', 0),
(26, 27, '/dashboard/pages', '1', 0),
(27, 28, '/dashboard/pages/themes', '1', 0),
(28, 29, '/dashboard/pages/themes/inspect', '1', 0),
(29, 30, '/dashboard/pages/types', '1', 0),
(30, 31, '/dashboard/pages/types/organize', '1', 0),
(31, 32, '/dashboard/pages/types/add', '1', 0),
(32, 33, '/dashboard/pages/types/form', '1', 0),
(33, 34, '/dashboard/pages/types/output', '1', 0),
(34, 35, '/dashboard/pages/types/attributes', '1', 0),
(35, 36, '/dashboard/pages/types/permissions', '1', 0),
(36, 37, '/dashboard/pages/templates', '1', 0),
(37, 38, '/dashboard/pages/templates/add', '1', 0),
(38, 39, '/dashboard/pages/attributes', '1', 0),
(39, 40, '/dashboard/pages/single', '1', 0),
(40, 41, '/dashboard/pages/feeds', '1', 0),
(41, 42, '/dashboard/conversations', '1', 0),
(42, 43, '/dashboard/conversations/messages', '1', 0),
(43, 44, '/dashboard/workflow', '1', 0),
(44, 45, '/dashboard/workflow/workflows', '1', 0),
(45, 46, '/dashboard/workflow/me', '1', 0),
(46, 47, '/dashboard/blocks', '1', 0),
(47, 48, '/dashboard/blocks/stacks', '1', 0),
(48, 49, '/dashboard/blocks/permissions', '1', 0),
(49, 50, '/dashboard/blocks/stacks/list', '1', 0),
(50, 51, '/dashboard/blocks/types', '1', 0),
(51, 52, '/dashboard/extend', '1', 0),
(52, 53, '/dashboard/news', '1', 0),
(53, 54, '/dashboard/extend/install', '1', 0),
(54, 55, '/dashboard/extend/update', '1', 0),
(55, 56, '/dashboard/extend/connect', '1', 0),
(56, 57, '/dashboard/extend/themes', '1', 0),
(57, 58, '/dashboard/extend/addons', '1', 0),
(58, 59, '/dashboard/system', '1', 0),
(59, 60, '/dashboard/system/basics', '1', 0),
(60, 61, '/dashboard/system/basics/name', '1', 0),
(61, 62, '/dashboard/system/basics/accessibility', '1', 0),
(62, 63, '/dashboard/system/basics/social', '1', 0),
(63, 64, '/dashboard/system/basics/icons', '1', 0),
(64, 65, '/dashboard/system/basics/editor', '1', 0),
(65, 66, '/dashboard/system/basics/multilingual', '1', 0),
(66, 67, '/dashboard/system/basics/timezone', '1', 0),
(67, 68, '/dashboard/system/multilingual', '1', 0),
(68, 69, '/dashboard/system/multilingual/setup', '1', 0),
(69, 70, '/dashboard/system/multilingual/page_report', '1', 0),
(70, 71, '/dashboard/system/multilingual/translate_interface', '1', 0),
(71, 72, '/dashboard/system/seo', '1', 0),
(72, 73, '/dashboard/system/seo/urls', '1', 0),
(73, 74, '/dashboard/system/seo/bulk', '1', 0),
(74, 75, '/dashboard/system/seo/codes', '1', 0),
(75, 76, '/dashboard/system/seo/excluded', '1', 0),
(76, 77, '/dashboard/system/seo/searchindex', '1', 0),
(77, 78, '/dashboard/system/files', '1', 0),
(78, 79, '/dashboard/system/files/permissions', '1', 0),
(79, 80, '/dashboard/system/files/filetypes', '1', 0),
(80, 81, '/dashboard/system/files/thumbnails', '1', 0),
(81, 82, '/dashboard/system/files/storage', '1', 0),
(82, 83, '/dashboard/system/optimization', '1', 0),
(83, 84, '/dashboard/system/optimization/cache', '1', 0),
(84, 85, '/dashboard/system/optimization/clearcache', '1', 0),
(85, 86, '/dashboard/system/optimization/jobs', '1', 0),
(86, 87, '/dashboard/system/optimization/query_log', '1', 0),
(87, 88, '/dashboard/system/permissions', '1', 0),
(88, 89, '/dashboard/system/permissions/site', '1', 0),
(89, 90, '/dashboard/system/permissions/tasks', '1', 0),
(90, 91, '/dashboard/system/permissions/users', '1', 0),
(91, 92, '/dashboard/system/permissions/advanced', '1', 0),
(92, 93, '/dashboard/system/permissions/blacklist', '1', 0),
(93, 94, '/dashboard/system/permissions/captcha', '1', 0),
(94, 95, '/dashboard/system/permissions/antispam', '1', 0),
(95, 96, '/dashboard/system/permissions/maintenance', '1', 0),
(96, 97, '/dashboard/system/registration', '1', 0),
(97, 98, '/dashboard/system/registration/postlogin', '1', 0),
(98, 99, '/dashboard/system/registration/profiles', '1', 0),
(99, 100, '/dashboard/system/registration/open', '1', 0),
(100, 101, '/dashboard/system/registration/authentication', '1', 0),
(101, 102, '/dashboard/system/mail', '1', 0),
(102, 103, '/dashboard/system/mail/method', '1', 0),
(103, 104, '/dashboard/system/mail/method/test', '1', 0),
(104, 105, '/dashboard/system/mail/importers', '1', 0),
(105, 106, '/dashboard/system/conversations', '1', 0),
(106, 107, '/dashboard/system/conversations/settings', '1', 0),
(107, 108, '/dashboard/system/conversations/points', '1', 0),
(108, 109, '/dashboard/system/conversations/bannedwords', '1', 0),
(109, 110, '/dashboard/system/conversations/permissions', '1', 0),
(110, 111, '/dashboard/system/attributes', '1', 0),
(111, 112, '/dashboard/system/attributes/sets', '1', 0),
(112, 113, '/dashboard/system/attributes/types', '1', 0),
(113, 114, '/dashboard/system/attributes/topics', '1', 0),
(114, 115, '/dashboard/system/attributes/topics/add', '1', 0),
(115, 116, '/dashboard/system/environment', '1', 0),
(116, 117, '/dashboard/system/environment/info', '1', 0),
(117, 118, '/dashboard/system/environment/debug', '1', 0),
(118, 119, '/dashboard/system/environment/logging', '1', 0),
(119, 120, '/dashboard/system/environment/proxy', '1', 0),
(120, 121, '/dashboard/system/backup', '1', 0),
(121, 122, '/dashboard/system/backup/backup', '1', 0),
(122, 123, '/dashboard/system/backup/update', '1', 0),
(123, 124, '/dashboard/welcome', '1', 0),
(124, 125, '/dashboard/home', '1', 0),
(125, 126, '/!drafts', '1', 0),
(126, 127, '/!trash', '1', 0),
(127, 128, '/!stacks', '1', 0),
(128, 129, '/login', '1', 0),
(129, 130, '/register', '1', 0),
(130, 131, '/account', '1', 0),
(131, 132, '/account/edit_profile', '1', 0),
(132, 133, '/account/avatar', '1', 0),
(133, 134, '/account/messages', '1', 0),
(134, 135, '/account/messages/inbox', '1', 0),
(135, 136, '/members', '1', 0),
(136, 137, '/members/profile', '1', 0),
(137, 138, '/members/directory', '1', 0),
(138, 139, '/page_not_found', '1', 0),
(139, 140, '/page_forbidden', '1', 0),
(140, 141, '/download_file', '1', 0),
(141, 143, '/!stacks/header-site-title', '1', 0),
(142, 144, '/!stacks/header-navigation', '1', 0),
(143, 145, '/!stacks/footer-legal', '1', 0),
(144, 146, '/!stacks/footer-navigation', '1', 0),
(145, 147, '/!stacks/footer-contact', '1', 0),
(146, 148, '/om-diers-klinik', '1', 0),
(147, 149, '/!stacks/header-search', '1', 0),
(148, 150, '/!stacks/footer-site-title', '1', 0),
(149, 151, '/!stacks/footer-social', '1', 0),
(150, 152, '/!trash/en', '1', 0),
(151, 153, '/!trash/en/om-diers-2', '1', 0),
(152, 154, '/!stacks/footerinfotext', '1', 0),
(153, 155, '/!stacks/twitter-feed', '1', 0),
(154, 156, '/!stacks/footer-kolonne', '1', 0),
(155, 157, '/!stacks/footer-kolonne-et', '1', 0),
(156, 158, '/!stacks/navigation', '1', 0),
(157, 159, '/insemination', '1', 0),
(158, 161, '/saeddonorerne', '1', 0),
(159, 162, '/hjemmeinsemination', '1', 0),
(160, 163, '/ultralydsscaning', '1', 0),
(161, 164, '/priser', '1', 0),
(162, 165, '/praktisk-info', '1', 0),
(163, 166, '/anmeldelser', '1', 0),
(164, 167, '/kontakt', '1', 0),
(165, 169, '/om-diers-klinik/personale', '1', 0),
(166, 170, '/om-diers-klinik/statistiker', '1', 0),
(167, 171, '/insemination/forberedelsen', '1', 0),
(168, 172, '/!stacks/sidebar-productgroup', '1', 0),
(169, 173, '/!stacks/breadcrumbs-container', '1', 0),
(170, 174, '/!stacks/header-title', '1', 0),
(171, 175, '/insemination/journalsamtalen', '1', 0),
(172, 176, '/!trash/scanning', '1', 0),
(173, 177, '/!drafts/177', '1', 0),
(174, 178, '/insemination/gode-rad', '1', 0),
(175, 179, '/saeddonorerne/ikke-anonym-do', '1', 0),
(176, 180, '/saeddonorerne/anonym-donor', '1', 0),
(177, 181, '/saeddonorerne/partnersaed', '1', 0),
(178, 182, '/!drafts/182', '1', 0),
(180, 184, '/kontakt/tak-din-henvendelse', '1', 0),
(181, 185, '/praktisk-info/abningstider', '1', 0),
(182, 186, '/!trash/par', '1', 0),
(183, 187, '/praktisk-info/overnatning', '1', 0),
(184, 188, '/praktisk-info/nyttige-links', '1', 0),
(185, 189, '/praktisk-info/boger-og-artikler', '1', 0),
(186, 190, '/dashboard/system/environment/entities', '1', 1),
(187, 191, '/saeddonorerne/kendt-do', '1', 1),
(188, 192, '/praktisk-info/medmoderskabfaderskab', '1', 1),
(189, 193, '/anmeldelser/diers-born', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `PagePermissionAssignments`
--

CREATE TABLE IF NOT EXISTS `PagePermissionAssignments` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkID` int(10) unsigned NOT NULL DEFAULT '0',
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`pkID`,`paID`),
  KEY `paID` (`paID`,`pkID`),
  KEY `pkID` (`pkID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `PagePermissionAssignments`
--

INSERT INTO `PagePermissionAssignments` (`cID`, `pkID`, `paID`) VALUES
(1, 1, 40),
(1, 2, 41),
(1, 3, 42),
(1, 4, 43),
(1, 5, 44),
(1, 6, 45),
(1, 7, 46),
(1, 8, 48),
(1, 9, 49),
(1, 11, 50),
(1, 12, 51),
(1, 13, 52),
(1, 14, 53),
(1, 15, 54),
(1, 16, 55),
(1, 17, 56),
(1, 18, 47),
(2, 1, 59),
(129, 1, 57),
(130, 1, 58);

-- --------------------------------------------------------

--
-- Table structure for table `PagePermissionPageTypeAccessList`
--

CREATE TABLE IF NOT EXISTS `PagePermissionPageTypeAccessList` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `externalLink` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`paID`,`peID`),
  KEY `peID` (`peID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `PagePermissionPageTypeAccessListCustom`
--

CREATE TABLE IF NOT EXISTS `PagePermissionPageTypeAccessListCustom` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `ptID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`paID`,`peID`,`ptID`),
  KEY `peID` (`peID`),
  KEY `ptID` (`ptID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `PagePermissionPropertyAccessList`
--

CREATE TABLE IF NOT EXISTS `PagePermissionPropertyAccessList` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `name` tinyint(1) DEFAULT '0',
  `publicDateTime` tinyint(1) DEFAULT '0',
  `uID` tinyint(1) DEFAULT '0',
  `description` tinyint(1) DEFAULT '0',
  `paths` tinyint(1) DEFAULT '0',
  `attributePermission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`paID`,`peID`),
  KEY `peID` (`peID`),
  KEY `uID` (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `PagePermissionPropertyAttributeAccessListCustom`
--

CREATE TABLE IF NOT EXISTS `PagePermissionPropertyAttributeAccessListCustom` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `akID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`paID`,`peID`,`akID`),
  KEY `peID` (`peID`),
  KEY `akID` (`akID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `PagePermissionThemeAccessList`
--

CREATE TABLE IF NOT EXISTS `PagePermissionThemeAccessList` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`paID`,`peID`),
  KEY `peID` (`peID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `PagePermissionThemeAccessListCustom`
--

CREATE TABLE IF NOT EXISTS `PagePermissionThemeAccessListCustom` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `pThemeID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`paID`,`peID`,`pThemeID`),
  KEY `peID` (`peID`),
  KEY `pThemeID` (`pThemeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `PageSearchIndex`
--

CREATE TABLE IF NOT EXISTS `PageSearchIndex` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longtext COLLATE utf8_unicode_ci,
  `cName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cDescription` text COLLATE utf8_unicode_ci,
  `cPath` text COLLATE utf8_unicode_ci,
  `cDatePublic` datetime DEFAULT NULL,
  `cDateLastIndexed` datetime DEFAULT NULL,
  `cDateLastSitemapped` datetime DEFAULT NULL,
  `cRequiresReindex` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`cID`),
  KEY `cDateLastIndexed` (`cDateLastIndexed`),
  KEY `cDateLastSitemapped` (`cDateLastSitemapped`),
  KEY `cRequiresReindex` (`cRequiresReindex`),
  FULLTEXT KEY `cName` (`cName`),
  FULLTEXT KEY `cDescription` (`cDescription`),
  FULLTEXT KEY `content` (`content`),
  FULLTEXT KEY `content2` (`cName`,`cDescription`,`content`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `PageSearchIndex`
--

INSERT INTO `PageSearchIndex` (`cID`, `content`, `cName`, `cDescription`, `cPath`, `cDatePublic`, `cDateLastIndexed`, `cDateLastSitemapped`, `cRequiresReindex`) VALUES
(2, '', 'Dashboard', '', '/dashboard', '2015-04-30 16:39:21', '2015-04-30 16:39:32', NULL, 0),
(3, '', 'Sitemap', 'Whole world at a glance.', '/dashboard/sitemap', '2015-04-30 16:39:21', '2015-04-30 16:39:32', NULL, 0),
(4, '', 'Full Sitemap', '', '/dashboard/sitemap/full', '2015-04-30 16:39:21', '2015-04-30 16:39:32', NULL, 0),
(5, '', 'Flat View', '', '/dashboard/sitemap/explore', '2015-04-30 16:39:22', '2015-04-30 16:39:32', NULL, 0),
(6, '', 'Page Search', '', '/dashboard/sitemap/search', '2015-04-30 16:39:22', '2015-04-30 16:39:32', NULL, 0),
(8, '', 'File Manager', '', '/dashboard/files/search', '2015-04-30 16:39:22', '2015-04-30 16:39:32', NULL, 0),
(9, '', 'Attributes', '', '/dashboard/files/attributes', '2015-04-30 16:39:22', '2015-04-30 16:39:32', NULL, 0),
(10, '', 'File Sets', '', '/dashboard/files/sets', '2015-04-30 16:39:22', '2015-04-30 16:39:32', NULL, 0),
(11, '', 'Add File Set', '', '/dashboard/files/add_set', '2015-04-30 16:39:22', '2015-04-30 16:39:32', NULL, 0),
(12, '', 'Members', 'Add and manage the user accounts and groups on your website.', '/dashboard/users', '2015-04-30 16:39:22', '2015-04-30 16:39:32', NULL, 0),
(13, '', 'Search Users', '', '/dashboard/users/search', '2015-04-30 16:39:22', '2015-04-30 16:39:32', NULL, 0),
(14, '', 'User Groups', '', '/dashboard/users/groups', '2015-04-30 16:39:22', '2015-04-30 16:39:32', NULL, 0),
(15, '', 'Attributes', '', '/dashboard/users/attributes', '2015-04-30 16:39:22', '2015-04-30 16:39:32', NULL, 0),
(16, '', 'Add User', '', '/dashboard/users/add', '2015-04-30 16:39:22', '2015-04-30 16:39:32', NULL, 0),
(17, '', 'Add Group', '', '/dashboard/users/add_group', '2015-04-30 16:39:23', '2015-04-30 16:39:32', NULL, 0),
(19, '', 'Group Sets', '', '/dashboard/users/group_sets', '2015-04-30 16:39:23', '2015-04-30 16:39:32', NULL, 0),
(20, '', 'Community Points', NULL, '/dashboard/users/points', '2015-04-30 16:39:23', '2015-04-30 16:39:32', NULL, 0),
(22, '', 'Actions', NULL, '/dashboard/users/points/actions', '2015-04-30 16:39:23', '2015-04-30 16:39:32', NULL, 0),
(23, '', 'Reports', 'Get data from forms and logs.', '/dashboard/reports', '2015-04-30 16:39:23', '2015-04-30 16:39:32', NULL, 0),
(24, '', 'Form Results', 'Get submission data.', '/dashboard/reports/forms', '2015-04-30 16:39:23', '2015-04-30 16:39:32', NULL, 0),
(25, '', 'Surveys', '', '/dashboard/reports/surveys', '2015-04-30 16:39:23', '2015-04-30 16:39:32', NULL, 0),
(26, '', 'Logs', '', '/dashboard/reports/logs', '2015-04-30 16:39:23', '2015-04-30 16:39:32', NULL, 0),
(28, '', 'Themes', 'Reskin your site.', '/dashboard/pages/themes', '2015-04-30 16:39:24', '2015-04-30 16:39:33', NULL, 0),
(29, '', 'Inspect', '', '/dashboard/pages/themes/inspect', '2015-04-30 16:39:24', '2015-04-30 16:39:33', NULL, 0),
(31, '', 'Organize Page Type Order', '', '/dashboard/pages/types/organize', '2015-04-30 16:39:24', '2015-04-30 16:39:33', NULL, 0),
(32, '', 'Add Page Type', '', '/dashboard/pages/types/add', '2015-04-30 16:39:24', '2015-04-30 16:39:33', NULL, 0),
(33, '', 'Compose Form', '', '/dashboard/pages/types/form', '2015-04-30 16:39:24', '2015-04-30 16:39:33', NULL, 0),
(34, '', 'Defaults and Output', '', '/dashboard/pages/types/output', '2015-04-30 16:39:24', '2015-04-30 16:39:33', NULL, 0),
(35, '', 'Page Type Attributes', '', '/dashboard/pages/types/attributes', '2015-04-30 16:39:24', '2015-04-30 16:39:33', NULL, 0),
(36, '', 'Page Type Permissions', '', '/dashboard/pages/types/permissions', '2015-04-30 16:39:24', '2015-04-30 16:39:33', NULL, 0),
(38, '', 'Add Page Template', 'Add page templates to your site.', '/dashboard/pages/templates/add', '2015-04-30 16:39:24', '2015-04-30 16:39:33', NULL, 0),
(39, '', 'Attributes', '', '/dashboard/pages/attributes', '2015-04-30 16:39:24', '2015-04-30 16:39:33', NULL, 0),
(40, '', 'Single Pages', '', '/dashboard/pages/single', '2015-04-30 16:39:24', '2015-04-30 16:39:33', NULL, 0),
(41, '', 'RSS Feeds', '', '/dashboard/pages/feeds', '2015-04-30 16:39:25', '2015-04-30 16:39:33', NULL, 0),
(43, '', 'Messages', '', '/dashboard/conversations/messages', '2015-04-30 16:39:25', '2015-04-30 16:39:33', NULL, 0),
(44, '', 'Workflow', '', '/dashboard/workflow', '2015-04-30 16:39:25', '2015-04-30 16:39:33', NULL, 0),
(48, '', 'Stacks', 'Share content across your site.', '/dashboard/blocks/stacks', '2015-04-30 16:39:25', '2015-04-30 16:39:33', NULL, 0),
(50, '', 'Stack List', '', '/dashboard/blocks/stacks/list', '2015-04-30 16:39:25', '2015-04-30 16:39:33', NULL, 0),
(51, '', 'Block Types', 'Manage the installed block types in your site.', '/dashboard/blocks/types', '2015-04-30 16:39:25', '2015-04-30 16:39:33', NULL, 0),
(52, '', 'Extend concrete5', '', '/dashboard/extend', '2015-04-30 16:39:25', '2015-04-30 16:39:33', NULL, 0),
(53, '', 'Dashboard', '', '/dashboard/news', '2015-04-30 16:39:25', '2015-04-30 16:39:33', NULL, 0),
(54, '', 'Add Functionality', 'Install add-ons & themes.', '/dashboard/extend/install', '2015-04-30 16:39:26', '2015-04-30 16:39:33', NULL, 0),
(55, '', 'Update Add-Ons', 'Update your installed packages.', '/dashboard/extend/update', '2015-04-30 16:39:26', '2015-04-30 16:39:33', NULL, 0),
(56, '', 'Connect to the Community', 'Connect to the concrete5 community.', '/dashboard/extend/connect', '2015-04-30 16:39:26', '2015-04-30 16:39:33', NULL, 0),
(57, '', 'Get More Themes', 'Download themes from concrete5.org.', '/dashboard/extend/themes', '2015-04-30 16:39:26', '2015-04-30 16:39:33', NULL, 0),
(58, '', 'Get More Add-Ons', 'Download add-ons from concrete5.org.', '/dashboard/extend/addons', '2015-04-30 16:39:26', '2015-04-30 16:39:34', NULL, 0),
(59, '', 'System & Settings', 'Secure and setup your site.', '/dashboard/system', '2015-04-30 16:39:26', '2015-04-30 16:39:34', NULL, 0),
(61, '', 'Site Name', '', '/dashboard/system/basics/name', '2015-04-30 16:39:26', '2015-04-30 16:39:34', NULL, 0),
(62, '', 'Accessibility', '', '/dashboard/system/basics/accessibility', '2015-04-30 16:39:26', '2015-04-30 16:39:34', NULL, 0),
(63, '', 'Social Links', '', '/dashboard/system/basics/social', '2015-04-30 16:39:26', '2015-04-30 16:39:34', NULL, 0),
(64, '', 'Bookmark Icons', 'Bookmark icon and mobile home screen icon setup.', '/dashboard/system/basics/icons', '2015-04-30 16:39:26', '2015-04-30 16:39:34', NULL, 0),
(65, '', 'Rich Text Editor', '', '/dashboard/system/basics/editor', '2015-04-30 16:39:26', '2015-04-30 16:39:34', NULL, 0),
(66, '', 'Languages', '', '/dashboard/system/basics/multilingual', '2015-04-30 16:39:26', '2015-04-30 16:39:34', NULL, 0),
(67, '', 'Time Zone', '', '/dashboard/system/basics/timezone', '2015-04-30 16:39:27', '2015-04-30 16:39:34', NULL, 0),
(68, '', 'Multilingual', 'Run your site in multiple languages.', '/dashboard/system/multilingual', '2015-04-30 16:39:27', '2015-04-30 16:39:34', NULL, 0),
(73, '', 'Pretty URLs', '', '/dashboard/system/seo/urls', '2015-04-30 16:39:27', '2015-04-30 16:39:34', NULL, 0),
(74, '', 'Bulk SEO Updater', '', '/dashboard/system/seo/bulk', '2015-04-30 16:39:27', '2015-04-30 16:39:34', NULL, 0),
(75, '', 'Tracking Codes', '', '/dashboard/system/seo/codes', '2015-04-30 16:39:27', '2015-04-30 16:39:34', NULL, 0),
(76, '', 'Excluded URL Word List', '', '/dashboard/system/seo/excluded', '2015-04-30 16:39:27', '2015-04-30 16:39:34', NULL, 0),
(77, '', 'Search Index', '', '/dashboard/system/seo/searchindex', '2015-04-30 16:39:27', '2015-04-30 16:39:34', NULL, 0),
(79, '', 'File Manager Permissions', '', '/dashboard/system/files/permissions', '2015-04-30 16:39:27', '2015-04-30 16:39:34', NULL, 0),
(80, '', 'Allowed File Types', '', '/dashboard/system/files/filetypes', '2015-04-30 16:39:27', '2015-04-30 16:39:34', NULL, 0),
(81, '', 'Thumbnails', '', '/dashboard/system/files/thumbnails', '2015-04-30 16:39:28', '2015-04-30 16:39:34', NULL, 0),
(82, '', 'File Storage Locations', '', '/dashboard/system/files/storage', '2015-04-30 16:39:28', '2015-04-30 16:39:34', NULL, 0),
(84, '', 'Cache & Speed Settings', '', '/dashboard/system/optimization/cache', '2015-04-30 16:39:28', '2015-04-30 16:39:34', NULL, 0),
(85, '', 'Clear Cache', '', '/dashboard/system/optimization/clearcache', '2015-04-30 16:39:28', '2015-04-30 16:39:34', NULL, 0),
(86, '', 'Automated Jobs', '', '/dashboard/system/optimization/jobs', '2015-04-30 16:39:28', '2015-04-30 16:39:34', NULL, 0),
(87, '', 'Database Query Log', '', '/dashboard/system/optimization/query_log', '2015-04-30 16:39:28', '2015-04-30 16:39:34', NULL, 0),
(89, '', 'Site Access', '', '/dashboard/system/permissions/site', '2015-04-30 16:39:28', '2015-04-30 16:39:34', NULL, 0),
(90, '', 'Task Permissions', '', '/dashboard/system/permissions/tasks', '2015-04-30 16:39:28', '2015-04-30 16:39:34', NULL, 0),
(93, '', 'IP Blacklist', '', '/dashboard/system/permissions/blacklist', '2015-04-30 16:39:29', '2015-04-30 16:39:34', NULL, 0),
(94, '', 'Captcha Setup', '', '/dashboard/system/permissions/captcha', '2015-04-30 16:39:29', '2015-04-30 16:39:35', NULL, 0),
(95, '', 'Spam Control', '', '/dashboard/system/permissions/antispam', '2015-04-30 16:39:29', '2015-04-30 16:39:35', NULL, 0),
(96, '', 'Maintenance Mode', '', '/dashboard/system/permissions/maintenance', '2015-04-30 16:39:29', '2015-04-30 16:39:35', NULL, 0),
(98, '', 'Login Destination', '', '/dashboard/system/registration/postlogin', '2015-04-30 16:39:29', '2015-04-30 16:39:35', NULL, 0),
(99, '', 'Public Profiles', '', '/dashboard/system/registration/profiles', '2015-04-30 16:39:29', '2015-04-30 16:39:35', NULL, 0),
(100, '', 'Public Registration', '', '/dashboard/system/registration/open', '2015-04-30 16:39:29', '2015-04-30 16:39:35', NULL, 0),
(101, '', 'Authentication Types', '', '/dashboard/system/registration/authentication', '2015-04-30 16:39:29', '2015-04-30 16:39:35', NULL, 0),
(102, '', 'Email', 'Control how your site send and processes mail.', '/dashboard/system/mail', '2015-04-30 16:39:29', '2015-04-30 16:39:35', NULL, 0),
(103, '', 'SMTP Method', '', '/dashboard/system/mail/method', '2015-04-30 16:39:29', '2015-04-30 16:39:35', NULL, 0),
(104, '', 'Test Mail Settings', '', '/dashboard/system/mail/method/test', '2015-04-30 16:39:29', '2015-04-30 16:39:35', NULL, 0),
(105, '', 'Email Importers', '', '/dashboard/system/mail/importers', '2015-04-30 16:39:29', '2015-04-30 16:39:35', NULL, 0),
(106, '', 'Conversations', 'Manage your conversations settings', '/dashboard/system/conversations', '2015-04-30 16:39:29', '2015-04-30 16:39:35', NULL, 0),
(107, '', 'Settings', '', '/dashboard/system/conversations/settings', '2015-04-30 16:39:29', '2015-04-30 16:39:36', NULL, 0),
(108, '', 'Community Points', '', '/dashboard/system/conversations/points', '2015-04-30 16:39:29', '2015-04-30 16:39:36', NULL, 0),
(109, '', 'Banned Words', '', '/dashboard/system/conversations/bannedwords', '2015-04-30 16:39:30', '2015-04-30 16:39:36', NULL, 0),
(111, '', 'Attributes', 'Setup attributes for pages, users, files and more.', '/dashboard/system/attributes', '2015-04-30 16:39:30', '2015-04-30 16:39:37', NULL, 0),
(112, '', 'Sets', 'Group attributes into sets for easier organization', '/dashboard/system/attributes/sets', '2015-04-30 16:39:30', '2015-04-30 16:39:37', NULL, 0),
(113, '', 'Types', 'Choose which attribute types are available for different items.', '/dashboard/system/attributes/types', '2015-04-30 16:39:30', '2015-04-30 16:39:37', NULL, 0),
(114, '', 'Topics', '', '/dashboard/system/attributes/topics', '2015-04-30 16:39:30', '2015-04-30 16:39:37', NULL, 0),
(116, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(117, '', 'Environment Information', '', '/dashboard/system/environment/info', '2015-04-30 16:39:30', '2015-04-30 16:39:37', NULL, 0),
(118, '', 'Debug Settings', '', '/dashboard/system/environment/debug', '2015-04-30 16:39:30', '2015-04-30 16:39:37', NULL, 0),
(119, '', 'Logging Settings', '', '/dashboard/system/environment/logging', '2015-04-30 16:39:30', '2015-04-30 16:39:37', NULL, 0),
(120, '', 'Proxy Server', '', '/dashboard/system/environment/proxy', '2015-04-30 16:39:31', '2015-04-30 16:39:37', NULL, 0),
(121, '', 'Backup & Restore', 'Backup or restore your website.', '/dashboard/system/backup', '2015-04-30 16:39:31', '2015-04-30 16:39:37', NULL, 0),
(123, '', 'Update concrete5', '', '/dashboard/system/backup/update', '2015-04-30 16:39:31', '2015-04-30 16:39:37', NULL, 0),
(124, '                                        ', 'Welcome to concrete5', 'Learn about how to use concrete5, how to develop for concrete5, and get general help.', '/dashboard/welcome', '2015-04-30 16:39:31', '2015-04-30 16:39:37', NULL, 0),
(125, '', 'Customize Dashboard Home', '', '/dashboard/home', '2015-04-30 16:39:31', '2015-04-30 16:39:37', NULL, 0),
(126, '', 'Drafts', '', '/!drafts', '2015-04-30 16:39:38', '2015-04-30 16:39:40', NULL, 0),
(127, '', 'Trash', '', '/!trash', '2015-04-30 16:39:39', '2015-04-30 16:39:40', NULL, 0),
(128, '', 'Stacks', '', '/!stacks', '2015-04-30 16:39:39', '2015-04-30 16:39:40', NULL, 0),
(131, '', 'My Account', '', '/account', '2015-04-30 16:39:39', '2015-04-30 16:39:40', NULL, 0),
(1, ' For din egen sikkerheds skyld er det vigtigt, at du vælger en klinik, der er godkendt af de danske sundhedsmyndigheder og lever op til EU lovgivningen. Vi er certificeret af den danske sundhedsstyrelse og lægemiddelstyrelse.   For din egen sikkerheds skyld er det vigtigt, at du vælger en klinik, der er godkendt af de danske sundhedsmyndigheder og lever op til EU lovgivningen. Vi er certificeret af den danske sundhedsstyrelse og lægemiddelstyrelse.  Start dit forløb i dag! Vi er her hver dag året rundt. Kontakt os i dag, for en uforpligtende samtale med os, omkring dine muligheder.  HVAD SIGER VORES KUNDER OM OS  Jeg hedder Liza Diers, jeg er jordemoder og har startet Diers Klinik. Min motivation for at åbne fertilitetsklinikken er, at jeg ønsker at hjælpe enhver kvinde eller ethvert par - single, lesbisk eller heteroseksuel -med at blive gravid.    De kvinder, som bliver inseminerede på Diers Klinik, ligger mig meget på sinde, og jeg er dybt engageret i det, jeg laver. Derfor vil du altid få en personlig, individuel vejledning og behandling på Diers Klinik. Jeg elsker mit fag og har løbende og meget tæt kontakt med de kvinder, jeg inseminerer - også efter opnået graviditet, hvor der ofte melder sig en række spørgsmål.    Jeg har selv gennemgået fertilitetsbehandling på klinikken og har tre donorbørn.  Velkommen til Diers Klinik.Vi er der hele vejen for dig. Insemination Når det er tid til at blive insemineret, ringer du til klinikken og sammen aftaler vi et tidspunkt for dette  							Læs mere  Høj succesrate  Vi har den højeste succesrate i landet for inseminationsbehandling med donorsæd og vi gør alt for at den rate stiger.  							Læs mere  Priser Diers Klinik har konkurrencedygtige priser og hos os kommer der ingen ekstra gebyrer på. Vi har priser på insemination fra kr. 3800 incl. scanning.   							Læs mere  ', 'Forside', '', NULL, '2015-04-30 16:39:00', '2015-05-26 08:28:52', NULL, 0),
(148, ' Diers Klinik er en fertilitetsklinik i hjertet af Aarhus.  Vi hjælper primært singler og lesbiske med insemination med donorsæd, men hvis du har en mandlig partner eller ven, der vil donere, så kan vi også hjælpe med det.  Ved fertilitetsbehandling med donorsæd kan du vælge i mellem flere forskellige typer af donorsæd – anonym donor, åben donor, hvor donor har samtykket til at barnet kan kontakte ham, anonym donor med profil, hvor du får barnebilleder, en lang  profil med oplysninger om donor osv. samt åben donor med profil se mere om donor.   Din chance for at blive gravid, og altså hvor mange forsøg, som du må påregne før du bliver gravid afhænger af din alder. Vores behandlingssuccesrate er blandt de allerbedste i Danmark.   Kontakt os til en uforpligtende snak om dine ønsker til en inseminationsbehandling.  Vi glæder os til at byde dig velkommen.  ', 'Om Diers klinik', '', '/om-diers-klinik', '2015-04-30 20:45:12', '2015-05-27 07:19:17', NULL, 0),
(152, '', 'EN', '', '/en', '2015-04-30 20:47:37', '2015-04-30 20:48:00', NULL, 0),
(153, '', 'om diers 2', '', '/en/om-diers-2', '2015-04-30 20:48:06', '2015-04-30 20:48:35', NULL, 0),
(156, ' For din egen sikkerheds skyld er det vigtigt, at du vælger en klinik, der er godkendt af de danske sundhedsmyndigheder og lever op til EU lovgivningen. Vi er certificeret af den danske sundhedsstyrelse og lægemiddelstyrelse.  ', 'Footer kolonne to', NULL, '/!stacks/footer-kolonne', '2015-05-07 23:27:40', '2015-05-07 23:35:17', NULL, 0),
(157, ' Diers Klinik er en fertilitets klinik, der tilbyder donorinsemination til par, singlekvinder og lesbiske par.  			  			Diers Klinik   Grønnegade 56, 1.    8000 Aarhus C  Tlf. +45 20 22 85 87    E-mail: info@diersklinik.dk  ', 'Footer kolonne et', NULL, '/!stacks/footer-kolonne-et', '2015-05-07 23:30:27', '2015-05-20 11:18:02', NULL, 0),
(158, '', 'navigation', NULL, '/!stacks/navigation', '2015-05-08 00:19:38', '2015-05-12 09:23:37', NULL, 0),
(159, ' Hos Diers Klinik hjælper vi dig trygt igennem hele forløbet. Derfor har vi også en af de bedste succesrater i Danmark.   På Diers Klinik er vi specialister i inseminationsbehandling. Vi har en af de  bedste succesrater i landet, og vores inseminationsbehandlinger bliver oftest  udført i naturlig cyklus, dvs. uden brug af hormoner eller anden medicin.  Vi kan lave insemination med både donorsæd eller med din partners sæd. Vi kan  også nedfryse sæd til senere insemination, hvis din partner ikke kan være  tilstede på selve inseminationsdagen.  På de næste sider kan du læse om, hvordan forløbet vil være.  ', 'Insemination', '', '/insemination', '2015-05-08 00:54:10', '2015-05-20 10:36:47', NULL, 0),
(161, ' Sæddonorerne, der benyttes kommer fra sædbanker, som lever op til Sundhedsstyrelsens anbefalinger.  I Diers Klinik har du flere muligheder, hvad angår valg af sæddonor. Vi tilbyder  både ”åbne” sæddonorer samt ”non contact” sæddonorer. Hvis du vælger en ”non  contact” sæddonor, vil det på intet tidspunkt være muligt for dig eller barnet at få oplysninger om donorens identitet. Hvis du vælger en ”åben” sæddonor, har dit kommende barn mulighed for at få kontakt til donoren, når det fylder 18 år.  Det er dog forskelligt, hvad de åbne donorer forpligter sig på afhængig af, hvilken sædbank de donorer ved. Derfor må du selv indhente informationerne fra sædbanken om, hvilke muligheder det kommende barn har for at kontakte  sæddonoren, når det fylder 18 år.  Hos os går vi meget op i, at du selv har mulighed for at vælge donor ud fra de  ønsker og behov du måtte have. Du har mulighed for at vælge donorerne enten ud  fra en basisprofil eller en udvidet profil. Ved valg af donorer ud fra  basisprofilen, får du oplysninger om sæddonorens øjenfarve, højde, vægt,  beskæftigelse samt blodtype. Hvor du ved donorer med udvidet profil desuden får oplysninger om f.eks. sygehistorie, familieforhold samt fritidsinteresser. Du får endvidere mulighed for at se babybilleder og høre en stemmeprøve. Det er både muligt at få ”åbne” samt ”non contact” donorer med udvidet  profil.  Sæddonorer har ingen juridiske forpligtelser overfor de børn, der undfanges ved hjælp af  sæd fra ham. Derfor har barnet ikke arveret eller andre rettigheder i forhold til donoren.  I henhold til Sundhedsstyrelsen skal følgende oplysninger gives til kvinder/par, der behandles med sæd fra en sæddonor: ”Ved  udvælgelse af donorer er risiko for videregivelse af arvelige sygdomme,  misdannelser m.v. søgt begrænset ved kun at anvende donorer som har oplyst, at  de ikke har kendskab til sådanne arverisici i deres slægt, og hvor der af en erfaren sundhedsperson er udspurgt og undersøgt for at belyse dette. Trods disse  forsigtighedsregler er enhver arverisiko alligevel ikke udelukket. Hvis barnet  mod forventning fejler noget ved fødslen eller i de første leveår, som du får at  vide kan være arveligt, er det derfor vigtigt, at du melder tilbage til  klinikken eller den sundhedsperson, der har behandlet dig, så der kan tages  stilling til, om donor fortsat kan anvendes. Det samme gælder, hvis du får at  vide, at det kan dreje sig om smitteoverførsel fra donor-sæd eller donor-æg.  Selvom donor er testet fri for overførbare sygdomme for eksempel HIV og  hepatitis, er risikoen aldrig nul.” Sæddonorerne er alle testet grundigt for diverse sygdomme  HIV, hepatitis, syphilis, CMV og gonoré. Derudover gennemgår donorerne en række  fysiske og psykiske undersøgelse. De er alle unge, raske mænd, der oftest er  studerende ved højere læreanstalter.   I Diers Klinik stiller  vi meget strenge krav til kvaliteten af sæden og sikrer os altid, at kvaliteten  er god, inden hver enkelt insemination. Hvis du har lyst, er du meget velkommen  til at se med i mikroskopet. Hos os bliver sæden altid først klargjort efter, at  vi har sikret os, at det er det helt rigtige tidspunkt for insemination. Klargøring af sæden tager fra 5 til 45 minutter.  Vi  har et udvalg af donorer her i klinikken, som vi har købt igennem sædbankerne. Dette betyder, at du ikke selv behøver at købe sæden hjem fra sædbanken og  oprette et depot hos os. Du kan derimod reservere et strå af den donor, som du  ønsker fra gang til gang uden at være bundet. Du er meget velkommen til at  kontakte os, hvis du ønsker at se listen over de donorer, som vi har  tilgængelige.   Hos Diers Klinik har du flere muligheder, hvad angår valg af sæddonor.  ', 'Sæddonorerne', '', '/saeddonorerne', '2015-05-08 00:59:16', '2015-05-26 10:20:53', NULL, 0),
(162, '', 'Hjemmeinsemination', '', '/hjemmeinsemination', '2015-05-08 01:00:00', '2015-05-19 08:53:45', NULL, 0),
(163, 'Tidlig graviditetsscanning/tryghedscanning På Diers Klinik kan du få foretaget en tidlig graviditetsscanning, dvs. fra  uge 7+0 til uge 11+6. Her scanner vi dig for: * Hjerteblink * Terminsbestemmelse (endelig termin fastsættes ved nakkefoldscanning på  sygehuset i uge 12) * Flerfoldsgraviditet Du får endvidere en faglig vurdering og svar på eventuelle spørgsmål.  Prisen for den tidligere graviditetsscanning er 500 Kr.  Du er naturligvis velkommen, også selvom du ikke tidligere har været i  behandling hos os.  Kontakt os for en tidsbestilling.   Vi tilbyder ultralydsscanning til alle gravide, så du kan følge udviklingen af dit barn  ', 'Ultralydsscaning', '', '/ultralydsscaning', '2015-05-08 01:01:21', '2015-05-13 09:42:43', NULL, 0),
(164, '          Journalsamtale:   600 kr.  Insemination (IUI)  med non-contact donor: 4.900 kr.  Insemination  (IUI) med non-contact donor med udvidet profil:    6.300 kr.  Insemination (IUI) med  åben donor: 6.500 kr.  Insemination (IUI) med  åben donor med udvidet profil:   7.900 kr.  Insemination med privat  sæd: 3.800 kr.  Håndteringsgebyr ved modtagelse af privat  sæd:             500 kr.  Samtlige blodprøver (Hiv,  Hepatitis B og C): 1.300 kr.  Sædanalyse: 900 kr.   Fragt af  donorsæd:  1000 kr.  Graviditetsscanning  (uge 7+0 til 11+6): 500 kr. Follikelscanning: 300 kr.  Akupunktur: 600 kr.   Fotomatch:   500 kr.     Ægløsningstest 5 stk. per  pakke: 150 kr.  Graviditetstest:   50 kr.  Overnatning i  dobbeltværelse:  600 kr.      Priseksempel Prisen for behandlingen afhænger af, hvilken  donorsæd du vælger, og hvor mange forsøg, der skal til før du bliver  gravid.  Hvis  du f.eks. er 33 år og ønsker en inseminationsbehandling med en anonym donor, så  kunne prisen se således ud:  Journalsamtale: 600  DKK  Insemination inklusiv anonym donorsæd og inklusiv ultralydsscanning: 4900 DKK  Når du f.eks. er  33 år, så har du i gennemsnit 26% chance for at blive gravid, dvs. at du må  påregne 4 forsøg før det lykkes at få en baby.   Regnestykket ser derfor således  ud:  600 DKK + 4 × 4900  DKK = 19.600 DKK  Der kommer ingen ekstra gebyrer eller omkostninger på prisen.  ', 'Priser', '', '/priser', '2015-05-08 01:02:26', '2015-05-20 11:05:42', NULL, 0),
(165, ' Du finder os på følgende adresse:  Diers Klinik Grønnegade 56, 1 8000 Aarhus C  Tlf.: 20 22 85 87   ', 'Praktisk info', '', '/praktisk-info', '2015-05-08 01:03:13', '2015-05-20 10:32:14', NULL, 0),
(166, ' Tekst  TestRedigeret tekst  Hej med jerFår en lille glad tåre i øjet, når jeg tænker på hvad I har hjulpet mig med. Har en meget glad dreng på snart 16 måneder. Tusind tak.Knus til jer  Pleasant atmosphere, professional service. A great employee. I remain very satisfied!  Thanks Kristine  Ich war nicht in der Klinik, aber interessantes Internetseitenangebot; Wunschbabies aus dem Einkaufskorb. Herzliche Glückwünsche zu euren Wareneinkäufen und eine schöne Lebzeit mit eurem gekauften Glück   Da har vi vært på ultralyd (uke16-17) og fått beskjed at det mest sansynelig er en liten jentebaby inni magen   Jeg kan ikke andet end være fuldt ud tilfreds og glad for den supergode behandling, jeg har fået på Diers, da jeg som enlig skulle insemineres i april. Det lykkedes i første forsøg, og nu glæder jeg mig til at få en lillebror til min dreng på 5 år til januar.Jeg vil varmt anbefale klinikken til andre, der har brug for fertilitetsbehandling.  Vi vil takke for hyggelig og imøtekommende ankomst til klinikken 24 Juni 2012 og vi er utrolig glad for den positive testen vi fikk den 7 Juli 2012 etter første forsøk   Ville lige sige Hej, og fortælle hvor lykkelig jeg er over at havde taget valget om et donorbarn som jeg føde i juli 2011. En søn som kom i første forsøg. Jeg har allerede fortalt andre der går med tanker om at få et donorbarn om jeres dejlige klinik og hvor søde og imødekommende i er.  Tak for utrolig god behandling og rådgivning, da beslutningen om at blive mor til et donorbarn skulle træffes. For fem dage siden blev jeg mor for første gang. Min vidunderlige søn blev undfanget i første forsøg på Diers Klinik. Verdens dejligste dreng   For snart et år siden blev jeg gravid på Diers Klinik efter et meget behageligt og professionelt forløb. Vi er to forældre, der derfor absolut kun kan videregive vores varmeste anbefalinger.Diers Klinik kan vi takke for vores vidunderlige tre måneder gamle søn, og vi vil ikke tøve med at bruge klinikken igen, når vi forhåbentlig skal beriges med endnu et barn engang i fremtiden.  Hej LizaHar lige stået og betragtet min skønne lille datter, på 2,5 år, som er blevet til på Diers klinik.Når jeg kigger på hende, ved jeg lige præcis, hvorfor jeg her om en uge eller to, igen besøger jeres hyggelige klinik, for at blive gravid igen.Kan overhovedet ikke forestille mig at skulle insemineres andre steder, end hos jer.Stor tak for super behandling !Mange kærlige hilsner Lotte  Kære Liza Vi har en lille lækker guldklump, som I har hjulpet til verden og vi vil bare sige tak for et super forløb. Vi glæder os til at skulle komme hos jer igen, når det bliver tid til en lillesøster eller lillebror. Det er dejligt at komme på Diers Klinik, hvor der er en uformel og afslappet atmosfære og hvor man altid føler sig velkommen og i trykke hænder.Kh. to stolte mødre  Jeg sidder i øjeblikket med en dejlig stor mave og venter bare på at føde mit donorbarn der er undfanget i første forsøg hos Diers Klinik. Jeg er så lykkelig for at have fået muligheden for igen at blive mor, denne gang med en donor som far. Den behandling jeg som enlig kvinde har fået hos jer, har været fantastisk. I er meget professionelle og søde og jeg glæder mig meget til at kunne sende jer et billede af min søn om små 6 uger.Jeg vil helt sikkert bruge Diers Klinik igen hvis jeg skal have een mere ... og det skal jeg nok.Stort knus og tak til jer begge. Lea  Vi har været rigtig glade for den behandling, vi har fået. Den har fuldt ud levet op til vores forventninger. Vi har følt, at der er blevet taget rigtig godt imod os, at vi er blevet taget seriøst og at behandlingen har været meget personlig og rettet mod netop vores ønsker og behov. Det har været utroligt rart og trygt, at vi altid har kunnet ringe, hvis vi har været i tvivl om noget, og i klinikken har der altid været god tid til at snakke og stille spørgsmål. Det har desuden været rigtigt dejligt, at I inseminerer på stort set alle tidspunkter, så behandling med hormoner har kunnet undgås.Mvh. Nina og Line  Jeg er veldig godt fornøyd, og deres klinikk er et veldig bra tilbud.... og dere to Liza og Kristine er noe hyggelige kvinner, som gjør jobben godt man føler seg trygg med det samme man kommer inn døra hos dere ....... )Klem Carina  Vi er rigtig glade for de mange fine anmeldelser, vi har fået.  Du er meget velkommen til at skrive en anmeldelse i feltet nederst på siden.  På forhånd tak.   ', 'Anmeldelser', '', '/anmeldelser', '2015-05-08 01:04:00', '2015-05-20 09:26:20', NULL, 0),
(167, '   For at optimere dine chancer for at opnå graviditet holder Diers Klinik åbent alle årets dage mandag - søndag	9.00 - 16.00 - men du kan kontakte os døgnet rundt via kontaktformularen eller via sms på 2022 8587   Har du yderligere spørgsmål, eller er du interesseret i at påbegynde et inseminationsforløb hos os eller at få tilsendt yderligere informationsmateriale, er du meget velkommen til at kontakte os via nedenstående formular eller på telefon nr. 20 22 8587.  ', 'Kontakt', '', '/kontakt', '2015-05-08 01:04:48', '2015-05-22 13:17:36', NULL, 0),
(184, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(185, ' For at optimere dine chancer for at opnå graviditet holder Diers  Klinik åbent alle årets dage - også i ferier og på søn- og helligdage.  På Diers Klinik garanterer vi, at du altid kan blive insemineret i din  aktuelle cyklus, når du har afholdt journalsamtale og har dokumentation for, at  du er ikke har HIV, Hepatitis B og C samt Klamydia.  Almindelige telefontid  Mandag til søndag fra kl. 9-16  I dette tidsrum er du velkommen til at ringe og bestille tid til  journalsamtale eller stille eventuelle spørgsmål til inseminationsforløbet. Hvis  der ikke er flere inseminationer, så går personalet hjem, og svarer ikke længere  telefonen. Vi holder dog altid øje med SMS, mail eller telefonsvarerbeskeder, så  kan du ikke komme igennem på telefonen, så læg en besked, send en e-mail eller  SMS.  Tlf.: + 45 2022 8587   Hvis du har en positiv ægløsningstest i løbet af eftermiddagen  eller aftenen, skal du sende os en sms eller lægge en besked på telefonsvareren,  så kontakter vi dig med en tid til insemination den efterfølgende dag. Hvis du  har en positiv ægløsningstest om morgenen og ønsker en tid den samme dag, kan du  melde dig på telefon, sms eller email indtil kl. 10.   ', 'Åbningstider', '', '/praktisk-info/abningstider', '2015-05-18 12:26:21', '2015-05-19 08:32:51', NULL, 0),
(186, '', 'Parkering', '', '/praktisk-info/par', '2015-05-18 12:27:25', '2015-05-18 12:28:19', NULL, 0),
(187, ' Det er muligt for vores kunder at leje et værelse til overnatning på Diers  Klinik. Værelset indeholder:  * Dobbeltseng  * Spisebord  * TV og DVD-afspiller  * Trådløst netværksforbindelse  * Mikroovn  * Køleskab  * El-keddel  Fra værelset, som er på 10 m2, er der direkte adgang til toilet og bad  samt tekøkken.  Vi gør opmærksom på, at toilet og bad deles med klinikkens  kunder i dagtimerne.  Pris pr./overnatning er 600,-   www.hotel-faber.dk Eckersbergsgade 17 8000 Aarhus C tlf.: 70267011  Afstand fra klinikken: 2 km.  Der ydes rabat ved henvisning fra Diers Klinik  Se andre overnatningsmuligheder  ', 'Overnatning', '', '/praktisk-info/overnatning', '2015-05-18 12:28:28', '2015-05-19 08:44:08', NULL, 0),
(169, '     Liza Diers, Jordemoder Jeg hedder Liza Diers og har startet Diers Klinik. Min  motivation for at åbne fertilitetsklinikken er, at jeg ønsker at hjælpe enhver  kvinde eller ethvert par - single, lesbisk eller heteroseksuel med at blive  gravid.       Kristina Sørensen, Sygeplejerske Jeg hedder Kristina Sørensen. Efter endt sygeplerskestudie har jeg læst en kandidat i sygeplejevidenskab, hvor jeg har specialiseret mig i donorbørn. Jeg afholder journalsamtaler, scanner for ægløsning og udfører inseminationer.  Jeg er på barselsorlov fra juni 2014 til sommeren 2015.       Janni Meisner, Jordemoder Jeg hedder Janni Meisner. Udover at være her på klinikken er jeg timevikar på Skejby Fødeafdeling. Jeg afholder journalsamtaler, scanner for ægløsning og udfører inseminationer.       Anne Louise Richardt Nielsen, Jordemoder Jeg hedder Anne Louise Richardt Nielsen. Jeg afholder journalsamtaler, scanner og udfører inseminationer. Jeg har tidligere arbejdet på fødegangen på Aarhus Universitetshospital, Skejby, hvor jeg fortsat er timeafløser.       Anne Sofie Bøtcher, Sygeplejerske Jeg hedder Anne Sofie Bøtcher. Jeg scanner for ægløsning og udfører inseminationer. Ved siden af arbejdet på klinikken er jeg ved at tage en kandidatgrad i Sygeplejevidenskab ved Aarhus Universitetshospital.    Louise Mabire, Jordemoder Jeg hedder Louise Mabire. Jeg scanner for ægløsning og udfører inseminationer. Udover arbejdet på klinikken, arbejder jeg også som jordemoder på Horsens Fødeafdeling.   Jeg er på barsel fra august 2014.    Lise Friis Jeg hedder Lise Friis og er Diers Kliniks tysktalende medarbejder, som sørger for en god start for vores tyske klienter. Udover at varetage journalsamtaler på tysk, er jeg også ansat som IT-ansvarlig på klinikken.  ', 'Personale', '', '/om-diers-klinik/personale', '2015-05-12 09:22:07', '2015-05-22 11:49:22', NULL, 0),
(170, '  På Diers Klinik gør vi alt hvad vi kan for at gøre vores succesrate så høj som muligt. Vi har den højeste succesrate i landet for inseminationsbehandling med donorsæd.  Vi gør meget ud af at finde det helt rigtige tidspunkt for inseminationen, og vi scanner altid gratis inden behandlingen for at sikre bedst mulige betingelser for at behandlingen skal lykkes.  Og vores anstrengelser bærer frugt. Vi har den højeste succesrate i landet for inseminationsbehandling med donorsæd.   Her kan du se vores tal sammenlignet med landsgennemsnittet for alle klinikker i Danmark.    På Diers klinik holder vi løbende statistik med vores inseminationsbehandlinger.    Dine graviditetschancer afhænger meget af din alder.   Til du fylder 35 år er chancerne 26 % pr. insemination.  Fra 35-39 år er den 20 % pr. insemination.  Er du mellem 40 og 42 er chancen 10 % pr. insemination.   Statistikken er baseret på tal fra 2014.   Hos Diers Klinik har vi den højeste succesrate i Danmark  ', 'Statistik', '', '/om-diers-klinik/statistiker', '2015-05-12 09:53:37', '2015-05-26 10:17:01', NULL, 0),
(171, '        Inden du påbegynder inseminationen hos Diers Klinik, er der nogle få forberedelser, som du skal have foretaget. Dette er med til at øge chancerne for at blive gravid og vi har brug for dokumentionen før vi må inseminere dig. Inden inseminationen skal du gå til din egen læge og få foretaget følgende  test: - HIV-test - Heptitis B+C - Klamydia: podning fra cervix eller ved urintest, hvis du er yngre end 30  år.  Disse prøver skal der foreligge dokumentation for, inden du kan blive  insemineret.  Når du er ved din egen læge, er det en god idé samtidig at få foretaget  følgende test:   - Celleskrab fra livmoderhalsen - Test for antistoffer mod rubella (røde hunde) Tidligere infektioner Hvis du har haft infektion i livmoderen eller æggelederne, eller hvis du  har haft klamydia eller gonoré, kan der være dannet arvæv i æggelederne, som  umuliggør passage for både æg og sædceller. Er det tilfældet, bør du gå til en  gynækolog og få lavet en undersøgelse, der afklarer, om der er passage i dine  æggeledere. Denne undersøgelse er gratis ved henvisning fra egen læge.   Hvis du har fået konstateret endometriose, er det ligeledes en god ide at  få undersøgt passagen i æggelederne.  Vi har samlet en række gode råd, der hjælper dig med at blive gravid   Med en god forberedelse, er du godt på vej til en vellykket behandling  ', 'Forberedelsen', '', '/insemination/forberedelsen', '2015-05-12 10:02:13', '2015-05-13 08:58:35', NULL, 0),
(173, '', 'Breadcrumbs container', NULL, '/!stacks/breadcrumbs-container', '2015-05-12 10:16:51', '2015-05-12 10:19:21', NULL, 0),
(172, '', 'Sidebar productgroup', NULL, '/!stacks/sidebar-productgroup', '2015-05-12 10:15:06', '2015-05-12 10:36:28', NULL, 0),
(174, 'Forberedelsen ', 'Header title', NULL, '/!stacks/header-title', '2015-05-12 10:20:46', '2015-05-12 10:36:28', NULL, 0),
(175, ' Journalsamtalen sikrer dig en god start på din insemination. Vores dygtige personale svarer på alle dine spørgsmål.   Inden du kan få foretaget insemination på Diers Klinik, skal du have været til en journalsamtale hos os.   Journalsamtalen foregår i rolige og trygge rammer, og vi tager udgangspunkt i dig og de ønsker, du har til dit forløb. Vi taler om, hvordan det hele kommer til at foregå, og du har mulighed for at få svar på alle de spørgsmål, du har.   Det kan eventuelt være en god idé, at du inden samtalen laver en liste med alle dine spørgsmål. Det er vigtigt, at du efter journalsamtalen har en klar  forestilling om, hvad der kommer til at ske, og at du føler dig forberedt på din fertilitetsbehandling.   Du får en grundig instruktion i, hvad du skal gøre, for at vi kan bestemme tidspunktet for din ægløsning. Desuden skal vi have klarlagt dine chancer for at opnå graviditet.   Til samtalen skal vi også finde ud af, hvilke ønsker du har til sæddonoren, f.eks.: Er det det rigtige for dig at vælge en anonym eller en åben sæddonor, ønsker du ekstra oplysninger om donor og eventuelt at se et  babyfoto, og har du nogen specielle ønsker, som vi skal prøve at imødekomme? Eller har du en ven eller bekendt, som du ønsker skal være donor/far til dit barn.  Du kan bestille en tid til journalsamtale ved at maile eller ringe til  os, og vi anbefaler, at vi afholder journalsamtalen senest en måned inden første  insemination, men det kan også lade sig gøre med kortere varsel. Bor du langt  væk, kan vi tage journalsamtalen over telefonen eller Skype.   Vi opfordrer til, at du/I har læst vores informationsmateriale grundigt, inden I kommer til samtalen.  ', 'Journalsamtalen', '', '/insemination/journalsamtalen', '2015-05-12 13:24:51', '2015-05-23 14:11:05', NULL, 0),
(176, '', 'Scanning', '', '/insemination/scanning', '2015-05-12 13:26:11', '2015-05-12 13:26:46', NULL, 0),
(178, ' Du kan selv øge chancerne for at blive gravid  Sundhedsstyrelsen anbefaler, at man indtager 400 mygram folinsyre om dagen,  når man planlægger at blive gravid.   Derudover bør man spise en sund og varieret kost. Hvis den mad, man spiser,  indeholder en masse frugt og grønt - mindst 6 stk. om dagen, er der ingen grund  til at spise yderligere kosttilskud, mens man prøver at opnå graviditet.   Andre gode råd, der øger chancerne  Rygning nedsætter fertiliteten væsentligt. Derudover har man større risiko  for at abortere, hvis man ryger. Man bør derfor stoppe med at ryge inden  insemination.   Sundhedsstyrelsen anbefaler, at man ikke drikker alkohol under graviditet,  og når man forsøger at blive gravid.   Der er lidt uenighed om, hvorvidt kaffe og koffeinindtag har indvirkning på  fertiliteten. Flere undersøgelser peger i forskellige retninger. For at være på  den sikre side, bør man derfor ikke drikke mere end svarende til 200mg koffein  dagligt. Vær opmærksom på, at der kan være stor forskel på indholdet af koffein i kaffe.    Overvægt har også indvirkning på fertiliteten. Fedtvævet bevirker nemlig, at  der dannes for meget mandligt kønshormon, som virker forstyrrende på  hormonbalancen. Man siger, at fertiliteten kan blive påvirket på et Body Mass  Index (BMI) på 27 eller derover. BMI beregnes på følgende måde: Din vægt i kilo  divideret med din højde mål i meter i anden. Hvis du f.eks. vejer 65 kg. og er  170 cm. ser regnestykket således ud: 65/(1,70x1,70) = 22,49 BMI. Hvis din  fertilitet er påvirket, vil det f.eks. give sig udslag i en lang  menstruationscyklus. Mange kvinder har forhøjet BMI uden, at det har indflydelse  på fertiliteten.  Du kan selv øge chancerne for at blive gravid Sundhedsstyrelsen anbefaler, at man indtager 400 mygram folinsyre om dagen, når man planlægger at blive gravid.   Derudover bør man spise en sund og varieret kost. Hvis den mad, man spiser, indeholder en masse frugt og grønt - mindst 6 stk. om dagen, er der ingen grund til at spise yderligere kosttilskud, mens man prøver at opnå graviditet.  Andre gode råd, der øger chancerne * Rygning nedsætter fertiliteten væsentligt. Derudover har man større risiko  for at abortere, hvis man ryger. Man bør derfor stoppe med at ryge inden insemination.   * Sundhedsstyrelsen anbefaler, at man ikke drikker alkohol under graviditet, og når man forsøger at blive gravid.   * Der er lidt uenighed om, hvorvidt kaffe og koffeinindtag har indvirkning på fertiliteten. Flere undersøgelser peger i forskellige retninger. For at være på  den sikre side, bør man derfor ikke drikke mere end svarende til 200mg koffein dagligt. Vær opmærksom på, at der kan være stor forskel på indholdet af koffein i kaffe.      * Overvægt har også indvirkning på fertiliteten. Fedtvævet bevirker nemlig, at  der dannes for meget mandligt kønshormon, som virker forstyrrende på  hormonbalancen.  Man siger, at fertiliteten kan blive påvirket på et Body Mass  Index (BMI) på 27 eller derover. BMI beregnes på følgende måde: Din vægt i kilo  divideret med din højde mål i meter i anden. Hvis du f.eks. vejer 65 kg. og er 170 cm. ser regnestykket således ud: 65/(1,70x1,70) = 22,49 BMI. Hvis din fertilitet er påvirket, vil det f.eks. give sig udslag i en lang  menstruationscyklus. Mange kvinder har forhøjet BMI uden, at det har indflydelse på fertiliteten.  ', 'Gode råd', '', '/insemination/gode-rad', '2015-05-13 08:33:15', '2015-05-20 10:41:46', NULL, 0),
(179, ' Hvis man ønsker at sikre, at barnet kan få sæddonorens identitet at vide i fremtiden, skal man vælge en Ikke-anonym sæddonor.  Ikke-anonyme sæddonorers identitet kan børn få oplyst, når de er 18 år, hvis de kan dokumentere eller sandsynliggøre, at de er et resultat af behandling med donorsæd fra en bestemt Ikke-anonym sæddonor. Der vil ikke blive stillet ret strenge krav til dokumentation, og i tvivlstilfælde vil identiteten blive udleveret til barnet. Sæddonorerne har accepteret at blive kontaktet af børnene.  Hvis man ønsker at sikre, at barnet kan få sæddonorens identitet at vide i fremtiden, skal man vælge en Ikke-anonym sæddonor. Det ser ud til, at især singler og par af samme køn har præference for Ikke-anonyme sæddonorer, mens heteroseksuelle par synes at have præference for Anonyme sæddonorer, for at beskytte mandens integritet som fader og hermed deres egen interesse som familie. Men det er ikke altid sådan. Nogle heteroseksuelle par vælger netop en Ikke-anonym sæddonor, fordi de planlægger at fortælle barnet om dets tilblivelse, og derfor gerne vil sikre, at der er mulighed for kontakt. Omvendt vælger mange singler en Anonym sæddonor,  fordi de planlægger at finde en partner senere, som således måske lettere kan træde ind i faderrollen og adoptere barnet, hvis der ikke er for meget fokus på sæddonors eksistens. Valget er meget individuelt og meget komplekst.  Kilde: Cryos International  ', 'Ikke-anonym donor', '', '/saeddonorerne/ikke-anonym-do', '2015-05-13 09:33:21', '2015-05-13 10:25:15', NULL, 0),
(180, ' Anonyme sæddonorers identitet holdes hemmelig for altid. Basisprofil Anonyme sæddonorer med enbasisprofil er registrerede med et nummer som 456, 8756, 11250, osv. Basisprofiler har kun oplysning om race, etnicitet, øjenfarve, hårfarve, højde, vægt, blodtype samt oftest erhverv/uddannelse. Man kan dog også vælge at få en udvidet profil. Udvidet profil Sæddonorer med udvidet profil er registrerede med et fiktivt navn som ERIK, IB, PER, OLUF, SVEND, osv. De Udvidede profiler består af op til 8-10 sider personlige oplysninger om sæddonorens baggrund, uddannelse, familiemæssige forhold, interesser, hobbies, etc. Der kan være følgende tillægsoplysninger: personalevurdering, fotos af sæddonor fra hans barndom, en håndskreven hilsen, en lydoptagelse af sæddonors stemme, EQ-profil, m.v. Basisprofil eller udvidet profil Hvis mange data om sæddonor er vigtig for dig, bør du vælge en sæddonor med Udvidet profil. Singler og par af samme køn, som ofte har et eksplicit ønske om at sikre informationer til børnene, kan have en præference for sæddonorer med Udvidet profil. Hvis du ønsker at vide så lidt som muligt om sæddonoren, skal du vælge en sæddonor med Basisprofil. I mange tilfælde vælger heteroseksuelle par en sæddonor med Basisprofil, hvor singler og par af samme køn vælger en sæddonor med Udvidet profil. Årsagerne er de samme som når man vælger Anonym hhv. Ikke-anonym sæddonor, men ofte er det lige omvendt. Valget er meget individuelt og meget komplekst.  Kilde: Cryos International  ', 'Anonym donor', '', '/saeddonorerne/anonym-donor', '2015-05-13 10:10:35', '2015-05-13 10:23:51', NULL, 0),
(181, ' Vil du gerne insemineres med din partners sæd, byder vi jer velkommen til en  personlig samtale her på klinikken.  Før behandlingsstart skal den kommende far have foretaget følgende  blodprøver:  * Hiv 1+2 (anti-Hiv - 1,2) * Hepatitis b virus (HBsAg og Anti HBc) * Hepatitis c virus (Anti-HCV-Ab)  Vi beder jer være opmærksomme på, at testresultaterne højest må være 12  måneder gamle, og at de skal foreligge ved behandlingsstart.   Derudover skal den kommende far have foretaget en sædanalyse, så vi kan  sikre de bedste forudsætninger for en successfuld insemination. Såvel sædprøven  til sædanalysen som blodprøverne kan foretages her på klinikken.  ', 'Partnersæd', '', '/saeddonorerne/partnersaed', '2015-05-13 10:37:07', '2015-05-13 10:40:14', NULL, 0),
(188, ' www.nordiccryobank.com www.lfub.dk (landsforeningen for  ufrivilligt barnløse) www.lbl.dk (landsforeningen for bøsser og lesbiske) www.endo.dk (endometrioseforeningen) www.pcoinfo.dk (PCO  foreningen) www.sundhed.dk (adgang til  sundhedsvæsenet) http://www.semdk@proboards.com (chatforum for selvvalgte  enlige mødre) http://www.fertilitetsselskab.dk/  ', 'Nyttige links', '', '/praktisk-info/nyttige-links', '2015-05-18 12:31:27', '2015-05-19 08:46:29', NULL, 0),
(189, ' Her har vi samlet nyttige artikler og informationer om bøger af relevans for  dig som overvejer inseminationsbehandling eller er gravid.   Rapport om at  være barn i en homoseksuel familie: www.regeringen.se - læs rapport her.   Lov: særligt børnetilskud ved kunstig befrugtning: www.retsinformation.dk -  læs lov her.  Bøger: "The ultimate guide to pregnancy for lesbians" af Rachel  Pepper   Et studie i San Francisco viser at ca. en tredjedel af de børn,  der er undfanget vha. en åben sæddonor vælger at få kontakt med donoren.  www.medpagetoday.com - læs  studie her.    ', 'Bøger og artikler', '', '/praktisk-info/boger-og-artikler', '2015-05-19 08:47:39', '2015-05-19 08:52:34', NULL, 0),
(192, ' Hvis I er et lesbisk par der ønsker et barn, så skal den kvindelige part, der ikke bærer barnet registreres som medmoder.   Et medmoderskab kan kun fastslås, hvis to kvinder får et barn sammen ved hjælp af kunstig befrugtning. Graviditet ad anden vej er ikke gyldig, hvis en kvinde skal have medmoderskab.  Hvis man skal fastslå et medmoderskab/faderskab, skal proceduren begynde inden selve fertilitetsbehandlingen.  Reglerne betyder, at medmoren får de samme rettigheder og pligter over for barnet.  Man skal være opmærksom på, at hvis der er tale om en kendt donor, anses donoren som udgangspunkt for at være barnets far. Donoren, den kommende mor og morens partner (ægtefælle, registreret partner eller kæreste) kan aftale, at donoren ikke skal have status som barnets far, men at det i stedet bliver morens partner, der får status som medmor. For at lave en gældende aftale om dette, skal børnelovens samtykkeerklæring § 27a, stk. 2 benyttes.  Er donoren kendt, og er barnets mor lesbisk (enten gift med en kvinde, er i registreret partnerskab med en kvinde eller lever sammen med en kvindelig partner) anvendes samtykkeerklæring § 27a, stk. 1 og 2:  Hvis donoren skal registreres som barnets far, udfyldes afsnittet om faderskab (stk. 1). Hvis morens kvindelige ægtefælle, registrerede partner eller kvindelige partner skal være medmor til barnet, udfyldes afsnittet om medmoderskab (stk. 2).  Når der er anvendt anonym/ åben donorsæd, og når barnets mor lever i et ægteskab med enten en mand eller kvinde, eller i et registreret partnerskab med en kvinde eller som kærester med enten en mand eller kvinde (§27), eller når den kendte donor skal registreres som barnets far, og når barnets mor ikke er i et forhold til en mand, der skal være barnets far (§27b), skal samtykkeerklæring §§27 og 27b anvendes.  Når dét er udfyldt og underskrevet, skal det afleveres på behandlingsstedet til det sundhedsfaglige personale, som skal underskrive det og udstede en kopi til kvinden/parret.  Ved opstart af faderskabs eller medmoderskabssagen skal kvinden/parret selv sende det underskrevne samtykke til Statsforvaltningen. Statsforvaltningen anbefaler, at de indsender samtykkeerklæringen 2-3 måneder før barnets forventede fødsel. Statsforvaltningen vil da registrere faderskabet/medmoderskabet i forbindelse med barnets fødsel. (Derfor skal de selv rette henvendelse til Statsforvaltningen, så hurtigt som muligt efter barnets fødsel, så fader- eller medmoderskabet kan registreres).  Læs mere medmoderskab og her.  ', 'Medmoderskab/faderskab', '', '/praktisk-info/medmoderskabfaderskab', '2015-05-22 13:48:16', '2015-05-22 13:53:09', NULL, 0);
INSERT INTO `PageSearchIndex` (`cID`, `content`, `cName`, `cDescription`, `cPath`, `cDatePublic`, `cDateLastIndexed`, `cDateLastSitemapped`, `cRequiresReindex`) VALUES
(190, '', 'Database Entities', NULL, '/dashboard/system/environment/entities', '2015-05-19 15:46:55', '2015-05-19 15:46:56', NULL, 0),
(191, ' Har du/I en ven eller bekendt, som ønsker at være kendt donor?   Hvis du ønsker at bruge en kendt donor, så kan vi også hjælpe jer med det.  Donoren skal i princippet efter loven behandles som en sæddonor, idet I ikke er et par. Derfor skal han svare på en række spørgsmål ang. hans helbredstilstand og familiesygehistorie. Han skal desuden testes for HIV 1+2, hepatitis B og C, klamydie, gonorré og syfilis. Hans sæd skal fryses ned på klinikken inden behandlingen.  Inden inseminationen skal du/I underskrive et informeret samtykke, hvor du/I accepterer de ricisi, der måtte være ved at benytte den kendte donor.  Kontakt os, hvis du ønsker yderligere information.  ', 'Kendt donor', '', '/saeddonorerne/kendt-do', '2015-05-22 12:01:50', '2015-05-22 12:05:29', NULL, 0),
(193, ' Vi er meget glade for at få billeder af alle de skønne børn, der kommer til verden via Diers Klinik   Se nogle af de skønne børn fra Diers Klinik. Langt over 2500 børn er kommet verden med hjælp fra Diers Klinik. Vi vil meget gerne modtage billeder til denne side. Send os en mail på info@diersklinik.dk med emnet: Billede til hjemmesiden  På forhånd mange tak.   ', 'Diers børn', '', '/anmeldelser/diers-born', '2015-05-25 08:16:00', '2015-05-26 09:14:27', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `PageTemplates`
--

CREATE TABLE IF NOT EXISTS `PageTemplates` (
  `pTemplateID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pTemplateHandle` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `pTemplateIcon` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pTemplateName` varchar(90) COLLATE utf8_unicode_ci NOT NULL,
  `pTemplateIsInternal` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`pTemplateID`),
  UNIQUE KEY `pTemplateHandle` (`pTemplateHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `PageTemplates`
--

INSERT INTO `PageTemplates` (`pTemplateID`, `pTemplateHandle`, `pTemplateIcon`, `pTemplateName`, `pTemplateIsInternal`, `pkgID`) VALUES
(1, 'core_stack', '', 'Stack', 1, 0),
(2, 'dashboard_primary_five', '', 'Dashboard Primary + Five', 1, 0),
(3, 'dashboard_header_four_col', '', 'Dashboard Header + Four Column', 1, 0),
(4, 'dashboard_full', '', 'Dashboard Full', 1, 0),
(5, 'full', 'full.png', 'Full', 0, 0),
(6, 'about', 'full.png', 'About', 0, 0),
(7, 'contact', 'full.png', 'Contact', 0, 0),
(8, 'front', 'full.png', 'Front', 0, 0),
(9, 'text', 'full.png', 'Text', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `PageThemeCustomStyles`
--

CREATE TABLE IF NOT EXISTS `PageThemeCustomStyles` (
  `pThemeID` int(10) unsigned NOT NULL DEFAULT '0',
  `scvlID` int(10) unsigned DEFAULT '0',
  `preset` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sccRecordID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`pThemeID`),
  KEY `scvlID` (`scvlID`),
  KEY `sccRecordID` (`sccRecordID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `PageThemes`
--

CREATE TABLE IF NOT EXISTS `PageThemes` (
  `pThemeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pThemeHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `pThemeName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pThemeDescription` text COLLATE utf8_unicode_ci,
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  `pThemeHasCustomClass` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pThemeID`),
  UNIQUE KEY `ptHandle` (`pThemeHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `PageThemes`
--

INSERT INTO `PageThemes` (`pThemeID`, `pThemeHandle`, `pThemeName`, `pThemeDescription`, `pkgID`, `pThemeHasCustomClass`) VALUES
(1, 'elemental', 'Elemental', 'Elegant, spacious theme with support for blogs, portfolios, layouts and more.', 0, 1),
(2, 'diers', 'Diers klinik website', 'Tema til Diers Klinik''s website. Udviklet af Geek Media', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `PageTypeComposerControlTypes`
--

CREATE TABLE IF NOT EXISTS `PageTypeComposerControlTypes` (
  `ptComposerControlTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ptComposerControlTypeHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptComposerControlTypeName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ptComposerControlTypeID`),
  UNIQUE KEY `ptComposerControlTypeHandle` (`ptComposerControlTypeHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `PageTypeComposerControlTypes`
--

INSERT INTO `PageTypeComposerControlTypes` (`ptComposerControlTypeID`, `ptComposerControlTypeHandle`, `ptComposerControlTypeName`, `pkgID`) VALUES
(1, 'core_page_property', 'Built-In Properties', 0),
(2, 'collection_attribute', 'Custom Attributes', 0),
(3, 'block', 'Block', 0);

-- --------------------------------------------------------

--
-- Table structure for table `PageTypeComposerFormLayoutSetControls`
--

CREATE TABLE IF NOT EXISTS `PageTypeComposerFormLayoutSetControls` (
  `ptComposerFormLayoutSetControlID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ptComposerFormLayoutSetID` int(10) unsigned DEFAULT '0',
  `ptComposerControlTypeID` int(10) unsigned DEFAULT '0',
  `ptComposerControlObject` longtext COLLATE utf8_unicode_ci,
  `ptComposerFormLayoutSetControlDisplayOrder` int(10) unsigned DEFAULT '0',
  `ptComposerFormLayoutSetControlCustomLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptComposerFormLayoutSetControlCustomTemplate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptComposerFormLayoutSetControlDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptComposerFormLayoutSetControlRequired` int(11) DEFAULT '0',
  PRIMARY KEY (`ptComposerFormLayoutSetControlID`),
  KEY `ptComposerControlTypeID` (`ptComposerControlTypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `PageTypeComposerFormLayoutSetControls`
--

INSERT INTO `PageTypeComposerFormLayoutSetControls` (`ptComposerFormLayoutSetControlID`, `ptComposerFormLayoutSetID`, `ptComposerControlTypeID`, `ptComposerControlObject`, `ptComposerFormLayoutSetControlDisplayOrder`, `ptComposerFormLayoutSetControlCustomLabel`, `ptComposerFormLayoutSetControlCustomTemplate`, `ptComposerFormLayoutSetControlDescription`, `ptComposerFormLayoutSetControlRequired`) VALUES
(1, 1, 1, 'O:78:"Concrete\\Core\\Page\\Type\\Composer\\Control\\CorePageProperty\\NameCorePageProperty":9:{s:37:"\0*\0ptComposerControlRequiredByDefault";b:1;s:17:"\0*\0propertyHandle";s:4:"name";s:30:"\0*\0ptComposerControlTypeHandle";s:18:"core_page_property";s:30:"\0*\0ptComposerControlIdentifier";s:4:"name";s:24:"\0*\0ptComposerControlName";s:9:"Page Name";s:27:"\0*\0ptComposerControlIconSRC";s:40:"/diers/concrete/attributes/text/icon.png";s:20:"\0*\0ptComposerControl";N;s:41:"\0*\0ptComposerControlRequiredOnThisRequest";b:0;s:5:"error";s:0:"";}', 0, 'Page Name', NULL, NULL, 1),
(2, 1, 1, 'O:85:"Concrete\\Core\\Page\\Type\\Composer\\Control\\CorePageProperty\\DescriptionCorePageProperty":9:{s:17:"\0*\0propertyHandle";s:11:"description";s:30:"\0*\0ptComposerControlTypeHandle";s:18:"core_page_property";s:30:"\0*\0ptComposerControlIdentifier";s:11:"description";s:24:"\0*\0ptComposerControlName";s:11:"Description";s:27:"\0*\0ptComposerControlIconSRC";s:44:"/diers/concrete/attributes/textarea/icon.png";s:20:"\0*\0ptComposerControl";N;s:37:"\0*\0ptComposerControlRequiredByDefault";b:0;s:41:"\0*\0ptComposerControlRequiredOnThisRequest";b:0;s:5:"error";s:0:"";}', 1, NULL, NULL, NULL, 0),
(3, 1, 1, 'O:81:"Concrete\\Core\\Page\\Type\\Composer\\Control\\CorePageProperty\\UrlSlugCorePageProperty":9:{s:17:"\0*\0propertyHandle";s:8:"url_slug";s:30:"\0*\0ptComposerControlTypeHandle";s:18:"core_page_property";s:30:"\0*\0ptComposerControlIdentifier";s:8:"url_slug";s:24:"\0*\0ptComposerControlName";s:8:"URL Slug";s:27:"\0*\0ptComposerControlIconSRC";s:40:"/diers/concrete/attributes/text/icon.png";s:20:"\0*\0ptComposerControl";N;s:37:"\0*\0ptComposerControlRequiredByDefault";b:0;s:41:"\0*\0ptComposerControlRequiredOnThisRequest";b:0;s:5:"error";s:0:"";}', 2, NULL, NULL, NULL, 0),
(4, 1, 1, 'O:86:"Concrete\\Core\\Page\\Type\\Composer\\Control\\CorePageProperty\\PageTemplateCorePageProperty":9:{s:17:"\0*\0propertyHandle";s:13:"page_template";s:30:"\0*\0ptComposerControlTypeHandle";s:18:"core_page_property";s:30:"\0*\0ptComposerControlIdentifier";s:13:"page_template";s:24:"\0*\0ptComposerControlName";s:13:"Page Template";s:27:"\0*\0ptComposerControlIconSRC";s:42:"/diers/concrete/attributes/select/icon.png";s:20:"\0*\0ptComposerControl";N;s:37:"\0*\0ptComposerControlRequiredByDefault";b:0;s:41:"\0*\0ptComposerControlRequiredOnThisRequest";b:0;s:5:"error";s:0:"";}', 3, NULL, NULL, NULL, 0),
(5, 1, 1, 'O:87:"Concrete\\Core\\Page\\Type\\Composer\\Control\\CorePageProperty\\PublishTargetCorePageProperty":9:{s:17:"\0*\0propertyHandle";s:14:"publish_target";s:30:"\0*\0ptComposerControlTypeHandle";s:18:"core_page_property";s:30:"\0*\0ptComposerControlIdentifier";s:14:"publish_target";s:24:"\0*\0ptComposerControlName";s:13:"Page Location";s:27:"\0*\0ptComposerControlIconSRC";s:46:"/diers/concrete/attributes/image_file/icon.png";s:20:"\0*\0ptComposerControl";N;s:37:"\0*\0ptComposerControlRequiredByDefault";b:0;s:41:"\0*\0ptComposerControlRequiredOnThisRequest";b:0;s:5:"error";s:0:"";}', 4, NULL, NULL, NULL, 0),
(6, 2, 3, 'O:53:"Concrete\\Core\\Page\\Type\\Composer\\Control\\BlockControl":11:{s:7:"\0*\0btID";i:12;s:30:"\0*\0ptComposerControlTypeHandle";s:5:"block";s:5:"\0*\0bt";b:0;s:4:"\0*\0b";b:0;s:30:"\0*\0ptComposerControlIdentifier";i:12;s:24:"\0*\0ptComposerControlName";s:7:"Content";s:27:"\0*\0ptComposerControlIconSRC";s:39:"/diers/concrete/blocks/content/icon.png";s:20:"\0*\0ptComposerControl";N;s:37:"\0*\0ptComposerControlRequiredByDefault";b:0;s:41:"\0*\0ptComposerControlRequiredOnThisRequest";b:0;s:5:"error";s:0:"";}', 0, 'Body', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `PageTypeComposerFormLayoutSets`
--

CREATE TABLE IF NOT EXISTS `PageTypeComposerFormLayoutSets` (
  `ptComposerFormLayoutSetID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ptID` int(10) unsigned DEFAULT '0',
  `ptComposerFormLayoutSetName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptComposerFormLayoutSetDescription` text COLLATE utf8_unicode_ci,
  `ptComposerFormLayoutSetDisplayOrder` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`ptComposerFormLayoutSetID`),
  KEY `ptID` (`ptID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `PageTypeComposerFormLayoutSets`
--

INSERT INTO `PageTypeComposerFormLayoutSets` (`ptComposerFormLayoutSetID`, `ptID`, `ptComposerFormLayoutSetName`, `ptComposerFormLayoutSetDescription`, `ptComposerFormLayoutSetDisplayOrder`) VALUES
(1, 5, 'Basics', '', 0),
(2, 5, 'Content', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `PageTypeComposerOutputBlocks`
--

CREATE TABLE IF NOT EXISTS `PageTypeComposerOutputBlocks` (
  `ptComposerOutputBlockID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `arHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cbDisplayOrder` int(10) unsigned DEFAULT '0',
  `ptComposerFormLayoutSetControlID` int(10) unsigned NOT NULL DEFAULT '0',
  `bID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`ptComposerOutputBlockID`),
  KEY `cID` (`cID`),
  KEY `bID` (`bID`,`cID`),
  KEY `ptComposerFormLayoutSetControlID` (`ptComposerFormLayoutSetControlID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=319 ;

--
-- Dumping data for table `PageTypeComposerOutputBlocks`
--

INSERT INTO `PageTypeComposerOutputBlocks` (`ptComposerOutputBlockID`, `cID`, `arHandle`, `cbDisplayOrder`, `ptComposerFormLayoutSetControlID`, `bID`) VALUES
(6, 148, 'Main', 0, 6, 15),
(10, 152, 'Main', 0, 6, 19),
(15, 153, 'Main', 0, 6, 24),
(35, 159, 'Main', 0, 6, 55),
(57, 164, 'Main', 0, 6, 77),
(63, 165, 'Main', 0, 6, 83),
(89, 171, 'Main', 0, 6, 120),
(98, 175, 'Main', 0, 6, 133),
(104, 177, 'Main', 0, 6, 146),
(142, 178, 'Main', 0, 6, 184),
(147, 176, 'Main', 0, 6, 197),
(178, 179, 'Main', 0, 6, 231),
(190, 161, 'Main', 0, 6, 243),
(192, 163, 'Main', 0, 6, 245),
(199, 180, 'Main', 0, 6, 254),
(206, 181, 'Main', 0, 6, 265),
(207, 182, 'Main', 0, 6, 276),
(224, 183, 'Main', 0, 6, 293),
(231, 184, 'Main', 0, 6, 300),
(240, 185, 'Main', 0, 6, 311),
(252, 187, 'Main', 0, 6, 323),
(258, 188, 'Main', 0, 6, 329),
(260, 162, 'Main', 0, 6, 331),
(267, 186, 'Main', 0, 6, 341),
(274, 189, 'Main', 0, 6, 350),
(275, 169, 'Main', 0, 6, 368),
(286, 166, 'Main', 0, 6, 401),
(291, 167, 'Main', 0, 6, 407),
(293, 170, 'Main', 0, 6, 430),
(300, 191, 'Main', 0, 6, 438),
(306, 192, 'Main', 0, 6, 449),
(318, 193, 'Main', 0, 6, 467);

-- --------------------------------------------------------

--
-- Table structure for table `PageTypeComposerOutputControls`
--

CREATE TABLE IF NOT EXISTS `PageTypeComposerOutputControls` (
  `ptComposerOutputControlID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pTemplateID` int(10) unsigned DEFAULT '0',
  `ptID` int(10) unsigned DEFAULT '0',
  `ptComposerFormLayoutSetControlID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`ptComposerOutputControlID`),
  KEY `pTemplateID` (`pTemplateID`,`ptComposerFormLayoutSetControlID`),
  KEY `ptID` (`ptID`),
  KEY `ptComposerFormLayoutSetControlID` (`ptComposerFormLayoutSetControlID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `PageTypeComposerOutputControls`
--

INSERT INTO `PageTypeComposerOutputControls` (`ptComposerOutputControlID`, `pTemplateID`, `ptID`, `ptComposerFormLayoutSetControlID`) VALUES
(1, 5, 5, 6),
(2, 6, 5, 6),
(3, 7, 5, 6),
(4, 8, 5, 6),
(5, 9, 5, 6);

-- --------------------------------------------------------

--
-- Table structure for table `PageTypePageTemplateDefaultPages`
--

CREATE TABLE IF NOT EXISTS `PageTypePageTemplateDefaultPages` (
  `pTemplateID` int(10) unsigned NOT NULL DEFAULT '0',
  `ptID` int(10) unsigned NOT NULL DEFAULT '0',
  `cID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`pTemplateID`,`ptID`),
  KEY `ptID` (`ptID`),
  KEY `cID` (`cID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `PageTypePageTemplateDefaultPages`
--

INSERT INTO `PageTypePageTemplateDefaultPages` (`pTemplateID`, `ptID`, `cID`) VALUES
(5, 5, 142),
(9, 5, 160),
(7, 5, 168);

-- --------------------------------------------------------

--
-- Table structure for table `PageTypePageTemplates`
--

CREATE TABLE IF NOT EXISTS `PageTypePageTemplates` (
  `ptID` int(10) unsigned NOT NULL DEFAULT '0',
  `pTemplateID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ptID`,`pTemplateID`),
  KEY `pTemplateID` (`pTemplateID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `PageTypePermissionAssignments`
--

CREATE TABLE IF NOT EXISTS `PageTypePermissionAssignments` (
  `ptID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkID` int(10) unsigned NOT NULL DEFAULT '0',
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ptID`,`pkID`,`paID`),
  KEY `pkID` (`pkID`),
  KEY `ptID` (`ptID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `PageTypePermissionAssignments`
--

INSERT INTO `PageTypePermissionAssignments` (`ptID`, `pkID`, `paID`) VALUES
(1, 59, 9),
(1, 60, 9),
(1, 61, 9),
(1, 62, 9),
(1, 63, 25),
(2, 59, 9),
(2, 60, 9),
(2, 61, 9),
(2, 62, 9),
(2, 63, 26),
(3, 59, 9),
(3, 60, 9),
(3, 61, 9),
(3, 62, 9),
(3, 63, 27),
(4, 59, 9),
(4, 60, 9),
(4, 61, 9),
(4, 62, 9),
(4, 63, 28),
(5, 59, 9),
(5, 60, 9),
(5, 61, 9),
(5, 62, 9),
(5, 63, 29);

-- --------------------------------------------------------

--
-- Table structure for table `PageTypePublishTargetTypes`
--

CREATE TABLE IF NOT EXISTS `PageTypePublishTargetTypes` (
  `ptPublishTargetTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ptPublishTargetTypeHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptPublishTargetTypeName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ptPublishTargetTypeID`),
  KEY `ptPublishTargetTypeHandle` (`ptPublishTargetTypeHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `PageTypePublishTargetTypes`
--

INSERT INTO `PageTypePublishTargetTypes` (`ptPublishTargetTypeID`, `ptPublishTargetTypeHandle`, `ptPublishTargetTypeName`, `pkgID`) VALUES
(1, 'parent_page', 'Always publish below a certain page', 0),
(2, 'page_type', 'Choose from pages of a certain type', 0),
(3, 'all', 'Choose from all pages when publishing', 0);

-- --------------------------------------------------------

--
-- Table structure for table `PageTypes`
--

CREATE TABLE IF NOT EXISTS `PageTypes` (
  `ptID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ptName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ptHandle` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ptPublishTargetTypeID` int(10) unsigned DEFAULT NULL,
  `ptDefaultPageTemplateID` int(10) unsigned DEFAULT NULL,
  `ptAllowedPageTemplates` varchar(1) COLLATE utf8_unicode_ci DEFAULT 'A',
  `ptIsInternal` tinyint(1) NOT NULL DEFAULT '0',
  `ptIsFrequentlyAdded` tinyint(1) NOT NULL DEFAULT '1',
  `ptDisplayOrder` int(10) unsigned DEFAULT NULL,
  `ptLaunchInComposer` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  `ptPublishTargetObject` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ptID`),
  UNIQUE KEY `ptHandle` (`ptHandle`),
  KEY `pkgID` (`pkgID`,`ptID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `PageTypes`
--

INSERT INTO `PageTypes` (`ptID`, `ptName`, `ptHandle`, `ptPublishTargetTypeID`, `ptDefaultPageTemplateID`, `ptAllowedPageTemplates`, `ptIsInternal`, `ptIsFrequentlyAdded`, `ptDisplayOrder`, `ptLaunchInComposer`, `pkgID`, `ptPublishTargetObject`) VALUES
(1, 'Stack', 'core_stack', NULL, 0, 'A', 1, 0, 0, 0, 0, NULL),
(2, 'Dashboard Primary + Five', 'dashboard_primary_five', NULL, 0, 'A', 1, 0, 1, 0, 0, NULL),
(3, 'Dashboard Header + Four Column', 'dashboard_header_four_col', NULL, 0, 'A', 1, 0, 2, 0, 0, NULL),
(4, 'Dashboard Full', 'dashboard_full', NULL, 0, 'A', 1, 0, 3, 0, 0, NULL),
(5, 'Page', 'page', 3, 5, 'A', 0, 0, 0, 0, 0, 'O:68:"Concrete\\Core\\Page\\Type\\PublishTarget\\Configuration\\AllConfiguration":4:{s:5:"error";s:0:"";s:21:"ptPublishTargetTypeID";s:1:"3";s:25:"ptPublishTargetTypeHandle";s:3:"all";s:9:"pkgHandle";b:0;}');

-- --------------------------------------------------------

--
-- Table structure for table `PageWorkflowProgress`
--

CREATE TABLE IF NOT EXISTS `PageWorkflowProgress` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `wpID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`wpID`),
  KEY `wpID` (`wpID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Pages`
--

CREATE TABLE IF NOT EXISTS `Pages` (
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `ptID` int(10) unsigned NOT NULL DEFAULT '0',
  `cIsTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `uID` int(10) unsigned DEFAULT NULL,
  `cIsCheckedOut` tinyint(1) NOT NULL DEFAULT '0',
  `cCheckedOutUID` int(10) unsigned DEFAULT NULL,
  `cCheckedOutDatetime` datetime DEFAULT NULL,
  `cCheckedOutDatetimeLastEdit` datetime DEFAULT NULL,
  `cOverrideTemplatePermissions` tinyint(1) NOT NULL DEFAULT '1',
  `cInheritPermissionsFromCID` int(10) unsigned NOT NULL DEFAULT '0',
  `cInheritPermissionsFrom` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PARENT',
  `cFilename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cPointerID` int(10) unsigned NOT NULL DEFAULT '0',
  `cPointerExternalLink` longtext COLLATE utf8_unicode_ci,
  `cPointerExternalLinkNewWindow` tinyint(1) NOT NULL DEFAULT '0',
  `cIsActive` tinyint(1) NOT NULL DEFAULT '1',
  `cChildren` int(10) unsigned NOT NULL DEFAULT '0',
  `cDisplayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  `cParentID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  `cDraftTargetParentPageID` int(10) unsigned NOT NULL DEFAULT '0',
  `cCacheFullPageContent` smallint(6) NOT NULL DEFAULT '-1',
  `cCacheFullPageContentOverrideLifetime` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `cCacheFullPageContentLifetimeCustom` int(10) unsigned NOT NULL DEFAULT '0',
  `cIsSystemPage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`ptID`),
  KEY `cParentID` (`cParentID`),
  KEY `cIsActive` (`cID`,`cIsActive`),
  KEY `cCheckedOutUID` (`cCheckedOutUID`),
  KEY `uID` (`uID`,`cPointerID`),
  KEY `cPointerID` (`cPointerID`,`cDisplayOrder`),
  KEY `cIsTemplate` (`cID`,`cIsTemplate`),
  KEY `cIsSystemPage` (`cID`,`cIsSystemPage`),
  KEY `pkgID` (`pkgID`),
  KEY `cParentMaxDisplay` (`cParentID`,`cDisplayOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Pages`
--

INSERT INTO `Pages` (`cID`, `ptID`, `cIsTemplate`, `uID`, `cIsCheckedOut`, `cCheckedOutUID`, `cCheckedOutDatetime`, `cCheckedOutDatetimeLastEdit`, `cOverrideTemplatePermissions`, `cInheritPermissionsFromCID`, `cInheritPermissionsFrom`, `cFilename`, `cPointerID`, `cPointerExternalLink`, `cPointerExternalLinkNewWindow`, `cIsActive`, `cChildren`, `cDisplayOrder`, `cParentID`, `pkgID`, `cDraftTargetParentPageID`, `cCacheFullPageContent`, `cCacheFullPageContentOverrideLifetime`, `cCacheFullPageContentLifetimeCustom`, `cIsSystemPage`) VALUES
(1, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'OVERRIDE', NULL, 0, NULL, 0, 1, 20, 0, 0, 0, 0, -1, '0', 0, 0),
(2, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'OVERRIDE', '/dashboard/view.php', 0, NULL, 0, 1, 13, 0, 0, 0, 0, -1, '0', 0, 1),
(3, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/sitemap/view.php', 0, NULL, 0, 1, 3, 0, 2, 0, 0, -1, '0', 0, 1),
(4, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/sitemap/full.php', 0, NULL, 0, 1, 0, 0, 3, 0, 0, -1, '0', 0, 1),
(5, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/sitemap/explore.php', 0, NULL, 0, 1, 0, 1, 3, 0, 0, -1, '0', 0, 1),
(6, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/sitemap/search.php', 0, NULL, 0, 1, 0, 2, 3, 0, 0, -1, '0', 0, 1),
(7, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/files/view.php', 0, NULL, 0, 1, 4, 1, 2, 0, 0, -1, '0', 0, 1),
(8, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/files/search.php', 0, NULL, 0, 1, 0, 0, 7, 0, 0, -1, '0', 0, 1),
(9, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/files/attributes.php', 0, NULL, 0, 1, 0, 1, 7, 0, 0, -1, '0', 0, 1),
(10, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/files/sets.php', 0, NULL, 0, 1, 0, 2, 7, 0, 0, -1, '0', 0, 1),
(11, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/files/add_set.php', 0, NULL, 0, 1, 0, 3, 7, 0, 0, -1, '0', 0, 1),
(12, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/view.php', 0, NULL, 0, 1, 7, 2, 2, 0, 0, -1, '0', 0, 1),
(13, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/search.php', 0, NULL, 0, 1, 0, 0, 12, 0, 0, -1, '0', 0, 1),
(14, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/groups.php', 0, NULL, 0, 1, 1, 1, 12, 0, 0, -1, '0', 0, 1),
(15, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/attributes.php', 0, NULL, 0, 1, 0, 2, 12, 0, 0, -1, '0', 0, 1),
(16, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/add.php', 0, NULL, 0, 1, 0, 3, 12, 0, 0, -1, '0', 0, 1),
(17, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/add_group.php', 0, NULL, 0, 1, 0, 4, 12, 0, 0, -1, '0', 0, 1),
(18, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/groups/bulkupdate.php', 0, NULL, 0, 1, 0, 0, 14, 0, 0, -1, '0', 0, 1),
(19, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/group_sets.php', 0, NULL, 0, 1, 0, 5, 12, 0, 0, -1, '0', 0, 1),
(20, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/points/view.php', 0, NULL, 0, 1, 2, 6, 12, 0, 0, -1, '0', 0, 1),
(21, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/points/assign.php', 0, NULL, 0, 1, 0, 0, 20, 0, 0, -1, '0', 0, 1),
(22, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/users/points/actions.php', 0, NULL, 0, 1, 0, 1, 20, 0, 0, -1, '0', 0, 1),
(23, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/reports.php', 0, NULL, 0, 1, 3, 3, 2, 0, 0, -1, '0', 0, 1),
(24, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/reports/forms.php', 0, NULL, 0, 1, 0, 0, 23, 0, 0, -1, '0', 0, 1),
(25, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/reports/surveys.php', 0, NULL, 0, 1, 0, 1, 23, 0, 0, -1, '0', 0, 1),
(26, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/reports/logs.php', 0, NULL, 0, 1, 0, 2, 23, 0, 0, -1, '0', 0, 1),
(27, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/view.php', 0, NULL, 0, 1, 6, 4, 2, 0, 0, -1, '0', 0, 1),
(28, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/themes/view.php', 0, NULL, 0, 1, 1, 0, 27, 0, 0, -1, '0', 0, 1),
(29, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/themes/inspect.php', 0, NULL, 0, 1, 0, 0, 28, 0, 0, -1, '0', 0, 1),
(30, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/view.php', 0, NULL, 0, 1, 6, 1, 27, 0, 0, -1, '0', 0, 1),
(31, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/organize.php', 0, NULL, 0, 1, 0, 0, 30, 0, 0, -1, '0', 0, 1),
(32, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/add.php', 0, NULL, 0, 1, 0, 1, 30, 0, 0, -1, '0', 0, 1),
(33, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/form.php', 0, NULL, 0, 1, 0, 2, 30, 0, 0, -1, '0', 0, 1),
(34, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/output.php', 0, NULL, 0, 1, 0, 3, 30, 0, 0, -1, '0', 0, 1),
(35, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/attributes.php', 0, NULL, 0, 1, 0, 4, 30, 0, 0, -1, '0', 0, 1),
(36, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/types/permissions.php', 0, NULL, 0, 1, 0, 5, 30, 0, 0, -1, '0', 0, 1),
(37, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/templates/view.php', 0, NULL, 0, 1, 1, 2, 27, 0, 0, -1, '0', 0, 1),
(38, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/templates/add.php', 0, NULL, 0, 1, 0, 0, 37, 0, 0, -1, '0', 0, 1),
(39, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/attributes.php', 0, NULL, 0, 1, 0, 3, 27, 0, 0, -1, '0', 0, 1),
(40, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/single.php', 0, NULL, 0, 1, 0, 4, 27, 0, 0, -1, '0', 0, 1),
(41, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/pages/feeds.php', 0, NULL, 0, 1, 0, 5, 27, 0, 0, -1, '0', 0, 1),
(42, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/conversations/view.php', 0, NULL, 0, 1, 1, 5, 2, 0, 0, -1, '0', 0, 1),
(43, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/conversations/messages.php', 0, NULL, 0, 1, 0, 0, 42, 0, 0, -1, '0', 0, 1),
(44, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/workflow/view.php', 0, NULL, 0, 1, 2, 6, 2, 0, 0, -1, '0', 0, 1),
(45, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/workflow/workflows.php', 0, NULL, 0, 1, 0, 0, 44, 0, 0, -1, '0', 0, 1),
(46, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/workflow/me.php', 0, NULL, 0, 1, 0, 1, 44, 0, 0, -1, '0', 0, 1),
(47, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/blocks/view.php', 0, NULL, 0, 1, 3, 7, 2, 0, 0, -1, '0', 0, 1),
(48, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/blocks/stacks/view.php', 0, NULL, 0, 1, 1, 0, 47, 0, 0, -1, '0', 0, 1),
(49, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/blocks/permissions.php', 0, NULL, 0, 1, 0, 1, 47, 0, 0, -1, '0', 0, 1),
(50, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/blocks/stacks/list/view.php', 0, NULL, 0, 1, 0, 0, 48, 0, 0, -1, '0', 0, 1),
(51, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/blocks/types/view.php', 0, NULL, 0, 1, 0, 2, 47, 0, 0, -1, '0', 0, 1),
(52, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/extend/view.php', 0, NULL, 0, 1, 5, 8, 2, 0, 0, -1, '0', 0, 1),
(53, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/news.php', 0, NULL, 0, 1, 0, 9, 2, 0, 0, -1, '0', 0, 1),
(54, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/extend/install.php', 0, NULL, 0, 1, 0, 0, 52, 0, 0, -1, '0', 0, 1),
(55, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/extend/update.php', 0, NULL, 0, 1, 0, 1, 52, 0, 0, -1, '0', 0, 1),
(56, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/extend/connect.php', 0, NULL, 0, 1, 0, 2, 52, 0, 0, -1, '0', 0, 1),
(57, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/extend/themes.php', 0, NULL, 0, 1, 0, 3, 52, 0, 0, -1, '0', 0, 1),
(58, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/extend/addons.php', 0, NULL, 0, 1, 0, 4, 52, 0, 0, -1, '0', 0, 1),
(59, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/view.php', 0, NULL, 0, 1, 12, 10, 2, 0, 0, -1, '0', 0, 1),
(60, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/view.php', 0, NULL, 0, 1, 7, 0, 59, 0, 0, -1, '0', 0, 1),
(61, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/name.php', 0, NULL, 0, 1, 0, 0, 60, 0, 0, -1, '0', 0, 1),
(62, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/accessibility.php', 0, NULL, 0, 1, 0, 1, 60, 0, 0, -1, '0', 0, 1),
(63, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/social.php', 0, NULL, 0, 1, 0, 2, 60, 0, 0, -1, '0', 0, 1),
(64, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/icons.php', 0, NULL, 0, 1, 0, 3, 60, 0, 0, -1, '0', 0, 1),
(65, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/editor.php', 0, NULL, 0, 1, 0, 4, 60, 0, 0, -1, '0', 0, 1),
(66, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/multilingual/view.php', 0, NULL, 0, 1, 0, 5, 60, 0, 0, -1, '0', 0, 1),
(67, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/basics/timezone.php', 0, NULL, 0, 1, 0, 6, 60, 0, 0, -1, '0', 0, 1),
(68, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/multilingual/view.php', 0, NULL, 0, 1, 3, 1, 59, 0, 0, -1, '0', 0, 1),
(69, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/multilingual/setup.php', 0, NULL, 0, 1, 0, 0, 68, 0, 0, -1, '0', 0, 1),
(70, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/multilingual/page_report.php', 0, NULL, 0, 1, 0, 1, 68, 0, 0, -1, '0', 0, 1),
(71, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/multilingual/translate_interface.php', 0, NULL, 0, 1, 0, 2, 68, 0, 0, -1, '0', 0, 1),
(72, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/seo/view.php', 0, NULL, 0, 1, 5, 2, 59, 0, 0, -1, '0', 0, 1),
(73, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/seo/urls.php', 0, NULL, 0, 1, 0, 0, 72, 0, 0, -1, '0', 0, 1),
(74, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/seo/bulk.php', 0, NULL, 0, 1, 0, 1, 72, 0, 0, -1, '0', 0, 1),
(75, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/seo/codes.php', 0, NULL, 0, 1, 0, 2, 72, 0, 0, -1, '0', 0, 1),
(76, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/seo/excluded.php', 0, NULL, 0, 1, 0, 3, 72, 0, 0, -1, '0', 0, 1),
(77, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/seo/searchindex.php', 0, NULL, 0, 1, 0, 4, 72, 0, 0, -1, '0', 0, 1),
(78, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/files/view.php', 0, NULL, 0, 1, 4, 3, 59, 0, 0, -1, '0', 0, 1),
(79, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/files/permissions.php', 0, NULL, 0, 1, 0, 0, 78, 0, 0, -1, '0', 0, 1),
(80, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/files/filetypes.php', 0, NULL, 0, 1, 0, 1, 78, 0, 0, -1, '0', 0, 1),
(81, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/files/thumbnails.php', 0, NULL, 0, 1, 0, 2, 78, 0, 0, -1, '0', 0, 1),
(82, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/files/storage.php', 0, NULL, 0, 1, 0, 3, 78, 0, 0, -1, '0', 0, 1),
(83, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/optimization/view.php', 0, NULL, 0, 1, 4, 4, 59, 0, 0, -1, '0', 0, 1),
(84, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/optimization/cache.php', 0, NULL, 0, 1, 0, 0, 83, 0, 0, -1, '0', 0, 1),
(85, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/optimization/clearcache.php', 0, NULL, 0, 1, 0, 1, 83, 0, 0, -1, '0', 0, 1),
(86, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/optimization/jobs.php', 0, NULL, 0, 1, 0, 2, 83, 0, 0, -1, '0', 0, 1),
(87, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/optimization/query_log.php', 0, NULL, 0, 1, 0, 3, 83, 0, 0, -1, '0', 0, 1),
(88, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/view.php', 0, NULL, 0, 1, 8, 5, 59, 0, 0, -1, '0', 0, 1),
(89, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/site.php', 0, NULL, 0, 1, 0, 0, 88, 0, 0, -1, '0', 0, 1),
(90, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/tasks.php', 0, NULL, 0, 1, 0, 1, 88, 0, 0, -1, '0', 0, 1),
(91, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/users.php', 0, NULL, 0, 1, 0, 2, 88, 0, 0, -1, '0', 0, 1),
(92, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/advanced.php', 0, NULL, 0, 1, 0, 3, 88, 0, 0, -1, '0', 0, 1),
(93, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/blacklist.php', 0, NULL, 0, 1, 0, 4, 88, 0, 0, -1, '0', 0, 1),
(94, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/captcha.php', 0, NULL, 0, 1, 0, 5, 88, 0, 0, -1, '0', 0, 1),
(95, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/antispam.php', 0, NULL, 0, 1, 0, 6, 88, 0, 0, -1, '0', 0, 1),
(96, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/permissions/maintenance.php', 0, NULL, 0, 1, 0, 7, 88, 0, 0, -1, '0', 0, 1),
(97, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/registration/view.php', 0, NULL, 0, 1, 4, 6, 59, 0, 0, -1, '0', 0, 1),
(98, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/registration/postlogin.php', 0, NULL, 0, 1, 0, 0, 97, 0, 0, -1, '0', 0, 1),
(99, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/registration/profiles.php', 0, NULL, 0, 1, 0, 1, 97, 0, 0, -1, '0', 0, 1),
(100, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/registration/open.php', 0, NULL, 0, 1, 0, 2, 97, 0, 0, -1, '0', 0, 1),
(101, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/registration/authentication.php', 0, NULL, 0, 1, 0, 3, 97, 0, 0, -1, '0', 0, 1),
(102, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/mail/view.php', 0, NULL, 0, 1, 2, 7, 59, 0, 0, -1, '0', 0, 1),
(103, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/mail/method.php', 0, NULL, 0, 1, 1, 0, 102, 0, 0, -1, '0', 0, 1),
(104, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/mail/method/test.php', 0, NULL, 0, 1, 0, 0, 103, 0, 0, -1, '0', 0, 1),
(105, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/mail/importers.php', 0, NULL, 0, 1, 0, 1, 102, 0, 0, -1, '0', 0, 1),
(106, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/conversations/view.php', 0, NULL, 0, 1, 4, 8, 59, 0, 0, -1, '0', 0, 1),
(107, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/conversations/settings.php', 0, NULL, 0, 1, 0, 0, 106, 0, 0, -1, '0', 0, 1),
(108, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/conversations/points.php', 0, NULL, 0, 1, 0, 1, 106, 0, 0, -1, '0', 0, 1),
(109, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/conversations/bannedwords.php', 0, NULL, 0, 1, 0, 2, 106, 0, 0, -1, '0', 0, 1),
(110, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/conversations/permissions.php', 0, NULL, 0, 1, 0, 3, 106, 0, 0, -1, '0', 0, 1),
(111, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/attributes/view.php', 0, NULL, 0, 1, 3, 9, 59, 0, 0, -1, '0', 0, 1),
(112, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/attributes/sets.php', 0, NULL, 0, 1, 0, 0, 111, 0, 0, -1, '0', 0, 1),
(113, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/attributes/types.php', 0, NULL, 0, 1, 0, 1, 111, 0, 0, -1, '0', 0, 1),
(114, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/attributes/topics/view.php', 0, NULL, 0, 1, 1, 2, 111, 0, 0, -1, '0', 0, 1),
(115, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/attributes/topics/add.php', 0, NULL, 0, 1, 0, 0, 114, 0, 0, -1, '0', 0, 1),
(116, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/environment/view.php', 0, NULL, 0, 1, 5, 10, 59, 0, 0, -1, '0', 0, 1),
(117, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/environment/info.php', 0, NULL, 0, 1, 0, 0, 116, 0, 0, -1, '0', 0, 1),
(118, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/environment/debug.php', 0, NULL, 0, 1, 0, 1, 116, 0, 0, -1, '0', 0, 1),
(119, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/environment/logging.php', 0, NULL, 0, 1, 0, 2, 116, 0, 0, -1, '0', 0, 1),
(120, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/environment/proxy.php', 0, NULL, 0, 1, 0, 3, 116, 0, 0, -1, '0', 0, 1),
(121, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/backup/view.php', 0, NULL, 0, 1, 2, 11, 59, 0, 0, -1, '0', 0, 1),
(122, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/backup/backup.php', 0, NULL, 0, 1, 0, 0, 121, 0, 0, -1, '0', 0, 1),
(123, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/backup/update.php', 0, NULL, 0, 1, 0, 1, 121, 0, 0, -1, '0', 0, 1),
(124, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', NULL, 0, NULL, 0, 1, 0, 11, 2, 0, 0, -1, '0', 0, 1),
(125, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', NULL, 0, NULL, 0, 1, 0, 12, 2, 0, 0, -1, '0', 0, 1),
(126, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/!drafts/view.php', 0, NULL, 0, 1, 2, 0, 0, 0, 0, -1, '0', 0, 1),
(127, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/!trash/view.php', 0, NULL, 0, 1, 3, 0, 0, 0, 0, -1, '0', 0, 1),
(128, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/!stacks/view.php', 0, NULL, 0, 1, 16, 0, 0, 0, 0, -1, '0', 0, 1),
(129, 0, 0, 1, 0, NULL, NULL, NULL, 1, 129, 'OVERRIDE', '/login.php', 0, NULL, 0, 1, 0, 0, 0, 0, 0, -1, '0', 0, 1),
(130, 0, 0, 1, 0, NULL, NULL, NULL, 1, 130, 'OVERRIDE', '/register.php', 0, NULL, 0, 1, 0, 0, 0, 0, 0, -1, '0', 0, 1),
(131, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/account/view.php', 0, NULL, 0, 1, 3, 0, 0, 0, 0, -1, '0', 0, 1),
(132, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/account/edit_profile.php', 0, NULL, 0, 1, 0, 0, 131, 0, 0, -1, '0', 0, 1),
(133, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/account/avatar.php', 0, NULL, 0, 1, 0, 1, 131, 0, 0, -1, '0', 0, 1),
(134, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/account/messages/view.php', 0, NULL, 0, 1, 1, 2, 131, 0, 0, -1, '0', 0, 1),
(135, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/account/messages/inbox.php', 0, NULL, 0, 1, 0, 0, 134, 0, 0, -1, '0', 0, 1),
(136, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/members/view.php', 0, NULL, 0, 1, 2, 0, 1, 0, 0, -1, '0', 0, 1),
(137, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/members/profile.php', 0, NULL, 0, 1, 0, 0, 136, 0, 0, -1, '0', 0, 1),
(138, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/members/directory.php', 0, NULL, 0, 1, 0, 1, 136, 0, 0, -1, '0', 0, 1),
(139, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/page_not_found.php', 0, NULL, 0, 1, 0, 1, 0, 0, 0, -1, '0', 0, 1),
(140, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/page_forbidden.php', 0, NULL, 0, 1, 0, 1, 0, 0, 0, -1, '0', 0, 1),
(141, 0, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', '/download_file.php', 0, NULL, 0, 1, 0, 1, 1, 0, 0, -1, '0', 0, 1),
(142, 5, 1, NULL, 0, NULL, NULL, NULL, 1, 142, 'OVERRIDE', NULL, 0, NULL, 0, 1, 0, 0, 0, 0, 0, -1, '0', 0, 0),
(143, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 0, 128, 0, 0, -1, '0', 0, 1),
(144, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 1, 128, 0, 0, -1, '0', 0, 1),
(145, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 2, 128, 0, 0, -1, '0', 0, 1),
(146, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 3, 128, 0, 0, -1, '0', 0, 1),
(147, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 4, 128, 0, 0, -1, '0', 0, 1),
(148, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 2, 0, 1, 0, 1, -1, '0', 0, 0),
(149, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 5, 128, 0, 0, -1, '0', 0, 1),
(150, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 6, 128, 0, 0, -1, '0', 0, 1),
(151, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 7, 128, 0, 0, -1, '0', 0, 1),
(152, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 0, 1, 4, 127, 0, 1, -1, '0', 0, 1),
(153, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 0, 0, 1, 152, 0, 152, -1, '0', 0, 1),
(154, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 8, 128, 0, 0, -1, '0', 0, 1),
(155, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 9, 128, 0, 0, -1, '0', 0, 1),
(156, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 10, 128, 0, 0, -1, '0', 0, 1),
(157, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 11, 128, 0, 0, -1, '0', 0, 1),
(158, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 12, 128, 0, 0, -1, '0', 0, 1),
(159, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 3, 1, 1, 0, 1, -1, '0', 0, 0),
(160, 5, 1, NULL, 0, NULL, NULL, NULL, 1, 160, 'OVERRIDE', NULL, 0, NULL, 0, 1, 0, 0, 0, 0, 0, -1, '0', 0, 0),
(161, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 4, 2, 1, 0, 1, -1, '0', 0, 0),
(162, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 4, 1, 0, 1, -1, '0', 0, 0),
(163, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 3, 1, 0, 1, -1, '0', 0, 0),
(164, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 5, 1, 0, 1, -1, '0', 0, 0),
(165, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 5, 6, 1, 0, 1, -1, '0', 0, 0),
(166, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 1, 7, 1, 0, 1, -1, '0', 0, 0),
(167, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 1, 8, 1, 0, 1, -1, '0', 0, 0),
(168, 5, 1, NULL, 0, NULL, NULL, NULL, 1, 168, 'OVERRIDE', NULL, 0, NULL, 0, 1, 0, 0, 0, 0, 0, -1, '0', 0, 0),
(169, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 3, 148, 0, 148, -1, '0', 0, 0),
(170, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 2, 148, 0, 148, -1, '0', 0, 0),
(171, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 2, 159, 0, 159, -1, '0', 0, 0),
(172, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 13, 128, 0, 0, -1, '0', 0, 1),
(173, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 14, 128, 0, 0, -1, '0', 0, 1),
(174, 1, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 15, 128, 0, 0, -1, '0', 0, 1),
(175, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 3, 159, 0, 159, -1, '0', 0, 0),
(176, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 0, 0, 6, 127, 0, 159, -1, '0', 0, 1),
(177, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 0, 0, 0, 126, 0, 0, -1, '0', 0, 1),
(178, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 5, 159, 0, 159, -1, '0', 0, 0),
(179, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 3, 161, 0, 0, -1, '0', 0, 0),
(180, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 4, 161, 0, 161, -1, '0', 0, 0),
(181, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 5, 161, 0, 161, -1, '0', 0, 0),
(182, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 0, 0, 1, 126, 0, 0, -1, '0', 0, 1),
(184, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 3, 167, 0, 167, -1, '0', 0, 0),
(185, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 0, 165, 0, 165, -1, '0', 0, 0),
(186, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 0, 0, 10, 127, 0, 165, -1, '0', 0, 1),
(187, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 1, 165, 0, 165, -1, '0', 0, 0),
(188, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 3, 165, 0, 165, -1, '0', 0, 0),
(189, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 2, 165, 0, 165, -1, '0', 0, 0),
(190, 0, 0, 1, 0, NULL, NULL, NULL, 1, 2, 'PARENT', '/dashboard/system/environment/entities.php', 0, NULL, 0, 1, 0, 4, 116, 0, 0, -1, '0', 0, 1),
(191, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 6, 161, 0, 161, -1, '0', 0, 0),
(192, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 4, 165, 0, 165, -1, '0', 0, 0),
(193, 5, 0, 1, 0, NULL, NULL, NULL, 1, 1, 'PARENT', NULL, 0, NULL, 0, 1, 0, 3, 166, 0, 166, -1, '0', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `PermissionAccess`
--

CREATE TABLE IF NOT EXISTS `PermissionAccess` (
  `paID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paIsInUse` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`paID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=76 ;

--
-- Dumping data for table `PermissionAccess`
--

INSERT INTO `PermissionAccess` (`paID`, `paIsInUse`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 0),
(75, 0);

-- --------------------------------------------------------

--
-- Table structure for table `PermissionAccessEntities`
--

CREATE TABLE IF NOT EXISTS `PermissionAccessEntities` (
  `peID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `petID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`peID`),
  KEY `petID` (`petID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `PermissionAccessEntities`
--

INSERT INTO `PermissionAccessEntities` (`peID`, `petID`) VALUES
(1, 1),
(3, 1),
(5, 1),
(2, 5),
(6, 6),
(4, 7);

-- --------------------------------------------------------

--
-- Table structure for table `PermissionAccessEntityGroupSets`
--

CREATE TABLE IF NOT EXISTS `PermissionAccessEntityGroupSets` (
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `gsID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`peID`,`gsID`),
  KEY `gsID` (`gsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `PermissionAccessEntityGroups`
--

CREATE TABLE IF NOT EXISTS `PermissionAccessEntityGroups` (
  `pegID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `gID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`pegID`),
  KEY `peID` (`peID`),
  KEY `gID` (`gID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `PermissionAccessEntityGroups`
--

INSERT INTO `PermissionAccessEntityGroups` (`pegID`, `peID`, `gID`) VALUES
(1, 1, 3),
(2, 3, 1),
(3, 5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `PermissionAccessEntityTypeCategories`
--

CREATE TABLE IF NOT EXISTS `PermissionAccessEntityTypeCategories` (
  `petID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkCategoryID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`petID`,`pkCategoryID`),
  KEY `pkCategoryID` (`pkCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `PermissionAccessEntityTypeCategories`
--

INSERT INTO `PermissionAccessEntityTypeCategories` (`petID`, `pkCategoryID`) VALUES
(1, 1),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(2, 1),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(3, 1),
(3, 4),
(3, 5),
(3, 6),
(3, 7),
(3, 8),
(3, 9),
(3, 10),
(3, 11),
(3, 12),
(3, 13),
(3, 14),
(3, 15),
(3, 16),
(3, 17),
(3, 18),
(3, 19),
(3, 20),
(4, 1),
(4, 4),
(4, 5),
(4, 6),
(4, 7),
(4, 8),
(4, 9),
(4, 10),
(4, 11),
(4, 12),
(4, 13),
(4, 14),
(4, 15),
(4, 16),
(4, 17),
(4, 18),
(4, 19),
(4, 20),
(4, 21),
(5, 1),
(5, 14),
(6, 5),
(6, 6),
(7, 19),
(7, 20);

-- --------------------------------------------------------

--
-- Table structure for table `PermissionAccessEntityTypes`
--

CREATE TABLE IF NOT EXISTS `PermissionAccessEntityTypes` (
  `petID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `petHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `petName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`petID`),
  UNIQUE KEY `petHandle` (`petHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `PermissionAccessEntityTypes`
--

INSERT INTO `PermissionAccessEntityTypes` (`petID`, `petHandle`, `petName`, `pkgID`) VALUES
(1, 'group', 'Group', 0),
(2, 'user', 'User', 0),
(3, 'group_set', 'Group Set', 0),
(4, 'group_combination', 'Group Combination', 0),
(5, 'page_owner', 'Page Owner', 0),
(6, 'file_uploader', 'File Uploader', 0),
(7, 'conversation_message_author', 'Message Author', 0);

-- --------------------------------------------------------

--
-- Table structure for table `PermissionAccessEntityUsers`
--

CREATE TABLE IF NOT EXISTS `PermissionAccessEntityUsers` (
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `uID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`peID`,`uID`),
  KEY `uID` (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `PermissionAccessList`
--

CREATE TABLE IF NOT EXISTS `PermissionAccessList` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `pdID` int(10) unsigned NOT NULL DEFAULT '0',
  `accessType` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`paID`,`peID`),
  KEY `accessType` (`accessType`),
  KEY `peID` (`peID`),
  KEY `peID_accessType` (`peID`,`accessType`),
  KEY `pdID` (`pdID`),
  KEY `permissionAccessDuration` (`paID`,`pdID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `PermissionAccessList`
--

INSERT INTO `PermissionAccessList` (`paID`, `peID`, `pdID`, `accessType`) VALUES
(1, 1, 0, 10),
(2, 1, 0, 10),
(3, 1, 0, 10),
(4, 1, 0, 10),
(5, 1, 0, 10),
(6, 1, 0, 10),
(7, 1, 0, 10),
(8, 1, 0, 10),
(9, 1, 0, 10),
(10, 1, 0, 10),
(11, 1, 0, 10),
(12, 1, 0, 10),
(13, 1, 0, 10),
(14, 1, 0, 10),
(15, 1, 0, 10),
(16, 1, 0, 10),
(17, 1, 0, 10),
(18, 1, 0, 10),
(19, 1, 0, 10),
(20, 1, 0, 10),
(21, 1, 0, 10),
(22, 1, 0, 10),
(23, 1, 0, 10),
(24, 1, 0, 10),
(25, 2, 0, 10),
(26, 2, 0, 10),
(27, 2, 0, 10),
(28, 2, 0, 10),
(29, 2, 0, 10),
(30, 3, 0, 10),
(31, 1, 0, 10),
(31, 3, 0, 10),
(32, 1, 0, 10),
(33, 1, 0, 10),
(34, 1, 0, 10),
(35, 1, 0, 10),
(36, 1, 0, 10),
(37, 1, 0, 10),
(38, 1, 0, 10),
(39, 1, 0, 10),
(40, 3, 0, 10),
(41, 1, 0, 10),
(42, 1, 0, 10),
(43, 1, 0, 10),
(44, 1, 0, 10),
(45, 1, 0, 10),
(46, 1, 0, 10),
(47, 1, 0, 10),
(48, 1, 0, 10),
(49, 1, 0, 10),
(50, 1, 0, 10),
(51, 1, 0, 10),
(52, 1, 0, 10),
(53, 1, 0, 10),
(54, 1, 0, 10),
(55, 1, 0, 10),
(56, 1, 0, 10),
(57, 3, 0, 10),
(58, 3, 0, 10),
(59, 1, 0, 10),
(60, 1, 0, 10),
(61, 1, 0, 10),
(62, 1, 0, 10),
(63, 1, 0, 10),
(64, 1, 0, 10),
(65, 3, 0, 10),
(66, 3, 0, 10),
(67, 1, 0, 10),
(67, 4, 0, 10),
(68, 1, 0, 10),
(68, 4, 0, 10),
(69, 1, 0, 10),
(69, 5, 0, 10),
(70, 1, 0, 10),
(71, 1, 0, 10),
(72, 1, 0, 10),
(73, 1, 0, 10),
(74, 3, 0, 10),
(75, 1, 0, 10),
(75, 4, 0, 10);

-- --------------------------------------------------------

--
-- Table structure for table `PermissionAccessWorkflows`
--

CREATE TABLE IF NOT EXISTS `PermissionAccessWorkflows` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `wfID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`paID`,`wfID`),
  KEY `wfID` (`wfID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `PermissionAssignments`
--

CREATE TABLE IF NOT EXISTS `PermissionAssignments` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`paID`,`pkID`),
  KEY `pkID` (`pkID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `PermissionAssignments`
--

INSERT INTO `PermissionAssignments` (`paID`, `pkID`) VALUES
(1, 19),
(2, 20),
(3, 74),
(4, 75),
(5, 76),
(6, 78),
(7, 79),
(8, 80),
(9, 86),
(10, 87),
(11, 89),
(12, 90),
(13, 91),
(14, 92),
(15, 93),
(16, 94),
(17, 95),
(18, 96),
(19, 97),
(20, 98),
(21, 99),
(22, 100),
(23, 101),
(24, 102),
(73, 103);

-- --------------------------------------------------------

--
-- Table structure for table `PermissionDurationObjects`
--

CREATE TABLE IF NOT EXISTS `PermissionDurationObjects` (
  `pdID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pdObject` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pdID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `PermissionKeyCategories`
--

CREATE TABLE IF NOT EXISTS `PermissionKeyCategories` (
  `pkCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pkCategoryHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`pkCategoryID`),
  UNIQUE KEY `pkCategoryHandle` (`pkCategoryHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Dumping data for table `PermissionKeyCategories`
--

INSERT INTO `PermissionKeyCategories` (`pkCategoryID`, `pkCategoryHandle`, `pkgID`) VALUES
(1, 'page', NULL),
(2, 'single_page', NULL),
(3, 'stack', NULL),
(4, 'user', NULL),
(5, 'file_set', NULL),
(6, 'file', NULL),
(7, 'area', NULL),
(8, 'block_type', NULL),
(9, 'block', NULL),
(10, 'admin', NULL),
(11, 'sitemap', NULL),
(12, 'marketplace_newsflow', NULL),
(13, 'basic_workflow', NULL),
(14, 'page_type', NULL),
(15, 'gathering', NULL),
(16, 'group_tree_node', NULL),
(17, 'topic_category_tree_node', NULL),
(18, 'topic_tree_node', NULL),
(19, 'conversation', NULL),
(20, 'conversation_message', NULL),
(21, 'multilingual_section', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `PermissionKeys`
--

CREATE TABLE IF NOT EXISTS `PermissionKeys` (
  `pkID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pkHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkCanTriggerWorkflow` tinyint(1) NOT NULL DEFAULT '0',
  `pkHasCustomClass` tinyint(1) NOT NULL DEFAULT '0',
  `pkDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pkCategoryID` int(10) unsigned DEFAULT NULL,
  `pkgID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`pkID`),
  UNIQUE KEY `akHandle` (`pkHandle`),
  KEY `pkCategoryID` (`pkCategoryID`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=104 ;

--
-- Dumping data for table `PermissionKeys`
--

INSERT INTO `PermissionKeys` (`pkID`, `pkHandle`, `pkName`, `pkCanTriggerWorkflow`, `pkHasCustomClass`, `pkDescription`, `pkCategoryID`, `pkgID`) VALUES
(1, 'view_page', 'View', 0, 0, 'Can see a page exists and read its content.', 1, 0),
(2, 'view_page_versions', 'View Versions', 0, 0, 'Can view the page versions dialog and read past versions of a page.', 1, 0),
(3, 'view_page_in_sitemap', 'View Page in Sitemap', 0, 0, 'Controls whether a user can see a page in the sitemap or intelligent search.', 1, 0),
(4, 'preview_page_as_user', 'Preview Page As User', 0, 0, 'Ability to see what this page will look like at a specific time in the future as a specific user.', 1, 0),
(5, 'edit_page_properties', 'Edit Properties', 0, 1, 'Ability to change anything in the Page Properties menu.', 1, 0),
(6, 'edit_page_contents', 'Edit Contents', 0, 0, 'Ability to make edits to at least some of the content in the page. You can lock down different block areas and specific blocks by clicking Permissions on them as well. ', 1, 0),
(7, 'edit_page_speed_settings', 'Edit Speed Settings', 0, 0, 'Ability to change caching settings.', 1, 0),
(8, 'edit_page_theme', 'Change Theme', 0, 1, 'Ability to change just the theme for this page.', 1, 0),
(9, 'edit_page_template', 'Change Page Template', 0, 0, 'Ability to change just the page template for this page.', 1, 0),
(10, 'edit_page_page_type', 'Edit Page Type', 0, 0, 'Change the type of an existing page.', 1, 0),
(11, 'edit_page_permissions', 'Edit Permissions', 1, 0, 'Ability to change permissions for this page. Warning: by granting this a user could give themselves more access.', 1, 0),
(12, 'delete_page', 'Delete', 1, 0, 'Ability to move this page to the site''s Trash.', 1, 0),
(13, 'delete_page_versions', 'Delete Versions', 1, 0, 'Ability to remove old versions of this page.', 1, 0),
(14, 'approve_page_versions', 'Approve Changes', 1, 0, 'Can publish an unapproved version of the page.', 1, 0),
(15, 'add_subpage', 'Add Sub-Page', 0, 1, 'Can add a page beneath the current page.', 1, 0),
(16, 'move_or_copy_page', 'Move or Copy Page', 1, 0, 'Can move or copy this page to another location.', 1, 0),
(17, 'schedule_page_contents_guest_access', 'Schedule Guest Access', 0, 0, 'Can control scheduled guest access to this page.', 1, 0),
(18, 'edit_page_multilingual_settings', 'Edit Multilingual Settings', 0, 0, 'Controls whether a user can see the multilingual settings menu, re-map a page or set a page as ignored in multilingual settings.', 1, 0),
(19, 'add_block', 'Add Block', 0, 1, 'Can add a block to any area on the site. If someone is added here they can add blocks to any area (unless that area has permissions that override these global permissions.)', 8, 0),
(20, 'add_stack', 'Add Stack', 0, 0, 'Can add a stack or block from a stack to any area on the site. If someone is added here they can add stacks to any area (unless that area has permissions that override these global permissions.)', 8, 0),
(21, 'view_area', 'View Area', 0, 0, 'Can view the area and its contents.', 7, 0),
(22, 'edit_area_contents', 'Edit Area Contents', 0, 0, 'Can edit blocks within this area.', 7, 0),
(23, 'add_block_to_area', 'Add Block to Area', 0, 1, 'Can add blocks to this area. This setting overrides the global Add Block permission for this area.', 7, 0),
(24, 'add_stack_to_area', 'Add Stack to Area', 0, 0, 'Can add stacks to this area. This setting overrides the global Add Stack permission for this area.', 7, 0),
(25, 'add_layout_to_area', 'Add Layouts to Area', 0, 0, 'Controls whether users get the ability to add layouts to a particular area.', 7, 0),
(26, 'edit_area_design', 'Edit Area Design', 0, 0, 'Controls whether users see design controls and can modify an area''s custom CSS.', 7, 0),
(27, 'edit_area_permissions', 'Edit Area Permissions', 0, 0, 'Controls whether users can access the permissions on an area. Custom area permissions could override those of the page.', 7, 0),
(28, 'delete_area_contents', 'Delete Area Contents', 0, 0, 'Controls whether users can delete blocks from this area.', 7, 0),
(29, 'schedule_area_contents_guest_access', 'Schedule Guest Access', 0, 0, 'Controls whether users can schedule guest access permissions on blocks in this area. Guest Access is a shortcut for granting permissions just to the Guest Group.', 7, 0),
(30, 'view_block', 'View Block', 0, 0, 'Controls whether users can view this block in the page.', 9, 0),
(31, 'edit_block', 'Edit Block', 0, 0, 'Controls whether users can edit this block. This overrides any area or page permissions.', 9, 0),
(32, 'edit_block_custom_template', 'Change Custom Template', 0, 0, 'Controls whether users can change the custom template on this block. This overrides any area or page permissions.', 9, 0),
(33, 'edit_block_cache_settings', 'Edit Cache Settings', 0, 0, 'Controls whether users can change the block cache settings for this block instance.', 9, 0),
(34, 'edit_block_name', 'Edit Name', 0, 0, 'Controls whether users can change the block''s name (rarely used.)', 9, 0),
(35, 'delete_block', 'Delete Block', 0, 0, 'Controls whether users can delete this block. This overrides any area or page permissions.', 9, 0),
(36, 'edit_block_design', 'Edit Design', 0, 0, 'Controls whether users can set custom design properties or CSS on this block.', 9, 0),
(37, 'edit_block_permissions', 'Edit Permissions', 0, 0, 'Controls whether users can change permissions on this block, potentially granting themselves or others greater access.', 9, 0),
(38, 'schedule_guest_access', 'Schedule Guest Access', 0, 0, 'Controls whether users can schedule guest access permissions on this block. Guest Access is a shortcut for granting permissions just to the Guest Group.', 9, 0),
(39, 'view_file_set_file', 'View Files', 0, 0, 'Can view and download files in the site.', 5, 0),
(40, 'search_file_set', 'Search Files in File Manager', 0, 0, 'Can access the file manager', 5, 0),
(41, 'edit_file_set_file_properties', 'Edit File Properties', 0, 0, 'Can edit a file''s properties.', 5, 0),
(42, 'edit_file_set_file_contents', 'Edit File Contents', 0, 0, 'Can edit or replace files in set.', 5, 0),
(43, 'copy_file_set_files', 'Copy File', 0, 0, 'Can copy files in file set.', 5, 0),
(44, 'edit_file_set_permissions', 'Edit File Access', 0, 0, 'Can edit access to file sets.', 5, 0),
(45, 'delete_file_set', 'Delete File Set', 0, 0, 'Can delete file set.', 5, 0),
(46, 'delete_file_set_files', 'Delete File', 0, 0, 'Can delete files in set.', 5, 0),
(47, 'add_file', 'Add File', 0, 1, 'Can add files to set.', 5, 0),
(48, 'view_file', 'View Files', 0, 0, 'Can view and download files.', 6, 0),
(49, 'view_file_in_file_manager', 'View File in File Manager', 0, 0, 'Can access the File Manager.', 6, 0),
(50, 'edit_file_properties', 'Edit File Properties', 0, 0, 'Can edit a file''s properties.', 6, 0),
(51, 'edit_file_contents', 'Edit File Contents', 0, 0, 'Can edit or replace files.', 6, 0),
(52, 'copy_file', 'Copy File', 0, 0, 'Can copy file.', 6, 0),
(53, 'edit_file_permissions', 'Edit File Access', 0, 0, 'Can edit access to file.', 6, 0),
(54, 'delete_file', 'Delete File', 0, 0, 'Can delete file.', 6, 0),
(55, 'approve_basic_workflow_action', 'Approve or Deny', 0, 0, 'Grant ability to approve workflow.', 13, 0),
(56, 'notify_on_basic_workflow_entry', 'Notify on Entry', 0, 0, 'Notify approvers that a change has entered the workflow.', 13, 0),
(57, 'notify_on_basic_workflow_approve', 'Notify on Approve', 0, 0, 'Notify approvers that a change has been approved.', 13, 0),
(58, 'notify_on_basic_workflow_deny', 'Notify on Deny', 0, 0, 'Notify approvers that a change has been denied.', 13, 0),
(59, 'add_page_type', 'Add Pages of This Type', 0, 0, '', 14, 0),
(60, 'edit_page_type', 'Edit Page Type', 0, 0, '', 14, 0),
(61, 'delete_page_type', 'Delete Page Type', 0, 0, '', 14, 0),
(62, 'edit_page_type_permissions', 'Edit Page Type Permissions', 0, 0, '', 14, 0),
(63, 'edit_page_type_drafts', 'Edit Page Type Drafts', 0, 0, '', 14, 0),
(64, 'view_topic_tree_node', 'View Topic Tree Node', 0, 0, '', 18, 0),
(65, 'view_topic_category_tree_node', 'View Topic Category Tree Node', 0, 0, '', 17, 0),
(66, 'add_conversation_message', 'Add Message to Conversation', 0, 1, '', 19, 0),
(67, 'add_conversation_message_attachments', 'Add Message Attachments', 0, 0, '', 19, 0),
(68, 'edit_conversation_permissions', 'Edit Conversation Permissions', 0, 0, '', 19, 0),
(69, 'delete_conversation_message', 'Delete Message', 0, 0, '', 19, 0),
(70, 'edit_conversation_message', 'Edit Message', 0, 0, '', 19, 0),
(71, 'rate_conversation_message', 'Rate Message', 0, 0, '', 19, 0),
(72, 'flag_conversation_message', 'Flag Message', 0, 0, '', 19, 0),
(73, 'approve_conversation_message', 'Approve Message', 0, 0, '', 19, 0),
(74, 'edit_user_properties', 'Edit User Details', 0, 1, NULL, 4, 0),
(75, 'view_user_attributes', 'View User Attributes', 0, 1, NULL, 4, 0),
(76, 'activate_user', 'Activate/Deactivate User', 0, 0, NULL, 4, 0),
(77, 'sudo', 'Sign in as User', 0, 0, NULL, 4, 0),
(78, 'upgrade', 'Upgrade concrete5', 0, 0, NULL, 10, 0),
(79, 'access_group_search', 'Access Group Search', 0, 0, NULL, 4, 0),
(80, 'delete_user', 'Delete User', 0, 0, NULL, 4, 0),
(81, 'search_users_in_group', 'Search User Group', 0, 0, NULL, 16, 0),
(82, 'edit_group', 'Edit Group', 0, 0, NULL, 16, 0),
(83, 'assign_group', 'Assign Group', 0, 0, NULL, 16, 0),
(84, 'add_sub_group', 'Add Child Group', 0, 0, NULL, 16, 0),
(85, 'edit_group_permissions', 'Edit Group Permissions', 0, 0, NULL, 16, 0),
(86, 'access_page_type_permissions', 'Access Page Type Permissions', 0, 0, NULL, 10, 0),
(87, 'backup', 'Perform Backups', 0, 0, NULL, 10, 0),
(88, 'access_task_permissions', 'Access Task Permissions', 0, 0, NULL, 10, 0),
(89, 'access_sitemap', 'Access Sitemap', 0, 0, NULL, 11, 0),
(90, 'access_page_defaults', 'Access Page Type Defaults', 0, 0, NULL, 10, 0),
(91, 'customize_themes', 'Customize Themes', 0, 0, NULL, 10, 0),
(92, 'manage_layout_presets', 'Manage Layout Presets', 0, 0, NULL, 10, 0),
(93, 'empty_trash', 'Empty Trash', 0, 0, NULL, 10, 0),
(94, 'add_topic_tree', 'Add Topic Tree', 0, 0, NULL, 10, 0),
(95, 'remove_topic_tree', 'Remove Topic Tree', 0, 0, NULL, 10, 0),
(96, 'uninstall_packages', 'Uninstall Packages', 0, 0, NULL, 12, 0),
(97, 'install_packages', 'Install Packages', 0, 0, NULL, 12, 0),
(98, 'view_newsflow', 'View Newsflow', 0, 0, NULL, 12, 0),
(99, 'access_user_search_export', 'Export Site Users', 0, 0, 'Controls whether a user can export site users or not', 4, 0),
(100, 'access_user_search', 'Access User Search', 0, 0, 'Controls whether a user can view the search user interface.', 4, 0),
(101, 'edit_gatherings', 'Edit Gatherings', 0, 0, 'Can edit the footprint and items in all gatherings.', 10, 0),
(102, 'edit_gathering_items', 'Edit Gathering Items', 0, 0, '', 15, 0),
(103, 'view_in_maintenance_mode', 'View Site in Maintenance Mode', 0, 0, 'Controls whether a user can access the website when its under maintenance.', 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `PileContents`
--

CREATE TABLE IF NOT EXISTS `PileContents` (
  `pcID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pID` int(10) unsigned NOT NULL DEFAULT '0',
  `itemID` int(10) unsigned NOT NULL DEFAULT '0',
  `itemType` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `displayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`pcID`),
  KEY `pID` (`pID`,`displayOrder`),
  KEY `itemID` (`itemID`),
  KEY `itemType` (`itemType`,`itemID`,`pID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `PileContents`
--

INSERT INTO `PileContents` (`pcID`, `pID`, `itemID`, `itemType`, `quantity`, `timestamp`, `displayOrder`) VALUES
(1, 1, 145, 'BLOCK', 1, '2015-05-20 06:19:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Piles`
--

CREATE TABLE IF NOT EXISTS `Piles` (
  `pID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uID` int(10) unsigned DEFAULT NULL,
  `isDefault` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pID`),
  KEY `uID` (`uID`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Piles`
--

INSERT INTO `Piles` (`pID`, `uID`, `isDefault`, `timestamp`, `name`, `state`) VALUES
(1, 1, 1, '2015-04-30 21:34:31', NULL, 'READY');

-- --------------------------------------------------------

--
-- Table structure for table `QueueMessages`
--

CREATE TABLE IF NOT EXISTS `QueueMessages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `queue_id` int(10) unsigned NOT NULL,
  `handle` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `md5` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `timeout` decimal(14,0) DEFAULT NULL,
  `created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`message_id`),
  UNIQUE KEY `message_handle` (`handle`),
  KEY `message_queueid` (`queue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `QueuePageDuplicationRelations`
--

CREATE TABLE IF NOT EXISTS `QueuePageDuplicationRelations` (
  `queue_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  `originalCID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cID`,`originalCID`),
  KEY `originalCID` (`originalCID`,`queue_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Queues`
--

CREATE TABLE IF NOT EXISTS `Queues` (
  `queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `queue_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `timeout` int(10) unsigned NOT NULL DEFAULT '30',
  PRIMARY KEY (`queue_id`),
  KEY `queue_name` (`queue_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Sessions`
--

CREATE TABLE IF NOT EXISTS `Sessions` (
  `sessionID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sessionValue` text COLLATE utf8_unicode_ci NOT NULL,
  `sessionTime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sessionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `SignupRequests`
--

CREATE TABLE IF NOT EXISTS `SignupRequests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ipFrom` tinyblob,
  `date_access` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ipFrom` (`ipFrom`(32))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SocialLinks`
--

CREATE TABLE IF NOT EXISTS `SocialLinks` (
  `slID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ssHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`slID`),
  UNIQUE KEY `ssHandle` (`ssHandle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Stacks`
--

CREATE TABLE IF NOT EXISTS `Stacks` (
  `stID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stType` int(10) unsigned NOT NULL DEFAULT '0',
  `cID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stID`),
  KEY `stType` (`stType`),
  KEY `stName` (`stName`),
  KEY `cID` (`cID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `Stacks`
--

INSERT INTO `Stacks` (`stID`, `stName`, `stType`, `cID`) VALUES
(1, 'Header Site Title', 20, 143),
(2, 'Header Navigation', 20, 144),
(3, 'Footer Legal', 20, 145),
(4, 'Footer Navigation', 20, 146),
(5, 'Footer Contact', 20, 147),
(6, 'Header Search', 20, 149),
(7, 'Footer Site Title', 20, 150),
(8, 'Footer Social', 20, 151),
(9, 'FooterInfoText', 20, 154),
(10, 'Twitter Feed', 20, 155),
(11, 'Footer kolonne to', 20, 156),
(12, 'Footer kolonne et', 20, 157),
(13, 'navigation', 20, 158),
(14, 'Sidebar productgroup', 20, 172),
(15, 'Breadcrumbs container', 20, 173),
(16, 'Header title', 20, 174);

-- --------------------------------------------------------

--
-- Table structure for table `StyleCustomizerCustomCssRecords`
--

CREATE TABLE IF NOT EXISTS `StyleCustomizerCustomCssRecords` (
  `sccRecordID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`sccRecordID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `StyleCustomizerInlineStylePresets`
--

CREATE TABLE IF NOT EXISTS `StyleCustomizerInlineStylePresets` (
  `pssPresetID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pssPresetName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `issID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`pssPresetID`),
  KEY `issID` (`issID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `StyleCustomizerInlineStyleSets`
--

CREATE TABLE IF NOT EXISTS `StyleCustomizerInlineStyleSets` (
  `issID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `backgroundColor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `backgroundImageFileID` int(11) DEFAULT NULL,
  `backgroundRepeat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borderWidth` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borderColor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borderStyle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borderRadius` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `baseFontSize` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alignment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `textColor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkColor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paddingTop` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paddingBottom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paddingLeft` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paddingRight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marginTop` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marginBottom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marginLeft` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marginRight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rotate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boxShadowHorizontal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boxShadowVertical` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boxShadowBlur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boxShadowSpread` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boxShadowColor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customClass` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`issID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `StyleCustomizerInlineStyleSets`
--

INSERT INTO `StyleCustomizerInlineStyleSets` (`issID`, `backgroundColor`, `backgroundImageFileID`, `backgroundRepeat`, `borderWidth`, `borderColor`, `borderStyle`, `borderRadius`, `baseFontSize`, `alignment`, `textColor`, `linkColor`, `paddingTop`, `paddingBottom`, `paddingLeft`, `paddingRight`, `marginTop`, `marginBottom`, `marginLeft`, `marginRight`, `rotate`, `boxShadowHorizontal`, `boxShadowVertical`, `boxShadowBlur`, `boxShadowSpread`, `boxShadowColor`, `customClass`) VALUES
(1, '', 0, 'no-repeat', NULL, '', 'none', NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', ''),
(2, '', 0, 'no-repeat', NULL, '', 'none', NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', ''),
(3, '', 0, 'no-repeat', NULL, '', 'none', NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', ''),
(4, '', 0, 'no-repeat', NULL, '', 'none', NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', ''),
(5, '', 0, 'no-repeat', NULL, '', 'none', NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', 'staff'),
(6, '', 0, 'no-repeat', NULL, '', 'none', NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', ''),
(7, '', 0, 'no-repeat', NULL, '', 'none', NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', ''),
(8, '', 0, 'no-repeat', NULL, '', '', NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', ''),
(9, '', 0, 'no-repeat', NULL, '', '', NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'contactForm-container');

-- --------------------------------------------------------

--
-- Table structure for table `StyleCustomizerValueLists`
--

CREATE TABLE IF NOT EXISTS `StyleCustomizerValueLists` (
  `scvlID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`scvlID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `StyleCustomizerValues`
--

CREATE TABLE IF NOT EXISTS `StyleCustomizerValues` (
  `scvID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `scvlID` int(10) unsigned DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`scvID`),
  KEY `scvlID` (`scvlID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SystemAntispamLibraries`
--

CREATE TABLE IF NOT EXISTS `SystemAntispamLibraries` (
  `saslHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `saslName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `saslIsActive` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`saslHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `SystemCaptchaLibraries`
--

CREATE TABLE IF NOT EXISTS `SystemCaptchaLibraries` (
  `sclHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `sclName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sclIsActive` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sclHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `SystemCaptchaLibraries`
--

INSERT INTO `SystemCaptchaLibraries` (`sclHandle`, `sclName`, `sclIsActive`, `pkgID`) VALUES
('securimage', 'SecurImage (Default)', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `SystemContentEditorSnippets`
--

CREATE TABLE IF NOT EXISTS `SystemContentEditorSnippets` (
  `scsHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `scsName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scsIsActive` tinyint(1) NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`scsHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `SystemContentEditorSnippets`
--

INSERT INTO `SystemContentEditorSnippets` (`scsHandle`, `scsName`, `scsIsActive`, `pkgID`) VALUES
('page_name', 'Page Name', 1, 0),
('user_name', 'User Name', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `SystemDatabaseMigrations`
--

CREATE TABLE IF NOT EXISTS `SystemDatabaseMigrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `SystemDatabaseMigrations`
--

INSERT INTO `SystemDatabaseMigrations` (`version`) VALUES
('20140919000000'),
('20140930000000'),
('20141017000000'),
('20141024000000'),
('20141113000000'),
('20141219000000'),
('20150109000000'),
('20150504000000'),
('5702'),
('5704'),
('571'),
('572'),
('5721'),
('573');

-- --------------------------------------------------------

--
-- Table structure for table `SystemDatabaseQueryLog`
--

CREATE TABLE IF NOT EXISTS `SystemDatabaseQueryLog` (
  `query` text COLLATE utf8_unicode_ci,
  `params` text COLLATE utf8_unicode_ci,
  `executionMS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `SystemImageEditorComponents`
--

CREATE TABLE IF NOT EXISTS `SystemImageEditorComponents` (
  `scsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `scsHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `scsName` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `scsDisplayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`scsID`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SystemImageEditorControlSets`
--

CREATE TABLE IF NOT EXISTS `SystemImageEditorControlSets` (
  `scsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `scsHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `scsName` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `scsDisplayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`scsID`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `SystemImageEditorControlSets`
--

INSERT INTO `SystemImageEditorControlSets` (`scsID`, `scsHandle`, `scsName`, `scsDisplayOrder`, `pkgID`) VALUES
(1, 'position', 'Position', 0, 0),
(2, 'filter', 'Filter', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `SystemImageEditorFilters`
--

CREATE TABLE IF NOT EXISTS `SystemImageEditorFilters` (
  `scsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `scsHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `scsName` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `scsDisplayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`scsID`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `SystemImageEditorFilters`
--

INSERT INTO `SystemImageEditorFilters` (`scsID`, `scsHandle`, `scsName`, `scsDisplayOrder`, `pkgID`) VALUES
(1, 'none', 'None', 0, 0),
(2, 'grayscale', 'Grayscale', 0, 0),
(3, 'sepia', 'Sepia', 0, 0),
(4, 'gaussian_blur', 'Blur', 0, 0),
(5, 'vignette', 'Vignette', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `SystemImageEditorShapes`
--

CREATE TABLE IF NOT EXISTS `SystemImageEditorShapes` (
  `scsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `scsHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `scsName` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `scsDisplayOrder` int(10) unsigned NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`scsID`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TopicTrees`
--

CREATE TABLE IF NOT EXISTS `TopicTrees` (
  `treeID` int(10) unsigned NOT NULL DEFAULT '0',
  `topicTreeName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`treeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `TreeCategoryNodes`
--

CREATE TABLE IF NOT EXISTS `TreeCategoryNodes` (
  `treeNodeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `treeNodeCategoryName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`treeNodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TreeGroupNodes`
--

CREATE TABLE IF NOT EXISTS `TreeGroupNodes` (
  `treeNodeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`treeNodeID`),
  KEY `gID` (`gID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `TreeGroupNodes`
--

INSERT INTO `TreeGroupNodes` (`treeNodeID`, `gID`) VALUES
(2, 1),
(3, 2),
(4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `TreeNodePermissionAssignments`
--

CREATE TABLE IF NOT EXISTS `TreeNodePermissionAssignments` (
  `treeNodeID` int(10) unsigned NOT NULL DEFAULT '0',
  `pkID` int(10) unsigned NOT NULL DEFAULT '0',
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`treeNodeID`,`pkID`,`paID`),
  KEY `pkID` (`pkID`),
  KEY `paID` (`paID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `TreeNodePermissionAssignments`
--

INSERT INTO `TreeNodePermissionAssignments` (`treeNodeID`, `pkID`, `paID`) VALUES
(1, 81, 60),
(1, 82, 61),
(1, 83, 62),
(1, 84, 63),
(1, 85, 64);

-- --------------------------------------------------------

--
-- Table structure for table `TreeNodeTypes`
--

CREATE TABLE IF NOT EXISTS `TreeNodeTypes` (
  `treeNodeTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `treeNodeTypeHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `pkgID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`treeNodeTypeID`),
  UNIQUE KEY `treeNodeTypeHandle` (`treeNodeTypeHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `TreeNodeTypes`
--

INSERT INTO `TreeNodeTypes` (`treeNodeTypeID`, `treeNodeTypeHandle`, `pkgID`) VALUES
(1, 'group', 0),
(2, 'topic_category', 0),
(3, 'topic', 0);

-- --------------------------------------------------------

--
-- Table structure for table `TreeNodes`
--

CREATE TABLE IF NOT EXISTS `TreeNodes` (
  `treeNodeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `treeNodeTypeID` int(10) unsigned DEFAULT '0',
  `treeID` int(10) unsigned DEFAULT '0',
  `treeNodeParentID` int(10) unsigned DEFAULT '0',
  `treeNodeDisplayOrder` int(10) unsigned DEFAULT '0',
  `treeNodeOverridePermissions` tinyint(1) DEFAULT '0',
  `inheritPermissionsFromTreeNodeID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`treeNodeID`),
  KEY `treeNodeParentID` (`treeNodeParentID`),
  KEY `treeNodeTypeID` (`treeNodeTypeID`),
  KEY `treeID` (`treeID`),
  KEY `inheritPermissionsFromTreeNodeID` (`inheritPermissionsFromTreeNodeID`,`treeNodeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `TreeNodes`
--

INSERT INTO `TreeNodes` (`treeNodeID`, `treeNodeTypeID`, `treeID`, `treeNodeParentID`, `treeNodeDisplayOrder`, `treeNodeOverridePermissions`, `inheritPermissionsFromTreeNodeID`) VALUES
(1, 1, 1, 0, 0, 1, 1),
(2, 1, 1, 1, 0, 0, 1),
(3, 1, 1, 1, 1, 0, 1),
(4, 1, 1, 1, 2, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `TreeTopicNodes`
--

CREATE TABLE IF NOT EXISTS `TreeTopicNodes` (
  `treeNodeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `treeNodeTopicName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`treeNodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TreeTypes`
--

CREATE TABLE IF NOT EXISTS `TreeTypes` (
  `treeTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `treeTypeHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `pkgID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`treeTypeID`),
  UNIQUE KEY `treeTypeHandle` (`treeTypeHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `TreeTypes`
--

INSERT INTO `TreeTypes` (`treeTypeID`, `treeTypeHandle`, `pkgID`) VALUES
(1, 'group', 0),
(2, 'topic', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Trees`
--

CREATE TABLE IF NOT EXISTS `Trees` (
  `treeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `treeTypeID` int(10) unsigned DEFAULT '0',
  `treeDateAdded` datetime DEFAULT NULL,
  `rootTreeNodeID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`treeID`),
  KEY `treeTypeID` (`treeTypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Trees`
--

INSERT INTO `Trees` (`treeID`, `treeTypeID`, `treeDateAdded`, `rootTreeNodeID`) VALUES
(1, 1, '2015-04-30 16:39:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `UserAttributeKeys`
--

CREATE TABLE IF NOT EXISTS `UserAttributeKeys` (
  `akID` int(10) unsigned NOT NULL,
  `uakProfileDisplay` tinyint(1) NOT NULL DEFAULT '0',
  `uakMemberListDisplay` tinyint(1) NOT NULL DEFAULT '0',
  `uakProfileEdit` tinyint(1) NOT NULL DEFAULT '1',
  `uakProfileEditRequired` tinyint(1) NOT NULL DEFAULT '0',
  `uakRegisterEdit` tinyint(1) NOT NULL DEFAULT '0',
  `uakRegisterEditRequired` tinyint(1) NOT NULL DEFAULT '0',
  `displayOrder` int(10) unsigned DEFAULT '0',
  `uakIsActive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`akID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `UserAttributeKeys`
--

INSERT INTO `UserAttributeKeys` (`akID`, `uakProfileDisplay`, `uakMemberListDisplay`, `uakProfileEdit`, `uakProfileEditRequired`, `uakRegisterEdit`, `uakRegisterEditRequired`, `displayOrder`, `uakIsActive`) VALUES
(12, 0, 0, 1, 0, 1, 0, 1, 1),
(13, 0, 0, 1, 0, 1, 0, 2, 1),
(16, 0, 0, 0, 0, 0, 0, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `UserAttributeValues`
--

CREATE TABLE IF NOT EXISTS `UserAttributeValues` (
  `uID` int(10) unsigned NOT NULL DEFAULT '0',
  `akID` int(10) unsigned NOT NULL DEFAULT '0',
  `avID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`uID`,`akID`),
  KEY `akID` (`akID`),
  KEY `avID` (`avID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `UserBannedIPs`
--

CREATE TABLE IF NOT EXISTS `UserBannedIPs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ipFrom` tinyblob,
  `ipTo` tinyblob,
  `banCode` tinyint(1) NOT NULL DEFAULT '1',
  `expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isManual` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ipFrom` (`ipFrom`(32),`ipTo`(32))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `UserGroups`
--

CREATE TABLE IF NOT EXISTS `UserGroups` (
  `uID` int(10) unsigned NOT NULL DEFAULT '0',
  `gID` int(10) unsigned NOT NULL DEFAULT '0',
  `ugEntered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`uID`,`gID`),
  KEY `uID` (`uID`),
  KEY `gID` (`gID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `UserPermissionEditPropertyAccessList`
--

CREATE TABLE IF NOT EXISTS `UserPermissionEditPropertyAccessList` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `uName` tinyint(1) DEFAULT '0',
  `uEmail` tinyint(1) DEFAULT '0',
  `uPassword` tinyint(1) DEFAULT '0',
  `uAvatar` tinyint(1) DEFAULT '0',
  `uTimezone` tinyint(1) DEFAULT '0',
  `uDefaultLanguage` tinyint(1) DEFAULT '0',
  `attributePermission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`paID`,`peID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `UserPermissionEditPropertyAttributeAccessListCustom`
--

CREATE TABLE IF NOT EXISTS `UserPermissionEditPropertyAttributeAccessListCustom` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `akID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`paID`,`peID`,`akID`),
  KEY `peID` (`peID`),
  KEY `akID` (`akID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `UserPermissionViewAttributeAccessList`
--

CREATE TABLE IF NOT EXISTS `UserPermissionViewAttributeAccessList` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `permission` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`paID`,`peID`),
  KEY `peID` (`peID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `UserPermissionViewAttributeAccessListCustom`
--

CREATE TABLE IF NOT EXISTS `UserPermissionViewAttributeAccessListCustom` (
  `paID` int(10) unsigned NOT NULL DEFAULT '0',
  `peID` int(10) unsigned NOT NULL DEFAULT '0',
  `akID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`paID`,`peID`,`akID`),
  KEY `peID` (`peID`),
  KEY `akID` (`akID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `UserPointActions`
--

CREATE TABLE IF NOT EXISTS `UserPointActions` (
  `upaID` int(11) NOT NULL AUTO_INCREMENT,
  `upaHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upaDefaultPoints` int(11) NOT NULL DEFAULT '0',
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  `upaHasCustomClass` tinyint(1) NOT NULL DEFAULT '0',
  `upaIsActive` tinyint(1) NOT NULL DEFAULT '1',
  `gBadgeID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`upaID`),
  UNIQUE KEY `upaHandle` (`upaHandle`),
  KEY `pkgID` (`pkgID`),
  KEY `gBBadgeID` (`gBadgeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `UserPointActions`
--

INSERT INTO `UserPointActions` (`upaID`, `upaHandle`, `upaName`, `upaDefaultPoints`, `pkgID`, `upaHasCustomClass`, `upaIsActive`, `gBadgeID`) VALUES
(1, 'won_badge', 'Won a Badge', 5, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `UserPointHistory`
--

CREATE TABLE IF NOT EXISTS `UserPointHistory` (
  `upID` int(11) NOT NULL AUTO_INCREMENT,
  `upuID` int(11) NOT NULL DEFAULT '0',
  `upaID` int(11) DEFAULT '0',
  `upPoints` int(11) DEFAULT '0',
  `object` longtext COLLATE utf8_unicode_ci,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`upID`),
  KEY `upuID` (`upuID`),
  KEY `upaID` (`upaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `UserPrivateMessages`
--

CREATE TABLE IF NOT EXISTS `UserPrivateMessages` (
  `msgID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uAuthorID` int(10) unsigned NOT NULL DEFAULT '0',
  `msgDateCreated` datetime NOT NULL,
  `msgSubject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `msgBody` text COLLATE utf8_unicode_ci,
  `uToID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`msgID`),
  KEY `uAuthorID` (`uAuthorID`,`msgDateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `UserPrivateMessagesTo`
--

CREATE TABLE IF NOT EXISTS `UserPrivateMessagesTo` (
  `msgID` int(10) unsigned NOT NULL DEFAULT '0',
  `uID` int(10) unsigned NOT NULL DEFAULT '0',
  `uAuthorID` int(10) unsigned NOT NULL DEFAULT '0',
  `msgMailboxID` int(11) NOT NULL,
  `msgIsNew` tinyint(1) NOT NULL DEFAULT '0',
  `msgIsUnread` tinyint(1) NOT NULL DEFAULT '0',
  `msgIsReplied` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`msgID`,`uID`,`uAuthorID`,`msgMailboxID`),
  KEY `uID` (`uID`),
  KEY `uAuthorID` (`uAuthorID`),
  KEY `msgFolderID` (`msgMailboxID`),
  KEY `msgIsNew` (`msgIsNew`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `UserSearchIndexAttributes`
--

CREATE TABLE IF NOT EXISTS `UserSearchIndexAttributes` (
  `uID` int(10) unsigned NOT NULL DEFAULT '0',
  `ak_profile_private_messages_enabled` tinyint(1) DEFAULT '0',
  `ak_profile_private_messages_notification_enabled` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `UserValidationHashes`
--

CREATE TABLE IF NOT EXISTS `UserValidationHashes` (
  `uvhID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uID` int(10) unsigned DEFAULT NULL,
  `uHash` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(5) unsigned NOT NULL DEFAULT '0',
  `uDateGenerated` int(10) unsigned NOT NULL DEFAULT '0',
  `uDateRedeemed` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uvhID`),
  KEY `uID` (`uID`,`type`),
  KEY `uHash` (`uHash`,`type`),
  KEY `uDateGenerated` (`uDateGenerated`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `uID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `uEmail` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `uPassword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uIsActive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `uIsValidated` tinyint(1) NOT NULL DEFAULT '-1',
  `uIsFullRecord` tinyint(1) NOT NULL DEFAULT '1',
  `uDateAdded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uLastPasswordChange` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uHasAvatar` tinyint(1) NOT NULL DEFAULT '0',
  `uLastOnline` int(10) unsigned NOT NULL DEFAULT '0',
  `uLastLogin` int(10) unsigned NOT NULL DEFAULT '0',
  `uLastIP` tinyblob,
  `uPreviousLogin` int(10) unsigned DEFAULT '0',
  `uNumLogins` int(10) unsigned NOT NULL DEFAULT '0',
  `uLastAuthTypeID` int(10) unsigned NOT NULL DEFAULT '0',
  `uTimezone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uDefaultLanguage` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`uID`),
  UNIQUE KEY `uName` (`uName`),
  KEY `uEmail` (`uEmail`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`uID`, `uName`, `uEmail`, `uPassword`, `uIsActive`, `uIsValidated`, `uIsFullRecord`, `uDateAdded`, `uLastPasswordChange`, `uHasAvatar`, `uLastOnline`, `uLastLogin`, `uLastIP`, `uPreviousLogin`, `uNumLogins`, `uLastAuthTypeID`, `uTimezone`, `uDefaultLanguage`) VALUES
(1, 'admin', 'cm@geekmedia.dk', '$2a$12$uDHIF5VfUQumlh4un1EXNOZAyJ2N2rvZbVSvmSOP2CuwfYYIKJyuW', '1', -1, 1, '2015-04-30 16:39:08', '2015-04-30 16:39:08', 0, 1432724321, 1432724320, 0x3265316631303063, 1432724281, 54, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `WorkflowProgress`
--

CREATE TABLE IF NOT EXISTS `WorkflowProgress` (
  `wpID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wpCategoryID` int(10) unsigned DEFAULT NULL,
  `wfID` int(10) unsigned NOT NULL DEFAULT '0',
  `wpApproved` tinyint(1) NOT NULL DEFAULT '0',
  `wpDateAdded` datetime DEFAULT NULL,
  `wpDateLastAction` datetime DEFAULT NULL,
  `wpCurrentStatus` int(11) NOT NULL DEFAULT '0',
  `wrID` int(11) NOT NULL DEFAULT '0',
  `wpIsCompleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`wpID`),
  KEY `wpCategoryID` (`wpCategoryID`),
  KEY `wfID` (`wfID`),
  KEY `wrID` (`wrID`,`wpID`,`wpIsCompleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=169 ;

-- --------------------------------------------------------

--
-- Table structure for table `WorkflowProgressCategories`
--

CREATE TABLE IF NOT EXISTS `WorkflowProgressCategories` (
  `wpCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wpCategoryHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`wpCategoryID`),
  UNIQUE KEY `wpCategoryHandle` (`wpCategoryHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `WorkflowProgressCategories`
--

INSERT INTO `WorkflowProgressCategories` (`wpCategoryID`, `wpCategoryHandle`, `pkgID`) VALUES
(1, 'page', NULL),
(2, 'file', NULL),
(3, 'user', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `WorkflowProgressHistory`
--

CREATE TABLE IF NOT EXISTS `WorkflowProgressHistory` (
  `wphID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wpID` int(10) unsigned NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `object` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`wphID`),
  KEY `wpID` (`wpID`,`timestamp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=189 ;

--
-- Dumping data for table `WorkflowProgressHistory`
--

INSERT INTO `WorkflowProgressHistory` (`wphID`, `wpID`, `timestamp`, `object`) VALUES
(1, 1, '2015-04-30 20:45:51', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"148";s:4:"cvID";s:1:"1";s:4:"wrID";s:1:"1";}'),
(2, 2, '2015-04-30 20:47:57', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"152";s:4:"cvID";s:1:"1";s:4:"wrID";s:1:"2";}'),
(3, 3, '2015-04-30 20:48:32', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"153";s:4:"cvID";s:1:"1";s:4:"wrID";s:1:"3";}'),
(4, 4, '2015-04-30 21:27:06', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"2";s:4:"wrID";s:1:"4";}'),
(5, 5, '2015-04-30 21:38:55', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"3";s:4:"wrID";s:1:"5";}'),
(6, 6, '2015-04-30 22:22:40', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"4";s:4:"wrID";s:1:"6";}'),
(7, 1, '2015-05-07 23:28:03', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"5";s:4:"wrID";s:1:"1";}'),
(8, 2, '2015-05-07 23:32:12', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"6";s:4:"wrID";s:1:"2";}'),
(9, 3, '2015-05-07 23:35:16', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"7";s:4:"wrID";s:1:"3";}'),
(10, 4, '2015-05-08 00:36:36', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"8";s:4:"wrID";s:1:"4";}'),
(11, 5, '2015-05-08 00:53:55', 'O:48:"Concrete\\Core\\Workflow\\Request\\DeletePageRequest":7:{s:14:"\0*\0wrStatusNum";i:100;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"12";s:3:"cID";s:3:"152";s:4:"wrID";s:1:"5";}'),
(12, 6, '2015-05-08 00:58:29', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"159";s:4:"cvID";s:1:"1";s:4:"wrID";s:1:"6";}'),
(13, 7, '2015-05-08 00:59:47', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"161";s:4:"cvID";s:1:"1";s:4:"wrID";s:1:"7";}'),
(14, 8, '2015-05-08 01:01:03', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"162";s:4:"cvID";s:1:"1";s:4:"wrID";s:1:"8";}'),
(15, 9, '2015-05-08 01:01:53', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"163";s:4:"cvID";s:1:"1";s:4:"wrID";s:1:"9";}'),
(16, 10, '2015-05-08 01:02:53', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"164";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"10";}'),
(17, 11, '2015-05-08 01:02:59', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"164";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"11";}'),
(18, 12, '2015-05-08 01:03:48', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"165";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"12";}'),
(19, 13, '2015-05-08 01:04:32', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"166";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"13";}'),
(20, 14, '2015-05-08 01:05:18', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"14";}'),
(21, 1, '2015-05-12 07:52:11', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:1:"2";s:4:"wrID";s:1:"1";}'),
(22, 2, '2015-05-12 08:00:54', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:1:"3";s:4:"wrID";s:1:"2";}'),
(23, 3, '2015-05-12 08:03:41', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:1:"4";s:4:"wrID";s:1:"3";}'),
(24, 4, '2015-05-12 08:15:03', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:1:"5";s:4:"wrID";s:1:"4";}'),
(25, 5, '2015-05-12 08:53:02', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:1:"6";s:4:"wrID";s:1:"5";}'),
(26, 6, '2015-05-12 09:01:07', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:1:"7";s:4:"wrID";s:1:"6";}'),
(27, 7, '2015-05-12 09:22:39', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"169";s:4:"cvID";s:1:"1";s:4:"wrID";s:1:"7";}'),
(28, 8, '2015-05-12 09:23:37', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:1:"9";s:4:"wrID";s:1:"8";}'),
(29, 9, '2015-05-12 09:54:09', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"170";s:4:"cvID";s:1:"1";s:4:"wrID";s:1:"9";}'),
(30, 10, '2015-05-12 10:12:11', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"171";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"10";}'),
(31, 11, '2015-05-12 10:14:51', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"171";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"11";}'),
(32, 12, '2015-05-12 10:19:21', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"171";s:4:"cvID";s:1:"3";s:4:"wrID";s:2:"12";}'),
(33, 13, '2015-05-12 10:36:27', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"171";s:4:"cvID";s:1:"4";s:4:"wrID";s:2:"13";}'),
(34, 14, '2015-05-12 13:25:54', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"175";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"14";}'),
(35, 15, '2015-05-12 13:26:42', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"176";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"15";}'),
(36, 16, '2015-05-13 08:31:42', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"159";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"16";}'),
(37, 17, '2015-05-13 08:36:39', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"178";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"17";}'),
(38, 18, '2015-05-13 08:38:46', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:1:"8";s:4:"wrID";s:2:"18";}'),
(39, 19, '2015-05-13 08:42:26', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"178";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"19";}'),
(40, 20, '2015-05-13 08:43:26', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"171";s:4:"cvID";s:1:"6";s:4:"wrID";s:2:"20";}'),
(41, 21, '2015-05-13 08:47:55', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"171";s:4:"cvID";s:1:"7";s:4:"wrID";s:2:"21";}'),
(42, 22, '2015-05-13 08:52:40', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"175";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"22";}'),
(43, 23, '2015-05-13 08:52:53', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"169";s:4:"cvID";s:1:"3";s:4:"wrID";s:2:"23";}'),
(44, 24, '2015-05-13 08:54:32', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:1:"9";s:4:"wrID";s:2:"24";}'),
(45, 25, '2015-05-13 08:58:29', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"171";s:4:"cvID";s:1:"8";s:4:"wrID";s:2:"25";}'),
(46, 26, '2015-05-13 09:00:31', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"159";s:4:"cvID";s:1:"3";s:4:"wrID";s:2:"26";}'),
(47, 27, '2015-05-13 09:03:06', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"175";s:4:"cvID";s:1:"3";s:4:"wrID";s:2:"27";}'),
(48, 28, '2015-05-13 09:04:25', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"175";s:4:"cvID";s:1:"4";s:4:"wrID";s:2:"28";}'),
(49, 29, '2015-05-13 09:09:30', 'O:48:"Concrete\\Core\\Workflow\\Request\\DeletePageRequest":7:{s:14:"\0*\0wrStatusNum";i:100;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"12";s:3:"cID";s:3:"176";s:4:"wrID";s:2:"29";}'),
(50, 30, '2015-05-13 09:09:34', 'O:48:"Concrete\\Core\\Workflow\\Request\\DeletePageRequest":7:{s:14:"\0*\0wrStatusNum";i:100;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"12";s:3:"cID";s:3:"176";s:4:"wrID";s:2:"30";}'),
(51, 31, '2015-05-13 09:10:18', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"163";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"31";}'),
(52, 32, '2015-05-13 09:13:44', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"163";s:4:"cvID";s:1:"3";s:4:"wrID";s:2:"32";}'),
(53, 33, '2015-05-13 09:14:46', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"163";s:4:"cvID";s:1:"4";s:4:"wrID";s:2:"33";}'),
(54, 34, '2015-05-13 09:15:13', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"163";s:4:"cvID";s:1:"5";s:4:"wrID";s:2:"34";}'),
(55, 35, '2015-05-13 09:15:44', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"162";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"35";}'),
(56, 36, '2015-05-13 09:20:22', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"161";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"36";}'),
(57, 37, '2015-05-13 09:23:53', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"161";s:4:"cvID";s:1:"3";s:4:"wrID";s:2:"37";}'),
(58, 38, '2015-05-13 09:36:09', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"179";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"38";}'),
(59, 39, '2015-05-13 09:41:08', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"161";s:4:"cvID";s:1:"4";s:4:"wrID";s:2:"39";}'),
(60, 40, '2015-05-13 09:41:11', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"161";s:4:"cvID";s:1:"5";s:4:"wrID";s:2:"40";}'),
(61, 41, '2015-05-13 09:41:12', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"161";s:4:"cvID";s:1:"6";s:4:"wrID";s:2:"41";}'),
(62, 42, '2015-05-13 09:42:39', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"163";s:4:"cvID";s:1:"6";s:4:"wrID";s:2:"42";}'),
(63, 43, '2015-05-13 10:01:18', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"179";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"43";}'),
(64, 44, '2015-05-13 10:03:29', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"179";s:4:"cvID";s:1:"3";s:4:"wrID";s:2:"44";}'),
(65, 45, '2015-05-13 10:04:10', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"179";s:4:"cvID";s:1:"4";s:4:"wrID";s:2:"45";}'),
(66, 46, '2015-05-13 10:05:31', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"179";s:4:"cvID";s:1:"5";s:4:"wrID";s:2:"46";}'),
(67, 47, '2015-05-13 10:11:18', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"180";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"47";}'),
(68, 48, '2015-05-13 10:20:08', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"180";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"48";}'),
(69, 49, '2015-05-13 10:23:46', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"180";s:4:"cvID";s:1:"3";s:4:"wrID";s:2:"49";}'),
(70, 50, '2015-05-13 10:25:11', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"179";s:4:"cvID";s:1:"6";s:4:"wrID";s:2:"50";}'),
(71, 51, '2015-05-13 10:31:34', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"161";s:4:"cvID";s:1:"7";s:4:"wrID";s:2:"51";}'),
(72, 52, '2015-05-13 10:37:48', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"181";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"52";}'),
(73, 53, '2015-05-13 10:40:09', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"181";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"53";}'),
(74, 54, '2015-05-13 10:45:18', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"148";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"54";}'),
(75, 55, '2015-05-13 11:20:09', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"164";s:4:"cvID";s:1:"3";s:4:"wrID";s:2:"55";}'),
(76, 56, '2015-05-13 11:29:49', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"164";s:4:"cvID";s:1:"4";s:4:"wrID";s:2:"56";}'),
(77, 57, '2015-05-13 11:43:16', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"164";s:4:"cvID";s:1:"5";s:4:"wrID";s:2:"57";}'),
(78, 58, '2015-05-13 11:49:19', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"164";s:4:"cvID";s:1:"6";s:4:"wrID";s:2:"58";}'),
(79, 59, '2015-05-13 11:57:24', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"164";s:4:"cvID";s:1:"7";s:4:"wrID";s:2:"59";}'),
(80, 60, '2015-05-13 11:59:09', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"164";s:4:"cvID";s:1:"8";s:4:"wrID";s:2:"60";}'),
(81, 61, '2015-05-18 12:14:31', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"10";s:4:"wrID";s:2:"61";}'),
(82, 62, '2015-05-18 12:17:25', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"11";s:4:"wrID";s:2:"62";}'),
(83, 63, '2015-05-18 12:18:30', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"184";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"63";}'),
(84, 64, '2015-05-18 12:19:56', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"184";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"64";}'),
(85, 65, '2015-05-18 12:21:34', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"12";s:4:"wrID";s:2:"65";}'),
(86, 66, '2015-05-18 12:27:11', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"185";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"66";}'),
(87, 67, '2015-05-18 12:28:13', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"186";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"67";}'),
(88, 68, '2015-05-18 12:29:02', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"187";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"68";}'),
(89, 69, '2015-05-18 12:32:07', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"188";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"69";}'),
(90, 70, '2015-05-18 12:32:43', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"184";s:4:"cvID";s:1:"3";s:4:"wrID";s:2:"70";}'),
(91, 71, '2015-05-18 12:35:59', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"10";s:4:"wrID";s:2:"71";}'),
(92, 72, '2015-05-18 12:36:34', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"162";s:4:"cvID";s:1:"3";s:4:"wrID";s:2:"72";}'),
(93, 73, '2015-05-18 12:37:15', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"162";s:4:"cvID";s:1:"4";s:4:"wrID";s:2:"73";}'),
(94, 74, '2015-05-18 12:39:26', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"11";s:4:"wrID";s:2:"74";}'),
(95, 75, '2015-05-19 08:29:43', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"12";s:4:"wrID";s:2:"75";}'),
(96, 76, '2015-05-19 08:32:46', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"185";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"76";}'),
(97, 77, '2015-05-19 08:36:31', 'O:48:"Concrete\\Core\\Workflow\\Request\\DeletePageRequest":7:{s:14:"\0*\0wrStatusNum";i:100;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"12";s:3:"cID";s:3:"186";s:4:"wrID";s:2:"77";}'),
(98, 78, '2015-05-19 08:36:34', 'O:48:"Concrete\\Core\\Workflow\\Request\\DeletePageRequest":7:{s:14:"\0*\0wrStatusNum";i:100;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"12";s:3:"cID";s:3:"186";s:4:"wrID";s:2:"78";}'),
(99, 79, '2015-05-19 08:36:46', 'O:48:"Concrete\\Core\\Workflow\\Request\\DeletePageRequest":7:{s:14:"\0*\0wrStatusNum";i:100;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"12";s:3:"cID";s:3:"186";s:4:"wrID";s:2:"79";}'),
(100, 80, '2015-05-19 08:36:47', 'O:48:"Concrete\\Core\\Workflow\\Request\\DeletePageRequest":7:{s:14:"\0*\0wrStatusNum";i:100;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"12";s:3:"cID";s:3:"186";s:4:"wrID";s:2:"80";}'),
(101, 81, '2015-05-19 08:44:02', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"187";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"81";}'),
(102, 82, '2015-05-19 08:46:24', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"188";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"82";}'),
(103, 83, '2015-05-19 08:48:23', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"189";s:4:"cvID";s:1:"1";s:4:"wrID";s:2:"83";}'),
(104, 84, '2015-05-19 08:50:55', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"189";s:4:"cvID";s:1:"2";s:4:"wrID";s:2:"84";}'),
(105, 85, '2015-05-19 08:52:26', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"189";s:4:"cvID";s:1:"3";s:4:"wrID";s:2:"85";}'),
(106, 86, '2015-05-19 08:53:38', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"162";s:4:"cvID";s:1:"5";s:4:"wrID";s:2:"86";}'),
(107, 87, '2015-05-19 09:01:34', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"13";s:4:"wrID";s:2:"87";}'),
(108, 88, '2015-05-19 09:02:29', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"13";s:4:"wrID";s:2:"88";}'),
(109, 89, '2015-05-19 09:05:23', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"14";s:4:"wrID";s:2:"89";}'),
(110, 90, '2015-05-19 09:30:37', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"175";s:4:"cvID";s:1:"5";s:4:"wrID";s:2:"90";}'),
(111, 91, '2015-05-19 10:04:02', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"148";s:4:"cvID";s:1:"3";s:4:"wrID";s:2:"91";}'),
(112, 92, '2015-05-19 10:06:06', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"148";s:4:"cvID";s:1:"4";s:4:"wrID";s:2:"92";}'),
(113, 93, '2015-05-19 10:08:01', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"148";s:4:"cvID";s:1:"5";s:4:"wrID";s:2:"93";}'),
(114, 94, '2015-05-19 10:09:06', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"159";s:4:"cvID";s:1:"4";s:4:"wrID";s:2:"94";}'),
(115, 95, '2015-05-19 10:15:02', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"14";s:4:"wrID";s:2:"95";}'),
(116, 96, '2015-05-19 10:16:34', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"15";s:4:"wrID";s:2:"96";}'),
(117, 97, '2015-05-19 10:17:47', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"148";s:4:"cvID";s:1:"6";s:4:"wrID";s:2:"97";}'),
(118, 98, '2015-05-19 11:17:49', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"15";s:4:"wrID";s:2:"98";}'),
(119, 99, '2015-05-19 11:18:29', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"16";s:4:"wrID";s:2:"99";}'),
(120, 100, '2015-05-19 11:21:16', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"17";s:4:"wrID";s:3:"100";}'),
(121, 101, '2015-05-19 12:22:32', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"178";s:4:"cvID";s:1:"3";s:4:"wrID";s:3:"101";}'),
(122, 102, '2015-05-19 12:53:16', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"161";s:4:"cvID";s:1:"8";s:4:"wrID";s:3:"102";}'),
(123, 103, '2015-05-19 12:54:31', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"169";s:4:"cvID";s:1:"4";s:4:"wrID";s:3:"103";}'),
(124, 104, '2015-05-19 13:04:11', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"18";s:4:"wrID";s:3:"104";}'),
(125, 105, '2015-05-19 13:38:20', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"169";s:4:"cvID";s:1:"5";s:4:"wrID";s:3:"105";}'),
(126, 106, '2015-05-19 13:43:49', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"169";s:4:"cvID";s:1:"6";s:4:"wrID";s:3:"106";}'),
(127, 107, '2015-05-19 13:58:52', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"166";s:4:"cvID";s:1:"2";s:4:"wrID";s:3:"107";}'),
(128, 108, '2015-05-19 14:21:53', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"166";s:4:"cvID";s:1:"3";s:4:"wrID";s:3:"108";}'),
(129, 109, '2015-05-19 15:52:54', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"166";s:4:"cvID";s:1:"4";s:4:"wrID";s:3:"109";}'),
(130, 110, '2015-05-19 16:48:24', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"19";s:4:"wrID";s:3:"110";}'),
(131, 111, '2015-05-20 06:14:19', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"20";s:4:"wrID";s:3:"111";}'),
(132, 112, '2015-05-20 06:19:43', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"15";s:4:"wrID";s:3:"112";}'),
(133, 113, '2015-05-20 06:21:02', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"165";s:4:"cvID";s:1:"2";s:4:"wrID";s:3:"113";}'),
(134, 114, '2015-05-20 06:24:25', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"165";s:4:"cvID";s:1:"3";s:4:"wrID";s:3:"114";}'),
(135, 115, '2015-05-20 06:32:40', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"16";s:4:"wrID";s:3:"115";}'),
(136, 116, '2015-05-20 07:05:19', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"17";s:4:"wrID";s:3:"116";}'),
(137, 117, '2015-05-20 07:10:23', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"18";s:4:"wrID";s:3:"117";}'),
(138, 118, '2015-05-20 07:10:33', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"19";s:4:"wrID";s:3:"118";}'),
(139, 119, '2015-05-20 07:14:48', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"20";s:4:"wrID";s:3:"119";}'),
(140, 120, '2015-05-20 07:16:26', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"21";s:4:"wrID";s:3:"120";}'),
(141, 121, '2015-05-20 07:18:36', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"22";s:4:"wrID";s:3:"121";}'),
(142, 122, '2015-05-20 07:25:23', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"21";s:4:"wrID";s:3:"122";}'),
(143, 123, '2015-05-20 09:06:45', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"166";s:4:"cvID";s:1:"5";s:4:"wrID";s:3:"123";}'),
(144, 124, '2015-05-20 09:26:11', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"166";s:4:"cvID";s:1:"6";s:4:"wrID";s:3:"124";}'),
(145, 125, '2015-05-20 10:27:11', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"22";s:4:"wrID";s:3:"125";}'),
(146, 126, '2015-05-20 10:29:42', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"23";s:4:"wrID";s:3:"126";}'),
(147, 127, '2015-05-20 10:32:07', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"165";s:4:"cvID";s:1:"4";s:4:"wrID";s:3:"127";}'),
(148, 128, '2015-05-20 10:36:01', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"148";s:4:"cvID";s:1:"7";s:4:"wrID";s:3:"128";}'),
(149, 129, '2015-05-20 10:36:40', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"159";s:4:"cvID";s:1:"5";s:4:"wrID";s:3:"129";}'),
(150, 130, '2015-05-20 10:39:27', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"175";s:4:"cvID";s:1:"6";s:4:"wrID";s:3:"130";}'),
(151, 131, '2015-05-20 10:41:39', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"178";s:4:"cvID";s:1:"4";s:4:"wrID";s:3:"131";}'),
(152, 132, '2015-05-20 10:57:27', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"161";s:4:"cvID";s:1:"9";s:4:"wrID";s:3:"132";}'),
(153, 133, '2015-05-20 11:02:16', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"164";s:4:"cvID";s:1:"9";s:4:"wrID";s:3:"133";}'),
(154, 134, '2015-05-20 11:05:31', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"164";s:4:"cvID";s:2:"10";s:4:"wrID";s:3:"134";}'),
(155, 135, '2015-05-20 11:09:11', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"169";s:4:"cvID";s:1:"7";s:4:"wrID";s:3:"135";}'),
(156, 136, '2015-05-20 11:18:01', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"23";s:4:"wrID";s:3:"136";}'),
(157, 137, '2015-05-20 11:20:02', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"24";s:4:"wrID";s:3:"137";}'),
(158, 138, '2015-05-20 11:23:20', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"24";s:4:"wrID";s:3:"138";}'),
(159, 139, '2015-05-22 10:36:25', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"169";s:4:"cvID";s:1:"8";s:4:"wrID";s:3:"139";}'),
(160, 140, '2015-05-22 11:41:03', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"170";s:4:"cvID";s:1:"2";s:4:"wrID";s:3:"140";}'),
(161, 141, '2015-05-22 11:41:08', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"170";s:4:"cvID";s:1:"2";s:4:"wrID";s:3:"141";}'),
(162, 142, '2015-05-22 11:41:10', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"170";s:4:"cvID";s:1:"2";s:4:"wrID";s:3:"142";}'),
(163, 143, '2015-05-22 11:41:11', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"170";s:4:"cvID";s:1:"2";s:4:"wrID";s:3:"143";}'),
(164, 144, '2015-05-22 11:42:11', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"170";s:4:"cvID";s:1:"3";s:4:"wrID";s:3:"144";}'),
(165, 145, '2015-05-22 11:42:22', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"170";s:4:"cvID";s:1:"3";s:4:"wrID";s:3:"145";}'),
(166, 146, '2015-05-22 11:49:15', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"169";s:4:"cvID";s:1:"9";s:4:"wrID";s:3:"146";}'),
(167, 147, '2015-05-22 12:02:34', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"191";s:4:"cvID";s:1:"1";s:4:"wrID";s:3:"147";}'),
(168, 148, '2015-05-22 12:05:24', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"191";s:4:"cvID";s:1:"2";s:4:"wrID";s:3:"148";}'),
(169, 149, '2015-05-22 13:12:07', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"170";s:4:"cvID";s:1:"4";s:4:"wrID";s:3:"149";}'),
(170, 150, '2015-05-22 13:17:30', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"167";s:4:"cvID";s:2:"25";s:4:"wrID";s:3:"150";}'),
(171, 151, '2015-05-22 13:43:22', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"25";s:4:"wrID";s:3:"151";}'),
(172, 152, '2015-05-22 13:46:17', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"26";s:4:"wrID";s:3:"152";}'),
(173, 153, '2015-05-22 13:48:56', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"192";s:4:"cvID";s:1:"1";s:4:"wrID";s:3:"153";}'),
(174, 154, '2015-05-22 13:50:09', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"192";s:4:"cvID";s:1:"2";s:4:"wrID";s:3:"154";}'),
(175, 155, '2015-05-22 13:50:11', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"192";s:4:"cvID";s:1:"2";s:4:"wrID";s:3:"155";}'),
(176, 156, '2015-05-22 13:51:37', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"192";s:4:"cvID";s:1:"3";s:4:"wrID";s:3:"156";}'),
(177, 157, '2015-05-22 13:53:03', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"192";s:4:"cvID";s:1:"4";s:4:"wrID";s:3:"157";}'),
(178, 158, '2015-05-22 13:55:31', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"27";s:4:"wrID";s:3:"158";}'),
(179, 159, '2015-05-23 14:10:59', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"175";s:4:"cvID";s:1:"7";s:4:"wrID";s:3:"159";}'),
(180, 160, '2015-05-23 15:44:23', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"170";s:4:"cvID";s:1:"5";s:4:"wrID";s:3:"160";}'),
(181, 161, '2015-05-25 08:17:14', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"193";s:4:"cvID";s:1:"1";s:4:"wrID";s:3:"161";}'),
(182, 162, '2015-05-26 08:28:46', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:1:"1";s:4:"cvID";s:2:"28";s:4:"wrID";s:3:"162";}');
INSERT INTO `WorkflowProgressHistory` (`wphID`, `wpID`, `timestamp`, `object`) VALUES
(183, 163, '2015-05-26 08:50:32', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"193";s:4:"cvID";s:1:"2";s:4:"wrID";s:3:"163";}'),
(184, 164, '2015-05-26 09:14:20', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"193";s:4:"cvID";s:1:"3";s:4:"wrID";s:3:"164";}'),
(185, 165, '2015-05-26 09:53:46', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"170";s:4:"cvID";s:1:"6";s:4:"wrID";s:3:"165";}'),
(186, 166, '2015-05-26 10:16:54', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"170";s:4:"cvID";s:1:"7";s:4:"wrID";s:3:"166";}'),
(187, 167, '2015-05-26 10:20:47', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"161";s:4:"cvID";s:2:"10";s:4:"wrID";s:3:"167";}'),
(188, 168, '2015-05-27 07:19:11', 'O:49:"Concrete\\Core\\Workflow\\Request\\ApprovePageRequest":8:{s:14:"\0*\0wrStatusNum";i:30;s:12:"\0*\0currentWP";N;s:6:"\0*\0uID";s:1:"1";s:5:"error";s:0:"";s:4:"pkID";s:2:"14";s:3:"cID";s:3:"148";s:4:"cvID";s:1:"8";s:4:"wrID";s:3:"168";}');

-- --------------------------------------------------------

--
-- Table structure for table `WorkflowRequestObjects`
--

CREATE TABLE IF NOT EXISTS `WorkflowRequestObjects` (
  `wrID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wrObject` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`wrID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=169 ;

-- --------------------------------------------------------

--
-- Table structure for table `WorkflowTypes`
--

CREATE TABLE IF NOT EXISTS `WorkflowTypes` (
  `wftID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wftHandle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `wftName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pkgID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`wftID`),
  UNIQUE KEY `wftHandle` (`wftHandle`),
  KEY `pkgID` (`pkgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `WorkflowTypes`
--

INSERT INTO `WorkflowTypes` (`wftID`, `wftHandle`, `wftName`, `pkgID`) VALUES
(1, 'basic', 'Basic Workflow', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Workflows`
--

CREATE TABLE IF NOT EXISTS `Workflows` (
  `wfID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wfName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wftID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`wfID`),
  UNIQUE KEY `wfName` (`wfName`),
  KEY `wftID` (`wftID`,`wfID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `atAddress`
--

CREATE TABLE IF NOT EXISTS `atAddress` (
  `avID` int(10) unsigned NOT NULL DEFAULT '0',
  `address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_province` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`avID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atAddressCustomCountries`
--

CREATE TABLE IF NOT EXISTS `atAddressCustomCountries` (
  `atAddressCustomCountryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `akID` int(10) unsigned NOT NULL DEFAULT '0',
  `country` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`atAddressCustomCountryID`),
  KEY `akID` (`akID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `atAddressSettings`
--

CREATE TABLE IF NOT EXISTS `atAddressSettings` (
  `akID` int(10) unsigned NOT NULL DEFAULT '0',
  `akHasCustomCountries` tinyint(1) NOT NULL DEFAULT '0',
  `akDefaultCountry` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`akID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atBoolean`
--

CREATE TABLE IF NOT EXISTS `atBoolean` (
  `avID` int(10) unsigned NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`avID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `atBoolean`
--

INSERT INTO `atBoolean` (`avID`, `value`) VALUES
(9, 1),
(16, 1),
(18, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(42, 1),
(43, 1),
(47, 1),
(51, 1),
(98, 1),
(105, 1),
(106, 1),
(107, 1),
(118, 0),
(123, 0),
(126, 1),
(127, 1),
(128, 1),
(133, 0),
(134, 1),
(135, 1),
(136, 0),
(137, 0),
(138, 0),
(139, 1),
(140, 1),
(141, 0),
(146, 0),
(151, 0),
(156, 0),
(161, 0),
(166, 0),
(171, 0),
(176, 0),
(181, 0),
(186, 0),
(191, 0),
(196, 0),
(201, 0),
(206, 0),
(222, 0),
(229, 0),
(234, 0),
(241, 0);

-- --------------------------------------------------------

--
-- Table structure for table `atBooleanSettings`
--

CREATE TABLE IF NOT EXISTS `atBooleanSettings` (
  `akID` int(10) unsigned NOT NULL,
  `akCheckedByDefault` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`akID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `atBooleanSettings`
--

INSERT INTO `atBooleanSettings` (`akID`, `akCheckedByDefault`) VALUES
(5, 0),
(6, 0),
(9, 0),
(10, 0),
(11, 0),
(12, 1),
(13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `atDateTime`
--

CREATE TABLE IF NOT EXISTS `atDateTime` (
  `avID` int(10) unsigned NOT NULL,
  `value` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`avID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atDateTimeSettings`
--

CREATE TABLE IF NOT EXISTS `atDateTimeSettings` (
  `akID` int(10) unsigned NOT NULL,
  `akDateDisplayMode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`akID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atDefault`
--

CREATE TABLE IF NOT EXISTS `atDefault` (
  `avID` int(10) unsigned NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`avID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `atDefault`
--

INSERT INTO `atDefault` (`avID`, `value`) VALUES
(1, 'fa fa-th-large'),
(2, 'pages, add page, delete page, copy, move, alias'),
(3, 'pages, add page, delete page, copy, move, alias'),
(4, 'pages, add page, delete page, copy, move, alias, bulk'),
(5, 'find page, search page, search, find, pages, sitemap'),
(6, 'add file, delete file, copy, move, alias, resize, crop, rename, images, title, attribute'),
(7, 'file, file attributes, title, attribute, description, rename'),
(8, 'files, category, categories'),
(10, 'new file set'),
(11, 'users, groups, people, find, delete user, remove user, change password, password'),
(12, 'find, search, people, delete user, remove user, change password, password'),
(13, 'user, group, people, permissions, expire, badges'),
(14, 'user attributes, user data, gather data, registration data'),
(15, 'new user, create'),
(17, 'new user group, new group, group, create'),
(19, 'group set'),
(20, 'community, points, karma'),
(21, 'action, community actions'),
(22, 'forms, log, error, email, mysql, exception, survey'),
(23, 'forms, questions, response, data'),
(24, 'questions, quiz, response'),
(25, 'forms, log, error, email, mysql, exception, survey, history'),
(26, 'new theme, theme, active theme, change theme, template, css'),
(27, 'page types'),
(36, 'page attributes, custom'),
(37, 'single, page, custom, application'),
(38, 'atom, rss, feed, syndication'),
(39, 'icon-bullhorn'),
(40, 'add workflow, remove workflow'),
(41, 'stacks, reusable content, scrapbook, copy, paste, paste block, copy block, site name, logo'),
(44, 'edit stacks, view stacks, all stacks'),
(45, 'block, refresh, custom'),
(46, 'add-on, addon, add on, package, app, ecommerce, discussions, forums, themes, templates, blocks'),
(48, 'add-on, addon, ecommerce, install, discussions, forums, themes, templates, blocks'),
(49, 'update, upgrade'),
(50, 'concrete5.org, my account, marketplace'),
(52, 'buy theme, new theme, marketplace, template'),
(53, 'buy addon, buy add on, buy add-on, purchase addon, purchase add on, purchase add-on, find addon, new addon, marketplace'),
(54, 'dashboard, configuration'),
(55, 'website name, title'),
(56, 'accessibility, easy mode'),
(57, 'sharing, facebook, twitter'),
(58, 'logo, favicon, iphone, icon, bookmark'),
(59, 'tinymce, content block, fonts, editor, content, overlay'),
(60, 'translate, translation, internationalization, multilingual'),
(61, 'timezone, profile, locale'),
(62, 'multilingual, localization, internationalization, i18n'),
(63, 'vanity, pretty url, seo, pageview, view'),
(64, 'bulk, seo, change keywords, engine, optimization, search'),
(65, 'traffic, statistics, google analytics, quant, pageviews, hits'),
(66, 'pretty, slug'),
(67, 'configure search, site search, search option'),
(68, 'file options, file manager, upload, modify'),
(69, 'security, files, media, extension, manager, upload'),
(70, 'images, picture, responsive, retina'),
(71, 'security, alternate storage, hide files'),
(72, 'cache option, change cache, override, turn on cache, turn off cache, no cache, page cache, caching'),
(73, 'cache option, turn off cache, no cache, page cache, caching'),
(74, 'index search, reindex search, build sitemap, sitemap.xml, clear old versions, page versions, remove old'),
(75, 'queries, database, mysql'),
(76, 'editors, hide site, offline, private, public, access'),
(77, 'security, actions, administrator, admin, package, marketplace, search'),
(78, 'security, lock ip, lock out, block ip, address, restrict, access'),
(79, 'security, registration'),
(80, 'antispam, block spam, security'),
(81, 'lock site, under construction, hide, hidden'),
(82, 'profile, login, redirect, specific, dashboard, administrators'),
(83, 'member profile, member page, community, forums, social, avatar'),
(84, 'signup, new user, community, public registration, public, registration'),
(85, 'auth, authentication, types, oauth, facebook, login, registration'),
(86, 'smtp, mail settings'),
(87, 'email server, mail settings, mail configuration, external, internal'),
(88, 'test smtp, test mail'),
(89, 'email server, mail settings, mail configuration, private message, message system, import, email, message'),
(90, 'conversations'),
(91, 'conversations'),
(92, 'conversations ratings, ratings, community, community points'),
(93, 'conversations bad words, banned words, banned, bad words, bad, words, list'),
(94, 'attribute configuration'),
(95, 'attributes, sets'),
(96, 'attributes, types'),
(97, 'topics, tags, taxonomy'),
(99, 'overrides, system info, debug, support, help'),
(100, 'errors, exceptions, develop, support, help'),
(101, 'email, logging, logs, smtp, pop, errors, mysql, log'),
(102, 'network, proxy server'),
(103, 'export, backup, database, sql, mysql, encryption, restore'),
(104, 'upgrade, new version, update'),
(108, 'fa fa-edit'),
(109, 'fa fa-trash-o'),
(110, 'fa fa-th'),
(111, 'fa fa-briefcase'),
(114, ''),
(115, ''),
(116, ''),
(117, ''),
(119, ''),
(120, ''),
(121, ''),
(122, ''),
(129, ''),
(130, ''),
(131, ''),
(132, ''),
(142, 'Diers Klinik - fertilitetsklinik i Aarhus'),
(143, 'Vil tilbyder insemination og ultralydsscanninger til par, singler og lesbiske'),
(144, ''),
(145, ''),
(147, 'Diers klinik har erfarne jordemødre og sygeplejersker'),
(148, 'Vores personale har stor erfaring med insemination og ultarlydsscanninger'),
(149, ''),
(150, ''),
(152, 'Diers Klinik ligger i toppen med insemination'),
(153, 'Hos Diers Klinik har opnået nogle af Danmarks bedste succesrater for graviditeter'),
(154, ''),
(155, ''),
(157, 'Diers Klinik er specialister i insemination'),
(158, 'Vi kan lave insemination med både donorsæd eller med din partners sæd'),
(159, ''),
(160, ''),
(162, 'Forberedelser til din graviditet'),
(163, 'Vi hjælper dig med forberedelsen til en vellykket insemination'),
(164, ''),
(165, ''),
(167, 'Trygge rammer for din insemination'),
(168, 'Inden du kan få foretaget kunstig befrugtning på Diers Klinik, skal du have været til en journalsamtale hos os. '),
(169, ''),
(170, ''),
(172, 'Sådan bliver du lettest gravid'),
(173, 'Du kan selv øge chancerne for at blive gravid og vi hjælper dig på vej'),
(174, ''),
(175, ''),
(177, 'Diers Klinik - fertilitetsklinik i Aarhus'),
(178, 'Vi tilbyder insemination og ultralydsscanninger til par, singler og lesbiske'),
(179, ''),
(180, ''),
(182, 'Diers Klinik - fertilitetsklinik i Aarhus'),
(183, 'Diers Klinik tilbyder insemination til par, singler og lesbiske på fertilitetsklinikken i Aarhus'),
(184, ''),
(185, ''),
(187, 'Meta titel'),
(188, 'Meta beskrivelse'),
(189, 'Meta nøgleord'),
(190, ''),
(192, 'Meta titel'),
(193, 'Meta beskrivelse'),
(194, 'Meta nøgleord'),
(195, ''),
(197, 'Diers Klinik - fertilitetsklinik i Aarhus'),
(198, 'Vi tilbyder insemination og ultralydsscanninger til par, singler og lesbiske'),
(199, 'Meta nøgleord'),
(200, ''),
(202, 'Insemination med både anonyme og kendte sæddonorer'),
(203, 'I Diers Klinik har du flere muligheder, hvad angår valg af sæddonor.'),
(204, ''),
(205, ''),
(217, 'database, entities, doctrine, orm'),
(218, 'Priser for insemination, donor og ultralydsscanning'),
(219, 'Diers Klinik har konkurrencedygtige priser for insemination. Her kommer ingen ekstra gebyrer'),
(220, ''),
(221, ''),
(225, 'Diers Klinik - fertilitetsklinik i Aarhus'),
(226, 'Diers Klinik tilbyder insemination og ultralydsscaning til par, singler og lesbiske på fertilitetsklinikken i Aarhus'),
(227, ''),
(228, ''),
(230, 'Diers Klinik - fertilitetsklinik i Aarhus'),
(231, 'Vi tilbyder insemination og ultralydsscanninger til par, singler og lesbiske '),
(232, 'Meta nøgleord'),
(233, ''),
(237, 'Hjælp til medmoderskab '),
(238, 'Links til medmoderskab og faderskab efter insemination'),
(239, ''),
(240, '');

-- --------------------------------------------------------

--
-- Table structure for table `atFile`
--

CREATE TABLE IF NOT EXISTS `atFile` (
  `avID` int(10) unsigned NOT NULL,
  `fID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`avID`),
  KEY `fID` (`fID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atNumber`
--

CREATE TABLE IF NOT EXISTS `atNumber` (
  `avID` int(10) unsigned NOT NULL,
  `value` decimal(14,4) DEFAULT '0.0000',
  PRIMARY KEY (`avID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `atNumber`
--

INSERT INTO `atNumber` (`avID`, `value`) VALUES
(112, '225.0000'),
(113, '112.0000'),
(124, '250.0000'),
(125, '167.0000'),
(207, '170.0000'),
(208, '165.0000'),
(209, '170.0000'),
(210, '165.0000'),
(211, '170.0000'),
(212, '165.0000'),
(213, '170.0000'),
(214, '165.0000'),
(215, '170.0000'),
(216, '165.0000'),
(223, '170.0000'),
(224, '165.0000'),
(235, '170.0000'),
(236, '165.0000'),
(242, '5184.0000'),
(243, '3456.0000'),
(244, '800.0000'),
(245, '533.0000'),
(246, '264.0000'),
(247, '237.0000'),
(248, '351.0000'),
(249, '349.0000'),
(250, '500.0000'),
(251, '500.0000'),
(252, '1900.0000'),
(253, '1267.0000'),
(254, '640.0000'),
(255, '852.0000'),
(256, '960.0000'),
(257, '638.0000'),
(258, '657.0000'),
(259, '960.0000'),
(260, '960.0000'),
(261, '1280.0000'),
(262, '843.0000'),
(263, '960.0000'),
(264, '640.0000'),
(265, '852.0000'),
(266, '720.0000'),
(267, '960.0000'),
(268, '3264.0000'),
(269, '2448.0000'),
(270, '1280.0000'),
(271, '960.0000'),
(272, '3264.0000'),
(273, '2448.0000'),
(274, '1280.0000'),
(275, '960.0000'),
(276, '3264.0000'),
(277, '2448.0000'),
(278, '1280.0000'),
(279, '960.0000'),
(280, '1280.0000'),
(281, '851.0000'),
(282, '640.0000'),
(283, '852.0000'),
(284, '960.0000'),
(285, '1280.0000'),
(286, '960.0000'),
(287, '1280.0000'),
(288, '2448.0000'),
(289, '3264.0000'),
(290, '1000.0000'),
(291, '1333.0000');

-- --------------------------------------------------------

--
-- Table structure for table `atSelectOptions`
--

CREATE TABLE IF NOT EXISTS `atSelectOptions` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `akID` int(10) unsigned DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayOrder` int(10) unsigned DEFAULT NULL,
  `isEndUserAdded` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `akID` (`akID`,`displayOrder`),
  KEY `value` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `atSelectOptionsSelected`
--

CREATE TABLE IF NOT EXISTS `atSelectOptionsSelected` (
  `avID` int(10) unsigned NOT NULL,
  `atSelectOptionID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`avID`,`atSelectOptionID`),
  KEY `atSelectOptionID` (`atSelectOptionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atSelectSettings`
--

CREATE TABLE IF NOT EXISTS `atSelectSettings` (
  `akID` int(10) unsigned NOT NULL,
  `akSelectAllowMultipleValues` tinyint(1) NOT NULL DEFAULT '0',
  `akSelectOptionDisplayOrder` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'display_asc',
  `akSelectAllowOtherValues` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`akID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `atSelectSettings`
--

INSERT INTO `atSelectSettings` (`akID`, `akSelectAllowMultipleValues`, `akSelectOptionDisplayOrder`, `akSelectAllowOtherValues`) VALUES
(8, 1, 'display_asc', 1);

-- --------------------------------------------------------

--
-- Table structure for table `atSelectedTopics`
--

CREATE TABLE IF NOT EXISTS `atSelectedTopics` (
  `avID` int(10) unsigned NOT NULL,
  `TopicNodeID` int(11) NOT NULL,
  PRIMARY KEY (`avID`,`TopicNodeID`),
  KEY `TopicNodeID` (`TopicNodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atSocialLinks`
--

CREATE TABLE IF NOT EXISTS `atSocialLinks` (
  `avsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `avID` int(10) unsigned NOT NULL DEFAULT '0',
  `service` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serviceInfo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`avsID`),
  KEY `avID` (`avID`,`avsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `atTextareaSettings`
--

CREATE TABLE IF NOT EXISTS `atTextareaSettings` (
  `akID` int(10) unsigned NOT NULL DEFAULT '0',
  `akTextareaDisplayMode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `akTextareaDisplayModeCustomOptions` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`akID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `atTextareaSettings`
--

INSERT INTO `atTextareaSettings` (`akID`, `akTextareaDisplayMode`, `akTextareaDisplayModeCustomOptions`) VALUES
(2, '', ''),
(3, '', ''),
(4, '', ''),
(7, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `atTopicSettings`
--

CREATE TABLE IF NOT EXISTS `atTopicSettings` (
  `akID` int(10) unsigned NOT NULL DEFAULT '0',
  `akTopicParentNodeID` int(11) DEFAULT NULL,
  `akTopicTreeID` int(11) DEFAULT NULL,
  PRIMARY KEY (`akID`),
  KEY `akTopicTreeID` (`akTopicTreeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `authTypeConcreteCookieMap`
--

CREATE TABLE IF NOT EXISTS `authTypeConcreteCookieMap` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uID` int(11) DEFAULT NULL,
  `validThrough` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `token` (`token`),
  KEY `uID` (`uID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `authTypeConcreteCookieMap`
--

INSERT INTO `authTypeConcreteCookieMap` (`ID`, `token`, `uID`, `validThrough`) VALUES
(2, 'a61197f2a0bae282f2b5e7977f06c569', 1, 1433933519),
(3, 'a800af90c4f07cbc2ad4a84ba120716e', 1, 1433933519),
(4, 'b5a680f69dc8c79282f6d00c90cd4fc7', 1, 1433933519),
(5, '1c7c83e2e344f3ab67bb690b943b4eb0', 1, 1433933519),
(6, '4d72feebe571186c0daee1c2c5bb7146', 1, 1433933519),
(7, '076d5c8e8a936e3303a4ecc35de3f3e6', 1, 1433933519),
(8, 'c0c7ad00418085a106a8c2de310bb8f2', 1, 1433933519),
(9, 'ff7f325e2e5b4f063494b7ed8a1bea82', 1, 1433933519);

-- --------------------------------------------------------

--
-- Table structure for table `btContentFile`
--

CREATE TABLE IF NOT EXISTS `btContentFile` (
  `bID` int(10) unsigned NOT NULL,
  `fID` int(10) unsigned DEFAULT NULL,
  `fileLinkText` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `filePassword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`bID`),
  KEY `fID` (`fID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btContentImage`
--

CREATE TABLE IF NOT EXISTS `btContentImage` (
  `bID` int(10) unsigned NOT NULL,
  `fID` int(10) unsigned DEFAULT '0',
  `fOnstateID` int(10) unsigned DEFAULT '0',
  `maxWidth` int(10) unsigned DEFAULT '0',
  `maxHeight` int(10) unsigned DEFAULT '0',
  `externalLink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `internalLinkCID` int(10) unsigned DEFAULT '0',
  `altText` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`bID`),
  KEY `fID` (`fID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `btContentImage`
--

INSERT INTO `btContentImage` (`bID`, `fID`, `fOnstateID`, `maxWidth`, `maxHeight`, `externalLink`, `internalLinkCID`, `altText`, `title`) VALUES
(34, 1, 0, 0, 0, '', 0, '', ''),
(370, 2, 0, 0, 0, '', 0, '', ''),
(411, 7, 0, 0, 0, '', 0, '', ''),
(412, 7, 0, 300, 0, '', 0, '', ''),
(454, 11, 0, 0, 0, '', 0, '', ''),
(455, 12, 0, 0, 0, '', 0, '', ''),
(468, 7, 0, 300, 0, '', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `btContentLocal`
--

CREATE TABLE IF NOT EXISTS `btContentLocal` (
  `bID` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `btContentLocal`
--

INSERT INTO `btContentLocal` (`bID`, `content`) VALUES
(1, '<div style="padding: 40px; text-align: center"> <iframe width="853" height="480" src="//www.youtube.com/embed/VB-R71zk06U" frameborder="0" allowfullscreen></iframe>                                     </div>'),
(15, ''),
(19, ''),
(24, ''),
(25, '<h3>Journalsamtalen</h3><p>Inden du kan få foretaget kunstig befrugtning op Diers Klinik, skal du have været til journalsamtale hos os.</p><p>							<a href="#">Læs mere</a></p>'),
(26, '<h3>Insemination</h3><p>Når det er tid til at blive insemineret, ringer du til klinikken og sammen aftaler vi et tidspunkt for dette</p><p>							<a href="#">Læs mere</a></p>'),
(27, '<h3>Åbningstider</h3><p>For at optimere dine chancer for at opnå graviditet holder Diers Klinik åbent alle årets dage   									  								Mandag - Søndag		9.00 - 16.00</p><p>							<a href="#">Læs mere</a></p>'),
(28, '<h1 style="text-align: center;">Diers Klinik. Vi er der hele vejen for dig.</h1><p style="text-align: center;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Nam vitae felis pretium, euismod ipsum nec</p>'),
(29, '<h2>Start dit forløb i dag! Vi er her hver dag året rundt.</h2><p>Kontakt os i dag, for en uforpligtende samtale med os, omkring dine muligheder.</p>'),
(30, '<p>For din egen sikkerheds skyld er det vigtigt, at du vælger en klinik, der er godkendt af de danske sundhedsmyndigheder og lever op til EU lovgivningen. Vi er certificeret af den danske sundhedsstyrelse og lægemiddelstyrelse.</p>'),
(31, '<p>For din egen sikkerheds skyld er det vigtigt, at du vælger en klinik, der er godkendt af de danske sundhedsmyndigheder og lever op til EU lovgivningen. Vi er certificeret af den danske sundhedsstyrelse og lægemiddelstyrelse.</p>'),
(32, '<p>For din egen sikkerheds skyld er det vigtigt, at du vælger en klinik, der er godkendt af de danske sundhedsmyndigheder og lever op til EU lovgivningen. Vi er certificeret af den danske sundhedsstyrelse og lægemiddelstyrelse.</p>'),
(33, '<p>Diers Klinik er en fertilitets klinik, der tilbyder donorinsemination til par, singlekvinder og lesbiske par.  			</p><p>			Diers Klinik<br>  Grønnegade 56, 1. <br>  8000 Århus C</p><p>Tlf. +45 20 22 85 87 <br>  E-mail:&nbsp;<a href="mailto:info@diersklinik.dk" data-concrete5-link-type="image">info@diersklinik.dk</a></p>'),
(55, ''),
(60, ''),
(67, ''),
(72, ''),
(76, ''),
(77, ''),
(83, ''),
(88, ''),
(94, ''),
(104, '<h2>Åbningstider</h2><p>  	For at optimere dine chancer for at opnå graviditet holder Diers Klinik åbent alle årets dage Mandag - Søndag	9.00 - 16.00  </p>'),
(109, ''),
(115, ''),
(119, ''),
(120, ''),
(123, '<h2>Inden inseminationen skal du gå til din egen læge og få foretaget følgende test:</h2><ul><li>HIV-test</li><li>Heptitis B+C</li><li>Klamydia: podning fra cervix eller ved urintest, hvis du er yngre end 30 år.</li></ul><p>Disse prøver skal der foreligge dokumentation for, inden du kan blive insemineret.</p><p>Når du er ved din egen læge, er det en god idé samtidig at få foretaget følgende test: </p><ul> <li>Celleskrab fra livmoderhalsen </li><li>Test for antistoffer mod rubella (røde hunde)</li></ul><h2>Tidligere infektioner:</h2><p>Hvis du har haft infektion i livmoderen eller æggelederne, eller hvis du har haft klamydia eller gonoré, kan der være dannet arvæv i æggelederne, som umuliggør passage for både æg og sædceller. Er det tilfældet, bør du gå til en gynækolog og få lavet en undersøgelse, der afklarer, om der er passage i dine æggeledere. Denne undersøgelse er gratis ved henvisning fra egen læge.  </p><div>Hvis du har fået konstateret endometriose, er det ligeledes en god ide at få undersøgt passagen i æggelederne.<h2>Du kan selv øge chancerne for at blive gravid:</h2><div>Sundhedsstyrelsen anbefaler, at man indtager 400 mygram folinsyre om dagen, når man planlægger at blive gravid.<div>Derudover bør man spise en sund og varieret kost. Hvis den mad, man spiser, indeholder en masse frugt og grønt - mindst 6 stk. om dagen, er der ingen grund til at spise yderligere kosttilskud, mens man prøver at opnå graviditet.  <h2>Andre gode råd, der øger chancerne:</h2><ul> <li>Rygning nedsætter fertiliteten væsentligt. Derudover har man større risiko for at abortere, hvis man ryger. Man bør derfor stoppe med at ryge inden insemination.</li><li>Sundhedsstyrelsen anbefaler, at man ikke drikker alkohol under graviditet, og når man forsøger at blive gravid.</li><li>Der er lidt uenighed om, hvorvidt kaffe og koffeinindtag har indvirkning på fertiliteten. Flere undersøgelser peger i forskellige retninger. For at være på den sikre side, bør man derfor ikke drikke mere end svarende til 200mg koffein dagligt. Vær opmærksom på, at der kan være stor forskel på indholdet af koffein i kaffe. </li><li>Overvægt har også indvirkning på fertiliteten. Fedtvævet bevirker nemlig, at der dannes for meget mandligt kønshormon, som virker forstyrrende på hormonbalancen. Man siger, at fertiliteten kan blive påvirket på et Body Mass Index (BMI) på 27 eller derover. BMI beregnes på følgende måde: Din vægt i kilo divideret med din højde mål i meter i anden. Hvis du f.eks. vejer 65 kg. og er 170 cm. ser regnestykket således ud: 65/(1,70x1,70) = 22,49 BMI. Hvis din fertilitet er påvirket, vil det f.eks. give sig udslag i en lang menstruationscyklus. Mange kvinder har forhøjet BMI uden, at det har indflydelse på fertiliteten.</li></ul></div></div></div>'),
(133, ''),
(138, ''),
(139, '<p><strong>Liza Diers, Jordemoder</strong><br>Jeg hedder Liza Diers og har startet Diers Klinik. Min  motivation for at åbne fertilitetsklinikken er, at jeg ønsker at hjælpe enhver  kvinde eller ethvert par - single, lesbisk eller heteroseksuel med at blive  gravid. </p><p><strong>Kristina</strong> <strong>Sørensen</strong><strong>, Sygeplejerske</strong><br>Jeg hedder Kristina Sørensen. Efter endt  sygeplerskestudie har jeg læst en kandidat i sygeplejevidenskab, hvor jeg har  specialiseret mig i donorbørn. Jeg afholder journalsamtaler, scanner for  ægløsning og udfører inseminationer.<br>Jeg er på barselsorlov fra juni 2014 til sommeren 2015.</p><p><strong>Janni Meisner, Jordemoder</strong><br>Jeg hedder Janni Meisner. Udover at være her på klinikken er jeg timevikar på Skejby Fødeafdeling. Jeg afholder journalsamtaler, scanner for ægløsning og udfører  inseminationer.</p><p><strong>Anne Louise Richardt Nielsen, Jordemoder</strong><br>Jeg hedder Anne Louise Richardt Nielsen. Jeg  afholder journalsamtaler, scanner og udfører inseminationer. Jeg har tidligere  arbejdet på fødegangen på Aarhus Universitetshospital, Skejby, hvor jeg fortsat  er timeafløser. </p><p><strong>Louise Mabire, Jordemoder</strong><br>Jeg hedder Louise Mabirer. Jeg scanner for ægløsning og  udfører inseminationer. Udover arbejdet på klinikken, arbejder jeg også som  jordemoder på Horsens Fødeafdeling. <br>Jeg er på barsel fra august 2014. </p><p><strong>Anne Sofie Bøtcher, Sygeplejerske</strong><br>Jeg hedder Anne Sofie Bøtcher. Jeg scanner for ægløsning  og udfører inseminationer. Ved siden af arbejdet på klinikken er jeg ved at tage  en kandidatgrad i Sygeplejevidenskab ved Aarhus Universitetshospital.</p>'),
(140, ''),
(141, '<p><strong>Liza Diers, Jordemoder</strong><br>Jeg hedder Liza Diers og har startet Diers Klinik. Min  motivation for at åbne fertilitetsklinikken er, at jeg ønsker at hjælpe enhver  kvinde eller ethvert par - single, lesbisk eller heteroseksuel med at blive  gravid. </p><p><strong>Kristina</strong> <strong>Sørensen</strong><strong>, Sygeplejerske</strong><br>Jeg hedder Kristina Sørensen. Efter endt  sygeplerskestudie har jeg læst en kandidat i sygeplejevidenskab, hvor jeg har  specialiseret mig i donorbørn. Jeg afholder journalsamtaler, scanner for  ægløsning og udfører inseminationer.<br>Jeg er på barselsorlov fra juni 2014 til sommeren 2015.</p><p><strong>Janni Meisner, Jordemoder</strong><br>Jeg hedder Janni Meisner. Udover at være her på klinikken er jeg timevikar på Skejby Fødeafdeling. Jeg afholder journalsamtaler, scanner for ægløsning og udfører  inseminationer.</p><p><strong>Anne Louise Richardt Nielsen, Jordemoder</strong><br>Jeg hedder Anne Louise Richardt Nielsen. Jeg  afholder journalsamtaler, scanner og udfører inseminationer. Jeg har tidligere  arbejdet på fødegangen på Aarhus Universitetshospital, Skejby, hvor jeg fortsat  er timeafløser. </p><p><strong>Louise Mabire, Jordemoder</strong><br>Jeg hedder Louise Mabirer. Jeg scanner for ægløsning og  udfører inseminationer. Udover arbejdet på klinikken, arbejder jeg også som  jordemoder på Horsens Fødeafdeling. <br>Jeg er på barsel fra august 2014. </p><p><strong>Anne Sofie Bøtcher, Sygeplejerske</strong><br>Jeg hedder Anne Sofie Bøtcher. Jeg scanner for ægløsning  og udfører inseminationer. Ved siden af arbejdet på klinikken er jeg ved at tage  en kandidatgrad i Sygeplejevidenskab ved Aarhus Universitetshospital.</p>'),
(142, '<p>På Diers Klinik er vi specialister i inseminationsbehandling. Vi har en af de  bedste succesrater i landet, og vores inseminationsbehandlinger bliver oftest  udført i naturlig cyklus, dvs. uden brug af hormoner eller anden medicin.</p><p>Vi kan lave insemination med både donorsæd eller med din partners sæd. Vi kan  også nedfryse sæd til senere insemination, hvis din partner ikke kan være  tilstede på selve inseminationsdagen.</p><p>På de næste sider kan du læse om, hvordan forløbet vil være.</p>'),
(143, '<div><div><ul></ul></div>  </div>'),
(144, '<p>Inden du påbegynder inseminationen hos Diers Klinik, er der nogle få forberedelser, som du skal have foretaget. Dette er med til at øge chancerne for at blive gravid og vi har brug for dokumentionen før vi må inseminere dig.</p><h3><strong></strong>Inden inseminationen skal du gå til din egen læge og få foretaget følgende  test:</h3><p>- HIV-test<br>- Heptitis B+C<br>- Klamydia: podning fra cervix eller ved urintest, hvis du er yngre end 30  år.</p><p>Disse prøver skal der foreligge dokumentation for, inden du kan blive  insemineret.</p><p>Når du er ved din egen læge, er det en god idé samtidig at få foretaget  følgende test: </p><p>- Celleskrab fra livmoderhalsen<br>- Test for antistoffer mod rubella (røde hunde)</p><h3>Tidligere infektioner</h3><p>Hvis du har haft infektion i livmoderen eller æggelederne, eller hvis du  har haft klamydia eller gonoré, kan der være dannet arvæv i æggelederne, som  umuliggør passage for både æg og sædceller. Er det tilfældet, bør du gå til en  gynækolog og få lavet en undersøgelse, der afklarer, om der er passage i dine  æggeledere. Denne undersøgelse er gratis ved henvisning fra egen læge. </p><p>Hvis du har fået konstateret endometriose, er det ligeledes en god ide at  få undersøgt passagen i æggelederne.</p><p>Vi har samlet en række gode råd, der hjælper dig med at blive gravid</p>'),
(145, '<h2>Adresse</h2><p>  	<i class="fa fa-globe"></i> Danmark, Aarhus</p><p>  	<i class="fa fa-map-marker"></i> 8800 Grønnegade 56, 1.</p><p>  	  	<i class="fa fa-phone"></i> +45 2022 8587</p><p>  	<i class="fa fa-envelope"></i>&nbsp;info@diersklinik.dk</p>'),
(146, ''),
(184, '<p>Du kan selv øge chancerne for at blive gravid</p><p>Sundhedsstyrelsen anbefaler, at man indtager 400 mygram folinsyre om dagen,&nbsp; når man planlægger at blive gravid. </p><p>Derudover bør man spise en sund og varieret kost. Hvis den mad, man spiser,&nbsp; indeholder en masse frugt og grønt - mindst 6 stk. om dagen, er der ingen grund&nbsp; til at spise yderligere kosttilskud, mens man prøver at opnå graviditet. </p><p>Andre gode råd, der øger chancerne</p><p>Rygning nedsætter fertiliteten væsentligt. Derudover har man større risiko&nbsp; for at abortere, hvis man ryger. Man bør derfor stoppe med at ryge inden&nbsp; insemination. </p><p>Sundhedsstyrelsen anbefaler, at man ikke drikker alkohol under graviditet,&nbsp; og når man forsøger at blive gravid. </p><p>Der er lidt uenighed om, hvorvidt kaffe og koffeinindtag har indvirkning på&nbsp; fertiliteten. Flere undersøgelser peger i forskellige retninger. For at være på&nbsp; den sikre side, bør man derfor ikke drikke mere end svarende til 200mg koffein&nbsp; dagligt. Vær opmærksom på, at der kan være stor forskel på indholdet af koffein i kaffe.&nbsp; </p><div><ul><li>Overvægt har også indvirkning på fertiliteten. Fedtvævet bevirker nemlig, at&nbsp; der dannes for meget mandligt kønshormon, som virker forstyrrende på&nbsp; hormonbalancen. Man siger, at fertiliteten kan blive påvirket på et Body Mass&nbsp; Index (BMI) på 27 eller derover. BMI beregnes på følgende måde: Din vægt i kilo&nbsp; divideret med din højde mål i meter i anden. Hvis du f.eks. vejer 65 kg. og er&nbsp; 170 cm. ser regnestykket således ud: 65/(1,70x1,70) = 22,49 BMI. Hvis din&nbsp; fertilitet er påvirket, vil det f.eks. give sig udslag i en lang&nbsp; menstruationscyklus. Mange kvinder har forhøjet BMI uden, at det har indflydelse&nbsp; på fertiliteten.</li></ul></div>'),
(185, '<h3>Du kan selv øge chancerne for at blive gravid</h3><p>Sundhedsstyrelsen anbefaler, at man indtager 400 mygram folinsyre om dagen,&nbsp; når man planlægger at blive gravid. </p><p>Derudover bør man spise en sund og varieret kost. Hvis den mad, man spiser,&nbsp; indeholder en masse frugt og grønt - mindst 6 stk. om dagen, er der ingen grund&nbsp; til at spise yderligere kosttilskud, mens man prøver at opnå graviditet. </p><h3>Andre gode råd, der øger chancerne</h3><p>* Rygning nedsætter fertiliteten væsentligt. Derudover har man større risiko&nbsp; for at abortere, hvis man ryger. Man bør derfor stoppe med at ryge inden&nbsp; insemination. </p><p>* Sundhedsstyrelsen anbefaler, at man ikke drikker alkohol under graviditet,&nbsp; og når man forsøger at blive gravid. </p><p>* Der er lidt uenighed om, hvorvidt kaffe og koffeinindtag har indvirkning på&nbsp; fertiliteten. Flere undersøgelser peger i forskellige retninger. For at være på&nbsp; den sikre side, bør man derfor ikke drikke mere end svarende til 200mg koffein&nbsp; dagligt. Vær opmærksom på, at der kan være stor forskel på indholdet af koffein&nbsp; i kaffe.&nbsp;   </p><p>* Overvægt har også indvirkning på fertiliteten. Fedtvævet bevirker nemlig, at&nbsp; der dannes for meget mandligt kønshormon, som virker forstyrrende på&nbsp; hormonbalancen.<br> Man siger, at fertiliteten kan blive påvirket på et Body Mass&nbsp; Index (BMI) på 27 eller derover. BMI beregnes på følgende måde: Din vægt i kilo&nbsp; divideret med din højde mål i meter i anden. Hvis du f.eks. vejer 65 kg. og er&nbsp; 170 cm. ser regnestykket således ud: 65/(1,70x1,70) = 22,49 BMI. Hvis din&nbsp; fertilitet er påvirket, vil det f.eks. give sig udslag i en lang&nbsp; menstruationscyklus. Mange kvinder har forhøjet BMI uden, at det har indflydelse&nbsp; på fertiliteten.</p>'),
(186, '<p>Inden du påbegynder inseminationen hos Diers Klinik, er der nogle få forberedelser, som du skal have foretaget. Dette er med til at øge chancerne for at blive gravid og vi har brug for dokumentionen før vi må inseminere dig.</p><h3><strong></strong>Inden inseminationen skal du gå til din egen læge og få foretaget følgende  test:</h3><p>- HIV-test<br>- Heptitis B+C<br>- Klamydia: podning fra cervix eller ved urintest, hvis du er yngre end 30  år.</p><p>Disse prøver skal der foreligge dokumentation for, inden du kan blive  insemineret.</p><p>Når du er ved din egen læge, er det en god idé samtidig at få foretaget  følgende test: </p><p>- Celleskrab fra livmoderhalsen<br>- Test for antistoffer mod rubella (røde hunde)</p><h3>Tidligere infektioner</h3><p>Hvis du har haft infektion i livmoderen eller æggelederne, eller hvis du  har haft klamydia eller gonoré, kan der være dannet arvæv i æggelederne, som  umuliggør passage for både æg og sædceller. Er det tilfældet, bør du gå til en  gynækolog og få lavet en undersøgelse, der afklarer, om der er passage i dine  æggeledere. Denne undersøgelse er gratis ved henvisning fra egen læge. </p><p>Hvis du har fået konstateret endometriose, er det ligeledes en god ide at  få undersøgt passagen i æggelederne.</p><p><a href="{CCM:CID_178}" data-concrete5-link-type="ajax">Vi har samlet en række gode råd, der hjælper dig med at blive gravid</a></p>'),
(187, '<p>Inden du kan få foretaget kunstig befrugtning på Diers Klinik, skal du have&nbsp; været til en journalsamtale hos os. </p><p>Journalsamtalen foregår i rolige og trygge rammer, og vi tager udgangspunkt&nbsp; i dig og de ønsker, du har til dit forløb. Vi taler om, hvordan det hele kommer&nbsp; til at foregå, og du har mulighed for at få svar på alle de spørgsmål, du har.&nbsp; <br>Det kan eventuelt være en god idé, at du inden samtalen laver en liste med&nbsp; alle dine spørgsmål. Det er vigtigt, at du efter journalsamtalen har en klar&nbsp; forestilling om, hvad der kommer til at ske, og at du føler dig forberedt på din&nbsp; fertilitetsbehandling. </p><p>Du får en grundig instruktion i, hvad du skal gøre, for at vi kan bestemme&nbsp; tidspunktet for din ægløsning. Desuden skal vi have klarlagt dine chancer for at&nbsp; opnå graviditet. <br>Til samtalen skal vi også finde ud af, hvilke ønsker du har&nbsp; til sæddonoren, f.eks.: Er det det rigtige for dig at vælge en anonym eller&nbsp; en åben sæddonor, ønsker du ekstra oplysninger om donor og eventuelt at se et&nbsp; babyfoto, og har du nogen specielle ønsker, som vi skal prøve at imødekomme?&nbsp; Eller har du en ven eller bekendt, som du ønsker skal være donor/far til dit&nbsp; barn.</p><p>Du kan bestille en tid til journalsamtale ved at maile eller ringe til&nbsp; os, og vi anbefaler, at vi afholder journalsamtalen senest en måned inden første&nbsp; insemination, men det kan også lade sig gøre med kortere varsel. Bor du langt&nbsp; væk, kan vi tage journalsamtalen over telefonen eller Skype. </p><p>Vi opfordrer til, at du/I har læst vores informationsmateriale grundigt,&nbsp; inden I kommer til samtalen.</p>'),
(188, '<h4>Du er meget velkommen til at kontakte os om stort og småt</h4>'),
(189, '<p>Med en god forberedelse, er du godt på vej til en vellykket behandling</p>'),
(190, '<p>Hos Diers Klinik hjælper vi dig trygt igennem hele forløbet. Derfor har vi også en af de bedste succesrater i Danmark.</p>'),
(191, '<p>Journalsamtalen sikrer dig en god start på din insemination. Vores dygtige personale svarer på alle dine spørgsmål.</p>'),
(192, '<p>Inden du kan få foretaget kunstig befrugtning på Diers Klinik, skal du have&nbsp; været til en journalsamtale hos os. </p><p>Journalsamtalen foregår i rolige og trygge rammer, og vi tager udgangspunkt&nbsp; i dig og de ønsker, du har til dit forløb. Vi taler om, hvordan det hele kommer&nbsp; til at foregå, og du har mulighed for at få svar på alle de spørgsmål, du har.&nbsp; <br>Det kan eventuelt være en god idé, at du inden samtalen laver en liste med&nbsp; alle dine spørgsmål. Det er vigtigt, at du efter journalsamtalen har en klar&nbsp; forestilling om, hvad der kommer til at ske, og at du føler dig forberedt på din&nbsp; fertilitetsbehandling. </p><p>Du får en grundig instruktion i, hvad du skal gøre, for at vi kan bestemme&nbsp; tidspunktet for din ægløsning. Desuden skal vi have klarlagt dine chancer for at&nbsp; opnå graviditet. </p><p>Til samtalen skal vi også finde ud af, hvilke ønsker du har&nbsp; til sæddonoren, f.eks.: Er det det rigtige for dig at vælge en anonym eller&nbsp; en åben sæddonor, ønsker du ekstra oplysninger om donor og eventuelt at se et&nbsp; babyfoto, og har du nogen specielle ønsker, som vi skal prøve at imødekomme?&nbsp; Eller har du en ven eller bekendt, som du ønsker skal være donor/far til dit&nbsp; barn.</p><p>Du kan bestille en tid til journalsamtale ved at maile eller ringe til&nbsp; os, og vi anbefaler, at vi afholder journalsamtalen senest en måned inden første&nbsp; insemination, men det kan også lade sig gøre med kortere varsel. Bor du langt&nbsp; væk, kan vi tage journalsamtalen over telefonen eller Skype. </p><p>Vi opfordrer til, at du/I har læst vores informationsmateriale grundigt,&nbsp; inden I kommer til samtalen.</p>'),
(197, ''),
(202, ''),
(203, '<h3>Tidlig graviditetsscanning/tryghedscanning</h3><p>På Diers Klinik kan du få foretaget en tidlig graviditetsscanning, dvs. fra&nbsp; uge 7+0 til uge 11+6. Her scanner vi dig for:</p><ul><li>* Hjerteblink <li>* Terminsbestemmelse (endelig termin fastsættes ved nakkefoldscanning på&nbsp; sygehuset i uge 12) <li>* Flerfoldsgraviditet</li></ul><p>Du får endvidere en faglig vurdering og svar på eventuelle spørgsmål.</p><p>Prisen for den tidligere graviditetsscanning er 500 Kr.</p><p>Du er naturligvis velkommen, også selvom du ikke tidligere har været i&nbsp; behandling hos os.</p><p><a href="{CCM:CID_167}" data-concrete5-link-type="ajax">Kontakt os</a> for en tidsbestilling.</p>'),
(204, '<p>Vi tilbyder ultralydsscanning til alle gravide, så du kan følge udviklingen af dit barn</p>'),
(206, ''),
(209, ''),
(213, ''),
(214, '<p>Sæddonorerne, der benyttes kommer fra sædbanker, som lever op til&nbsp; Sundhedsstyrelsens anbefalinger.</p><p>I&nbsp; Diers Klinik har du flere muligheder, hvad angår valg af sæddonor. Vi tilbyder&nbsp; både ”åbne” sæddonorer samt ”non contact” sæddonorer. Hvis du vælger en ”non&nbsp; contact” sæddonor, vil det på intet tidspunkt være muligt for dig eller barnet&nbsp; at få oplysninger om donorens identitet. Hvis du vælger en ”åben” sæddonor, har&nbsp; dit kommende barn mulighed for at få kontakt til donoren, når det fylder 18 år.&nbsp; Det er dog forskelligt, hvad de åbne donorer forpligter sig på afhængig af,&nbsp; hvilken sædbank de donorer ved. Derfor må du selv indhente informationerne fra&nbsp; sædbanken om, hvilke muligheder det kommende barn har for at kontakte&nbsp; sæddonoren, når det fylder 18 år.</p><p>Hos&nbsp; os går vi meget op i, at du selv har mulighed for at vælge donor ud fra de&nbsp; ønsker og behov du måtte have. Du har mulighed for at vælge donorerne enten ud&nbsp; fra en basisprofil eller en udvidet profil. Ved valg af donorer ud fra&nbsp; basisprofilen, får du oplysninger om sæddonorens øjenfarve, højde, vægt,&nbsp; beskæftigelse samt blodtype. Hvor du ved donorer med udvidet profil desuden får&nbsp; oplysninger om f.eks. sygehistorie, familieforhold samt fritidsinteresser. Du&nbsp; får endvidere mulighed for at se babybilleder og høre en stemmeprøve. Det er&nbsp; både muligt at få ”åbne” samt ”non contact” donorer med udvidet&nbsp; profil.</p><p>Sæddonorer&nbsp; har ingen juridiske forpligtelser overfor de børn, der undfanges ved hjælp af&nbsp; sæd fra ham. Derfor har barnet ikke arveret eller andre rettigheder i forhold&nbsp; til donoren.</p><p>I&nbsp; henhold til Sundhedsstyrelsen skal følgende oplysninger gives til kvinder/par,&nbsp; der behandles med sæd fra en sæddonor:</p><p>”Ved&nbsp; udvælgelse af donorer er risiko for videregivelse af arvelige sygdomme,&nbsp; misdannelser m.v. søgt begrænset ved kun at anvende donorer som har oplyst, at&nbsp; de ikke har kendskab til sådanne arverisici i deres slægt, og hvor der af en&nbsp; erfaren sundhedsperson er udspurgt og undersøgt for at belyse dette. Trods disse&nbsp; forsigtighedsregler er enhver arverisiko alligevel ikke udelukket. Hvis barnet&nbsp; mod forventning fejler noget ved fødslen eller i de første leveår, som du får at&nbsp; vide kan være arveligt, er det derfor vigtigt, at du melder tilbage til&nbsp; klinikken eller den sundhedsperson, der har behandlet dig, så der kan tages&nbsp; stilling til, om donor fortsat kan anvendes. Det samme gælder, hvis du får at&nbsp; vide, at det kan dreje sig om smitteoverførsel fra donor-sæd eller donor-æg.&nbsp; Selvom donor er testet fri for overførbare sygdomme for eksempel HIV og&nbsp; hepatitis, er risikoen aldrig nul.”</p><p><o><span>Sæddonorerne er alle testet grundigt for diverse sygdomme&nbsp; HIV, hepatitis, syphilis, CMV og gonoré. Derudover gennemgår donorerne en række&nbsp; fysiske og psykiske undersøgelse. De er alle unge, raske mænd, der oftest er&nbsp; studerende ved højere læreranstalter. </span></o></p><p>I Diers Klinik stiller&nbsp; vi meget strenge krav til kvaliteten af sæden og sikrer os altid, at kvaliteten&nbsp; er god, inden hver enkelt insemination. Hvis du har lyst, er du meget velkommen&nbsp; til at se med i mikroskopet. Hos os bliver sæden altid først klargjort efter, at&nbsp; vi har sikret os, at det er det helt rigtige tidspunkt for insemination.&nbsp; Klargøring af sæden tager fra 5 til 45 minutter.</p><p>Vi&nbsp; har et udvalg af donorer her i klinikken, som vi har købt igennem sædbankerne.&nbsp; Dette betyder, at du ikke selv behøver at købe sæden hjem fra sædbanken og&nbsp; oprette et depot hos os. Du kan derimod reservere et strå af den donor, som du&nbsp; ønsker fra gang til gang uden at være bundet. Du er meget velkommen til at&nbsp; kontakte os, hvis du ønsker at se listen over de donorer, som vi har&nbsp; tilgængelige.</p>'),
(225, ''),
(241, ''),
(242, ''),
(243, ''),
(245, ''),
(247, '<p>Hvis man ønsker at sikre, at barnet kan få sæddonorens identitet at vide i fremtiden, skal man vælge en Ikke-anonym sæddonor.</p><p>Ikke-anonyme sæddonorers identitet kan børn få oplyst, når de er 18 år, hvis de kan dokumentere eller sandsynliggøre, at de er et resultat af behandling med donorsæd fra en bestemt Ikke-anonym sæddonor. Der vil ikke blive stillet ret strenge krav til dokumentation, og i tvivlstilfælde vil identiteten blive udleveret til barnet. Sæddonorerne har accepteret at blive kontaktet af børnene.</p>'),
(254, ''),
(255, '<p>Anonyme sæddonorers identitet holdes hemmelig for altid.</p><h3>Basisprofil</h3><p>Anonyme sæddonorer med enbasisprofil er registrerede med et nummer som 456, 8756, 11250, osv. Basisprofiler har kun oplysning om race, etnicitet, øjenfarve, hårfarve, højde, vægt, blodtype samt oftest erhverv/uddannelse. Man kan dog også vælge at få en udvidet profil.</p><h3>Udvidet profil</h3><p>Sæddonorer med udvidet profil er registrerede med et fiktivt navn som ERIK, IB, PER, OLUF, SVEND, osv. De Udvidede profiler består af op til 8-10 sider personlige oplysninger om sæddonorens baggrund, uddannelse, familiemæssige forhold, interesser, hobbies, etc. Der kan være følgende tillægsoplysninger: personalevurdering, fotos af sæddonor fra hans barndom, en håndskreven hilsen, en lydoptagelse af sæddonors stemme, EQ-profil, m.v.</p>'),
(256, '<p>Anonyme sæddonorers identitet holdes hemmelig for altid.</p><h3>Basisprofil</h3><p>Anonyme sæddonorer med enbasisprofil er registrerede med et nummer som 456, 8756, 11250, osv. Basisprofiler har kun oplysning om race, etnicitet, øjenfarve, hårfarve, højde, vægt, blodtype samt oftest erhverv/uddannelse. Man kan dog også vælge at få en udvidet profil.</p><h3>Udvidet profil</h3><p>Sæddonorer med udvidet profil er registrerede med et fiktivt navn som ERIK, IB, PER, OLUF, SVEND, osv. De Udvidede profiler består af op til 8-10 sider personlige oplysninger om sæddonorens baggrund, uddannelse, familiemæssige forhold, interesser, hobbies, etc. Der kan være følgende tillægsoplysninger: personalevurdering, fotos af sæddonor fra hans barndom, en håndskreven hilsen, en lydoptagelse af sæddonors stemme, EQ-profil, m.v.</p><h3>Basisprofil eller udvidet profil</h3><p>Hvis mange data om sæddonor er vigtig for dig, bør du vælge en sæddonor med Udvidet profil. Singler og par af samme køn, som ofte har et eksplicit ønske om at sikre informationer til børnene, kan have en præference for sæddonorer med Udvidet profil. Hvis du ønsker at vide så lidt som muligt om sæddonoren, skal du vælge en sæddonor med Basisprofil. I mange tilfælde vælger heteroseksuelle par en sæddonor med Basisprofil, hvor singler og par af samme køn vælger en sæddonor med Udvidet profil. Årsagerne er de samme som når man vælger Anonym hhv. Ikke-anonym sæddonor, men ofte er det lige omvendt. Valget er meget individuelt og meget komplekst.</p><p><em>Kilde: Cryos International</em></p>'),
(257, '<p>Hvis man ønsker at sikre, at barnet kan få sæddonorens identitet at vide i fremtiden, skal man vælge en Ikke-anonym sæddonor.</p><p>Ikke-anonyme sæddonorers identitet kan børn få oplyst, når de er 18 år, hvis de kan dokumentere eller sandsynliggøre, at de er et resultat af behandling med donorsæd fra en bestemt Ikke-anonym sæddonor. Der vil ikke blive stillet ret strenge krav til dokumentation, og i tvivlstilfælde vil identiteten blive udleveret til barnet. Sæddonorerne har accepteret at blive kontaktet af børnene.</p><p>Hvis man ønsker at sikre, at barnet kan få sæddonorens identitet at vide i fremtiden, skal man vælge en Ikke-anonym sæddonor. Det ser ud til, at især singler og par af samme køn har præference for Ikke-anonyme sæddonorer, mens heteroseksuelle par synes at have præference for Anonyme sæddonorer, for at beskytte mandens integritet som fader og hermed deres egen interesse som familie. Men det er ikke altid sådan. Nogle heteroseksuelle par vælger netop en Ikke-anonym sæddonor, fordi de planlægger at fortælle barnet om dets tilblivelse, og derfor gerne vil sikre, at der er mulighed for kontakt. Omvendt vælger mange singler en Anonym sæddonor,&nbsp; fordi de planlægger at finde en partner senere, som således måske lettere kan træde ind i faderrollen og adoptere barnet, hvis der ikke er for meget fokus på sæddonors eksistens. Valget er meget individuelt og meget komplekst.</p><p><em>Kilde: Cryos International</em></p>'),
(258, '<p>Sæddonorerne, der benyttes kommer fra sædbanker, som lever op til&nbsp; Sundhedsstyrelsens anbefalinger.</p><p>I&nbsp; Diers Klinik har du flere muligheder, hvad angår valg af sæddonor. Vi tilbyder&nbsp; både <a href="{CCM:CID_179}" data-concrete5-link-type="ajax">”åbne” sæddonorer </a>samt <a href="{CCM:CID_180}" data-concrete5-link-type="ajax">”non contact” sæddonorer</a>. Hvis du vælger en ”non&nbsp; contact” sæddonor, vil det på intet tidspunkt være muligt for dig eller barnet&nbsp; at få oplysninger om donorens identitet. Hvis du vælger en ”åben” sæddonor, har&nbsp; dit kommende barn mulighed for at få kontakt til donoren, når det fylder 18 år.&nbsp; Det er dog forskelligt, hvad de åbne donorer forpligter sig på afhængig af,&nbsp; hvilken sædbank de donorer ved. Derfor må du selv indhente informationerne fra&nbsp; sædbanken om, hvilke muligheder det kommende barn har for at kontakte&nbsp; sæddonoren, når det fylder 18 år.</p><p>Hos&nbsp; os går vi meget op i, at du selv har mulighed for at vælge donor ud fra de&nbsp; ønsker og behov du måtte have. Du har mulighed for at vælge donorerne enten ud&nbsp; fra en basisprofil eller en udvidet profil. Ved valg af donorer ud fra&nbsp; basisprofilen, får du oplysninger om sæddonorens øjenfarve, højde, vægt,&nbsp; beskæftigelse samt blodtype. Hvor du ved donorer med udvidet profil desuden får&nbsp; oplysninger om f.eks. sygehistorie, familieforhold samt fritidsinteresser. Du&nbsp; får endvidere mulighed for at se babybilleder og høre en stemmeprøve. Det er&nbsp; både muligt at få ”åbne” samt ”non contact” donorer med udvidet&nbsp; profil.</p><p>Sæddonorer&nbsp; har ingen juridiske forpligtelser overfor de børn, der undfanges ved hjælp af&nbsp; sæd fra ham. Derfor har barnet ikke arveret eller andre rettigheder i forhold&nbsp; til donoren.</p><p>I&nbsp; henhold til Sundhedsstyrelsen skal følgende oplysninger gives til kvinder/par,&nbsp; der behandles med sæd fra en sæddonor:</p><blockquote>”Ved&nbsp; udvælgelse af donorer er risiko for videregivelse af arvelige sygdomme,&nbsp; misdannelser m.v. søgt begrænset ved kun at anvende donorer som har oplyst, at&nbsp; de ikke har kendskab til sådanne arverisici i deres slægt, og hvor der af en&nbsp; erfaren sundhedsperson er udspurgt og undersøgt for at belyse dette. Trods disse&nbsp; forsigtighedsregler er enhver arverisiko alligevel ikke udelukket. Hvis barnet&nbsp; mod forventning fejler noget ved fødslen eller i de første leveår, som du får at&nbsp; vide kan være arveligt, er det derfor vigtigt, at du melder tilbage til&nbsp; klinikken eller den sundhedsperson, der har behandlet dig, så der kan tages&nbsp; stilling til, om donor fortsat kan anvendes. Det samme gælder, hvis du får at&nbsp; vide, at det kan dreje sig om smitteoverførsel fra donor-sæd eller donor-æg.&nbsp; Selvom donor er testet fri for overførbare sygdomme for eksempel HIV og&nbsp; hepatitis, er risikoen aldrig nul.”</blockquote><p><o><span>Sæddonorerne er alle testet grundigt for diverse sygdomme&nbsp; HIV, hepatitis, syphilis, CMV og gonoré. Derudover gennemgår donorerne en række&nbsp; fysiske og psykiske undersøgelse. De er alle unge, raske mænd, der oftest er&nbsp; studerende ved højere læreranstalter. </span></o></p><p>I Diers Klinik stiller&nbsp; vi meget strenge krav til kvaliteten af sæden og sikrer os altid, at kvaliteten&nbsp; er god, inden hver enkelt insemination. Hvis du har lyst, er du meget velkommen&nbsp; til at se med i mikroskopet. Hos os bliver sæden altid først klargjort efter, at&nbsp; vi har sikret os, at det er det helt rigtige tidspunkt for insemination.&nbsp; Klargøring af sæden tager fra 5 til 45 minutter.</p><p>Vi&nbsp; har et udvalg af donorer her i klinikken, som vi har købt igennem sædbankerne.&nbsp; Dette betyder, at du ikke selv behøver at købe sæden hjem fra sædbanken og&nbsp; oprette et depot hos os. Du kan derimod reservere et strå af den donor, som du&nbsp; ønsker fra gang til gang uden at være bundet. Du er meget velkommen til at&nbsp; kontakte os, hvis du ønsker at se listen over de donorer, som vi har&nbsp; tilgængelige.</p>'),
(265, ''),
(266, '<p>Vil du gerne insemineres med din partners sæd, byder vi jer velkommen til en&nbsp; personlig samtale her på klinikken.</p><p>Før behandlingsstart skal den kommende far have foretaget følgende&nbsp; blodprøver:</p><p>* Hiv 1+2 (anti-Hiv - 1,2)<br>* Hepatitis b virus (HBsAg og Anti HBc)<br>* Hepatitis c virus (Anti-HCV-Ab)</p><p>Vi beder jer være opmærksomme på, at testresultaterne højest må være 12&nbsp; måneder gamle, og at de skal foreligge ved behandlingsstart. </p><p>Derudover skal den kommende far have foretaget en sædanalyse, så vi kan&nbsp; sikre de bedste forudsætninger for en successfuld insemination. Såvel sædprøven&nbsp; til sædanalysen som blodprøverne kan foretages her på klinikken.</p>'),
(267, '<p>Diers Kliniker en&nbsp; fertilitetsklinik i hjertet af Aarhus.</p><p>Vi hjælper primært&nbsp; singler og lesbiske med insemination med donorsæd, men hvis du har en mandlig&nbsp; partner eller ven, der vil donere, så kan vi også hjælpe med&nbsp; det.</p><p>Ved&nbsp; fertilitetsbehandling med donorsæd kan du vælge i mellem flere forskellige typer&nbsp; af donorsæd – anonym donor, åben donor, hvor donor har samtykket til at barnet&nbsp; kan kontakte ham, anonym donor med profil, hvor du får barnebilleder, en lang&nbsp; profil med oplysninger om donor osv. samt åben donor med profil se mere om <a href="{CCM:CID_161}" data-concrete5-link-type="ajax">donor.</a> </p><p>Din chance for at&nbsp; blive gravid, og altså hvor mange forsøg, som du må påregne før du bliver gravid&nbsp; afhænger af din alder. Vores <a href="{CCM:CID_170}" data-concrete5-link-type="ajax">behandlingssuccesrate&nbsp;</a> er blandt de allerbedste i Danmark. </p><p><em><a href="{CCM:CID_167}" data-concrete5-link-type="ajax">Kontakt os</a> til en&nbsp; uforpligtende snak om dine ønsker til en&nbsp; inseminationsbehandling.</em></p><p><em>Vi glæder os til&nbsp; at byde dig velkommen.</p><p></em></p>'),
(268, '<table><tbody><tr><td><p>Journalsamtale:</p></td><td style="text-align: right;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;600 kr.&nbsp; </td></tr><tr><td>Insemination (IUI)&nbsp; med non-contact donor:<br></td><td style="text-align: right;">4.900 kr.</td></tr><tr><td>Insemination&nbsp; (IUI) med non-contact donor med udvidet profil:<br><br></td><td style="text-align: right;">6.300 kr.<br>. </td></tr><tr><td>Insemination (IUI) med&nbsp; åben donor:<br></td><td style="text-align: right;">6.500 kr.<br></td></tr><tr><td>Insemination (IUI) med&nbsp; åben donor med udvidet profil:<br><br></td><td style="text-align: right;">7.900 kr.<br></td></tr><tr><td>Insemination med privat&nbsp; sæd:<br></td><td style="text-align: right;">3.800 kr.<br></td></tr><tr><td><p>Håndteringsgebyr <span>ved modtagelse af privat&nbsp; sæd:<br></span></p></td><td style="text-align: right;">500 kr.</td></tr><tr><td>Samtlige blodprøver (Hiv,&nbsp; Hepatitis B og C):<br></td><td style="text-align: right;">1.300&nbsp; kr.<br></td></tr><tr><td>Sædanalyse:<br></td><td style="text-align: right;">900&nbsp; Kr.<br></td></tr><tr><td>Fragt af&nbsp; donorsæd:<br></td><td style="text-align: right;">1000&nbsp; kr.<br></td></tr><tr><td>Follikelscanning:<br></td><td></td></tr><tr><td>Akupunktur:<br></td><td></td></tr><tr><td>Fotomatch:<br></td><td></td></tr><tr><td>Overnatning i dobbeltværelse:<br></td><td></td></tr><tr><td>Ægløsningstest 5 stk. per&nbsp; pakke:<br></td><td></td></tr><tr><td>Graviditetstest:<br></td><td></td></tr><tr><td>Graviditetsscanning (uge 7+0 til 11+6):<br></td><td></td></tr></tbody></table>'),
(269, '<table><tbody><tr><td>Journalsamtale:<br><br></td><td style="text-align: right;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;600 kr.&nbsp; </td></tr><tr><td>Insemination (IUI)&nbsp; med non-contact donor:<br></td><td style="text-align: right;">4.900 kr.</td></tr><tr><td>Insemination&nbsp; (IUI) med non-contact donor med udvidet profil:<br><br></td><td style="text-align: right;">6.300 kr.<br>. </td></tr><tr><td>Insemination (IUI) med&nbsp; åben donor:<br></td><td style="text-align: right;">6.500 kr.<br></td></tr><tr><td>Insemination (IUI) med&nbsp; åben donor med udvidet profil:<br><br></td><td style="text-align: right;">7.900 kr.<br></td></tr><tr><td><p>Insemination med privat&nbsp; sæd:</p></td><td style="text-align: right;">3.800 kr.<br></td></tr><tr><td></td><td style="text-align: right;"></td></tr><tr><td><p>Håndteringsgebyr <span>ved modtagelse af privat&nbsp; sæd:<br></span></p></td><td style="text-align: right;">500 kr.</td></tr><tr><td>Samtlige blodprøver (Hiv,&nbsp; Hepatitis B og C):<br></td><td style="text-align: right;">1.300&nbsp; kr.<br></td></tr><tr><td>Sædanalyse:<br></td><td style="text-align: right;">900&nbsp; Kr.<br></td></tr><tr><td>Fragt af&nbsp; donorsæd:<br><br></td><td style="text-align: right;">1000&nbsp; kr.<br></td></tr><tr><td>Graviditetsscanning (uge 7+0 til 11+6):<br></td><td style="text-align: right;">500 kr.<br></td></tr><tr><td>Follikelscanning:<br></td><td style="text-align: right;">300&nbsp; kr.<br></td></tr><tr><td>Fotomatch:<br><br></td><td style="text-align: right;">500&nbsp; kr.<br></td></tr><tr><td>Akupunktur:<br></td><td style="text-align: right;">600 kr.</td></tr><tr><td>Ægløsningstest 5 stk. per&nbsp; pakke:<br></td><td style="text-align: right;">150&nbsp; kr.<br></td></tr><tr><td>Graviditetstest:<br><br></td><td style="text-align: right;">50&nbsp; kr.<br></td></tr><tr><td>Overnatning i dobbeltværelse:<br></td><td style="text-align: right;">600&nbsp; kr.<br></td></tr></tbody></table>'),
(271, '<p style="text-align: right;"><table>&nbsp; <tbody><tr> <td>Journalsamtale:<br></td> <td style="text-align: right;">600 kr.</td></tr> <tr> <td>Insemination (IUI)&nbsp; med non-contact donor:</td> <td style="text-align: right;">4.900 kr.</td></tr> <tr> <td>Insemination&nbsp; (IUI) med non-contact donor med udvidet profil:<br><br> </td> <td style="text-align: right;">6.300 kr.</td></tr> <tr> <td>Insemination (IUI) med&nbsp; åben donor:</td> <td style="text-align: right;">6.500 kr.</td></tr> <tr> <td>Insemination (IUI) med&nbsp; åben donor med udvidet profil:<br><br></td> <td style="text-align: right;">7.900 kr.</td></tr> <tr> <td>Insemination med privat&nbsp; sæd:</td> <td style="text-align: right;">3.800 kr.</td></tr> <tr> <td><p>Håndteringsgebyr <span>ved modtagelse af privat&nbsp; sæd:</span></p></td> <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 500 kr.</td></tr> <tr> <td>Samtlige blodprøver (Hiv,&nbsp; Hepatitis B og C):</td> <td style="text-align: right;">1.300&nbsp;kr.</td></tr> <tr> <td>Sædanalyse:</td> <td style="text-align: right;">900&nbsp;kr.</td></tr> <tr> <td>Fragt af&nbsp; donorsæd:<br><br></td> <td style="text-align: right;">1000&nbsp;kr.</td></tr> <tr> <td>Graviditetsscanning&nbsp; (uge 7+0 til 11+6):</td> <td style="text-align: right;">500 kr.</td></tr><tr> <td>Follikelscanning:</td> <td style="text-align: right;">300&nbsp;kr.</td></tr> <tr> <td>Akupunktur:</td> <td style="text-align: right;">600 kr. </td></tr> <tr> <td>Fotomatch:<br></td> <td style="text-align: right;">500&nbsp;kr.</td></tr> <tr> <td></td> <td style="text-align: right;"></td></tr> <tr> <td>Ægløsningstest 5 stk. per&nbsp; pakke:</td> <td style="text-align: right;">150&nbsp;kr.</td></tr> <tr> <td>Graviditetstest:<br><br></td> <td style="text-align: right;">50&nbsp;kr.</td></tr> <tr> <td>Overnatning i  dobbeltværelse: </td> <td style="text-align: right;">600 kr.</td></tr></tbody> </table>'),
(272, '<ol>  <ol>  <li>  <p style="text-align: right;"></p></li></ol><table>&nbsp; <tbody><tr> <td>Journalsamtale:<br><br></td> <td style="text-align: right;">600 kr.</td></tr> <tr> <td>Insemination (IUI)&nbsp; med non-contact donor:</td> <td style="text-align: right;">4.900 kr.</td></tr> <tr> <td>Insemination&nbsp; (IUI) med non-contact donor med udvidet profil:<br><br> </td> <td style="text-align: right;">6.300 kr.</td></tr> <tr> <td>Insemination (IUI) med&nbsp; åben donor:</td> <td style="text-align: right;">6.500 kr.</td></tr> <tr> <td>Insemination (IUI) med&nbsp; åben donor med udvidet profil:<br><br></td> <td style="text-align: right;">7.900 kr.</td></tr> <tr> <td>Insemination med privat&nbsp; sæd:</td> <td style="text-align: right;">3.800 kr.</td></tr> <tr> <td>Håndteringsgebyr <span>ved modtagelse af privat&nbsp; sæd:<br><br></span></td> <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 500 kr.</td></tr> <tr> <td>Samtlige blodprøver (Hiv,&nbsp; Hepatitis B og C):</td> <td style="text-align: right;">1.300&nbsp;kr.</td></tr> <tr> <td>Sædanalyse:</td> <td style="text-align: right;">900&nbsp;kr.</td></tr> <tr> <td><p>Fragt af&nbsp; donorsæd:</p></td> <td style="text-align: right;">1000&nbsp;kr.</td></tr> <tr> <td>Graviditetsscanning&nbsp; (uge 7+0 til 11+6):</td> <td style="text-align: right;">500 kr.</td></tr><tr> <td>Follikelscanning:</td> <td style="text-align: right;">300&nbsp;kr.</td></tr> <tr> <td>Akupunktur:</td> <td style="text-align: right;">600 kr. </td></tr> <tr> <td>Fotomatch:<br><br></td> <td style="text-align: right;">500&nbsp;kr.</td></tr> <tr> <td></td> <td style="text-align: right;"></td></tr> <tr> <td>Ægløsningstest 5 stk. per&nbsp; pakke:</td> <td style="text-align: right;">150&nbsp;kr.</td></tr> <tr> <td>Graviditetstest:<br><br></td> <td style="text-align: right;">50&nbsp;kr.</td></tr> <tr> <td>Overnatning i  dobbeltværelse: </td> <td style="text-align: right;">600 kr.</td></tr></tbody> </table>  </ol>'),
(274, '<h3>Priseksempel</h3><p>Prisen for behandlingen afhænger af, hvilken&nbsp; donorsæd du vælger, og hvor mange forsøg, der skal til før du bliver&nbsp; gravid.</p><p><em>PriseksempelHvis&nbsp; du f.eks. er 33 år og ønsker en inseminationsbehandling med en anonym donor, så&nbsp; kunne prisen se således ud:</em></p><p><em>Journalsamtale: 600&nbsp; DKK<p>Insemination&nbsp;inklusiv anonym donorsæd og inklusiv ultralydsscanning: 4900 DKK</p><p>Når du f.eks. er&nbsp; 33 år, så har du i gennemsnit 26% chance for at blive gravid, dvs. at du må&nbsp; påregne 4 forsøg før det lykkes at få en baby. Regnestykket ser derfor således&nbsp; ud:</p><p>600 DKK + 4 × 4900&nbsp; DKK = 19.600 DKK</p><p><span>Der kommer ingen&nbsp;ekstra gebyrer eller omkostninger på prisen.</span></p></em>'),
(275, '<h3>&nbsp;</h3><h3>Priseksempel</h3><p>Prisen for behandlingen afhænger af, hvilken&nbsp; donorsæd du vælger, og hvor mange forsøg, der skal til før du bliver&nbsp; gravid.</p><p><em>Hvis&nbsp; du f.eks. er 33 år og ønsker en inseminationsbehandling med en anonym donor, så&nbsp; kunne prisen se således ud:</em></p><p><em>Journalsamtale: 600&nbsp; DKK</em></p><em><p>Insemination&nbsp;inklusiv anonym donorsæd og inklusiv ultralydsscanning: 4900 DKK</p><p>Når du f.eks. er&nbsp; 33 år, så har du i gennemsnit 26% chance for at blive gravid, dvs. at du må&nbsp; påregne 4 forsøg før det lykkes at få en baby. </p><p><strong>Regnestykket ser derfor således&nbsp; ud:</strong></p><p>600 DKK + 4 × 4900&nbsp; DKK = 19.600 DKK</p><p><span>Der kommer ingen&nbsp;ekstra gebyrer eller omkostninger på prisen.</span></p></em>'),
(276, ''),
(279, ''),
(300, ''),
(301, '<p>Vi er glade for, at du har kontaktet os.</p><p>Vi vender tilbage med et svar hurtigst muligt.</p>'),
(311, ''),
(317, ''),
(323, ''),
(329, ''),
(331, ''),
(332, '<h3>Åbningstider</h3><p>For at optimere dine chancer for at opnå graviditet holder Diers Klinik åbent alle årets dage   									  								Mandag - Søndag		9.00 - 16.00</p><p>							<a href="#">Læs mere</a></p>'),
(333, '<h2>Start dit forløb i dag! </h2><h2>Vi er her hver dag året rundt.</h2><p>Kontakt os i dag, for en uforpligtende samtale med os, omkring dine muligheder.</p>'),
(334, '<p>For at optimere dine chancer for at opnå graviditet holder Diers  Klinik åbent alle årets dage - også i ferier og på søn- og helligdage.</p><p>På Diers Klinik garanterer vi, at du altid kan blive insemineret i din  aktuelle cyklus, når du har afholdt journalsamtale og har dokumentation for, at  du er ikke har HIV, Hepatitis B og C samt Klamydia.</p><p><strong>Almindelige telefontid</strong></p><p>Mandag til søndag fra kl. 9-16</p><p>I dette tidsrum er du velkommen til at ringe og bestille tid til  journalsamtale eller stille eventuelle spørgsmål til inseminationsforløbet. Hvis  der ikke er flere inseminationer, så går personalet hjem, og svarer ikke længere  telefonen. Vi holder dog altid øje med SMS, mail eller telefonsvarerbeskeder, så  kan du ikke komme igennem på telefonen, så læg en besked, send en e-mail eller  SMS.</p><p><strong>Tlf.: + 45 2022 8587 </strong></p><p>Hvis du har en positiv ægløsningstest i løbet af eftermiddagen  eller aftenen, skal du sende os en sms eller lægge en besked på telefonsvareren,  så kontakter vi dig med en tid til insemination den efterfølgende dag. Hvis du  har en positiv ægløsningstest om morgenen og ønsker en tid den samme dag, kan du  melde dig på telefon, sms eller email indtil kl. 10. </p>'),
(341, ''),
(342, '<p>Det er muligt for vores kunder at leje et værelse til overnatning på Diers  Klinik.<p><strong>Værelset indeholder:</strong></p><p><o>* Dobbeltseng</o> <br>* <o>Spisebord</o> <br>* <o>TV</o> og DVD-afspiller <br>* <o>Trådløst netværksforbindelse</o> <br>* <o>Mikroovn</o> <br>* <o>Køleskab</o> <br>* <o>El-keddel</o></p><p><o>Fra værelset, som er på 10 m2, er der direkte adgang til toilet og bad  samt tekøkken.</o></p><p><o></o><o>Vi gør opmærksom på, at t</o><o>oilet og bad deles med klinikkens  kunder i dagtimerne.</o></p><p><o><strong>Pris pr./overnatning er 600,-<br></strong></o></p><p><o><a href="http://www.hotel-faber.dk/"><u>www.hotel-faber.dk</u></a></o><o><br>Eckersbergsgade 17<br></o><o>8000 Aarhus C<br></o><o>tlf.: 70267011 <br></o><o>Afstand fra klinikken: 2 km. <br></o><o>Der ydes rabat ved henvisning fra Diers Klinik</o></p><p><o><a href="http://www.visitaarhus.dk/danmark/overnatning-i-aarhus" data-concrete5-link-type="image">Se andre overnatningsmuligheder</a></o></p>'),
(343, '<p><a href="http://www.nordiccryobank.com/"><u>www.nordiccryobank.com</u></a><br><a href="http://www.lfub.dk"><u>www.lfub.dk</u></a> (landsforeningen for  ufrivilligt barnløse)<br><a href="http://www.lbl.dk"><u>www.lbl.dk</u></a> (landsforeningen for bøsser og lesbiske)<br><a href="http://www.endo.dk/"><u>www.endo.dk</u></a> (endometrioseforeningen)<br><a href="http://www.pcoinfo.dk/"><u>www.pcoinfo.dk</u></a> (PCO  foreningen)<br><a href="http://www.sundhed.dk/"><u>www.sundhed.dk</u></a> (adgang til  sundhedsvæsenet)<br><a href="http://www.semdk@proboards.com">http://www.semdk@proboards.com</a> (chatforum for selvvalgte  enlige mødre)<br><a href="http://www.fertilitetsselskab.dk/"><u>http://www.fertilitetsselskab.dk/</u></a></p>'),
(350, '');
INSERT INTO `btContentLocal` (`bID`, `content`) VALUES
(351, '<p>Her har vi samlet nyttige artikler og informationer om bøger af relevans for  dig som overvejer inseminationsbehandling eller er gravid.<br> <br>Rapport om at  være barn i en homoseksuel familie:<br><a href="http://www.regeringen.se/"><u>www.regeringen.se</u></a> - læs rapport <a href="http://www.regeringen.se/sb/d/108/a/608"><u>her</u></a>.<p><br>Artikel: donorbørn bør oplyses:<br><a href="http://www.lgbt.dk/"><u>www.lgbt.dk</u></a> - læs artikel <a href="http://www.lgbt.dk/111/?tx_ttnews%5Bmonth%5D=07&amp;tx_ttnews%5Btt_news%5D=2598&amp;tx_ttnews%5Byear%5D=2008&amp;cHash=a0ca4297bb"><u>her</u></a>.</p><p>Lov: særligt børnetilskud ved kunstig befrugtning:<br><a href="http://www.retsinformation.dk/"><u>www.retsinformation.dk</u></a> -  læs lov <a href="http://www.retsinformation.dk/Forms/R0710.aspx?id=122939"><u>her</u></a>.</p><p>Ny, hollandsk undersøgelse viser, at det kan have en positiv effekt at  ligge i 15 minutter efter inseminationen.<br><a href="http://www.dagensmedicin.dk/"><u>www.dagensmedicin.dk</u></a> - læs  undersøgelse <a href="http://www.dagensmedicin.dk/nyheder/2009/11/03/ro-efter-insemination-oger/index.xml"><u>her</u></a>.</p><p><strong>Bøger:</strong><br>"The ultimate guide to pregnancy for lesbians" af Rachel  Pepper<br> <br>Et studie i San Francisco viser at ca. en tredjedel af de børn,  der er undfanget vha. en åben sæddonor vælger at få kontakt med donoren. <br><a href="http://www.medpagetoday.com/"><u>www.medpagetoday.com</u></a> - læs  studie <a href="http://www.medpagetoday.com/MeetingCoverage/ASRM/11783"><u>her</u></a>.<br> <br>Kvinder, som bliver gravide inden for seks  måneder efter abort, har større chancer for at undgå ny abort.<br><a href="http://www.dagensmedicin.dk/"><u>www.dagensmedicin.dk</u></a> - læs  studie <a href="http://www.dagensmedicin.dk/nyheder/2010/08/06/hurtig-graviditet-efter-ab/index.xml"><u>her</u></a>.</p></p>'),
(352, '<p>Her har vi samlet nyttige artikler og informationer om bøger af relevans for  dig som overvejer inseminationsbehandling eller er gravid.<br> <br>Rapport om at  være barn i en homoseksuel familie:<br><a href="http://www.regeringen.se/"><u>www.regeringen.se</u></a> - læs rapport <a href="http://www.regeringen.se/sb/d/108/a/608"><u>her</u></a>.  <p>Lov: særligt børnetilskud ved kunstig befrugtning:<br><a href="http://www.retsinformation.dk/"><u>www.retsinformation.dk</u></a> -  læs lov <a href="http://www.retsinformation.dk/Forms/R0710.aspx?id=122939"><u>her</u></a>.</p><p><strong>Bøger:</strong><br>"The ultimate guide to pregnancy for lesbians" af Rachel  Pepper<br> <br>Et studie i San Francisco viser at ca. en tredjedel af de børn,  der er undfanget vha. en åben sæddonor vælger at få kontakt med donoren. <br><a href="http://www.medpagetoday.com/"><u>www.medpagetoday.com</u></a> - læs  studie <a href="http://www.medpagetoday.com/MeetingCoverage/ASRM/11783"><u>her</u></a>.<br>&nbsp;</p>'),
(353, '<h3>Åbningstider</h3><p>For at optimere dine chancer for at opnå graviditet holder Diers Klinik åbent alle årets dage&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	mandag - møndag		9.00 - 16.00</p><p>							<a href="{CCM:CID_167}" data-concrete5-link-type="ajax">Læs mere</a></p>'),
(354, '<h2>Åbningstider</h2><p>  	For at optimere dine chancer for at opnå graviditet holder Diers Klinik åbent alle årets dage mandag - søndag	9.00 - 16.00  </p>'),
(355, '<h3>Åbningstider</h3><p>For at optimere dine chancer for at opnå graviditet holder Diers Klinik åbent alle årets dage&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	mandag - møndag		9.00 - 16.00</p><p>							<a href="{CCM:CID_185}" data-concrete5-link-type="ajax">Læs mere</a></p>'),
(356, '<h3>Sæddonorerne</h3><p>I  Diers Klinik har du flere muligheder, hvad angår valg af sæddonor. Vi tilbyder  både ”åbne” sæddonorer samt ”non contact” sæddonorer.</p><p>							<a href="{CCM:CID_161}" data-concrete5-link-type="ajax">Læs mere</a></p>'),
(357, '<p>Inden du kan få foretaget&nbsp;insemination på Diers Klinik, skal du have&nbsp;været til en journalsamtale hos os. </p><p>Journalsamtalen foregår i rolige og trygge rammer, og vi tager udgangspunkt&nbsp; i dig og de ønsker, du har til dit forløb. Vi taler om, hvordan det hele kommer&nbsp; til at foregå, og du har mulighed for at få svar på alle de spørgsmål, du har.&nbsp; <br>Det kan eventuelt være en god idé, at du inden samtalen laver en liste med&nbsp; alle dine spørgsmål. Det er vigtigt, at du efter journalsamtalen har en klar&nbsp; forestilling om, hvad der kommer til at ske, og at du føler dig forberedt på din&nbsp; fertilitetsbehandling. </p><p>Du får en grundig instruktion i, hvad du skal gøre, for at vi kan bestemme&nbsp; tidspunktet for din ægløsning. Desuden skal vi have klarlagt dine chancer for at&nbsp; opnå graviditet. </p><p>Til samtalen skal vi også finde ud af, hvilke ønsker du har&nbsp; til sæddonoren, f.eks.: Er det det rigtige for dig at vælge en anonym eller&nbsp; en åben sæddonor, ønsker du ekstra oplysninger om donor og eventuelt at se et&nbsp; babyfoto, og har du nogen specielle ønsker, som vi skal prøve at imødekomme?&nbsp; Eller har du en ven eller bekendt, som du ønsker skal være donor/far til dit&nbsp; barn.</p><p>Du kan bestille en tid til journalsamtale ved at maile eller ringe til&nbsp; os, og vi anbefaler, at vi afholder journalsamtalen senest en måned inden første&nbsp; insemination, men det kan også lade sig gøre med kortere varsel. Bor du langt&nbsp; væk, kan vi tage journalsamtalen over telefonen eller Skype. </p><p>Vi opfordrer til, at du/I har læst vores informationsmateriale grundigt,&nbsp; inden I kommer til samtalen.</p>'),
(358, '<h4><p>For at optimere dine chancer for at opnå graviditet holder Diers Klinik åbent alle årets dage mandag - søndag	9.00 - 16.00</p>&nbsp;- men du kan kontakte os døgnet rundt via nedenståend kontaktformulaer</h4>'),
(359, '<h4>  <p>For at optimere dine chancer for at opnå graviditet holder Diers Klinik åbent alle årets dage mandag - søndag	9.00 - 16.00 - men du kan kontakte os døgnet rundt via kontaktformularen eller via sms på 2022 8587</p></h4>'),
(360, '<p>Diers Klinik er en&nbsp; fertilitetsklinik i hjertet af Aarhus.</p><p>Vi hjælper primært&nbsp;singler og lesbiske med insemination med donorsæd, men hvis du har en mandlig&nbsp; partner eller ven, der vil donere, så kan vi også hjælpe med&nbsp; det.</p><p>Ved&nbsp; fertilitetsbehandling med donorsæd kan du vælge i mellem flere forskellige typer&nbsp; af donorsæd – anonym donor, åben donor, hvor donor har samtykket til at barnet&nbsp; kan kontakte ham, anonym donor med profil, hvor du får barnebilleder, en lang&nbsp; profil med oplysninger om donor osv. samt åben donor med profil se mere om <a href="{CCM:CID_161}" data-concrete5-link-type="ajax">donor.</a> </p><p>Din chance for at&nbsp; blive gravid, og altså hvor mange forsøg, som du må påregne før du bliver gravid&nbsp; afhænger af din alder. Vores <a href="{CCM:CID_170}" data-concrete5-link-type="ajax">behandlingssuccesrate&nbsp;</a> er blandt de allerbedste i Danmark. </p><p><em><a href="{CCM:CID_167}" data-concrete5-link-type="ajax">Kontakt os</a> til en&nbsp; uforpligtende snak om dine ønsker til en&nbsp; inseminationsbehandling.</em></p><p><em>Vi glæder os til&nbsp; at byde dig velkommen.</em></p>'),
(361, '<p><concrete-picture fID="2" alt="" style="float: right; width: 148px; margin: 0px 0px 10px 10px;" /><strong>Liza Diers, Jordemoder<br></strong>Jeg hedder Liza Diers og har startet Diers Klinik. Min  motivation for at åbne fertilitetsklinikken er, at jeg ønsker at hjælpe enhver  kvinde eller ethvert par - single, lesbisk eller heteroseksuel med at blive  gravid.</p>'),
(362, '<p><strong>Kristina</strong> <strong>Sørensen</strong><strong>, Sygeplejerske<br></strong>Jeg hedder Kristina Sørensen. Efter endt sygeplerskestudie har jeg læst en kandidat i sygeplejevidenskab, hvor jeg har specialiseret mig i donorbørn. Jeg afholder journalsamtaler, scanner for ægløsning og udfører inseminationer.</p><p>Jeg er på barselsorlov fra juni 2014 til sommeren 2015.</p>'),
(363, '<p><strong>Janni Meisner, Jordemoder<br></strong>Jeg hedder Janni Meisner. Udover at være her på klinikken er jeg timevikar på Skejby Fødeafdeling. Jeg afholder journalsamtaler, scanner for ægløsning og udfører inseminationer.</p>'),
(364, '<p><strong>Anne Louise Richardt Nielsen, Jordemoder<br></strong>Jeg hedder Anne Louise Richardt Nielsen. Jeg afholder journalsamtaler, scanner og udfører inseminationer. Jeg har tidligere arbejdet på fødegangen på Aarhus Universitetshospital, Skejby, hvor jeg fortsat er timeafløser.</p>'),
(365, '<p><strong>Louise Mabire, Jordemoder<br></strong>Jeg hedder Louise Mabirer. Jeg scanner for ægløsning og udfører inseminationer. Udover arbejdet på klinikken, arbejder jeg også som jordemoder på Horsens Fødeafdeling.&nbsp;</p><p>Jeg er på barsel fra august 2014.</p>'),
(366, '<p><strong>Anne Sofie Bøtcher, Sygeplejerske<br></strong>Jeg hedder Anne Sofie Bøtcher. Jeg scanner for ægløsning og udfører inseminationer. Ved siden af arbejdet på klinikken er jeg ved at tage en kandidatgrad i Sygeplejevidenskab ved Aarhus Universitetshospital.</p>'),
(367, '<p>Sæddonorerne, der benyttes kommer fra sædbanker, som lever op til Sundhedsstyrelsens anbefalinger.</p><p>I&nbsp; Diers Klinik har du flere muligheder, hvad angår valg af sæddonor. Vi tilbyder&nbsp; både <a href="{CCM:CID_179}" data-concrete5-link-type="ajax">”åbne” sæddonorer </a>samt <a href="{CCM:CID_180}" data-concrete5-link-type="ajax">”non contact” sæddonorer</a>. Hvis du vælger en ”non&nbsp; contact” sæddonor, vil det på intet tidspunkt være muligt for dig eller barnet&nbsp; at få oplysninger om donorens identitet. Hvis du vælger en ”åben” sæddonor, har&nbsp; dit kommende barn mulighed for at få kontakt til donoren, når det fylder 18 år.&nbsp; Det er dog forskelligt, hvad de åbne donorer forpligter sig på afhængig af,&nbsp; hvilken sædbank de donorer ved. Derfor må du selv indhente informationerne fra&nbsp; sædbanken om, hvilke muligheder det kommende barn har for at kontakte&nbsp; sæddonoren, når det fylder 18 år.</p><p>Hos&nbsp; os går vi meget op i, at du selv har mulighed for at vælge donor ud fra de&nbsp; ønsker og behov du måtte have. Du har mulighed for at vælge donorerne enten ud&nbsp; fra en basisprofil eller en udvidet profil. Ved valg af donorer ud fra&nbsp; basisprofilen, får du oplysninger om sæddonorens øjenfarve, højde, vægt,&nbsp; beskæftigelse samt blodtype. Hvor du ved donorer med udvidet profil desuden får&nbsp; oplysninger om f.eks. sygehistorie, familieforhold samt fritidsinteresser. Du&nbsp; får endvidere mulighed for at se babybilleder og høre en stemmeprøve. Det er&nbsp; både muligt at få ”åbne” samt ”non contact” donorer med udvidet&nbsp; profil.</p><p>Sæddonorer&nbsp; har ingen juridiske forpligtelser overfor de børn, der undfanges ved hjælp af&nbsp; sæd fra ham. Derfor har barnet ikke arveret eller andre rettigheder i forhold&nbsp; til donoren.</p><p>I&nbsp; henhold til Sundhedsstyrelsen skal følgende oplysninger gives til kvinder/par,&nbsp; der behandles med sæd fra en sæddonor:</p><blockquote>”Ved&nbsp; udvælgelse af donorer er risiko for videregivelse af arvelige sygdomme,&nbsp; misdannelser m.v. søgt begrænset ved kun at anvende donorer som har oplyst, at&nbsp; de ikke har kendskab til sådanne arverisici i deres slægt, og hvor der af en&nbsp; erfaren sundhedsperson er udspurgt og undersøgt for at belyse dette. Trods disse&nbsp; forsigtighedsregler er enhver arverisiko alligevel ikke udelukket. Hvis barnet&nbsp; mod forventning fejler noget ved fødslen eller i de første leveår, som du får at&nbsp; vide kan være arveligt, er det derfor vigtigt, at du melder tilbage til&nbsp; klinikken eller den sundhedsperson, der har behandlet dig, så der kan tages&nbsp; stilling til, om donor fortsat kan anvendes. Det samme gælder, hvis du får at&nbsp; vide, at det kan dreje sig om smitteoverførsel fra donor-sæd eller donor-æg.&nbsp; Selvom donor er testet fri for overførbare sygdomme for eksempel HIV og&nbsp; hepatitis, er risikoen aldrig nul.”</blockquote><p><o><span>Sæddonorerne er alle testet grundigt for diverse sygdomme&nbsp; HIV, hepatitis, syphilis, CMV og gonoré. Derudover gennemgår donorerne en række&nbsp; fysiske og psykiske undersøgelse. De er alle unge, raske mænd, der oftest er&nbsp; studerende ved højere læreranstalter. </span></o></p><p>I Diers Klinik stiller&nbsp; vi meget strenge krav til kvaliteten af sæden og sikrer os altid, at kvaliteten&nbsp; er god, inden hver enkelt insemination. Hvis du har lyst, er du meget velkommen&nbsp; til at se med i mikroskopet. Hos os bliver sæden altid først klargjort efter, at&nbsp; vi har sikret os, at det er det helt rigtige tidspunkt for insemination.&nbsp; Klargøring af sæden tager fra 5 til 45 minutter.</p><p>Vi&nbsp; har et udvalg af donorer her i klinikken, som vi har købt igennem sædbankerne.&nbsp; Dette betyder, at du ikke selv behøver at købe sæden hjem fra sædbanken og&nbsp; oprette et depot hos os. Du kan derimod reservere et strå af den donor, som du&nbsp; ønsker fra gang til gang uden at være bundet. Du er meget velkommen til at&nbsp; kontakte os, hvis du ønsker at se listen over de donorer, som vi har&nbsp; tilgængelige.</p>'),
(368, ''),
(369, '<p><strong></strong></p><p><concrete-picture fID="7" alt="" style="float: right; margin: 0px 0px 10px 10px;" /></p><p><strong>Liza Diers, Jordemoder<br></strong>Jeg hedder Liza Diers og har startet Diers Klinik. Min  motivation for at åbne fertilitetsklinikken er, at jeg ønsker at hjælpe enhver  kvinde eller ethvert par - single, lesbisk eller heteroseksuel med at blive  gravid.</p>'),
(371, '<h2>Mit Navn er Liza Diers</h2><p>Jeg hedder Liza Diers, jeg er jordemoder og har startet Diers Klinik. Min motivation for at åbne fertilitetsklinikken er, at jeg ønsker at hjælpe enhver kvinde eller ethvert par - single, lesbisk eller heteroseksuel -med at blive gravid.   						</p><p>						De kvinder, som bliver inseminerede på Diers Klinik, ligger mig meget på sinde, og jeg er dybt engageret i det, jeg laver. Derfor vil du altid få en personlig, individuel vejledning og behandling på Diers Klinik. Jeg elsker mit fag og har løbende og meget tæt kontakt med de kvinder, jeg inseminerer - også efter opnået graviditet, hvor der ofte melder sig en række spørgsmål.  						</p><p>					Jeg har selv gennemgået fertilitetsbehandling på klinikken og har tre donorbørn.</p>'),
(372, '<p><strong></strong></p><p><concrete-picture fID="5" alt="" style="float: right; margin: 0px 0px 10px 10px;" /></p><p><strong>Kristina</strong> <strong>Sørensen</strong><strong>, Sygeplejerske<br></strong>Jeg hedder Kristina Sørensen. Efter endt sygeplerskestudie har jeg læst en kandidat i sygeplejevidenskab, hvor jeg har specialiseret mig i donorbørn. Jeg afholder journalsamtaler, scanner for ægløsning og udfører inseminationer.</p><p>Jeg er på barselsorlov fra juni 2014 til sommeren 2015.</p>'),
(373, '<p><strong></strong></p><p><concrete-picture fID="6" alt="" style="float: right; margin: 0px 0px 10px 10px;" /></p><p><strong>Janni Meisner, Jordemoder<br></strong>Jeg hedder Janni Meisner. Udover at være her på klinikken er jeg timevikar på Skejby Fødeafdeling. Jeg afholder journalsamtaler, scanner for ægløsning og udfører inseminationer.</p>'),
(374, '<p><strong></strong></p><p><concrete-picture fID="3" alt="" style="float: right; margin: 0px 0px 10px 10px;" /></p><p><strong>Anne Louise Richardt Nielsen, Jordemoder<br></strong>Jeg hedder Anne Louise Richardt Nielsen. Jeg afholder journalsamtaler, scanner og udfører inseminationer. Jeg har tidligere arbejdet på fødegangen på Aarhus Universitetshospital, Skejby, hvor jeg fortsat er timeafløser.</p>'),
(375, '<p><strong></strong></p><p><concrete-picture fID="4" alt="" style="float: right; margin: 0px 0px 10px 10px;" /></p><p><strong>Anne Sofie Bøtcher, Sygeplejerske<br></strong>Jeg hedder Anne Sofie Bøtcher. Jeg scanner for ægløsning og udfører inseminationer. Ved siden af arbejdet på klinikken er jeg ved at tage en kandidatgrad i Sygeplejevidenskab ved Aarhus Universitetshospital.</p>'),
(385, '<h2 style="text-align: center;">HVAD SIGER VORES KUNDER OM OS</h2>'),
(387, '<h1 style="text-align: center;">Velkommen til Diers Klinik. </h1><h1 style="text-align: center;">Vi er der hele vejen for dig.</h1><p>Jeg hedder Liza Diers, jeg er jordemoder og har startet Diers Klinik. Min motivation for at åbne fertilitetsklinikken er, at jeg ønsker at hjælpe enhver kvinde eller ethvert par - single, lesbisk eller heteroseksuel -med at blive gravid. </p><p> De kvinder, som bliver inseminerede på Diers Klinik, ligger mig meget på sinde, og jeg er dybt engageret i det, jeg laver. Derfor vil du altid få en personlig, individuel vejledning og behandling på Diers Klinik. Jeg elsker mit fag og har løbende og meget tæt kontakt med de kvinder, jeg inseminerer - også efter opnået graviditet, hvor der ofte melder sig en række spørgsmål. </p><p> Jeg har selv gennemgået fertilitetsbehandling på klinikken og har tre donorbørn.</p>'),
(388, '<p>J</p><br>'),
(390, ''),
(391, ''),
(398, ''),
(401, ''),
(406, ''),
(407, ''),
(408, '<p>Jeg hedder Liza Diers, jeg er jordemoder og har startet Diers Klinik. Min motivation for at åbne fertilitetsklinikken er, at jeg ønsker at hjælpe enhver kvinde eller ethvert par - single, lesbisk eller heteroseksuel -med at blive gravid.<br></p><p> De kvinder, som bliver inseminerede på Diers Klinik, ligger mig meget på sinde, og jeg er dybt engageret i det, jeg laver. Derfor vil du altid få en personlig, individuel vejledning og behandling på Diers Klinik. Jeg elsker mit fag og har løbende og meget tæt kontakt med de kvinder, jeg inseminerer - også efter opnået graviditet, hvor der ofte melder sig en række spørgsmål. </p><p> Jeg har selv gennemgået fertilitetsbehandling på klinikken og har tre donorbørn.</p>'),
(409, '<h1 style="text-align: center;">Velkommen til Diers Klinik.</h1><h1 style="text-align: center;">Vi er der hele vejen for dig.</h1>'),
(413, '<p><strong>Vi er rigtig glade for de mange fine anmeldelser, vi har fået.</strong></p><p>Du er meget velkommen til at skrive en anmeldelse i feltet nederst på siden.</p><p>På forhånd tak.<br></p>'),
(415, '<p>Har du yderligere spørgsmål, eller er du interesseret i at påbegynde et inseminationsforløb hos os eller at få tilsendt yderligere informationsmateriale, er du meget velkommen til at kontakte os via nedenstående formular eller på telefon nr. 20 22 8587.</p>'),
(416, '<p>Du finder os på følgende adresse:</p><p>Diers Klinik<br>Grønnegade 56, 1<br>8000 Aarhus C</p><p>Tlf.: 20 22 85 87<br></p>'),
(417, '<p>Diers Klinik er en fertilitetsklinik i hjertet af Aarhus.</p><p>Vi hjælper primært singler og lesbiske med insemination med donorsæd, men hvis du har en mandlig partner eller ven, der vil donere, så kan vi også hjælpe med det.</p><p>Ved fertilitetsbehandling med donorsæd kan du vælge i mellem flere forskellige typer af donorsæd – anonym donor, åben donor, hvor donor har samtykket til at barnet kan kontakte ham, anonym donor med profil, hvor du får barnebilleder, en lang  profil med oplysninger om donor osv. samt åben donor med profil se mere om <a href="{CCM:CID_161}" data-concrete5-link-type="ajax">donor.</a> </p><p>Din chance for at blive gravid, og altså hvor mange forsøg, som du må påregne før du bliver gravid afhænger af din alder. Vores <a href="{CCM:CID_170}" data-concrete5-link-type="ajax">behandlingssuccesrate </a>er blandt de allerbedste i Danmark. </p><p><em><a href="{CCM:CID_167}" data-concrete5-link-type="ajax">Kontakt os</a> til en uforpligtende snak om dine ønsker til en inseminationsbehandling.</em></p><p><em>Vi glæder os til at byde dig velkommen.</em></p>'),
(418, '<p>På Diers Klinik er vi specialister i inseminationsbehandling. Vi har en af de  bedste succesrater i landet, og vores inseminationsbehandlinger bliver oftest  udført i naturlig cyklus, dvs. uden brug af hormoner eller anden medicin.</p><p>Vi kan lave insemination med både donorsæd eller med din partners sæd. Vi kan  også nedfryse sæd til senere insemination, hvis din partner ikke kan være  tilstede på selve inseminationsdagen.</p><p>På de næste sider kan du læse om, hvordan forløbet vil være.</p>'),
(419, '<p>Inden du kan få foretaget insemination på Diers Klinik, skal du have været til en journalsamtale hos os. </p><p>Journalsamtalen foregår i rolige og trygge rammer, og vi tager udgangspunkt i dig og de ønsker, du har til dit forløb. Vi taler om, hvordan det hele kommer til at foregå, og du har mulighed for at få svar på alle de spørgsmål, du har.  <br>Det kan eventuelt være en god idé, at du inden samtalen laver en liste med alle dine spørgsmål. Det er vigtigt, at du efter journalsamtalen har en klar  forestilling om, hvad der kommer til at ske, og at du føler dig forberedt på din fertilitetsbehandling. </p><p>Du får en grundig instruktion i, hvad du skal gøre, for at vi kan bestemme tidspunktet for din ægløsning. Desuden skal vi have klarlagt dine chancer for at opnå graviditet. </p><p>Til samtalen skal vi også finde ud af, hvilke ønsker du har til sæddonoren, f.eks.: Er det det rigtige for dig at vælge en anonym eller en åben sæddonor, ønsker du ekstra oplysninger om donor og eventuelt at se et  babyfoto, og har du nogen specielle ønsker, som vi skal prøve at imødekomme? Eller har du en ven eller bekendt, som du ønsker skal være donor/far til dit barn.</p><p>Du kan bestille en tid til journalsamtale ved at maile eller ringe til  os, og vi anbefaler, at vi afholder journalsamtalen senest en måned inden første  insemination, men det kan også lade sig gøre med kortere varsel. Bor du langt  væk, kan vi tage journalsamtalen over telefonen eller Skype. </p><p>Vi opfordrer til, at du/I har læst vores informationsmateriale grundigt, inden I kommer til samtalen.</p>'),
(420, '<h3>Du kan selv øge chancerne for at blive gravid</h3><p>Sundhedsstyrelsen anbefaler, at man indtager 400 mygram folinsyre om dagen, når man planlægger at blive gravid. </p><p>Derudover bør man spise en sund og varieret kost. Hvis den mad, man spiser, indeholder en masse frugt og grønt - mindst 6 stk. om dagen, er der ingen grund til at spise yderligere kosttilskud, mens man prøver at opnå graviditet. </p><h3>Andre gode råd, der øger chancerne</h3><p>* Rygning nedsætter fertiliteten væsentligt. Derudover har man større risiko  for at abortere, hvis man ryger. Man bør derfor stoppe med at ryge inden insemination. </p><p>* Sundhedsstyrelsen anbefaler, at man ikke drikker alkohol under graviditet, og når man forsøger at blive gravid. </p><p>* Der er lidt uenighed om, hvorvidt kaffe og koffeinindtag har indvirkning på fertiliteten. Flere undersøgelser peger i forskellige retninger. For at være på  den sikre side, bør man derfor ikke drikke mere end svarende til 200mg koffein dagligt. Vær opmærksom på, at der kan være stor forskel på indholdet af koffein i kaffe.    </p><p>* Overvægt har også indvirkning på fertiliteten. Fedtvævet bevirker nemlig, at  der dannes for meget mandligt kønshormon, som virker forstyrrende på  hormonbalancen.<br><br>Man siger, at fertiliteten kan blive påvirket på et Body Mass  Index (BMI) på 27 eller derover. BMI beregnes på følgende måde: Din vægt i kilo  divideret med din højde mål i meter i anden. Hvis du f.eks. vejer 65 kg. og er 170 cm. ser regnestykket således ud: 65/(1,70x1,70) = 22,49 BMI. Hvis din fertilitet er påvirket, vil det f.eks. give sig udslag i en lang  menstruationscyklus. Mange kvinder har forhøjet BMI uden, at det har indflydelse på fertiliteten.</p>'),
(421, '<p>Sæddonorerne, der benyttes kommer fra sædbanker, som lever op til Sundhedsstyrelsens anbefalinger.</p><p>I Diers Klinik har du flere muligheder, hvad angår valg af sæddonor. Vi tilbyder  både <a href="{CCM:CID_179}" data-concrete5-link-type="ajax">”åbne” sæddonorer </a>samt <a href="{CCM:CID_180}" data-concrete5-link-type="ajax">”non contact” sæddonorer</a>. Hvis du vælger en ”non  contact” sæddonor, vil det på intet tidspunkt være muligt for dig eller barnet at få oplysninger om donorens identitet. Hvis du vælger en ”åben” sæddonor, har dit kommende barn mulighed for at få kontakt til donoren, når det fylder 18 år.  Det er dog forskelligt, hvad de åbne donorer forpligter sig på afhængig af, hvilken sædbank de donorer ved. Derfor må du selv indhente informationerne fra sædbanken om, hvilke muligheder det kommende barn har for at kontakte  sæddonoren, når det fylder 18 år.</p><p>Hos os går vi meget op i, at du selv har mulighed for at vælge donor ud fra de  ønsker og behov du måtte have. Du har mulighed for at vælge donorerne enten ud  fra en basisprofil eller en udvidet profil. Ved valg af donorer ud fra  basisprofilen, får du oplysninger om sæddonorens øjenfarve, højde, vægt,  beskæftigelse samt blodtype. Hvor du ved donorer med udvidet profil desuden får oplysninger om f.eks. sygehistorie, familieforhold samt fritidsinteresser. Du får endvidere mulighed for at se babybilleder og høre en stemmeprøve. Det er både muligt at få ”åbne” samt ”non contact” donorer med udvidet  profil.</p><p>Sæddonorer har ingen juridiske forpligtelser overfor de børn, der undfanges ved hjælp af  sæd fra ham. Derfor har barnet ikke arveret eller andre rettigheder i forhold til donoren.</p><p>I henhold til Sundhedsstyrelsen skal følgende oplysninger gives til kvinder/par, der behandles med sæd fra en sæddonor:</p><blockquote>”Ved  udvælgelse af donorer er risiko for videregivelse af arvelige sygdomme,  misdannelser m.v. søgt begrænset ved kun at anvende donorer som har oplyst, at  de ikke har kendskab til sådanne arverisici i deres slægt, og hvor der af en erfaren sundhedsperson er udspurgt og undersøgt for at belyse dette. Trods disse  forsigtighedsregler er enhver arverisiko alligevel ikke udelukket. Hvis barnet  mod forventning fejler noget ved fødslen eller i de første leveår, som du får at  vide kan være arveligt, er det derfor vigtigt, at du melder tilbage til  klinikken eller den sundhedsperson, der har behandlet dig, så der kan tages  stilling til, om donor fortsat kan anvendes. Det samme gælder, hvis du får at  vide, at det kan dreje sig om smitteoverførsel fra donor-sæd eller donor-æg.  Selvom donor er testet fri for overførbare sygdomme for eksempel HIV og  hepatitis, er risikoen aldrig nul.”</blockquote><p><o>Sæddonorerne er alle testet grundigt for diverse sygdomme  HIV, hepatitis, syphilis, CMV og gonoré. Derudover gennemgår donorerne en række  fysiske og psykiske undersøgelse. De er alle unge, raske mænd, der oftest er  studerende ved højere læreanstalter. </o></p><p>I Diers Klinik stiller  vi meget strenge krav til kvaliteten af sæden og sikrer os altid, at kvaliteten  er god, inden hver enkelt insemination. Hvis du har lyst, er du meget velkommen  til at se med i mikroskopet. Hos os bliver sæden altid først klargjort efter, at  vi har sikret os, at det er det helt rigtige tidspunkt for insemination. Klargøring af sæden tager fra 5 til 45 minutter.</p><p>Vi  har et udvalg af donorer her i klinikken, som vi har købt igennem sædbankerne. Dette betyder, at du ikke selv behøver at købe sæden hjem fra sædbanken og  oprette et depot hos os. Du kan derimod reservere et strå af den donor, som du  ønsker fra gang til gang uden at være bundet. Du er meget velkommen til at  kontakte os, hvis du ønsker at se listen over de donorer, som vi har  tilgængelige.</p>'),
(422, '<h3><br> </h3><h3>Priseksempel</h3><p>Prisen for behandlingen afhænger af, hvilken  donorsæd du vælger, og hvor mange forsøg, der skal til før du bliver  gravid.</p><p><em>Hvis  du f.eks. er 33 år og ønsker en inseminationsbehandling med en anonym donor, så  kunne prisen se således ud:</em></p><p><em>Journalsamtale: 600  DKK</em></p><em><p>Insemination inklusiv anonym donorsæd og inklusiv ultralydsscanning: 4900 DKK</p><p>Når du f.eks. er  33 år, så har du i gennemsnit 26% chance for at blive gravid, dvs. at du må  påregne 4 forsøg før det lykkes at få en baby. </p><p><strong>Regnestykket ser derfor således  ud:</strong></p><p>600 DKK + 4 × 4900  DKK = 19.600 DKK</p><p>Der kommer ingen ekstra gebyrer eller omkostninger på prisen.</p></em>'),
(423, '<p data-redactor-inserted-image="true"><img rel="float: right; margin: 0px 0px 10px 10px;" alt="" style="float: right; margin: 0px 0px 10px 10px;" id="image-marker" src="{CCM:FID_DL_8}"></p><p><strong>Louise Mabire, Jordemoder<br></strong>Jeg hedder Louise Mabirer. Jeg scanner for ægløsning og udfører inseminationer. Udover arbejdet på klinikken, arbejder jeg også som jordemoder på Horsens Fødeafdeling. </p><p>Jeg er på barsel fra august 2014.</p>'),
(424, '<p>Diers Klinik er en fertilitets klinik, der tilbyder donorinsemination til par, singlekvinder og lesbiske par.  			</p><p>			Diers Klinik<br>  Grønnegade 56, 1. <br>  8000 Aarhus C</p><p>Tlf. +45 20 22 85 87 <br>  E-mail: <a href="mailto:info@diersklinik.dk" data-concrete5-link-type="image">info@diersklinik.dk</a></p>'),
(426, '<h3>Insemination</h3><p>Når det er tid til at blive insemineret, ringer du til klinikken og sammen aftaler vi et tidspunkt for dette</p><p>							<a href="{CCM:CID_159}">Læs mere</a></p>'),
(427, '<p data-redactor-inserted-image="true"><img style="float: right; margin: 0px 0px 10px 10px;" rel="float: right; margin: 0px 0px 10px 10px;" alt="" id="image-marker" src="{CCM:FID_DL_8}"></p><p><strong>Louise Mabire, Jordemoder<br></strong>Jeg hedder Louise Mabire. Jeg scanner for ægløsning og udfører inseminationer. Udover arbejdet på klinikken, arbejder jeg også som jordemoder på Horsens Fødeafdeling. </p><p>Jeg er på barsel fra august 2014.</p>'),
(428, '<p>På Diers klinik holder vi løbende statistik med vores inseminationsbehandlinger. </p> <p>Dine graviditetschancer afhænger meget af din alder. </p><p>Til du fylder 35 år er chancerne 26 % pr. insemination. <br>Fra 35-39 år er den 20 % pr. insemination. <br>Er du mellem 40 og 42 er chancen 10 % pr. insemination. </p><p>Statistikken er baseret på tal fra 2014.</p>'),
(430, ''),
(431, '<hr><p data-redactor-inserted-image="true"><img rel="float: right; margin: 0px 0px 10px 10px;" alt="" style="float: right; margin: 0px 0px 10px 10px;" id="image-marker" src="{CCM:FID_DL_9}"></p><p><strong>Lise Friis</strong><br>Jeg hedder Lise Friis og er Diers Kliniks tysktalende medarbejder, som sørger for en god start for vores tyske klienter. Udover at varetage journalsamtaler på tysk, er jeg også ansat som IT-ansvarlig på klinikken.</p>'),
(438, ''),
(439, '<p><strong>Har du/I en ven eller bekendt, som ønsker at være kendt donor? </strong></p><p>Hvis du ønsker at bruge en kendt donor, så kan vi også hjælpe jer med det.</p><p>Donoren skal i princippet efter loven behandles som en sæddonor, idet I ikke er et par. Derfor skal han svare på en række spørgsmål ang. hans helbredstilstand og familiesygehistorie. Han skal desuden testes for HIV 1+2, hepatitis B og C, klamydie, gonorré og syfilis. Hans sæd skal fryses ned på klinikken inden behandlingen.</p><p>Inden inseminationen skal du/I underskrive et informeret samtykke, hvor du/I accepterer de ricisi, der måtte være ved at benytte den kendte donor.</p><p><a href="{CCM:CID_167}">Kontakt os</a>, hvis du ønsker yderligere information.</p>'),
(440, '<p> <strong>På Diers Klinik gør vi alt hvad vi kan for at gøre vores succesrate så høj som muligt. Vi har den højeste succesrate i landet for inseminationsbehandling med donorsæd.</strong></p><p>Vi gør meget ud af at finde det helt rigtige tidspunkt for inseminationen, og vi scanner altid gratis inden behandlingen for at sikre bedst mulige betingelser for at behandlingen skal lykkes.</p><p>Og vores anstrengelser bærer frugt. Vi har den højeste succesrate i landet for inseminationsbehandling med donorsæd.<br> <br>Her kan du se vores tal sammenlignet med landsgennemsnittet for alle klinikker i Danmark.</p><p><br>På Diers klinik holder vi løbende statistik med vores inseminationsbehandlinger. </p> <p>Dine graviditetschancer afhænger meget af din alder. </p><p>Til du fylder 35 år er chancerne 26 % pr. insemination. <br>Fra 35-39 år er den 20 % pr. insemination. <br>Er du mellem 40 og 42 er chancen 10 % pr. insemination. </p><p>Statistikken er baseret på tal fra 2014.</p>'),
(442, '<h3>Priser</h3><p>Diers Klinik har konkurrencedygtige priser og hos os kommer der ingen ekstra gebyr på. Vi har priser på insemination fra kr. 3800 incl. scanning.<br></p><p>							<a href="{CCM:CID_164}" data-concrete5-link-type="ajax">Læs mere</a></p>'),
(443, '<h3>Høj succesrate</h3><p> Vi har den højeste succesrate i landet for inseminationsbehandling med donorsæd og vi gør alt for at den rate stiger.</p><p>							<a href="{CCM:CID_170}" data-concrete5-link-type="ajax">Læs mere</a></p>'),
(449, ''),
(450, '<p>Hvis I er et lesbisk par der ønsker et barn, så skal den kvindelige part, der ikke bærer barnet registreres som medmoder. </p><p>Et medmoderskab kan kun fastslås, hvis to kvinder får et barn sammen ved hjælp af kunstig befrugtning. Graviditet ad anden vej er ikke gyldig, hvis en kvinde skal have medmoderskab.</p><p>Hvis man skal fastslå et medmoderskab/faderskab, skal proceduren begynde <i><u>inden</u></i> selve fertilitetsbehandlingen.</p><p>Reglerne betyder, at medmoren får de samme rettigheder og pligter over for barnet.</p><p>Man skal være opmærksom på, at hvis der er tale om en kendt donor, anses donoren som udgangspunkt for at være barnets far. Donoren, den kommende mor og morens partner (ægtefælle, registreret partner eller kæreste) kan aftale, at donoren ikke skal have status som barnets far, men at det i stedet bliver morens partner, der får <u>status som medmor</u>. For at lave en gældende aftale om dette, skal børnelovens samtykkeerklæring § 27a, stk. 2 benyttes.</p><p>Er donoren kendt, og er barnets mor lesbisk (enten gift med en kvinde, er i registreret partnerskab med en kvinde eller lever sammen med en kvindelig partner) anvendes <a href="http://www.statsforvaltningen.dk/sfdocs/Blanketter/Faderskab/blanket%209.pdf" target="_blank">samtykkeerklæring § 27a, stk. 1 og 2</a><strong>:</strong></p> <ul><li>Hvis donoren skal registreres som barnets far, udfyldes afsnittet om faderskab (stk. 1).</li></ul> <ul><li>Hvis morens kvindelige ægtefælle, registrerede partner eller kvindelige partner skal være medmor til barnet, udfyldes afsnittet om medmoderskab (stk. 2).</li></ul> <p>Når der er anvendt anonym/ åben donorsæd, og når barnets mor lever i et ægteskab med enten en mand eller kvinde, eller i et registreret partnerskab med en kvinde eller som kærester med enten en mand eller kvinde (§27), eller når den kendte donor skal registreres som barnets far, og når barnets mor ikke er i et forhold til en mand, der skal være barnets far (§27b), skal <a href="http://www.statsforvaltningen.dk/sfdocs/Blanketter/Faderskab/blanket%208.pdf" target="_blank">samtykkeerklæring §§27 og 27b</a> anvendes.</p><p>Når dét er udfyldt og underskrevet, skal det afleveres på behandlingsstedet til det sundhedsfaglige personale, som skal underskrive det og udstede en kopi til kvinden/parret.</p><p>Ved opstart af faderskabs eller medmoderskabssagen skal kvinden/parret selv sende det underskrevne samtykke til Statsforvaltningen. Statsforvaltningen anbefaler, at de indsender samtykkeerklæringen 2-3 måneder før barnets forventede fødsel. Statsforvaltningen vil da registrere faderskabet/medmoderskabet i forbindelse med barnets fødsel. (Derfor skal de selv rette henvendelse til Statsforvaltningen, så hurtigt som muligt efter barnets fødsel, så fader- eller medmoderskabet kan registreres).</p><p>Læs mere medmoderskab og her: http://www.statsforvaltningen.dk/site.aspx?p=8721</p>'),
(451, '<p>Hvis I er et lesbisk par der ønsker et barn, så skal den kvindelige part, der ikke bærer barnet registreres som medmoder. </p><p>Et medmoderskab kan kun fastslås, hvis to kvinder får et barn sammen ved hjælp af kunstig befrugtning. Graviditet ad anden vej er ikke gyldig, hvis en kvinde skal have medmoderskab.</p><p>Hvis man skal fastslå et medmoderskab/faderskab, skal proceduren begynde <i><u>inden</u></i> selve fertilitetsbehandlingen.</p><p>Reglerne betyder, at medmoren får de samme rettigheder og pligter over for barnet.</p><p>Man skal være opmærksom på, at hvis der er tale om en kendt donor, anses donoren som udgangspunkt for at være barnets far. Donoren, den kommende mor og morens partner (ægtefælle, registreret partner eller kæreste) kan aftale, at donoren ikke skal have status som barnets far, men at det i stedet bliver morens partner, der får <u>status som medmor</u>. For at lave en gældende aftale om dette, skal børnelovens samtykkeerklæring § 27a, stk. 2 benyttes.</p><p>Er donoren kendt, og er barnets mor lesbisk (enten gift med en kvinde, er i registreret partnerskab med en kvinde eller lever sammen med en kvindelig partner) anvendes <a href="http://www.statsforvaltningen.dk/sfdocs/Blanketter/Faderskab/blanket%209.pdf" target="_blank">samtykkeerklæring § 27a, stk. 1 og 2</a><strong>:</strong></p> <ul><li>Hvis donoren skal registreres som barnets far, udfyldes afsnittet om faderskab (stk. 1).</li></ul> <ul><li>Hvis morens kvindelige ægtefælle, registrerede partner eller kvindelige partner skal være medmor til barnet, udfyldes afsnittet om medmoderskab (stk. 2).</li></ul> <p>Når der er anvendt anonym/ åben donorsæd, og når barnets mor lever i et ægteskab med enten en mand eller kvinde, eller i et registreret partnerskab med en kvinde eller som kærester med enten en mand eller kvinde (§27), eller når den kendte donor skal registreres som barnets far, og når barnets mor ikke er i et forhold til en mand, der skal være barnets far (§27b), skal <a href="http://www.statsforvaltningen.dk/sfdocs/Blanketter/Faderskab/blanket%208.pdf" target="_blank">samtykkeerklæring §§27 og 27b</a> anvendes.</p><p>Når dét er udfyldt og underskrevet, skal det afleveres på behandlingsstedet til det sundhedsfaglige personale, som skal underskrive det og udstede en kopi til kvinden/parret.</p><p>Ved opstart af faderskabs eller medmoderskabssagen skal kvinden/parret selv sende det underskrevne samtykke til Statsforvaltningen. Statsforvaltningen anbefaler, at de indsender samtykkeerklæringen 2-3 måneder før barnets forventede fødsel. Statsforvaltningen vil da registrere faderskabet/medmoderskabet i forbindelse med barnets fødsel. (Derfor skal de selv rette henvendelse til Statsforvaltningen, så hurtigt som muligt efter barnets fødsel, så fader- eller medmoderskabet kan registreres).</p><p>Læs mere medmoderskab og <a href="http://www.statsforvaltningen.dk/site.aspx?p=8721">her</a>.</p>'),
(452, '<p>Hvis I er et lesbisk par der ønsker et barn, så skal den kvindelige part, der ikke bærer barnet registreres som medmoder. </p><p>Et medmoderskab kan kun fastslås, hvis to kvinder får et barn sammen ved hjælp af kunstig befrugtning. Graviditet ad anden vej er ikke gyldig, hvis en kvinde skal have medmoderskab.</p><p>Hvis man skal fastslå et medmoderskab/faderskab, skal proceduren begynde <i><u>inden</u></i> selve fertilitetsbehandlingen.</p><p>Reglerne betyder, at medmoren får de samme rettigheder og pligter over for barnet.</p><p>Man skal være opmærksom på, at hvis der er tale om en kendt donor, anses donoren som udgangspunkt for at være barnets far. Donoren, den kommende mor og morens partner (ægtefælle, registreret partner eller kæreste) kan aftale, at donoren ikke skal have status som barnets far, men at det i stedet bliver morens partner, der får <u>status som medmor</u>. For at lave en gældende aftale om dette, skal børnelovens samtykkeerklæring § 27a, stk. 2 benyttes.</p><p>Er donoren kendt, og er barnets mor lesbisk (enten gift med en kvinde, er i registreret partnerskab med en kvinde eller lever sammen med en kvindelig partner) anvendes <a href="http://www.statsforvaltningen.dk/sfdocs/Blanketter/Faderskab/blanket%209.pdf" target="_blank">samtykkeerklæring § 27a, stk. 1 og 2</a><strong>:</strong></p> <ul><li>Hvis donoren skal registreres som barnets far, udfyldes afsnittet om faderskab (stk. 1).</li></ul> <ul><li>Hvis morens kvindelige ægtefælle, registrerede partner eller kvindelige partner skal være medmor til barnet, udfyldes afsnittet om medmoderskab (stk. 2).</li></ul> <p>Når der er anvendt anonym/ åben donorsæd, og når barnets mor lever i et ægteskab med enten en mand eller kvinde, eller i et registreret partnerskab med en kvinde eller som kærester med enten en mand eller kvinde (§27), eller når den kendte donor skal registreres som barnets far, og når barnets mor ikke er i et forhold til en mand, der skal være barnets far (§27b), skal <a href="http://www.statsforvaltningen.dk/sfdocs/Blanketter/Faderskab/blanket%208.pdf" target="_blank">samtykkeerklæring §§27 og 27b</a> anvendes.</p><p>Når dét er udfyldt og underskrevet, skal det afleveres på behandlingsstedet til det sundhedsfaglige personale, som skal underskrive det og udstede en kopi til kvinden/parret.</p><p>Ved opstart af faderskabs eller medmoderskabssagen skal kvinden/parret selv sende det underskrevne samtykke til Statsforvaltningen. Statsforvaltningen anbefaler, at de indsender samtykkeerklæringen 2-3 måneder før barnets forventede fødsel. Statsforvaltningen vil da registrere faderskabet/medmoderskabet i forbindelse med barnets fødsel. (Derfor skal de selv rette henvendelse til Statsforvaltningen, så hurtigt som muligt efter barnets fødsel, så fader- eller medmoderskabet kan registreres).</p><p>Læs mere medmoderskab og <a target="_blank" href="http://www.statsforvaltningen.dk/site.aspx?p=8721">her</a>.</p>'),
(453, '<h3>Priser</h3><p>Diers Klinik har konkurrencedygtige priser og hos os kommer der ingen ekstra gebyrer på. Vi har priser på insemination fra kr. 3800 incl. scanning.<br></p><p>							<a href="{CCM:CID_164}" data-concrete5-link-type="ajax">Læs mere</a></p>'),
(467, ''),
(470, '<p>Vi er meget glade for at få billeder af alle de skønne børn, der kommer til verden via Diers Klinik</p>'),
(471, '<p>Se nogle af de skønne børn fra Diers Klinik. Langt over 2500 børn er kommet verden med hjælp fra Diers Klinik. Vi vil meget gerne modtage billeder til denne side. Send os en mail på info@diersklinik.dk med emnet: Billede til hjemmesiden</p><p>På forhånd mange tak.<br></p>'),
(472, '<p> <strong>På Diers Klinik gør vi alt hvad vi kan for at gøre vores succesrate så høj som muligt. Vi har den højeste succesrate i landet for inseminationsbehandling med donorsæd.</strong></p><p>Vi gør meget ud af at finde det helt rigtige tidspunkt for inseminationen, og vi scanner altid gratis inden behandlingen for at sikre bedst mulige betingelser for at behandlingen skal lykkes.</p><p>Og vores anstrengelser bærer frugt. Vi har den højeste succesrate i landet for inseminationsbehandling med donorsæd.<br> <br>Her kan du se vores tal sammenlignet med landsgennemsnittet for alle klinikker i Danmark.</p><p data-redactor-inserted-image="true"><img id="image-marker" src="{CCM:FID_DL_12}"></p><p><br>På Diers klinik holder vi løbende statistik med vores inseminationsbehandlinger. </p> <p>Dine graviditetschancer afhænger meget af din alder. </p><p>Til du fylder 35 år er chancerne 26 % pr. insemination. <br>Fra 35-39 år er den 20 % pr. insemination. <br>Er du mellem 40 og 42 er chancen 10 % pr. insemination. </p><p>Statistikken er baseret på tal fra 2014.</p>'),
(473, '<p>Hos Diers Klinik har vi den højeste succesrate i Danmark</p>'),
(474, '<p>Sæddonorerne, der benyttes kommer fra sædbanker, som lever op til Sundhedsstyrelsens anbefalinger.</p><p>I Diers Klinik har du flere muligheder, hvad angår valg af sæddonor. Vi tilbyder  både <a href="{CCM:CID_179}" data-concrete5-link-type="ajax">”åbne” sæddonorer </a>samt <a href="{CCM:CID_180}" data-concrete5-link-type="ajax">”non contact” sæddonorer</a>. Hvis du vælger en ”non  contact” sæddonor, vil det på intet tidspunkt være muligt for dig eller barnet at få oplysninger om donorens identitet. Hvis du vælger en ”åben” sæddonor, har dit kommende barn mulighed for at få kontakt til donoren, når det fylder 18 år.  Det er dog forskelligt, hvad de åbne donorer forpligter sig på afhængig af, hvilken sædbank de donorer ved. Derfor må du selv indhente informationerne fra sædbanken om, hvilke muligheder det kommende barn har for at kontakte  sæddonoren, når det fylder 18 år.</p><p>Hos os går vi meget op i, at du selv har mulighed for at vælge donor ud fra de  ønsker og behov du måtte have. Du har mulighed for at vælge donorerne enten ud  fra en basisprofil eller en udvidet profil. Ved valg af donorer ud fra  basisprofilen, får du oplysninger om sæddonorens øjenfarve, højde, vægt,  beskæftigelse samt blodtype. Hvor du ved donorer med udvidet profil desuden får oplysninger om f.eks. sygehistorie, familieforhold samt fritidsinteresser. Du får endvidere mulighed for at se babybilleder og høre en stemmeprøve. Det er både muligt at få ”åbne” samt ”non contact” donorer med udvidet  profil.</p><p>Sæddonorer har ingen juridiske forpligtelser overfor de børn, der undfanges ved hjælp af  sæd fra ham. Derfor har barnet ikke arveret eller andre rettigheder i forhold til donoren.</p><p>I henhold til Sundhedsstyrelsen skal følgende oplysninger gives til kvinder/par, der behandles med sæd fra en sæddonor:</p><blockquote>”Ved  udvælgelse af donorer er risiko for videregivelse af arvelige sygdomme,  misdannelser m.v. søgt begrænset ved kun at anvende donorer som har oplyst, at  de ikke har kendskab til sådanne arverisici i deres slægt, og hvor der af en erfaren sundhedsperson er udspurgt og undersøgt for at belyse dette. Trods disse  forsigtighedsregler er enhver arverisiko alligevel ikke udelukket. Hvis barnet  mod forventning fejler noget ved fødslen eller i de første leveår, som du får at  vide kan være arveligt, er det derfor vigtigt, at du melder tilbage til  klinikken eller den sundhedsperson, der har behandlet dig, så der kan tages  stilling til, om donor fortsat kan anvendes. Det samme gælder, hvis du får at  vide, at det kan dreje sig om smitteoverførsel fra donor-sæd eller donor-æg.  Selvom donor er testet fri for overførbare sygdomme for eksempel HIV og  hepatitis, er risikoen aldrig nul.”</blockquote><p><o>Sæddonorerne er alle testet grundigt for diverse sygdomme  HIV, hepatitis, syphilis, CMV og gonoré. Derudover gennemgår donorerne en række  fysiske og psykiske undersøgelse. De er alle unge, raske mænd, der oftest er  studerende ved højere læreanstalter. </o></p><p>I Diers Klinik stiller  vi meget strenge krav til kvaliteten af sæden og sikrer os altid, at kvaliteten  er god, inden hver enkelt insemination. Hvis du har lyst, er du meget velkommen  til at se med i mikroskopet. Hos os bliver sæden altid først klargjort efter, at  vi har sikret os, at det er det helt rigtige tidspunkt for insemination. Klargøring af sæden tager fra 5 til 45 minutter.</p><p>Vi  har et udvalg af donorer her i klinikken, som vi har købt igennem sædbankerne. Dette betyder, at du ikke selv behøver at købe sæden hjem fra sædbanken og  oprette et depot hos os. Du kan derimod reservere et strå af den donor, som du  ønsker fra gang til gang uden at være bundet. Du er meget velkommen til at  kontakte os, hvis du ønsker at se listen over de donorer, som vi har  tilgængelige.</p>'),
(475, '<p>Hos Diers Klinik har du flere muligheder, hvad angår valg af sæddonor.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `btCoreAreaLayout`
--

CREATE TABLE IF NOT EXISTS `btCoreAreaLayout` (
  `bID` int(10) unsigned NOT NULL DEFAULT '0',
  `arLayoutID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bID`),
  KEY `arLayoutID` (`arLayoutID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btCoreConversation`
--

CREATE TABLE IF NOT EXISTS `btCoreConversation` (
  `bID` int(10) unsigned NOT NULL,
  `cnvID` int(11) DEFAULT NULL,
  `enablePosting` int(11) DEFAULT '1',
  `paginate` tinyint(1) NOT NULL DEFAULT '1',
  `itemsPerPage` smallint(5) unsigned NOT NULL DEFAULT '50',
  `displayMode` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'threaded',
  `orderBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'date_desc',
  `enableOrdering` tinyint(1) NOT NULL DEFAULT '1',
  `enableCommentRating` tinyint(1) NOT NULL DEFAULT '1',
  `displayPostingForm` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'top',
  `addMessageLabel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dateFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'default',
  `customDateFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insertNewMessages` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'top',
  PRIMARY KEY (`bID`),
  KEY `cnvID` (`cnvID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `btCoreConversation`
--

INSERT INTO `btCoreConversation` (`bID`, `cnvID`, `enablePosting`, `paginate`, `itemsPerPage`, `displayMode`, `orderBy`, `enableOrdering`, `enableCommentRating`, `displayPostingForm`, `addMessageLabel`, `dateFormat`, `customDateFormat`, `insertNewMessages`) VALUES
(382, 1, 1, 1, 50, 'threaded', 'date_desc', 0, 0, 'bottom', 'Tilføj anmeldelse', 'default', '', 'top'),
(383, 1, 1, 1, 50, 'threaded', 'date_desc', 0, 0, 'bottom', 'Tilføj anmeldelse', 'default', '', 'top'),
(384, 2, 1, 1, 25, 'threaded', 'date_desc', 0, 0, 'bottom', 'Skriv andmeldelse', 'default', '', 'top');

-- --------------------------------------------------------

--
-- Table structure for table `btCorePageTypeComposerControlOutput`
--

CREATE TABLE IF NOT EXISTS `btCorePageTypeComposerControlOutput` (
  `bID` int(10) unsigned NOT NULL,
  `ptComposerOutputControlID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bID`),
  KEY `ptComposerOutputControlID` (`ptComposerOutputControlID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `btCorePageTypeComposerControlOutput`
--

INSERT INTO `btCorePageTypeComposerControlOutput` (`bID`, `ptComposerOutputControlID`) VALUES
(9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `btCoreScrapbookDisplay`
--

CREATE TABLE IF NOT EXISTS `btCoreScrapbookDisplay` (
  `bID` int(10) unsigned NOT NULL,
  `bOriginalID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`bID`),
  KEY `bOriginalID` (`bOriginalID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btCoreStackDisplay`
--

CREATE TABLE IF NOT EXISTS `btCoreStackDisplay` (
  `bID` int(10) unsigned NOT NULL,
  `stID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`bID`),
  KEY `stID` (`stID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btDashboardNewsflowLatest`
--

CREATE TABLE IF NOT EXISTS `btDashboardNewsflowLatest` (
  `bID` int(10) unsigned NOT NULL,
  `slot` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `btDashboardNewsflowLatest`
--

INSERT INTO `btDashboardNewsflowLatest` (`bID`, `slot`) VALUES
(4, 'A'),
(5, 'B'),
(8, 'C');

-- --------------------------------------------------------

--
-- Table structure for table `btDateNavigation`
--

CREATE TABLE IF NOT EXISTS `btDateNavigation` (
  `bID` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `filterByParent` tinyint(1) DEFAULT '0',
  `redirectToResults` tinyint(1) DEFAULT '0',
  `cParentID` int(10) unsigned NOT NULL DEFAULT '0',
  `cTargetID` int(10) unsigned NOT NULL DEFAULT '0',
  `ptID` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btExternalForm`
--

CREATE TABLE IF NOT EXISTS `btExternalForm` (
  `bID` int(10) unsigned NOT NULL,
  `filename` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btFaq`
--

CREATE TABLE IF NOT EXISTS `btFaq` (
  `bID` int(10) unsigned NOT NULL,
  `blockTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btFaqEntries`
--

CREATE TABLE IF NOT EXISTS `btFaqEntries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bID` int(10) unsigned DEFAULT NULL,
  `linkTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `bID` (`bID`,`sortOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `btFeature`
--

CREATE TABLE IF NOT EXISTS `btFeature` (
  `bID` int(10) unsigned NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paragraph` text COLLATE utf8_unicode_ci,
  `externalLink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `internalLinkCID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btForm`
--

CREATE TABLE IF NOT EXISTS `btForm` (
  `bID` int(10) unsigned NOT NULL,
  `questionSetId` int(10) unsigned DEFAULT '0',
  `surveyName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thankyouMsg` text COLLATE utf8_unicode_ci,
  `notifyMeOnSubmission` tinyint(1) NOT NULL DEFAULT '0',
  `recipientEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayCaptcha` int(11) DEFAULT '1',
  `redirectCID` int(11) DEFAULT '0',
  `addFilesToSet` int(11) DEFAULT '0',
  `submitText` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Submit',
  PRIMARY KEY (`bID`),
  KEY `questionSetIdForeign` (`questionSetId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `btForm`
--

INSERT INTO `btForm` (`bID`, `questionSetId`, `surveyName`, `thankyouMsg`, `notifyMeOnSubmission`, `recipientEmail`, `displayCaptcha`, `redirectCID`, `addFilesToSet`, `submitText`) VALUES
(99, 1431418205, 'Kontakt os', 'Tak for din besked! Vi vender tilbage til dig hurtigst muligt.', 0, '', 0, 0, 0, 'Submit'),
(302, 1431418205, 'Kontakt os', '', 0, '', 0, 184, 0, 'Submit'),
(404, 1432105761, 'Kontakt', 'Tak!', 0, '', 0, 0, 0, 'Submit'),
(410, 1432106026, 'Kontakt', 'Tak!', 0, '', 0, 0, 0, 'Submit'),
(414, 1432106026, 'Kontakt', 'Tak!', 0, '', 0, 184, 0, 'Submit'),
(425, 1432106026, 'Kontakt', 'Tak!', 0, 'info@diersklinik.dk', 0, 184, 0, 'Submit'),
(441, 1432106026, 'Kontakt', 'Tak!', 0, 'info@diersklinik.dk', 0, 184, 0, 'Submit');

-- --------------------------------------------------------

--
-- Table structure for table `btFormAnswerSet`
--

CREATE TABLE IF NOT EXISTS `btFormAnswerSet` (
  `asID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `questionSetId` int(10) unsigned DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uID` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`asID`),
  KEY `questionSetId` (`questionSetId`),
  KEY `uID` (`uID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `btFormAnswerSet`
--

INSERT INTO `btFormAnswerSet` (`asID`, `questionSetId`, `created`, `uID`) VALUES
(1, 1431418205, '2015-05-18 12:22:04', 1),
(2, 1432106026, '2015-05-20 11:23:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `btFormAnswers`
--

CREATE TABLE IF NOT EXISTS `btFormAnswers` (
  `aID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asID` int(10) unsigned DEFAULT '0',
  `msqID` int(10) unsigned DEFAULT '0',
  `answer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `answerLong` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`aID`),
  KEY `asID` (`asID`),
  KEY `msqID` (`msqID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `btFormAnswers`
--

INSERT INTO `btFormAnswers` (`aID`, `asID`, `msqID`, `answer`, `answerLong`) VALUES
(1, 1, 1, 'Bodil Meldgaard', ''),
(2, 1, 2, 'Bodilmeldgaard@gmail.com', ''),
(3, 1, 3, '27145404', ''),
(4, 1, 4, '', 'test'),
(5, 2, 8, 'Bodil', ''),
(6, 2, 9, 'bme@midtjyskturisme.com', ''),
(7, 2, 10, '27145404', ''),
(8, 2, 11, '', 'Får I denne test');

-- --------------------------------------------------------

--
-- Table structure for table `btFormQuestions`
--

CREATE TABLE IF NOT EXISTS `btFormQuestions` (
  `qID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `msqID` int(10) unsigned DEFAULT '0',
  `bID` int(10) unsigned DEFAULT '0',
  `questionSetId` int(10) unsigned DEFAULT '0',
  `question` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inputType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `options` text COLLATE utf8_unicode_ci,
  `position` int(10) unsigned DEFAULT '1000',
  `width` int(10) unsigned DEFAULT '50',
  `height` int(10) unsigned DEFAULT '3',
  `required` int(11) DEFAULT '0',
  `defaultDate` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`qID`),
  KEY `questionSetId` (`questionSetId`),
  KEY `msqID` (`msqID`),
  KEY `bID` (`bID`,`questionSetId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `btFormQuestions`
--

INSERT INTO `btFormQuestions` (`qID`, `msqID`, `bID`, `questionSetId`, `question`, `inputType`, `options`, `position`, `width`, `height`, `required`, `defaultDate`) VALUES
(1, 1, 99, 1431418205, 'Dit navn', 'field', '', 0, 50, 3, 1, ''),
(2, 2, 99, 1431418205, 'Din e-mail', 'email', 'a:1:{s:22:"send_notification_from";i:1;}', 0, 0, 0, 1, ''),
(3, 3, 99, 1431418205, 'Dit telefon nr.', 'telephone', '', 0, 50, 3, 0, ''),
(4, 4, 99, 1431418205, 'Dit spørgsmål', 'text', '', 0, 50, 10, 1, ''),
(5, 1, 302, 1431418205, 'Dit navn', 'field', '', 0, 50, 3, 1, ''),
(6, 2, 302, 1431418205, 'Din e-mail', 'email', 'a:1:{s:22:"send_notification_from";i:1;}', 0, 0, 0, 1, ''),
(7, 3, 302, 1431418205, 'Dit telefon nr.', 'telephone', '', 0, 50, 3, 0, ''),
(8, 4, 302, 1431418205, 'Dit spørgsmål', 'text', '', 0, 50, 10, 1, ''),
(9, 5, 404, 1432105761, 'Navn', 'field', '', 0, 50, 3, 1, ''),
(10, 6, 404, 1432105761, 'Email', 'field', '', 0, 50, 3, 1, ''),
(11, 7, 0, 1432105761, 'Tlf. nr.', 'field', '', 0, 50, 3, 1, ''),
(12, 8, 410, 1432106026, 'Navn', 'field', '', 0, 50, 3, 1, ''),
(13, 9, 410, 1432106026, 'Email', 'field', '', 1, 50, 3, 1, ''),
(14, 10, 410, 1432106026, 'Tlf. nr.', 'field', '', 2, 50, 3, 1, ''),
(15, 11, 410, 1432106026, 'Kommentar', 'field', '', 4, 50, 3, 1, ''),
(16, 8, 414, 1432106026, 'Navn', 'field', '', 0, 50, 3, 1, ''),
(17, 9, 414, 1432106026, 'Email', 'field', '', 1, 50, 3, 1, ''),
(18, 10, 414, 1432106026, 'Tlf. nr.', 'field', '', 2, 50, 3, 1, ''),
(19, 11, 414, 1432106026, 'Kommentar', 'field', '', 4, 50, 3, 1, ''),
(20, 11, 425, 1432106026, 'Kommentar', 'text', '', 4, 50, 8, 1, ''),
(21, 8, 425, 1432106026, 'Navn', 'field', '', 0, 50, 3, 1, ''),
(25, 10, 425, 1432106026, 'Tlf. nr.', 'telephone', '', 2, 50, 3, 1, ''),
(26, 9, 425, 1432106026, 'Email', 'email', 'a:1:{s:22:"send_notification_from";i:1;}', 1, 50, 3, 1, ''),
(30, 9, 441, 1432106026, 'Email', 'email', 'a:1:{s:22:"send_notification_from";i:1;}', 1, 50, 3, 1, ''),
(32, 10, 441, 1432106026, 'Tlf. nr.', 'telephone', '', 2, 50, 3, 0, ''),
(33, 11, 441, 1432106026, 'Kommentar', 'text', '', 4, 50, 8, 0, ''),
(34, 8, 441, 1432106026, 'Navn', 'field', '', 0, 50, 3, 0, ''),
(35, 12, 441, 1432106026, 'Jeg ønsker at blive kontaktet', 'radios', 'Ja tak%%', 3, 50, 3, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `btGoogleMap`
--

CREATE TABLE IF NOT EXISTS `btGoogleMap` (
  `bID` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `zoom` smallint(6) DEFAULT NULL,
  `width` varchar(8) COLLATE utf8_unicode_ci DEFAULT '100%',
  `height` varchar(8) COLLATE utf8_unicode_ci DEFAULT '400px',
  `scrollwheel` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `btGoogleMap`
--

INSERT INTO `btGoogleMap` (`bID`, `title`, `location`, `latitude`, `longitude`, `zoom`, `width`, `height`, `scrollwheel`) VALUES
(98, '', 'Grønnegade 56, Århus, Danmark', 56.15909970000001, 10.201978999999938, 14, '100%', '475px', 1),
(389, '', 'Grønnegade 56, 8000 Danmark', 56.15909970000001, 10.201978999999938, 14, '100%', '400px', 1);

-- --------------------------------------------------------

--
-- Table structure for table `btImageSlider`
--

CREATE TABLE IF NOT EXISTS `btImageSlider` (
  `bID` int(10) unsigned NOT NULL,
  `navigationType` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btImageSliderEntries`
--

CREATE TABLE IF NOT EXISTS `btImageSliderEntries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bID` int(10) unsigned DEFAULT NULL,
  `cID` int(10) unsigned DEFAULT '0',
  `fID` int(10) unsigned DEFAULT '0',
  `linkURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `internalLinkCID` int(10) unsigned DEFAULT '0',
  `title` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `sortOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `btNavigation`
--

CREATE TABLE IF NOT EXISTS `btNavigation` (
  `bID` int(10) unsigned NOT NULL,
  `orderBy` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'alpha_asc',
  `displayPages` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'top',
  `displayPagesCID` int(10) unsigned NOT NULL DEFAULT '1',
  `displayPagesIncludeSelf` tinyint(1) NOT NULL DEFAULT '0',
  `displaySubPages` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'none',
  `displaySubPageLevels` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'none',
  `displaySubPageLevelsNum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `displayUnavailablePages` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `btNavigation`
--

INSERT INTO `btNavigation` (`bID`, `orderBy`, `displayPages`, `displayPagesCID`, `displayPagesIncludeSelf`, `displaySubPages`, `displaySubPageLevels`, `displaySubPageLevelsNum`, `displayUnavailablePages`) VALUES
(35, 'display_asc', 'top', 0, 0, 'none', 'enough', 0, 0),
(96, 'display_asc', 'top', 0, 0, 'relevant_breadcrumb', 'enough', 0, 0),
(110, 'display_asc', 'top', 0, 0, 'all', 'all', 0, 0),
(121, 'display_asc', 'top', 0, 0, 'relevant_breadcrumb', 'enough', 0, 0),
(124, 'display_asc', 'second_level', 0, 0, 'none', 'none', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `btNextPrevious`
--

CREATE TABLE IF NOT EXISTS `btNextPrevious` (
  `bID` int(10) unsigned NOT NULL,
  `nextLabel` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `previousLabel` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parentLabel` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loopSequence` int(11) DEFAULT '1',
  `excludeSystemPages` int(11) DEFAULT '1',
  `orderBy` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'display_asc',
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btPageAttributeDisplay`
--

CREATE TABLE IF NOT EXISTS `btPageAttributeDisplay` (
  `bID` int(10) unsigned NOT NULL,
  `attributeHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attributeTitleText` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayTag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateFormat` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'div',
  `thumbnailHeight` int(10) unsigned DEFAULT NULL,
  `thumbnailWidth` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btPageList`
--

CREATE TABLE IF NOT EXISTS `btPageList` (
  `bID` int(10) unsigned NOT NULL,
  `num` smallint(5) unsigned NOT NULL,
  `orderBy` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cParentID` int(10) unsigned NOT NULL DEFAULT '1',
  `cThis` tinyint(1) NOT NULL DEFAULT '0',
  `useButtonForLink` tinyint(1) NOT NULL DEFAULT '0',
  `buttonLinkText` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pageListTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relatedTopicAttributeKeyHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `includeName` tinyint(1) NOT NULL DEFAULT '1',
  `includeDescription` tinyint(1) NOT NULL DEFAULT '1',
  `includeDate` tinyint(1) NOT NULL DEFAULT '0',
  `includeAllDescendents` tinyint(1) NOT NULL DEFAULT '0',
  `paginate` tinyint(1) NOT NULL DEFAULT '0',
  `displayAliases` tinyint(1) NOT NULL DEFAULT '1',
  `enableExternalFiltering` tinyint(1) NOT NULL DEFAULT '0',
  `filterByRelated` tinyint(1) NOT NULL DEFAULT '0',
  `ptID` smallint(5) unsigned DEFAULT NULL,
  `pfID` int(11) DEFAULT '0',
  `truncateSummaries` int(11) DEFAULT '0',
  `displayFeaturedOnly` tinyint(1) DEFAULT '0',
  `noResultsMessage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayThumbnail` tinyint(1) DEFAULT '0',
  `truncateChars` int(11) DEFAULT '128',
  `filterByCustomTopic` tinyint(1) NOT NULL DEFAULT '0',
  `customTopicAttributeKeyHandle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customTopicTreeNodeID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bID`),
  KEY `ptID` (`ptID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btPageTitle`
--

CREATE TABLE IF NOT EXISTS `btPageTitle` (
  `bID` int(10) unsigned NOT NULL,
  `useCustomTitle` int(10) unsigned DEFAULT '0',
  `titleText` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `formatting` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `btPageTitle`
--

INSERT INTO `btPageTitle` (`bID`, `useCustomTitle`, `titleText`, `formatting`) VALUES
(122, 0, 'Forberedelsen', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `btRssDisplay`
--

CREATE TABLE IF NOT EXISTS `btRssDisplay` (
  `bID` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateFormat` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itemsToDisplay` int(10) unsigned DEFAULT '5',
  `showSummary` tinyint(1) NOT NULL DEFAULT '1',
  `launchInNewWindow` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btSearch`
--

CREATE TABLE IF NOT EXISTS `btSearch` (
  `bID` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buttonText` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `baseSearchPath` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postTo_cID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resultsURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btShareThisPage`
--

CREATE TABLE IF NOT EXISTS `btShareThisPage` (
  `btShareThisPageID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bID` int(10) unsigned DEFAULT '0',
  `service` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayOrder` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`btShareThisPageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `btSocialLinks`
--

CREATE TABLE IF NOT EXISTS `btSocialLinks` (
  `btSocialLinkID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bID` int(10) unsigned DEFAULT '0',
  `slID` int(10) unsigned DEFAULT '0',
  `displayOrder` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`btSocialLinkID`),
  KEY `bID` (`bID`,`displayOrder`),
  KEY `slID` (`slID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `btSurvey`
--

CREATE TABLE IF NOT EXISTS `btSurvey` (
  `bID` int(10) unsigned NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `requiresRegistration` int(11) DEFAULT '0',
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btSurveyOptions`
--

CREATE TABLE IF NOT EXISTS `btSurveyOptions` (
  `optionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bID` int(11) DEFAULT NULL,
  `optionName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayOrder` int(11) DEFAULT '0',
  PRIMARY KEY (`optionID`),
  KEY `bID` (`bID`,`displayOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `btSurveyResults`
--

CREATE TABLE IF NOT EXISTS `btSurveyResults` (
  `resultID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `optionID` int(10) unsigned DEFAULT '0',
  `uID` int(10) unsigned DEFAULT '0',
  `bID` int(11) DEFAULT NULL,
  `cID` int(11) DEFAULT NULL,
  `ipAddress` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`resultID`),
  KEY `optionID` (`optionID`),
  KEY `cID` (`cID`,`optionID`,`bID`),
  KEY `bID` (`bID`,`cID`,`uID`),
  KEY `uID` (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `btSwitchLanguage`
--

CREATE TABLE IF NOT EXISTS `btSwitchLanguage` (
  `bID` int(10) unsigned NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT '''',
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btTags`
--

CREATE TABLE IF NOT EXISTS `btTags` (
  `bID` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `targetCID` int(11) DEFAULT NULL,
  `displayMode` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'page',
  `cloudCount` int(11) DEFAULT '10',
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btTestimonial`
--

CREATE TABLE IF NOT EXISTS `btTestimonial` (
  `bID` int(10) unsigned NOT NULL,
  `fID` int(10) unsigned DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paragraph` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btTopicList`
--

CREATE TABLE IF NOT EXISTS `btTopicList` (
  `bID` int(10) unsigned NOT NULL,
  `mode` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `topicAttributeKeyHandle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `topicTreeID` int(10) unsigned NOT NULL DEFAULT '0',
  `cParentID` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btVideo`
--

CREATE TABLE IF NOT EXISTS `btVideo` (
  `bID` int(10) unsigned NOT NULL,
  `webmfID` int(10) unsigned DEFAULT '0',
  `oggfID` int(10) unsigned DEFAULT '0',
  `posterfID` int(10) unsigned DEFAULT '0',
  `mp4fID` int(10) unsigned DEFAULT '0',
  `width` int(10) unsigned DEFAULT NULL,
  `height` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `btVividThumbGallery`
--

CREATE TABLE IF NOT EXISTS `btVividThumbGallery` (
  `bID` int(10) unsigned NOT NULL,
  `fileset` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbWidth` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbHeight` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imageWidth` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imageHeight` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zoomType` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cols` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colsMobile` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `btVividThumbGallery`
--

INSERT INTO `btVividThumbGallery` (`bID`, `fileset`, `thumbWidth`, `thumbHeight`, `imageWidth`, `imageHeight`, `zoomType`, `cols`, `colsMobile`) VALUES
(469, '1', '300', '220', '800', '600', 'lightbox', '4', '2');

-- --------------------------------------------------------

--
-- Table structure for table `btVividThumbGalleryThumb`
--

CREATE TABLE IF NOT EXISTS `btVividThumbGalleryThumb` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bID` int(11) DEFAULT NULL,
  `fID` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `btYouTube`
--

CREATE TABLE IF NOT EXISTS `btYouTube` (
  `bID` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `videoURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vHeight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vWidth` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vPlayer` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gaPage`
--

CREATE TABLE IF NOT EXISTS `gaPage` (
  `gaiID` int(10) unsigned NOT NULL,
  `cID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`gaiID`),
  KEY `cID` (`cID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `QueueMessages`
--
ALTER TABLE `QueueMessages`
  ADD CONSTRAINT `QueueMessages_ibfk_1` FOREIGN KEY (`queue_id`) REFERENCES `Queues` (`queue_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
